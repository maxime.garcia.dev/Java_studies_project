package fr.humanbooster.max.slam2ville.service;

import java.util.List;

import fr.humanbooster.max.slam2ville.business.Ville;

public interface VilleService {

	void recupererVillesEnCsv();

	void trierVilles(List<Ville> villes);

	List<Ville> recupererVille(String nomSaisi);

}