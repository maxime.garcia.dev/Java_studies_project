package fr.humanbooster.max.slam2ville.controller;

import java.util.Iterator;
import java.util.List;

import javax.annotation.PostConstruct;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.ModelAndView;

import fr.humanbooster.max.slam2ville.business.Ville;
import fr.humanbooster.max.slam2ville.business.VillesCorrespondantes;
import fr.humanbooster.max.slam2ville.service.VilleService;

@RestController
public class Slam2VilleRestController {

	private VilleService villeService;
	
	public Slam2VilleRestController(VilleService villeService) {
		super();
		this.villeService = villeService;
	}

	@RequestMapping(value="{index, /}")
	public ModelAndView accueil() {
		ModelAndView mav = new ModelAndView("index");
		return mav;
	}
	
	@GetMapping(value="RechercheVilleJson")
	public VillesCorrespondantes rechercherVilleCorrespondante(@RequestParam("name") String nom, @RequestParam("start") int start, @RequestParam("count") int fin) {
		VillesCorrespondantes villesCorrespondantes = new VillesCorrespondantes();
		List<Ville> villes = villeService.recupererVille(nom);
		if (villes != null) {
			villeService.trierVilles(villes);
			int nbVilles = villes.size();
			villesCorrespondantes.setNbVillesTotales(nbVilles);
			Iterator<Ville> itr = villes.iterator();
			for (; start < fin && itr.hasNext(); start++) {
				Ville ville = (Ville) itr.next();
				villesCorrespondantes.getVilles().add(ville);
			}
		}
		return villesCorrespondantes;	
	}
	
	@PostConstruct
    public void init() {
        villeService.recupererVillesEnCsv();
    }
}
