package fr.humanbooster.max.slam2ville.business;

import java.util.ArrayList;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonProperty;

public class VillesCorrespondantes {

	@JsonProperty(value = "items")
	private List<Ville> villes;
	private int nbVillesTotales;

	public int getNombreDeVilles() {
		return villes.size();
	}

	@JsonProperty(value = "identifier")
	public String getIdentifiant() {
		return "id";
	}

	public VillesCorrespondantes() {
		villes = new ArrayList<>();
	}

	public void ajouterVille(Ville ville) {
		villes.add(ville);
	}
	
	@JsonProperty(value = "items")
	public List<Ville> getVilles() {
		return villes;
	}

	public void setVilles(List<Ville> villes) {
		this.villes = villes;
	}

	public void setNbVillesTotales(int nbVillesTotales) {
		this.nbVillesTotales = nbVillesTotales;
	}

	@JsonProperty(value = "numRows")
	public int getNbVillesTotales() {
		return nbVillesTotales;
	}
}
