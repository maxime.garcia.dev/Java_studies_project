package fr.humanbooster.max.slam2ville;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Slam2villeApplication {

	public static void main(String[] args) {
		SpringApplication.run(Slam2villeApplication.class, args);
	}

}
