package fr.humanbooster.max.series.business;

import java.io.Serializable;
import javax.persistence.*;
import java.util.Date;
import java.util.List;


/**
 * The persistent class for the utilisateur database table.
 * 
 */
@Entity
@NamedQuery(name="Utilisateur.findAll", query="SELECT u FROM Utilisateur u")
public class Utilisateur implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private String id;

	@Temporal(TemporalType.DATE)
	@Column(name="date_de_naissance")
	private Date dateDeNaissance;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="date_inscription")
	private Date dateInscription;

	@Lob
	private String description;

	private String email;

	@Column(name="mot_de_passe")
	private String motDePasse;

	//bi-directional many-to-one association to Achat
	@OneToMany(mappedBy="utilisateur")
	private List<Achat> achats;

	//bi-directional many-to-many association to Interet
	@ManyToMany(mappedBy="utilisateurs")
	private List<Interet> interets;

	public Utilisateur() {
	}

	public String getId() {
		return this.id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public Date getDateDeNaissance() {
		return this.dateDeNaissance;
	}

	public void setDateDeNaissance(Date dateDeNaissance) {
		this.dateDeNaissance = dateDeNaissance;
	}

	public Date getDateInscription() {
		return this.dateInscription;
	}

	public void setDateInscription(Date dateInscription) {
		this.dateInscription = dateInscription;
	}

	public String getDescription() {
		return this.description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getEmail() {
		return this.email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getMotDePasse() {
		return this.motDePasse;
	}

	public void setMotDePasse(String motDePasse) {
		this.motDePasse = motDePasse;
	}

	public List<Achat> getAchats() {
		return this.achats;
	}

	public void setAchats(List<Achat> achats) {
		this.achats = achats;
	}

	public Achat addAchat(Achat achat) {
		getAchats().add(achat);
		achat.setUtilisateur(this);

		return achat;
	}

	public Achat removeAchat(Achat achat) {
		getAchats().remove(achat);
		achat.setUtilisateur(null);

		return achat;
	}

	public List<Interet> getInterets() {
		return this.interets;
	}

	public void setInterets(List<Interet> interets) {
		this.interets = interets;
	}

}