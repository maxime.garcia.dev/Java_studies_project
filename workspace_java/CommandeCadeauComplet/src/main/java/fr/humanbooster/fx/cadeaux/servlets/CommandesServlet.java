package fr.humanbooster.fx.cadeaux.servlets;

import java.io.IOException;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import fr.humanbooster.fx.cadeaux.business.Commande;
import fr.humanbooster.fx.cadeaux.business.Utilisateur;
import fr.humanbooster.fx.cadeaux.service.CommandeService;
import fr.humanbooster.fx.cadeaux.service.impl.CommandeServiceImpl;

/**
 * Servlet implementation class ListeCommandeServlet
 */
@WebServlet("/commandes")
public class CommandesServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
	private CommandeService commandeService;
	
    /**
     * @see HttpServlet#HttpServlet()
     */
    public CommandesServlet() {
        super();
        commandeService = new CommandeServiceImpl();
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		Utilisateur utilisateur = (Utilisateur) request.getSession().getAttribute("utilisateur");
		List<Commande> commandes = commandeService.recupererCommandesUtilisateur(utilisateur);
		request.setAttribute("commandes", commandes);
		request.getRequestDispatcher("WEB-INF/commandes.jsp").forward(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		doGet(request, response);
	}

}
