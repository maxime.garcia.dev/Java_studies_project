package fr.humanbooster.fx.cadeaux.servlets;

import java.io.IOException;
import java.util.Date;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import fr.humanbooster.fx.cadeaux.business.Article;
import fr.humanbooster.fx.cadeaux.business.Commande;
import fr.humanbooster.fx.cadeaux.business.Utilisateur;
import fr.humanbooster.fx.cadeaux.exceptions.StockInsuffisantException;
import fr.humanbooster.fx.cadeaux.service.ArticleService;
import fr.humanbooster.fx.cadeaux.service.CommandeService;
import fr.humanbooster.fx.cadeaux.service.impl.ArticleServiceImpl;
import fr.humanbooster.fx.cadeaux.service.impl.CommandeServiceImpl;

/**
 * Servlet implementation class CommandeServlet
 */
@WebServlet({"/CommandeServlet", "/commande"})
public class CommandeServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
      
	private ArticleService articleService;
	private CommandeService commandeService;
	
	
    /**
     * @see HttpServlet#HttpServlet()
     */
    public CommandeServlet() {
        super();
        articleService = new ArticleServiceImpl();
        commandeService = new CommandeServiceImpl();
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// On récupère l'objet utilisateur dans la session HTTP
		Utilisateur utilisateur = (Utilisateur) request.getSession().getAttribute("utilisateur");
		
		if(utilisateur == null) {
			request.setAttribute("utilisateurNonConnecte", "Vous devez vous connecter pour commander!");
			request.getRequestDispatcher("WEB-INF/erreur.jsp").forward(request, response);
			return;
		}
		
		Article article = articleService.recupererArticleParId(Long.parseLong(request.getParameter("ID")));
		
		int quantite = Integer.parseInt(request.getParameter("QUANTITE"));
		Commande commande;
		try {
			commande = commandeService.ajouterCommande(new Date(), quantite, article, utilisateur);
			request.setAttribute("commande", commande);
			request.getRequestDispatcher("WEB-INF/merciCommande.jsp").forward(request, response);
		} catch (StockInsuffisantException e) {
			request.setAttribute("commandeErrone", "Commande impossible! Veuillez commander moins que le stock disponible");
			request.getRequestDispatcher("article?ID="+article.getId()).forward(request, response);
		}
		
	}

}
