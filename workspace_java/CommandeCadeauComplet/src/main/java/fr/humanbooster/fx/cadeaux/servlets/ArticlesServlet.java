package fr.humanbooster.fx.cadeaux.servlets;

import java.io.IOException;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import fr.humanbooster.fx.cadeaux.business.Article;
import fr.humanbooster.fx.cadeaux.business.Categorie;
import fr.humanbooster.fx.cadeaux.service.ArticleService;
import fr.humanbooster.fx.cadeaux.service.CategorieService;
import fr.humanbooster.fx.cadeaux.service.impl.ArticleServiceImpl;
import fr.humanbooster.fx.cadeaux.service.impl.CategorieServiceImpl;

/**
 * Servlet implementation class ArticlesServlet
 */
@WebServlet(urlPatterns = { "/index", "/", "/articles" }, loadOnStartup = 1)
public class ArticlesServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	private ArticleService articleService = null;
	private CategorieService categorieService = null;

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public ArticlesServlet() {
		super();
		articleService = new ArticleServiceImpl();
		categorieService = new CategorieServiceImpl();

		if (categorieService.recupereCategories().isEmpty() && articleService.recupereArticles().isEmpty()) {
			Categorie electromenager = categorieService.ajouterCategorie("Electromenager");
			Categorie jouet = categorieService.ajouterCategorie("Jouets");
			Categorie informatique = categorieService.ajouterCategorie("Informatique");
			Categorie telephonie = categorieService.ajouterCategorie("Telephonie");
			Categorie jeux = categorieService.ajouterCategorie("Jeux");
			Categorie gourmet = categorieService.ajouterCategorie("Gourmet");
			articleService.ajouterArticle("Cafetière Bialetti", 20, 50, gourmet);
			articleService.ajouterArticle("AirPods", 40, 100, jouet);
			articleService.ajouterArticle("Thermomix", 100, 25, gourmet);
			articleService.ajouterArticle("Train", 10, 200, jouet);
			articleService.ajouterArticle("MacBook", 1000, 75, informatique);
			articleService.ajouterArticle("Aspirateur Dyson", 500, 100, electromenager);
			articleService.ajouterArticle("OnePlus 6", 500, 75, telephonie);
			articleService.ajouterArticle("HTC VIVE", 750, 60, jeux);
		}
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		
		String navigateur = request.getHeader("user-agent");
		request.setAttribute("navigateur", navigateur);
		
		List<Article> articles = articleService.recupereArticles();
		request.setAttribute("articles", articles);
		request.getRequestDispatcher("WEB-INF/index.jsp").include(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		doGet(request, response);
	}

	@Override
	protected void doDelete(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		super.doDelete(req, resp);
	}
}
