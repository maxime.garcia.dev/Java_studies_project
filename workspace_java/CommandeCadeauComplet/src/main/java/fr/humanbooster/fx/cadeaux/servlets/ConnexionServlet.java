package fr.humanbooster.fx.cadeaux.servlets;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import fr.humanbooster.fx.cadeaux.business.Utilisateur;
import fr.humanbooster.fx.cadeaux.business.Ville;
import fr.humanbooster.fx.cadeaux.service.UtilisateurService;
import fr.humanbooster.fx.cadeaux.service.VilleService;
import fr.humanbooster.fx.cadeaux.service.impl.UtilisateurServiceImpl;
import fr.humanbooster.fx.cadeaux.service.impl.VilleServiceImpl;

/**
 * Servlet implementation class ConnexionServlet
 */
@WebServlet(urlPatterns = { "/ConnexionServlet", "/connexion" }, loadOnStartup = 2)
public class ConnexionServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	private UtilisateurService utilisateurService;
	private VilleService villeService;

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public ConnexionServlet() {
		super();
		utilisateurService = new UtilisateurServiceImpl();
		villeService = new VilleServiceImpl();

		Ville lyon = villeService.recupererVilleParId(2L);
		utilisateurService.ajouterUtilisateur("COTE", "Fx", "fxcote@clelia.fr", "1234", lyon);
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		request.getRequestDispatcher("WEB-INF/connexion.jsp").forward(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		String email = request.getParameter("EMAIL");
		String motDePasse = request.getParameter("MOT_DE_PASSE");

		Utilisateur utilisateur = utilisateurService.recupererUtilisateurParEmailEtMotDePasse(email, motDePasse);

		if (utilisateur == null) {
			request.setAttribute("utilisateurNonTrouve", "Utilisateur non trouvé!");
			request.getRequestDispatcher("WEB-INF/erreur.jsp").forward(request, response);
		} else {
			request.getSession().setAttribute("utilisateur", utilisateur);
			request.getRequestDispatcher("index").forward(request, response);
		}

	}

}
