<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<c:choose>
	<c:when test="${sessionScope.utilisateur eq null }">
		<a href="inscription">Inscription</a>
		<br>
		<a href="connexion">Connexion</a>
		<br>
	</c:when>
	<c:when test="${sessionScope.utilisateur ne null }">
		<a href="deconnexion">Deconnexion</a>
		<br>
		Bonjour ${sessionScope.utilisateur.prenom} !
		<br>
		<a href="commandes">afficher mes commandes</a>
		<br>
	</c:when>
</c:choose>