<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Liste des Articles</title>
</head>
<body>

	<jsp:include page = "entete.jsp" />

	<h1>Liste des Articles</h1>
	<br>
	<c:forEach items="${articles}" var="article">
            <c:choose>
                <c:when test="${article.stock > 5}">
                    <c:set var="circle" value="&#x1F7E2;"></c:set>
                </c:when>
                <c:when test="${article.stock == 0}">
                    <c:set var="circle" value="&#128308;"></c:set>
                </c:when>
                <c:otherwise>
                    <c:set var="circle" value="&#x1F7E0;"></c:set>
                </c:otherwise>
            </c:choose>
            <a href="article?ID=${article.id}">${article.designation}</a>
            (${article.categorie.nom}) (nbPts: ${article.nbPoints}) (stock: ${circle} ${article.stock})
            <br>
    </c:forEach>
	<br>
	<br>
	<p>Nombre total d'article(s): ${articles.size()}</p>
	<br>
	<br>
	Navigateur: ${navigateur}
</body>
</html>
