<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>${article.designation}</title>
</head>
<body>

	<jsp:include page="entete.jsp" />

	<h1>${article.designation}</h1>
	<h2>${article.categorie.nom}</h2>
	<h3>Nb Points: ${article.nbPoints}</h3>
	<h3>Stock: ${article.stock}</h3>
	<h3>Commandes passées: ${article.commandes.size()}</h3>
	<p>Description: ${article.description}</p>


	<c:if test="${commandeErrone ne null}">
	${commandeErrone}
</c:if>

	<form action="commande?ID=${article.id}" method="post">
		Quantité <input type="text" name="QUANTITE"> <input type="submit" value="Commander">
	</form>
</body>
</html>