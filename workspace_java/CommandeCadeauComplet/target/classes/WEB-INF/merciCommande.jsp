<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Merci!</title>
</head>
<body>
	<jsp:include page="entete.jsp" />
	<br>
	Merci ${utilisateur.prenom} pour votre commande de ${commande.article.designation}, quantite: ${commande.quantite}
	<br>
	<a href="articles">Retour aux articles</a>
</body>
</html>