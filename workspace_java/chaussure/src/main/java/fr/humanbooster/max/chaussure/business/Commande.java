package fr.humanbooster.max.chaussure.business;

public class Commande {
	
	private Long id;
	private Long reference;
	private Destinataire destinataire;
	private float pointure;
	private String messageTalonGauche;
	private String messageTalonDroite;
	private boolean avecMoodSensor;
	private boolean avecLed;
	private TypeDeChaussure typeDeChaussure;
	
}
