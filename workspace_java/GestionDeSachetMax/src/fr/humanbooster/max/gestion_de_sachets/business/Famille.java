package fr.humanbooster.max.gestion_de_sachets.business;

import java.util.List;

public class Famille {
	
	private List<TypeDeGraine> typeDeGraine;
	private Long id;
	private static Long compteur = 0L;
	private String nom;
	
	public Famille() {
		super();
		id = ++compteur;
	}
	
	public Famille(String nom) {
		this();
		this.nom = nom;
	}

	public Famille(List<TypeDeGraine> typeDeGraine, String nom) {
		this(nom);
		this.typeDeGraine = typeDeGraine;
	}

	public List<TypeDeGraine> getTypeDeGraine() {
		return typeDeGraine;
	}

	public void setTypeDeGraine(List<TypeDeGraine> typeDeGraine) {
		this.typeDeGraine = typeDeGraine;
	}

	public String getNom() {
		return nom;
	}

	public void setNom(String nom) {
		this.nom = nom;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	@Override
	public String toString() {
		return "Famille [id=" + id + ", nom=" + nom + "]";
	}
}
