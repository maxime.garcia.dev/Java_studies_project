package fr.humanbooster.max.gestion_de_sachets.business;

import java.util.List;

public class TypeDeGraine {

	private List<Sachet> sachets;
	private Famille famille;
	private Long id;
	private static Long compteur = 0L;
	private String nom;
	private Integer semaineDePlantationMin;
	private Integer semaineDePlantationMax;
	private Float espacementEntrePiedsEnCentimetres;
	private Float espacementEntreLignesEnCentimetres;
	private String conseils;

	public TypeDeGraine() {
		super();
		id = ++compteur;
	}

	public TypeDeGraine(String nom) {
		this();
		this.nom = nom;
	}
	

	public TypeDeGraine(String nom, Famille famille) {
		this(nom);
		this.famille = famille;
	}

	public TypeDeGraine(String nom,Famille famille, String conseils) {
		this(nom, famille);
		this.conseils = conseils;
	}

	public TypeDeGraine(String nom,Famille famille, String conseils, Integer semaineDePlantationMin, Integer semaineDePlantationMax,
			Float espacementEntrePiedsEnCentimetres, Float espacementEntreLignesEnCentimetres) {
		this(nom, famille, conseils);
		this.semaineDePlantationMin = semaineDePlantationMin;
		this.semaineDePlantationMax = semaineDePlantationMax;
		this.espacementEntrePiedsEnCentimetres = espacementEntrePiedsEnCentimetres;
		this.espacementEntreLignesEnCentimetres = espacementEntreLignesEnCentimetres;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public List<Sachet> getSachets() {
		return sachets;
	}

	public void setSachets(List<Sachet> sachets) {
		this.sachets = sachets;
	}

	public String getNom() {
		return nom;
	}

	public void setNom(String nom) {
		this.nom = nom;
	}

	public Integer getSemaineDePlantationMin() {
		return semaineDePlantationMin;
	}

	public void setSemaineDePlantationMin(Integer semaineDePlantationMin) {
		this.semaineDePlantationMin = semaineDePlantationMin;
	}

	public Integer getSemaineDePlantationMax() {
		return semaineDePlantationMax;
	}

	public void setSemaineDePlantationMax(Integer semaineDePlantationMax) {
		this.semaineDePlantationMax = semaineDePlantationMax;
	}

	public Float getEspacementEntrePiedsEnCentimetres() {
		return espacementEntrePiedsEnCentimetres;
	}

	public void setEspacementEntrePiedsEnCentimetres(Float espacementEntrePiedsEnCentimetres) {
		this.espacementEntrePiedsEnCentimetres = espacementEntrePiedsEnCentimetres;
	}

	public Float getEspacementEntreLignesEnCentimetres() {
		return espacementEntreLignesEnCentimetres;
	}

	public void setEspacementEntreLignesEnCentimetres(Float espacementEntreLignesEnCentimetres) {
		this.espacementEntreLignesEnCentimetres = espacementEntreLignesEnCentimetres;
	}

	public String getConseils() {
		return conseils;
	}

	public void setConseils(String conseils) {
		this.conseils = conseils;
	}

	@Override
	public String toString() {
		return "TypeDeGraine [famille=" + famille + ", id=" + id + ", nom=" + nom
				+ ", semaineDePlantationMin=" + semaineDePlantationMin + ", semaineDePlantationMax="
				+ semaineDePlantationMax + ", espacementEntrePiedsEnCentimetres=" + espacementEntrePiedsEnCentimetres
				+ ", espacementEntreLignesEnCentimetres=" + espacementEntreLignesEnCentimetres + ", conseils="
				+ conseils + "]";
	}
}
