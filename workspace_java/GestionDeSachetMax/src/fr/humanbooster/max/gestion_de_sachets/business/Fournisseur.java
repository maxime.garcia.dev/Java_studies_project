package fr.humanbooster.max.gestion_de_sachets.business;

import java.util.List;

public class Fournisseur {
	
	private List<Sachet> sachets;
	private Long id;
	private static Long compteur = 0L;
	private String nom;

	public Fournisseur() {
		super();
		id = ++compteur;
	}
	
	public Fournisseur(String nom) {
		this();
		this.nom = nom;
	}

	public Fournisseur(List<Sachet> sachets, String nom) {
		this(nom);
		this.sachets = sachets;
	}

	public List<Sachet> getSachets() {
		return sachets;
	}

	public void setSachets(List<Sachet> sachets) {
		this.sachets = sachets;
	}

	public String getNom() {
		return nom;
	}

	public void setNom(String nom) {
		this.nom = nom;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	@Override
	public String toString() {
		return "Fournisseur [sachets=" + sachets + ", id=" + id + ", nom=" + nom + "]";
	}
}
