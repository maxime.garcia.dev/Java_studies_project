package fr.humanbooster.max.gestion_de_sachets.business;

import java.util.Date;

public class Sachet {
	
	private TypeDeGraine typeDeGraine;
	private Fournisseur fournisseur;
	private Long id;
	private static Long compteur = 0L;
	private Date dateDeCreation;
	private float poidsEnGrammes;
	private float prixEnEuros;
	
	public Sachet() {
		super();
		id = ++compteur;
	}
		
	public Sachet(TypeDeGraine typeDeGraine, Fournisseur fournisseur) {
		this();
		this.typeDeGraine = typeDeGraine;
		this.fournisseur = fournisseur;
	}

	public Sachet(TypeDeGraine typeDeGraine, Fournisseur fournisseur, Date dateDeCreation, float poidsEnGrammes,
			float prixEnEuros) {
		this(typeDeGraine, fournisseur);
		this.dateDeCreation = dateDeCreation;
		this.poidsEnGrammes = poidsEnGrammes;
		this.prixEnEuros = prixEnEuros;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public TypeDeGraine getTypeDeGraine() {
		return typeDeGraine;
	}

	public void setTypeDeGraine(TypeDeGraine typeDeGraine) {
		this.typeDeGraine = typeDeGraine;
	}

	public Fournisseur getFournisseur() {
		return fournisseur;
	}

	public void setFournisseur(Fournisseur fournisseur) {
		this.fournisseur = fournisseur;
	}

	public Date getDateDeCreation() {
		return dateDeCreation;
	}

	public void setDateDeCreation(Date dateDeCreation) {
		this.dateDeCreation = dateDeCreation;
	}

	public float getPoidsEnGrammes() {
		return poidsEnGrammes;
	}

	public void setPoidsEnGrammes(float poidsEnGrammes) {
		this.poidsEnGrammes = poidsEnGrammes;
	}

	public float getPrixEnEuros() {
		return prixEnEuros;
	}

	public void setPrixEnEuros(float prixEnEuros) {
		this.prixEnEuros = prixEnEuros;
	}

	@Override
	public String toString() {
		return "Sachet [typeDeGraine=" + typeDeGraine + ", fournisseur=" + fournisseur + ", id=" + id
				+ ", dateDeCreation=" + dateDeCreation + ", poidsEnGrammes=" + poidsEnGrammes + ", prixEnEuros="
				+ prixEnEuros + "]";
	}
}
