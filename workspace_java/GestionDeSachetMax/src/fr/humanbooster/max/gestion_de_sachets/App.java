package fr.humanbooster.max.gestion_de_sachets;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Scanner;

import fr.humanbooster.max.gestion_de_sachets.business.Famille;
import fr.humanbooster.max.gestion_de_sachets.business.Fournisseur;
import fr.humanbooster.max.gestion_de_sachets.business.TypeDeGraine;
import fr.humanbooster.max.gestion_de_sachets.service.FamilleService;
import fr.humanbooster.max.gestion_de_sachets.service.FournisseurService;
import fr.humanbooster.max.gestion_de_sachets.service.SachetService;
import fr.humanbooster.max.gestion_de_sachets.service.TypeDeGraineService;
import fr.humanbooster.max.gestion_de_sachets.service.impl.FamilleServiceImpl;
import fr.humanbooster.max.gestion_de_sachets.service.impl.FournisseurServiceImpl;
import fr.humanbooster.max.gestion_de_sachets.service.impl.SachetServiceImpl;
import fr.humanbooster.max.gestion_de_sachets.service.impl.TypeDeGraineServiceImpl;
import fr.humanbooster.max.gestion_de_sachets.util.FormateurDate;

public class App {
	
	private static final int AFFICHER_SACHETS_SUR_TYPE_DE_GRAINES = 1;
	private static final int AFFICHER_SACHETS_SUR_PRIX_DESCENDANT = 2;
	private static final int AJOUTER_SACHET = 3;
	private static final int MODIFIER_PRIX_SACHET = 4;
	private static final int SUPPRIMER_SACHET = 5;
	private static final int CONNAITRE_TYPE_DE_GRAINE = 6;
	private static final int QUITTER = 7;

	private static FamilleService familleService = new FamilleServiceImpl();
	private static FournisseurService fournisseurService = new FournisseurServiceImpl();
	private static SachetService sachetService = new SachetServiceImpl();
	private static TypeDeGraineService typeDeGraineService = new TypeDeGraineServiceImpl();
	
	private static SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd/MM");
	private static Scanner sc = new Scanner(System.in);
	
	public static void main(String[] args) {
	
		ajouterDonnees();
		
		while (true) {
			afficherMenuPrincipal();
			int choix = selectionnerOption("Veuillez selectionner une des options propos�es: ", 1 , QUITTER);
			
			switch (choix) {
			case AFFICHER_SACHETS_SUR_TYPE_DE_GRAINES:
				//afficherSachetsSurTypeDeGraines();
				break;
			case AFFICHER_SACHETS_SUR_PRIX_DESCENDANT:
				//afficherSachetsSurPrixDescendant();
				break;
			case AJOUTER_SACHET:
				ajouterSachet();
				System.out.println(sachetService.recupererSachets());
				break;
			case MODIFIER_PRIX_SACHET:
				//modifierPrixSachet();
				break;
			case SUPPRIMER_SACHET:
				//supprimerSachet();
				break;
			case CONNAITRE_TYPE_DE_GRAINE:
				
			case QUITTER:
				System.out.println("Merci de votre visite.");
				sc.close();
				System.exit(0);
				break;
			default:
				break;
			}
		}
	}
	
	private static void afficherChoixFournisseurs() {
		for (Fournisseur fournisseur : fournisseurService.recupererFournisseurs()) {
			System.out.println(fournisseur.getId() + ". " + fournisseur.getNom());
		}
	}
	
	private static void afficherChoixTypeDeGraine() {
		for (TypeDeGraine type : typeDeGraineService.recupererTypeDeGraines()) {
			System.out.println(type.getId() + ". " + type.getNom());
		}
	}
	private static void ajouterSachet() {
		Fournisseur fournisseur = null;
		TypeDeGraine type = null;
		Date dateDeCreation = null;
		float poidsEnGramme;
		float prixEnEuros;
		
		afficherChoixFournisseurs();
		
		Long choixFournisseur = ((long) selectionnerOption("Veuillez choisir un fournisseur parmi la liste : ", 1,
				fournisseurService.recupererFournisseurs().size()));
		
		fournisseur = fournisseurService.recupererFournisseur(choixFournisseur);
		
		afficherChoixTypeDeGraine();
		
		Long choixType = ((long) selectionnerOption("Veuillez choisir un type de graines parmi la liste : ", 1,
				typeDeGraineService.recupererTypeDeGraines().size()));
		
		type = typeDeGraineService.recupererTypeDeGraine(choixType);
		
		do {
			System.out.println("Veuillez indiquer le poids en grammes");
			try {
				poidsEnGramme = Float.parseFloat(sc.nextLine());
			} catch (Exception e) {
				System.out.println("Veuillez saisir une valeur num�rique");
				poidsEnGramme = 0;
			}
		} while (poidsEnGramme <= 0);
		
		do {
			System.out.println("Veuillez indiquer le prix en euros");
			try {
				prixEnEuros = Float.parseFloat(sc.nextLine());
			} catch (Exception e) {
				System.out.println("Veuillez saisir une valeur num�rique");
				prixEnEuros = 0;
			}
		} while (prixEnEuros <= 0);
		
		dateDeCreation = new Date();
		
		sachetService.ajouterSachet(type, fournisseur, dateDeCreation, poidsEnGramme, prixEnEuros);
	}
	
	private static void afficherMenuPrincipal() {
		System.out.println("Menu principal");
		System.out.println(AFFICHER_SACHETS_SUR_TYPE_DE_GRAINES + ". afficher les sachets tri�s sur le nom du type de graine");
		System.out.println(AFFICHER_SACHETS_SUR_PRIX_DESCENDANT + ". afficher les sachets tri�s sur le prix descendant");
		System.out.println(AJOUTER_SACHET + ". ajouter un sachet");
		System.out.println(MODIFIER_PRIX_SACHET + ". modifier le prix d�un sachet");
		System.out.println(SUPPRIMER_SACHET + ". supprimer un sachet");
		System.out.println(CONNAITRE_TYPE_DE_GRAINE + ". conna�tre les types de graine qu�il est possible de plante");
		System.out.println(QUITTER + ". Quitter");
	}

	private static void ajouterDonnees() {
		fournisseurService.ajouterFournisseur("Jardiland");
		fournisseurService.ajouterFournisseur("Potager Sympa");
		familleService.ajouterFamille("l�gume");
		familleService.ajouterFamille("haricot");
		typeDeGraineService.ajouterTypeDeGraine("haricot nain extra fin", familleService.recupererFamille(2L));
		typeDeGraineService.ajouterTypeDeGraine("courgette verte noire mara�ch�re", familleService.recupererFamille(1L));
		typeDeGraineService.ajouterTypeDeGraine("oignon jaune paille des vertus", familleService.recupererFamille(1L));
		typeDeGraineService.ajouterTypeDeGraine("carotte nantaise", familleService.recupererFamille(1L));
	}
	
	private static int selectionnerOption(String message, int min, int max) {
		int valeur = min - 1;
		do {
			System.out.println(message);
			try {
				valeur = Integer.parseInt(sc.nextLine());
				if (valeur < min || valeur > max) {
				System.out.println("Veuillez saisir un nombre compris entre " + min + " et " + max + " s'il vous plait.");
			}
			} catch (Exception e) {
				System.out.println("Veuillez saisir un nombre s'il vous plait.");
			}
		} while (valeur < min || valeur > max);
		return valeur;
	}
}
