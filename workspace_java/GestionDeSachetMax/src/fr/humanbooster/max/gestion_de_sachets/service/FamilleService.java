package fr.humanbooster.max.gestion_de_sachets.service;

import java.util.List;

import fr.humanbooster.max.gestion_de_sachets.business.Famille;

public interface FamilleService {

	Famille ajouterFamille(String nom);
	
	List<Famille> recupererFamilles();
	
	Famille recupererFamille(Long id);
	
	boolean supprimerFamille(Long id);
	
}
