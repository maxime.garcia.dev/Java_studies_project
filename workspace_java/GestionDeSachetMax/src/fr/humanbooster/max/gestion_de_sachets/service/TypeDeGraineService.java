package fr.humanbooster.max.gestion_de_sachets.service;

import java.util.List;

import fr.humanbooster.max.gestion_de_sachets.business.Famille;
import fr.humanbooster.max.gestion_de_sachets.business.TypeDeGraine;

public interface TypeDeGraineService {

	TypeDeGraine ajouterTypeDeGraine(String nom,Famille famille);
	
	List<TypeDeGraine> recupererTypeDeGraines();
	
	TypeDeGraine recupererTypeDeGraine(Long id);
	
	boolean supprimerTypeDeGraine(Long id);
	
}
