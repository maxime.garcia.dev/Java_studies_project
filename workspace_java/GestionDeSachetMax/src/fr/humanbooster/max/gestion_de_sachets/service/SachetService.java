package fr.humanbooster.max.gestion_de_sachets.service;

import java.util.Date;
import java.util.List;

import fr.humanbooster.max.gestion_de_sachets.business.Fournisseur;
import fr.humanbooster.max.gestion_de_sachets.business.Sachet;
import fr.humanbooster.max.gestion_de_sachets.business.TypeDeGraine;

public interface SachetService {

	Sachet ajouterSachet(TypeDeGraine typeDeGraine, Fournisseur fournisseur, Date dateDeCreation, float poidsEnGrammes,
			float prixEnEuros);
	
	List<Sachet> recupererSachets();
	
	Sachet recupererSachet(Long id);
	
	boolean supprimerSachet(Long id);
	
}
