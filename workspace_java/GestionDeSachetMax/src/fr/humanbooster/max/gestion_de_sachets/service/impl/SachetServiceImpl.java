package fr.humanbooster.max.gestion_de_sachets.service.impl;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import fr.humanbooster.max.gestion_de_sachets.business.Fournisseur;
import fr.humanbooster.max.gestion_de_sachets.business.Sachet;
import fr.humanbooster.max.gestion_de_sachets.business.TypeDeGraine;
import fr.humanbooster.max.gestion_de_sachets.service.SachetService;

public class SachetServiceImpl implements SachetService {

	private List<Sachet> sachets = new ArrayList<>();
	
	@Override
	public Sachet ajouterSachet(TypeDeGraine typeDeGraine, Fournisseur fournisseur, Date dateDeCreation, float poidsEnGrammes,
			float prixEnEuros) {
		Sachet sachet = new Sachet(typeDeGraine, fournisseur, dateDeCreation, poidsEnGrammes, prixEnEuros);
		sachets.add(sachet);
		return sachet;
	}

	@Override
	public List<Sachet> recupererSachets() {
		return sachets;
	}

	@Override
	public Sachet recupererSachet(Long id) {
		for (Sachet sachet : sachets) {
			if (id.equals(sachet.getId())) {
				return sachet;
			}
		}
		return null;
	}

	@Override
	public boolean supprimerSachet(Long id) {
		Sachet sachet = recupererSachet(id);
		if (sachet != null) {
			sachets.remove(sachet);
			return true;
		}
		return false;
	}

}
