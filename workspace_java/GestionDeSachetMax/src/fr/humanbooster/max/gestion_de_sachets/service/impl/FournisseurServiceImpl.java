package fr.humanbooster.max.gestion_de_sachets.service.impl;

import java.util.ArrayList;
import java.util.List;

import fr.humanbooster.max.gestion_de_sachets.business.Fournisseur;
import fr.humanbooster.max.gestion_de_sachets.service.FournisseurService;

public class FournisseurServiceImpl implements FournisseurService {

	private List<Fournisseur> fournisseurs = new ArrayList<>();
	
	@Override
	public Fournisseur ajouterFournisseur(String nom) {
		Fournisseur fournisseur = new Fournisseur(nom);
		fournisseurs.add(fournisseur);
		return fournisseur;
	}

	@Override
	public List<Fournisseur> recupererFournisseurs() {
		return fournisseurs;
	}

	@Override
	public Fournisseur recupererFournisseur(Long id) {
		for (Fournisseur fournisseur : fournisseurs) {
			if (id.equals(fournisseur.getId())) {
				return fournisseur;
			}
		}
		return null;
	}

	@Override
	public boolean supprimerFournisseur(Long id) {
		Fournisseur fournisseur = recupererFournisseur(id);
		if (fournisseur != null) {
			fournisseurs.remove(fournisseur);
			return true;
		}
		return false;
	}

}
