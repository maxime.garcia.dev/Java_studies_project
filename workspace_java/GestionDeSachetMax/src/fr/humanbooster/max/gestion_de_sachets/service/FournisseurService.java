package fr.humanbooster.max.gestion_de_sachets.service;

import java.util.List;

import fr.humanbooster.max.gestion_de_sachets.business.Fournisseur;

public interface FournisseurService {

	Fournisseur ajouterFournisseur(String nom);
	
	List<Fournisseur> recupererFournisseurs();
	
	Fournisseur recupererFournisseur(Long id);
	
	boolean supprimerFournisseur(Long id);
	
}
