package fr.humanbooster.max.gestion_de_sachets.service.impl;

import java.util.ArrayList;
import java.util.List;

import fr.humanbooster.max.gestion_de_sachets.business.Famille;
import fr.humanbooster.max.gestion_de_sachets.business.TypeDeGraine;
import fr.humanbooster.max.gestion_de_sachets.service.TypeDeGraineService;

public class TypeDeGraineServiceImpl implements TypeDeGraineService {

	private List<TypeDeGraine> typeDeGraines = new ArrayList<>();
	
	@Override
	public TypeDeGraine ajouterTypeDeGraine(String nom,Famille famille) {
		TypeDeGraine typeDeGraine = new TypeDeGraine(nom, famille);
		typeDeGraines.add(typeDeGraine);
		return typeDeGraine;
	}

	@Override
	public List<TypeDeGraine> recupererTypeDeGraines() {
		return typeDeGraines;
	}

	@Override
	public TypeDeGraine recupererTypeDeGraine(Long id) {
		for (TypeDeGraine typeDeGraine : typeDeGraines) {
			if (id.equals(typeDeGraine.getId())) {
				return typeDeGraine;
			}
		}
		return null;
	}

	@Override
	public boolean supprimerTypeDeGraine(Long id) {
		TypeDeGraine typeDeGraine = recupererTypeDeGraine(id);
		if (typeDeGraine != null) {
			typeDeGraines.remove(typeDeGraine);
			return true;
		}
		return false;
	}

}
