package fr.humanbooster.max.gestion_de_sachets.util;

import java.util.Comparator;

import fr.humanbooster.max.gestion_de_sachets.business.Famille;

public class ComparateurDeFamilleSurId implements Comparator<Famille> {
	
	@Override
	public int compare(Famille famille1, Famille famille2) {
		return Long.compare(famille1.getId(), famille2.getId());
	}

}
