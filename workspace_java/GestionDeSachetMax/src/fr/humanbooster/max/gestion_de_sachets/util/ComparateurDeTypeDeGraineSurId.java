package fr.humanbooster.max.gestion_de_sachets.util;

import java.util.Comparator;

import fr.humanbooster.max.gestion_de_sachets.business.TypeDeGraine;

public class ComparateurDeTypeDeGraineSurId implements Comparator<TypeDeGraine> {
	
	@Override
	public int compare(TypeDeGraine typeDeGraine1, TypeDeGraine typeDeGraine2) {
		return Long.compare(typeDeGraine1.getId(), typeDeGraine2.getId());
	}

}
