package fr.humanbooster.max.gestion_de_sachets.util;

import java.util.Comparator;

import fr.humanbooster.max.gestion_de_sachets.business.Fournisseur;

public class ComparateurDeFournisseurSurId implements Comparator<Fournisseur> {
	
	@Override
	public int compare(Fournisseur fournisseur1, Fournisseur fournisseur2) {
		return Long.compare(fournisseur1.getId(), fournisseur2.getId());
	}

}
