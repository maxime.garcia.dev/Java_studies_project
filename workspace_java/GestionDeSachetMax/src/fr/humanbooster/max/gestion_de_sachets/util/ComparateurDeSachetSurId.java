package fr.humanbooster.max.gestion_de_sachets.util;

import java.util.Comparator;

import fr.humanbooster.max.gestion_de_sachets.business.Sachet;

public class ComparateurDeSachetSurId implements Comparator<Sachet> {
	
	@Override
	public int compare(Sachet sachet1, Sachet sachet2) {
		return Long.compare(sachet1.getId(), sachet2.getId());
	}

}
