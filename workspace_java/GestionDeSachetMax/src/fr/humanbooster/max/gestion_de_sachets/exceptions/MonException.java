package fr.humanbooster.max.gestion_de_sachets.exceptions;

public class MonException extends Exception {

	private static final long serialVersionUID = 1L;

	public MonException(String message) {
        super(message);
    } 
}
