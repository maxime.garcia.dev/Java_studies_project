<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Insert title here</title>
</head>

<body>
	<jsp:include page="entete.jsp" />
	<h1>Articles</h1>
	<c:forEach items="${articles}" var="article">
		<a href="article?ID=${article.id}">${article.designation}</a>
		(${article.categorie.nom}) (nbPts: ${article.nbPoints}) (stock: ${article.stock})
		<br>
	</c:forEach>
	<br>Nombre total d'article(s): <c:out value="${nbArticles}"></c:out>		
</body>
</html>