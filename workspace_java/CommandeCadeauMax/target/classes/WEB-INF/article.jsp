<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Insert title here</title>
</head>
<body>
	<h1>${article.designation}</h1>
	<h2>${article.categorie.nom}</h2>
	<h3>nbPts: ${article.nbPoints}</h3>
	<h3>stock: ${article.stock}</h3>
	<h3>commandes passées:${article.commandes.size()}</h3>
	<span>description: ${article.description}</span>
	<br>
	<br>
	<form action="commander" method="post">
		<span>Quantity: </span><input type="number" name="quantité" id="quantité" placeholder="quantité">
		<input type="submit" value="commander">
	</form>
	
</body>
</html>