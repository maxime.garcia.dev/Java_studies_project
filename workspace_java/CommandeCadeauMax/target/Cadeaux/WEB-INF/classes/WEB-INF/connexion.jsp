<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Connexion</title>
</head>
<body>
	<jsp:include page="entete.jsp" />

	<h1>Connexion</h1>
	<c:if test="${notification ne null}">
		<span style="color : red">${notification}</span>
	</c:if>
	<form action="connexion" method="post">
		<input type="text" name="EMAIL" placeHolder="Email" /> <br />
		<input type="password" name="MOT_DE_PASSE" placeHolder="Mot de passe" /> <br/>
		<input type="submit" value="connexion" />
	</form>
</body>
</html>