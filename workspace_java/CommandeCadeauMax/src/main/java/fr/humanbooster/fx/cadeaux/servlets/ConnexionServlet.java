package fr.humanbooster.fx.cadeaux.servlets;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import fr.humanbooster.fx.cadeaux.business.Utilisateur;
import fr.humanbooster.fx.cadeaux.service.UtilisateurService;
import fr.humanbooster.fx.cadeaux.service.impl.UtilisateurServiceImpl;

/**
 * Servlet implementation class ConnexionServlet
 */
@WebServlet(urlPatterns = { "/connexion", "/sign-up", "/signUp" }, loadOnStartup = 2)
public class ConnexionServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
	
	private UtilisateurService utilisateurService;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public ConnexionServlet() {
        super();
        utilisateurService = new UtilisateurServiceImpl();
        utilisateurService.ajouterUtilisateur("max", "garcia", "max@max.fr", "max", null);
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		//On redirige vers la page de connexion
		request.getRequestDispatcher("WEB-INF/connexion.jsp").forward(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		Utilisateur utilisateur = utilisateurService.recupererUtilisateurParEmailEtMotDePasse(request.getParameter("EMAIL"), request.getParameter("MOT_DE_PASSE"));
		
		if (utilisateur != null) {
			//si on rentre dans cette branche c'est que l'utilisateur a rentrer les bonnes infos de connexion
			// On crée une session Http (par defaut cette session dure 60 minutes)
			request.getSession().setAttribute("utilisateur", utilisateur);
			//On redirige vers l'url articles
			response.sendRedirect("articles");
		}
		else {
			//Si on rentre dans cette branche c'est que l'utilisateur c'est trompé en saisissant ses informations de connexion
			request.setAttribute("notification", "email/mdp erroné");
			doGet(request, response);
		}
	}

}
