package fr.humanbooster.fx.pizza_decorator.util;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Set;

import fr.humanbooster.fx.pizza_decorator.business.Pizza;

public class Pile implements Iterator<Pizza>, Iterable<Pizza> {
	
	private Set<Pizza> stack = new HashSet<>();
	private int offset = 0;
	
    public void empiler(Pizza e) {
        stack.add(e);
    }
   
    public Pizza depiler() {
    	Pizza pizza = (Pizza) stack.toArray()[stack.size() - 1];
    	stack.remove(stack.toArray()[stack.size() - 1]);
        return (pizza);
    }
    
	public void testerIterator() {
		List<Pizza> pizzas = new ArrayList<>();
		for (Pizza pizza : stack) {
			pizzas.add(pizza);
		}
		Collections.sort(pizzas);
		System.out.println(pizzas);
		Iterator<Pizza> iteratorTemp = pizzas.iterator();
		while (iteratorTemp.hasNext()) {
			Pizza e = (Pizza) iteratorTemp.next();
			System.out.println("Test iterator : " + e + " iterator.hasNext() = " + iteratorTemp.hasNext());
		}
	}
	
    @Override
    public boolean hasNext() {
        return (offset < stack.size());
    }
    
    @Override
    public Pizza next() {
        return (Pizza) stack.toArray()[offset++];
    }
    
    @Override
    public Iterator<Pizza> iterator(){
        return stack.iterator();
    }
}