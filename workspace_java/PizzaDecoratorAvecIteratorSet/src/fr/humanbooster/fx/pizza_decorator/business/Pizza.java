package fr.humanbooster.fx.pizza_decorator.business;

public class Pizza implements Comparable<Pizza>{

	private Long id;
	private static Long compteur = 0L;
	private float PRIX_D_ACHAT = 0.1f;
	private float PRIX_DE_VENTE = 1f;

	private float prixDeVente;
	private float prixDAchat;
	private String nom;

	public Pizza() {
		id = ++compteur;
		nom = "Pizza";
		prixDeVente = PRIX_DE_VENTE;
		prixDAchat = PRIX_D_ACHAT;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public float getPrixDeVente() {
		return prixDeVente;
	}

	public void setPrixDeVente(float prixDeVente) {
		this.prixDeVente = prixDeVente;
	}

	public float getPrixDAchat() {
		return prixDAchat;
	}

	public void setPrixDAchat(float prixDAchat) {
		this.prixDAchat = prixDAchat;
	}

	public String getNom() {
		return nom;
	}

	public void setNom(String nom) {
		this.nom = nom;
	}
	
	@Override
	public int compareTo(Pizza autrePizza) {
		return getId().compareTo(autrePizza.getId());
	}

	@Override
	public String toString() {
		return "Pizza [id=" + id + "  prixDeVente=" + prixDeVente + ", prixDAchat=" + prixDAchat + ", nom=" + nom + "]";
	}	
}
