package fr.humanbooster.max.pots_de_miel.builder;

import java.util.ArrayList;
import java.util.List;

import fr.humanbooster.max.pots_de_miel.business.Acacia;
import fr.humanbooster.max.pots_de_miel.business.BarquetteDeDatte;
import fr.humanbooster.max.pots_de_miel.business.BoiteDeThe;
import fr.humanbooster.max.pots_de_miel.business.Chataigner;
import fr.humanbooster.max.pots_de_miel.business.FromageDeChevre;
import fr.humanbooster.max.pots_de_miel.business.Lavande;
import fr.humanbooster.max.pots_de_miel.business.Litchi;
import fr.humanbooster.max.pots_de_miel.business.Miel;
import fr.humanbooster.max.pots_de_miel.business.Pot;
import fr.humanbooster.max.pots_de_miel.business.Thym;
import fr.humanbooster.max.pots_de_miel.business.ToutesFleurs;
import fr.humanbooster.max.pots_de_miel.util.ListeDeCadeau;

public class PotBuilderImpl implements PotBuilder{
	
	private List<Miel> miels = new ArrayList<>();
	private ListeDeCadeau cadeaux = new ListeDeCadeau();
	Pot pot = null;
	
	public PotBuilderImpl() {
		miels.add(new Acacia());
		miels.add(new Chataigner());
		miels.add(new Lavande());
		miels.add(new Thym());
		miels.add(new Litchi());
		miels.add(new ToutesFleurs());
		
		cadeaux.ajouterCadeau(new FromageDeChevre());
		cadeaux.ajouterCadeau(new BoiteDeThe());
		cadeaux.ajouterCadeau(new BarquetteDeDatte());
	}

	public List<Miel> getMiels() {
		return miels;
	}

	public void setMiels(List<Miel> miels) {
		this.miels = miels;
	}

	public ListeDeCadeau getCadeaux() {
		return cadeaux;
	}

	public void setCadeaux(ListeDeCadeau cadeaux) {
		this.cadeaux = cadeaux;
	}
	
	@Override
	public PotBuilder definirPoids(int poids) {
		pot.setPoidsEnGrammes(poids);
		return this;
	}
	
	@Override
	public PotBuilder choisirTypeDeMiel(String typeDeMiel) {
		if (typeDeMiel.equals("Acacia")) {
			for (Miel miel : miels) {
				if (miel instanceof Acacia) {
					pot.setMiel(miel);
					return this;
				}
			}
		}
		if (typeDeMiel.equals("Chataigner")) {
			for (Miel miel : miels) {
				if (miel instanceof Chataigner) {
					pot.setMiel(miel);
					return this;
				}
			}
		}
		if (typeDeMiel.equals("Lavande")) {
			for (Miel miel : miels) {
				if (miel instanceof Lavande) {
					pot.setMiel(miel);
					return this;
				}
			}
		}
		if (typeDeMiel.equals("Thym")) {
			for (Miel miel : miels) {
				if (miel instanceof Thym) {
					pot.setMiel(miel);
					return this;
				}
			}
		}
		if (typeDeMiel.equals("Litchi")) {
			for (Miel miel : miels) {
				if (miel instanceof Litchi) {
					pot.setMiel(miel);
					return this;
				}
			}
		}
		if (typeDeMiel.equals("Toutes fleurs")) {
			for (Miel miel : miels) {
				if (miel instanceof ToutesFleurs) {
					pot.setMiel(miel);
					return this;
				}
			}
		}
		return this;
	}
}
