package fr.humanbooster.max.pots_de_miel.builder;

public interface PotBuilder {

	PotBuilder definirPoids(int poids);

	PotBuilder choisirTypeDeMiel(String typeDeMiel);
	
}
