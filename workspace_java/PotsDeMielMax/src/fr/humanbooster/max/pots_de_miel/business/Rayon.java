package fr.humanbooster.max.pots_de_miel.business;

public class Rayon extends Ingredient{
	
private float PRIX_DE_BASE = 2.0f;
	
	public Rayon(Pot pot) {
		super(pot);
		super.setPrixDuPot(super.getPrixDuPot() + PRIX_DE_BASE);
	}
}
