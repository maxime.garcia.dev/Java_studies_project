package fr.humanbooster.max.pots_de_miel.business;

public class GeleeRoyale extends Ingredient{
	
private float PRIX_DE_BASE = 1.2f;
	
	public GeleeRoyale(Pot pot) {
		super(pot);
		super.setPrixDuPot(super.getPrixDuPot() + PRIX_DE_BASE);
	}
}
