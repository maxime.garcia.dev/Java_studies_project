package fr.humanbooster.max.pots_de_miel.business;

public class BoiteDeThe extends Cadeau {
	
	private String intitule;

	public BoiteDeThe() {
		super();
		intitule = "une bo�te de 10 grammes de th� � la menthe";
	}

	public String getIntitule() {
		return intitule;
	}

	public void setIntitule(String intitule) {
		this.intitule = intitule;
	}

	@Override
	public String toString() {
		return intitule;
	}
}
