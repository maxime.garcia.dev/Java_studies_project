package fr.humanbooster.max.pots_de_miel.business;

public class FromageDeChevre extends Cadeau{
	
	private String intitule;
	
	public FromageDeChevre() {
		super();
		intitule = "un fromage de ch�vre du plateau Ard�chois";
	}

	public String getIntitule() {
		return intitule;
	}

	public void setIntitule(String intitule) {
		this.intitule = intitule;
	}

	@Override
	public String toString() {
		return intitule;
	}
}
