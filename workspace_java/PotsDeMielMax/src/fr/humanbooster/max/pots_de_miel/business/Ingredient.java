package fr.humanbooster.max.pots_de_miel.business;

public abstract class Ingredient extends PotDecorator{

	public Ingredient(Pot pot) {
		super(pot);
	}
}
