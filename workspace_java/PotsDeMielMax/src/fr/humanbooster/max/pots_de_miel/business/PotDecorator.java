package fr.humanbooster.max.pots_de_miel.business;

public abstract class PotDecorator extends Pot{
	
	Pot pot;
	
	public PotDecorator(Pot pot) {
		super();
		this.pot = pot;
	}
}
