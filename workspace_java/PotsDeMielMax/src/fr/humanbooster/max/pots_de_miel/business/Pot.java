package fr.humanbooster.max.pots_de_miel.business;

import java.util.List;

public class Pot {
	
	private int poidsEnGrammes;
	private Miel miel;
	private List<Cadeau> cadeaux;
	private float prixDuPot;
	private String etiquette;
	
	public Pot() {
		super();
	}
	
	public Pot(int poidsEnGrammes, Miel miel, List<Cadeau> cadeaux, float prixDuPot, String etiquette) {
		this();
		this.poidsEnGrammes = poidsEnGrammes;
		this.miel = miel;
		this.cadeaux = cadeaux;
		this.prixDuPot = prixDuPot;
		this.etiquette = etiquette;
	}


	public int getPoidsEnGrammes() {
		return poidsEnGrammes;
	}
	public void setPoidsEnGrammes(int poidsEnGrammes) {
		this.poidsEnGrammes = poidsEnGrammes;
	}
	public Miel getMiel() {
		return miel;
	}
	public void setMiel(Miel miel) {
		this.miel = miel;
	}
	public List<Cadeau> getCadeaux() {
		return cadeaux;
	}
	public void setCadeaux(List<Cadeau> cadeaux) {
		this.cadeaux = cadeaux;
	}
	public float getPrixDuPot() {
		return prixDuPot;
	}
	public void setPrixDuPot(float prixDuPot) {
		this.prixDuPot = prixDuPot;
	}
	public String getEtiquette() {
		return etiquette;
	}
	public void setEtiquette(String etiquette) {
		this.etiquette = etiquette;
	}
	@Override
	public String toString() {
		return "Pot [poidsEnGrammes=" + poidsEnGrammes + ", miel=" + miel + ", cadeaux=" + cadeaux + ", prixDuPot="
				+ prixDuPot + ", etiquette=" + etiquette + "]";
	}
}
