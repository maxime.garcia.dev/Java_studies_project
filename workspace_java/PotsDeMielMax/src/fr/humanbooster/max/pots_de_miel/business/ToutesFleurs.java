package fr.humanbooster.max.pots_de_miel.business;

public class ToutesFleurs extends Miel{
	
	private float PRIX_250_GRAMMES = 6.80f;
	private float PRIX_500_GRAMMES = 12.50f;
	private float PRIX_1_KILOGRAMME = 18.00f;
	private String nom;
	
	public ToutesFleurs() {
		super();
		nom = "Toutes fleurs";
	}

	public float getPRIX_250_GRAMMES() {
		return PRIX_250_GRAMMES;
	}

	public void setPRIX_250_GRAMMES(float pRIX_250_GRAMMES) {
		PRIX_250_GRAMMES = pRIX_250_GRAMMES;
	}

	public float getPRIX_500_GRAMMES() {
		return PRIX_500_GRAMMES;
	}

	public void setPRIX_500_GRAMMES(float pRIX_500_GRAMMES) {
		PRIX_500_GRAMMES = pRIX_500_GRAMMES;
	}

	public float getPRIX_1_KILOGRAMME() {
		return PRIX_1_KILOGRAMME;
	}

	public void setPRIX_1_KILOGRAMME(float pRIX_1_KILOGRAMME) {
		PRIX_1_KILOGRAMME = pRIX_1_KILOGRAMME;
	}

	public String getNom() {
		return nom;
	}

	public void setNom(String nom) {
		this.nom = nom;
	}

	@Override
	public String toString() {
		return "ToutesFleurs [PRIX_250_GRAMMES=" + PRIX_250_GRAMMES + ", PRIX_500_GRAMMES=" + PRIX_500_GRAMMES
				+ ", PRIX_1_KILOGRAMME=" + PRIX_1_KILOGRAMME + ", nom=" + nom + "]";
	}
	
}
