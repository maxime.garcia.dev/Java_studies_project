package fr.humanbooster.max.pots_de_miel.business;

public class Litchi extends Miel{
	
	private float PRIX_250_GRAMMES = 9.75f;
	private float PRIX_500_GRAMMES = 14.50f;
	private float PRIX_1_KILOGRAMME = 20.00f;
	private String nom;
	
	public Litchi() {
		super();
		nom = "Lichi";
	}

	public String getNom() {
		return nom;
	}

	public void setNom(String nom) {
		this.nom = nom;
	}

	public float getPRIX_250_GRAMMES() {
		return PRIX_250_GRAMMES;
	}

	public void setPRIX_250_GRAMMES(float pRIX_250_GRAMMES) {
		PRIX_250_GRAMMES = pRIX_250_GRAMMES;
	}

	public float getPRIX_500_GRAMMES() {
		return PRIX_500_GRAMMES;
	}

	public void setPRIX_500_GRAMMES(float pRIX_500_GRAMMES) {
		PRIX_500_GRAMMES = pRIX_500_GRAMMES;
	}

	public float getPRIX_1_KILOGRAMME() {
		return PRIX_1_KILOGRAMME;
	}

	public void setPRIX_1_KILOGRAMME(float pRIX_1_KILOGRAMME) {
		PRIX_1_KILOGRAMME = pRIX_1_KILOGRAMME;
	}

	@Override
	public String toString() {
		return "Litchi [PRIX_250_GRAMMES=" + PRIX_250_GRAMMES + ", PRIX_500_GRAMMES=" + PRIX_500_GRAMMES
				+ ", PRIX_1_KILOGRAMME=" + PRIX_1_KILOGRAMME + ", nom=" + nom + "]";
	}
	
}
