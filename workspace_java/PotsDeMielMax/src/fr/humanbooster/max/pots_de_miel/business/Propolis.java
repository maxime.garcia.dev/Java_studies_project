package fr.humanbooster.max.pots_de_miel.business;

public class Propolis extends Ingredient{
	
	private float PRIX_DE_BASE = 0.8f;
	
	public Propolis(Pot pot) {
		super(pot);
		super.setPrixDuPot(super.getPrixDuPot() + PRIX_DE_BASE);
	}
}
