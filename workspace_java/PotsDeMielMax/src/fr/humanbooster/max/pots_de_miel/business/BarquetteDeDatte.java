package fr.humanbooster.max.pots_de_miel.business;

public class BarquetteDeDatte extends Cadeau{

	private String intitule;

	public BarquetteDeDatte() {
		super();
		intitule = "une barquette de 50 grammes de dattes Deglet Nour";
	}

	public String getIntitule() {
		return intitule;
	}

	public void setIntitule(String intitule) {
		this.intitule = intitule;
	}

	@Override
	public String toString() {
		return intitule;
	}
}
