package fr.humanbooster.max.pots_de_miel;

import java.util.Scanner;

import fr.humanbooster.max.pots_de_miel.builder.PotBuilder;
import fr.humanbooster.max.pots_de_miel.builder.PotBuilderImpl;
import fr.humanbooster.max.pots_de_miel.business.Pot;

public class App {

	private static Scanner sc = new Scanner(System.in);

	public static void main(String[] args) {

		int choix = 0;

		while (!(choix >=1 && choix <= 3)) {
			afficherChoixDePoids();
			choix = demanderChoix("Veuillez saisir votre choix : ", 1, 3);

			switch (choix) {
			case 1:
				creerPotDeMiel(250);
				break;
			case 2:
				creerPotDeMiel(500);
				break;
			case 3:
				creerPotDeMiel(1000);
				break;
			default:
				break;
			}
		}
		sc.close();
	}

	public static void creerPotDeMiel(int poids) {
		PotBuilder potBuilder = new PotBuilderImpl();
		Pot pot = null;
		int choix = 0;
		
		while (!(choix >= 1 && choix <= 6)) {
			afficherChoixMiels();
			choix = demanderChoix("Veuillez choisir un type de miel :", 1, 6);
			
			switch (choix) {
			case 1:
				
			}
		}
	}
	
	public static void afficherChoixDePoids() {
		System.out.println("250 grammes");
		System.out.println("500 grammes");
		System.out.println("1 kilogramme");
	}
	
	public static void afficherChoixMiels() {
		System.out.println("Acacia");
		System.out.println("Châtaigner");
		System.out.println("Lavande");
		System.out.println("Thym");
		System.out.println("Litchi");
		System.out.println("Toutes fleurs");
	}

	private static int demanderChoix(String message, int min, int max) {
		int valeur = min - 1;
		do {
			System.out.print(message);
			try {
				String saisie = sc.nextLine();
				valeur = Integer.parseInt(saisie);
				if (valeur < min || valeur > max) {
					System.out.println("Merci de saisir un nombre compris entre " + min + " et " + max);
				}
			} catch (Exception e) {
				System.out.println("Merci de saisir un nombre");
			}

		} while (!(valeur >= min && valeur <= max));
		return valeur;
	}
}
