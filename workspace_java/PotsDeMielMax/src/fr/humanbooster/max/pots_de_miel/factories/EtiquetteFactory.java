package fr.humanbooster.max.pots_de_miel.factories;

public class EtiquetteFactory {
	
	private static EtiquetteFactory etiquetteFactory;
	
	private EtiquetteFactory() {	
	}
	
	public static EtiquetteFactory getInstance() {
		if (etiquetteFactory == null) {
			synchronized (EtiquetteFactory.class) {
				if (etiquetteFactory == null) {
					etiquetteFactory = new EtiquetteFactory();
				}
			}
		}
		return etiquetteFactory;
	}
	
	public String genererEtiquette(String message) {
		String etiquette = message;
		return etiquette;
	}
}
