package fr.humanbooster.max.pots_de_miel.util;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import fr.humanbooster.max.pots_de_miel.business.Cadeau;

public class ListeDeCadeau implements Iterator<Cadeau>, Iterable<Cadeau>{
	
	List<Cadeau> cadeaux = new ArrayList<>();
	private int offset = 0;
	
	
	public void ajouterCadeau(Cadeau cadeau) {
		cadeaux.add(cadeau);
	}
	
	@Override
	public Iterator<Cadeau> iterator() {
		return cadeaux.iterator();
	}

	@Override
	public boolean hasNext() {
		return (offset < cadeaux.size());
	}

	@Override
	public Cadeau next() {
		return cadeaux.get(offset++);
	}

}
