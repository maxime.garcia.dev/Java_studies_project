package fr.humanbooster.fx.pret_a_la_consommation;

import java.util.Scanner;

import fr.humanbooster.fx.pret_a_la_consommation.buisness.Duree;
import fr.humanbooster.fx.pret_a_la_consommation.buisness.Motif;
import fr.humanbooster.fx.pret_a_la_consommation.service.ClientService;
import fr.humanbooster.fx.pret_a_la_consommation.service.DureeService;
import fr.humanbooster.fx.pret_a_la_consommation.service.MotifService;
import fr.humanbooster.fx.pret_a_la_consommation.service.PretService;
import fr.humanbooster.fx.pret_a_la_consommation.service.TauxService;
import fr.humanbooster.fx.pret_a_la_consommation.service.impl.ClientServiceImpl;
import fr.humanbooster.fx.pret_a_la_consommation.service.impl.DureeServiceImpl;
import fr.humanbooster.fx.pret_a_la_consommation.service.impl.MotifServiceImpl;
import fr.humanbooster.fx.pret_a_la_consommation.service.impl.PretServiceImpl;
import fr.humanbooster.fx.pret_a_la_consommation.service.impl.TauxServiceImpl;
import fr.humanbooster.fx.pret_a_la_consommation.util.FormateurDate;

public class App {
	
	private static final int AFFICHER_PRETS_TRIES_PAR_MONTANT_DECROISSANT = 1;
	private static final int AFFICHER_PRETS_TRIES_PAR_TAUX_DECROISSANT = 2;
	private static final int AFFICHER_PRETS_DEBUTANT_ENTRE_DEUX_DATES = 3;
	private static final int AJOUTER_PRET = 4;
	private static final int QUITTER = 5;
	private static ClientService clientService = new ClientServiceImpl();
	private static DureeService dureeService = new DureeServiceImpl();
    private static MotifService motifService = new MotifServiceImpl();
    private static TauxService tauxService = new TauxServiceImpl();
    private static PretService pretService = new PretServiceImpl();
    private static Scanner sc = new Scanner(System.in);
    
	
	public static void main(String[] args) {
		ajouterCinqClients();
		ajouterDeuxMotifs();
		ajouterDeuxDurees();
		ajouterCinqTaux();
	}
	
	private static void ajouterCinqClients() {
		clientService.ajouterClient("Garcia","Adrien");
		clientService.ajouterClient("Garcia","Maxime");
		clientService.ajouterClient("Razanajatovo","Tony");
		clientService.ajouterClient("Hattam","Maty");
		clientService.ajouterClient("Quach","Tritin");
	}
	
	private static void ajouterDeuxMotifs() {
		motifService.ajouterMotif("auto");
		motifService.ajouterMotif("moto");
	}
	
	private static void ajouterDeuxDurees() {
		dureeService.ajouterDuree(12);
		dureeService.ajouterDuree(24);
	}
	
	private static void ajouterCinqTaux() {
		for (int i = 0; i < 5; i++) {
			Duree dureeChoisi;
			Motif motifChoisi;
			System.out.println("Veuillez choisir une duree: 1 = 12 mois; 2 = 24 mois");
			dureeChoisi = dureeService.recupererDuree(Long.parseLong(sc.nextLine()) - 1);
			System.out.println("Veuillez choisir un motif: 1 = auto; 2 = moto");
			motifChoisi = motifService.recupererMotif(Long.parseLong(sc.nextLine()) - 1);
			tauxService.ajouterTaux(dureeChoisi, motifChoisi, i + 1);
		}
	}
	
	private static void afficherMenuPrincipal() {
        System.out.println(AFFICHER_PRETS_TRIES_PAR_MONTANT_DECROISSANT + ". Voir tous les pr�ts tri�es par montant (du plus �lev� au plus petit)");
        System.out.println(AFFICHER_PRETS_TRIES_PAR_TAUX_DECROISSANT + ". Voir tous les pr�ts tri�es par taux (du plus �lev� au plus petit)");
        System.out.println(AFFICHER_PRETS_DEBUTANT_ENTRE_DEUX_DATES + ". Voir la liste des pr�ts qui d�butent entre deux dates donn�es");
        System.out.println(AJOUTER_PRET + ". Ajouter un pr�t");
        System.out.println(QUITTER + ". Quitter");
    }
}
