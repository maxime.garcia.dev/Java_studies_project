package fr.humanbooster.fx.pret_a_la_consommation.util;

import java.text.SimpleDateFormat;
import java.util.Date;

public class FormateurDate {
	private static final String FORMAT_DATE_FRANCAIS = "dd/MM/yyyy";
	private static SimpleDateFormat simpleDateFormat = new SimpleDateFormat(FORMAT_DATE_FRANCAIS);
	
	public static String formaterDate(Date date) {
		return simpleDateFormat.format(date);
	}
}