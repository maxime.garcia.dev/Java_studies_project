package fr.humanbooster.fx.pret_a_la_consommation.util;

import java.util.Comparator;

import fr.humanbooster.fx.pret_a_la_consommation.buisness.Pret;

public class ComparateurSurMontant implements Comparator<Pret>{

	@Override
	public int compare(Pret pret1, Pret pret2) {
		return Float.compare(pret1.getMontantDemande(), pret2.getMontantDemande());
	}

}
