package fr.humanbooster.fx.pret_a_la_consommation.service;

import java.util.List;

import fr.humanbooster.fx.pret_a_la_consommation.buisness.Duree;
import fr.humanbooster.fx.pret_a_la_consommation.buisness.Motif;
import fr.humanbooster.fx.pret_a_la_consommation.buisness.Taux;

public interface TauxService {
	
	Taux ajouterTaux(Duree duree, Motif motif, float valeur);
	
	List<Taux> recupererTaux();
	
	Taux recupererUnTaux(Long id);
	
	boolean supprimerTaux(Long id);
}
