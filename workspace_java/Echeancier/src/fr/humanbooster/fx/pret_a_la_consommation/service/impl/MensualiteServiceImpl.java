package fr.humanbooster.fx.pret_a_la_consommation.service.impl;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import fr.humanbooster.fx.pret_a_la_consommation.buisness.Mensualite;
import fr.humanbooster.fx.pret_a_la_consommation.buisness.Pret;
import fr.humanbooster.fx.pret_a_la_consommation.service.MensualiteService;

public class MensualiteServiceImpl implements MensualiteService {
	
	private static List<Mensualite> mensualites = new ArrayList<>();
	@Override
	public Mensualite ajouterMensualite(Pret pret, Date datePrelevement, float partInteretsrembourses,
			float partCapitalRembourse) {
		Mensualite mensualite = new Mensualite(pret, datePrelevement, partInteretsrembourses, partCapitalRembourse);
		mensualites.add(mensualite);
		return mensualite;
	}

	@Override
	public List<Mensualite> recupererMensualites() {
		return mensualites;
	}
	/**
	 * Cette m�thode permet de recup�rer � partir de l'id d'une mensualit� toute la liste mensualit�s dans le pr�t associer
	 */
	@Override
    public List<Mensualite> recupererMensualites(Long id) {
        Mensualite mensualite = recupererMensualite(id);
        if (mensualite != null) {
            return mensualite.getPret().getMensualites();
        }
        return null;
    }
	
	@Override
	public Mensualite recupererMensualite(Long id) {
		for (Mensualite mensualite : mensualites) {
			if (mensualite.getId().equals(id)) {
				return mensualite;
			}
		}
		return null;
	}
	
	@Override
	public boolean supprimerMensualite(Long id) {
		Mensualite mensualite = recupererMensualite(id);
		if (mensualite != null) {
			mensualite.getPret().getMensualites().remove(mensualite);
			mensualites.remove(mensualite);
			return true;
		}
		return false;
	}
}
