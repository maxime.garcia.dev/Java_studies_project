package fr.humanbooster.fx.pret_a_la_consommation.service;

import java.util.Date;
import java.util.List;

import fr.humanbooster.fx.pret_a_la_consommation.buisness.Mensualite;
import fr.humanbooster.fx.pret_a_la_consommation.buisness.Pret;

public interface MensualiteService {
	
	Mensualite ajouterMensualite(Pret pret, Date datePrelevement, float partInteretsrembourses, float partCapitalRembourse);
	
	List<Mensualite> recupererMensualites();
	
	List<Mensualite> recupererMensualites(Long id);
	
	Mensualite recupererMensualite(Long id);
	
	boolean supprimerMensualite(Long id);

}