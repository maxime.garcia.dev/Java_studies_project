package fr.humanbooster.fx.pret_a_la_consommation.service.impl;

import java.util.ArrayList;
import java.util.List;

import fr.humanbooster.fx.pret_a_la_consommation.buisness.Duree;
import fr.humanbooster.fx.pret_a_la_consommation.service.DureeService;

public class DureeServiceImpl implements DureeService {
    private static List<Duree> durees = new ArrayList<>();

    @Override
    public Duree ajouterDuree(int dureeEnMois) {
          Duree duree1 = new Duree(dureeEnMois);
            durees.add(duree1);
            return duree1;
            
    }

    @Override
    public List<Duree> recupererDurees() {

        return durees;
    }

    @Override
    public Duree recupererDuree(Long id) {
        // boucle pour parcourir liste duree, si l'id plac� en param�tre dans la
        // m�thode
        for (Duree duree: durees) {
            if (id.equals(duree.getId())) {
                return duree;
            }
        }
        return null;
    }


    @Override
    public boolean supprimerDuree(Long id) {
        // on utilise la m�thode recuperer duree pour ne pas reecrire 
         Duree duree = recupererDuree(id);
         // on teste si la duree est bien ds la liste (!=null)
         if (duree != null) {
        // si c'est le cas on invoque la m�thode remove pour effacer
                return durees.remove(duree);
         }
         // si non on retourne faux
         return false;
    }
    


}
