package fr.humanbooster.fx.pret_a_la_consommation.service;

import java.util.Date;
import java.util.List;

import fr.humanbooster.fx.pret_a_la_consommation.buisness.Client;
import fr.humanbooster.fx.pret_a_la_consommation.buisness.Pret;
import fr.humanbooster.fx.pret_a_la_consommation.buisness.Taux;

public interface PretService {
    
    Pret ajouterPret(Taux taux, Client client, float montantDemande, Date dateEffet);

    List<Pret> recupererPrets();

    Pret recupererPret(Long id);
    
    boolean supprimerPret(Long id);

}