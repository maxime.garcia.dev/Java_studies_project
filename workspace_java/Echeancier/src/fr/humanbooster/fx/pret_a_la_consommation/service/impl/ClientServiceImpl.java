package fr.humanbooster.fx.pret_a_la_consommation.service.impl;

import java.util.ArrayList;
import java.util.List;
import fr.humanbooster.fx.pret_a_la_consommation.buisness.Client;
import fr.humanbooster.fx.pret_a_la_consommation.service.ClientService;

public class ClientServiceImpl implements ClientService {
	private static List<Client> clients = new ArrayList<>();
	
	@Override
	public Client ajouterClient(String nom, String prenom) {
		Client client = new Client(nom, prenom);
		clients.add(client);
		return (client);
	}
	
	@Override
	public List<Client> recupererClients(){	
		return clients;
	}
	
	@Override
	public Client recuperClient(Long id) {
		// On parcours la liste des clients
		for (Client client : clients) {
			// On test si l'id du client est �gal � l'id en param�tre
			if (client.getId().equals(id)) {
				return (client);
			}
		}
		return null;
	}

	@Override
	public boolean supprimerClient(Long id) {
		Client client = recuperClient(id);
		// On v�rifie que la methode recupererClient a bien trouver un client dans la liste
		if (client != null) {
			//si tel est le cas on invoque la methode remove
			//On peut l'utiliser dans le return car la methode remove renvoie un boolean.
			return (clients.remove(client));
			
		}
		return false;
	}

}
