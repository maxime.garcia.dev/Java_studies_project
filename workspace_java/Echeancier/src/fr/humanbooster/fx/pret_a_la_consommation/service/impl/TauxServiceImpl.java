package fr.humanbooster.fx.pret_a_la_consommation.service.impl;

import java.util.ArrayList;
import java.util.List;

import fr.humanbooster.fx.pret_a_la_consommation.buisness.Duree;
import fr.humanbooster.fx.pret_a_la_consommation.buisness.Motif;
import fr.humanbooster.fx.pret_a_la_consommation.buisness.Taux;
import fr.humanbooster.fx.pret_a_la_consommation.service.TauxService;

public class TauxServiceImpl implements TauxService {
	
	private static List<Taux> taux = new ArrayList<>();
	
	@Override
	public Taux ajouterTaux(Duree duree, Motif motif,float valeur) {
		Taux taux1 = new Taux(duree, motif, valeur);
		taux.add(taux1);
		return taux1;
	}

	@Override
	public List<Taux> recupererTaux() {
		return taux;
	}

	@Override
	public Taux recupererUnTaux(Long id) {
		for (Taux taux2 : taux) {
			if(taux2.getId().equals(id)) {
				return taux2;
			}
		}
		return null;
	}

	@Override
	public boolean supprimerTaux(Long id) {
		Taux taux2 = recupererUnTaux(id);
		if(taux2 != null) {
			return taux.remove(taux2);
		}
		return false;
	}

}