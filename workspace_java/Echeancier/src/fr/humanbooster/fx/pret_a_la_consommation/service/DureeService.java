package fr.humanbooster.fx.pret_a_la_consommation.service;

import java.util.List;

import fr.humanbooster.fx.pret_a_la_consommation.buisness.Duree;


public interface DureeService {
    
    Duree ajouterDuree(int dureeEnMois);

    List<Duree> recupererDurees();

    Duree recupererDuree(Long id);
    
    boolean supprimerDuree(Long id);

}