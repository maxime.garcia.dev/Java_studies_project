package fr.humanbooster.fx.pret_a_la_consommation.service;

import java.util.List;

import fr.humanbooster.fx.pret_a_la_consommation.buisness.Client;

/**
 * Les intefaces contiennent uniquement des d�clarations de m�thode Une
 * interface pet �tre vu comme l'ardoise d'un restaurant. L'implementation quand
 * a elle sera faite dans la classe
 * 
 * @author HB
 *
 */
public interface ClientService {
	Client ajouterClient(String nom, String prenom);

	List<Client> recupererClients();

	Client recuperClient(Long id);
	
	boolean supprimerClient(Long id);
}
