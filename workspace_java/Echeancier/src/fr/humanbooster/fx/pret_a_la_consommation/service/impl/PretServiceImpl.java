package fr.humanbooster.fx.pret_a_la_consommation.service.impl;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import fr.humanbooster.fx.pret_a_la_consommation.buisness.Client;
import fr.humanbooster.fx.pret_a_la_consommation.buisness.Pret;
import fr.humanbooster.fx.pret_a_la_consommation.buisness.Taux;
import fr.humanbooster.fx.pret_a_la_consommation.service.PretService;

public class PretServiceImpl implements PretService {
   
	    private static List<Pret> prets = new ArrayList<>();

	    @Override
	    public Pret ajouterPret(Taux taux, Client client, float montantDemande, Date dateEffet) {
	        // On d�termine le montant de l'�ch�ance (ce que le client va rembourser chaque mois)
	        // montantEcheance = (double) (montantDemande * tauxMensuel / (1 - Math.pow(1+tauxMensuel, -dureeEnMois)));
	        double tauxMensuel = taux.getValeur() / 12;
	        int nbMois = taux.getDuree().getDureeEnMois();
	        float montantMensualite = (float) (montantDemande * tauxMensuel / (1 - Math.pow(1 + tauxMensuel, -nbMois)));
	        // On cree un objet pret
	        Pret pret = new Pret(taux, client, montantDemande, montantMensualite, dateEffet);
			return pret;
	    }

	    @Override
	    public List<Pret> recupererPrets() {
	        return prets;
	    }

	    @Override
	    public Pret recupererPret(Long id) {
	        // On parcourt la liste des clients
	        for (Pret pret : prets) {
	            // On test si l'id du client est �gal � l'id donn� en param�tre
	            if (id.equals(pret.getId())) {
	                return pret;
	            }
	        }
	        return null;
	    }

	    @Override
	    public boolean supprimerPret(Long id) {
	        Pret pret = recupererPret(id);
	        // On test si le le client n'est pas null (bien pr�sent dans la liste des
	        // clients)
	        if (pret != null) {
	            // Si c'est le cas, on invoque la m�thode remove pour l'effacer
	            // On le retourne car remove renvoie un booleen
	            return prets.remove(pret);
	        }
	        // Sinon on retourne faux.
	        return false;
	    }
	}
