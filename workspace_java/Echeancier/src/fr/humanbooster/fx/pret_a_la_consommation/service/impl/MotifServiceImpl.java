package fr.humanbooster.fx.pret_a_la_consommation.service.impl;

import java.util.ArrayList;
import java.util.List;

import fr.humanbooster.fx.pret_a_la_consommation.buisness.Motif;
import fr.humanbooster.fx.pret_a_la_consommation.service.MotifService;

public class MotifServiceImpl implements MotifService {
    private static List<Motif> motifs = new ArrayList<>();
    
    @Override
    public Motif ajouterMotif(String nom) {
        motifs.add(new Motif(nom));
        return null;
    }

    @Override
    public List<Motif> recupererMotifs() {
        return motifs;
    }

    @Override
    public Motif recupererMotif(Long id) {
        for (Motif motif : motifs) {
            if(motif.getId().equals(id));
            return motif;
        }
        return null;
    }

    @Override
    public boolean supprimerMotif(Long id) {
        Motif motif = recupererMotif(id);
        if (motif != null) {
            return motifs.remove(motif);
        }
        return false;
    }

}