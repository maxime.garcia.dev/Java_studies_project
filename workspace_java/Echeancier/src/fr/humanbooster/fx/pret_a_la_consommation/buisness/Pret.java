package fr.humanbooster.fx.pret_a_la_consommation.buisness;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class Pret {
	private List<Mensualite> mensualites = new ArrayList<>();
	private Taux taux;
	private Client client;
	private static Long compteur = 0L;
	private Long id;
	private float montantDemande;
	private float montantMensualite;
	private Date dateSouscription;
	private Date dateEffet;
	private String observations;
	
	public Pret() {
		id = ++compteur;
	}
	
	public Pret(Taux taux, Client client, float montantDemande, float montantMensualite, Date dateEffet) {
		super();
		this.taux = taux;
		this.client = client;
		this.montantDemande = montantDemande;
		this.montantMensualite = montantMensualite;
		this.dateEffet = dateEffet;
	}


	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}
	
	public List<Mensualite> getMensualites() {
		return mensualites;
	}

	public void setMensualites(List<Mensualite> mensualites) {
		this.mensualites = mensualites;
	}

	public Taux getTaux() {
		return taux;
	}

	public void setTaux(Taux taux) {
		this.taux = taux;
	}

	public Client getClient() {
		return client;
	}

	public void setClient(Client client) {
		this.client = client;
	}

	public float getMontantDemande() {
		return montantDemande;
	}

	public void setMontantDemande(float montantDemande) {
		this.montantDemande = montantDemande;
	}

	public float getMontantMensualite() {
		return montantMensualite;
	}

	public void setMontantMensualite(float montantMensualite) {
		this.montantMensualite = montantMensualite;
	}

	public Date getDateSouscription() {
		return dateSouscription;
	}

	public void setDateSouscription(Date dateSouscription) {
		this.dateSouscription = dateSouscription;
	}

	public Date getDateEffet() {
		return dateEffet;
	}

	public void setDateEffet(Date dateEffet) {
		this.dateEffet = dateEffet;
	}

	public String getObservations() {
		return observations;
	}

	public void setObservations(String observations) {
		this.observations = observations;
	}
	
	@Override
	public String toString() {
		return "Pret [id=" + id + ", montantDemande=" + montantDemande + ", montantMensualite=" + montantMensualite
				+ ", dateSouscription=" + dateSouscription + ", dateEffet=" + dateEffet + ", observations="
				+ observations + "]";
	}
}