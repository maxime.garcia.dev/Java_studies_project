package fr.humanbooster.fx.pret_a_la_consommation.buisness;

import java.util.ArrayList;
import java.util.List;

public class Duree {
	private List<Taux> taux = new ArrayList<>();
	private static Long compteur = 0L;
	private Long id;
	private int dureeEnMois;
	
	public Duree() {
		id = ++compteur;
	}

	public Duree(int dureeEnMois) {
		this();
		this.dureeEnMois = dureeEnMois;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public List<Taux> getTaux() {
		return taux;
	}

	public void setTaux(List<Taux> taux) {
		this.taux = taux;
	}

	public int getDureeEnMois() {
		return dureeEnMois;
	}

	public void setDureeEnMois(int dureeEnMois) {
		this.dureeEnMois = dureeEnMois;
	}

	@Override
	public String toString() {
		return "Duree [id=" + id + ", dureeEnMois=" + dureeEnMois + "]";
	}
}
