package fr.humanbooster.fx.pret_a_la_consommation.buisness;

import java.util.ArrayList;
import java.util.List;

public class Taux {
	private List<Pret> prets = new ArrayList<>();
	private Duree duree;
	private Motif motif;
	private static Long compteur = 0L;
	private Long id;
	private float valeur;
	
	public Taux() {
		id = ++compteur;
	}

	public Taux(Duree duree, Motif motif, float valeur) {
		this();
		this.duree = duree;
		this.motif = motif;
		this.valeur = valeur;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public List<Pret> getPrets() {
		return prets;
	}

	public void setPrets(List<Pret> prets) {
		this.prets = prets;
	}

	public Duree getDuree() {
		return duree;
	}

	public void setDuree(Duree duree) {
		this.duree = duree;
	}

	public Motif getMotif() {
		return motif;
	}

	public void setMotif(Motif motif) {
		this.motif = motif;
	}

	public float getValeur() {
		return valeur;
	}

	public void setValeur(float valeur) {
		this.valeur = valeur;
	}

	@Override
	public String toString() {
		return "Taux [id=" + id + ", valeur=" + valeur + "]";
	}

}
