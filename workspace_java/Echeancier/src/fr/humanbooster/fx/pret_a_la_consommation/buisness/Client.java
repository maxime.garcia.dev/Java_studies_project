package fr.humanbooster.fx.pret_a_la_consommation.buisness;

import java.util.ArrayList;
import java.util.List;

public class Client {
	private List<Pret> prets = new ArrayList<>();
	private static Long compteur = 0L;
	private Long id;
	private String nom;
	private String prenom;
	
	public Client() {
		id = ++compteur;
	}

	public Client(String nom, String prenom) {
		this();
		this.nom = nom;
		this.prenom = prenom;
	}

	public List<Pret> getPrets() {
		return prets;
	}

	public void setPrets(List<Pret> prets) {
		this.prets = prets;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getNom() {
		return nom;
	}

	public void setNom(String nom) {
		this.nom = nom;
	}

	public String getPrenom() {
		return prenom;
	}

	public void setPrenom(String prenom) {
		this.prenom = prenom;
	}

	@Override
	public String toString() {
		return "Client [id=" + id + ", nom=" + nom + ", prenom=" + prenom + "]";
	}
}
