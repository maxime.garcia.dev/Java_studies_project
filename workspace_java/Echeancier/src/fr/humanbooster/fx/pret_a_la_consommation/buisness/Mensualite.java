package fr.humanbooster.fx.pret_a_la_consommation.buisness;

import java.util.Date;

public class Mensualite {
	private Pret pret;
	private static Long compteur = 0L;
	private Long id;
	private Date datePrelevement;
	private float partInteretsRembourses;
	private float partCapitalRembourse;
	
	public Mensualite() {
		id = ++compteur;
	}

	public Mensualite(Pret pret, Date datePrelevement, float partInteretsRembourses, float partCapitalRembourse) {
		this();
		this.pret = pret;
		this.datePrelevement = datePrelevement;
		this.partInteretsRembourses = partInteretsRembourses;
		this.partCapitalRembourse = partCapitalRembourse;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Pret getPret() {
		return pret;
	}

	public void setPret(Pret pret) {
		this.pret = pret;
	}

	public Date getDatePrelevement() {
		return datePrelevement;
	}

	public void setDatePrelevement(Date datePrelevement) {
		this.datePrelevement = datePrelevement;
	}

	public float getPartInteretsRembourses() {
		return partInteretsRembourses;
	}

	public void setPartInteretsRembourses(float partInteretsRembourses) {
		this.partInteretsRembourses = partInteretsRembourses;
	}

	public float getPartCapitalRembourse() {
		return partCapitalRembourse;
	}

	public void setPartCapitalRembourse(float partCapitalRembourse) {
		this.partCapitalRembourse = partCapitalRembourse;
	}

	@Override
	public String toString() {
		return "Mensualite [id=" + id + ", datePrelevement=" + datePrelevement + ", partInteretsRembourses="
				+ partInteretsRembourses + ", partCapitalRembourse=" + partCapitalRembourse + "]";
	}
}
