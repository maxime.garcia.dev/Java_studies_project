package fr.humanbooster.max.avis.service.impl;

import java.util.List;

import org.springframework.stereotype.Service;

import fr.humanbooster.max.avis.business.Genre;
import fr.humanbooster.max.avis.dao.GenreDao;
import fr.humanbooster.max.avis.service.GenreService;

@Service
public class GenreServiceImpl implements GenreService {

	private GenreDao genreDao;
	
	public GenreServiceImpl(GenreDao genreDao) {
		super();
		this.genreDao = genreDao;
	}
	
	@Override
	public Genre ajouterGenre(String nom) {
		return genreDao.save(new Genre(nom));
	}

	@Override
	public Genre recupererGenre(String nom) {
		return genreDao.findFirstByNomLike("%"+nom+"%");
	}

	@Override
	public List<Genre> recuperergenres() {
		return genreDao.findAll();
	}

}
