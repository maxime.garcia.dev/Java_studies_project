package fr.humanbooster.max.avis.controller;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpSession;

import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import fr.humanbooster.max.avis.business.Avis;
import fr.humanbooster.max.avis.business.Classification;
import fr.humanbooster.max.avis.business.Editeur;
import fr.humanbooster.max.avis.business.Jeu;
import fr.humanbooster.max.avis.business.Joueur;
import fr.humanbooster.max.avis.service.AvisService;
import fr.humanbooster.max.avis.service.ClassificationService;
import fr.humanbooster.max.avis.service.EditeurService;
import fr.humanbooster.max.avis.service.GenreService;
import fr.humanbooster.max.avis.service.JeuService;
import fr.humanbooster.max.avis.service.JoueurService;
import fr.humanbooster.max.avis.service.ModeleEconomiqueService;
import fr.humanbooster.max.avis.service.PlateformeService;

@RestController
public class AvisRestController {

	private EditeurService editeurService;
	private GenreService genreService;
	private ClassificationService classificationService;
	private PlateformeService plateformeService;
	private ModeleEconomiqueService modeleEconomiqueService;
	private JeuService jeuService;
	private JoueurService joueurService;
	private AvisService avisService;
	private HttpSession httpSession;
	private static SimpleDateFormat simpleDateFormatAmericain = new SimpleDateFormat("yyyy-MM-dd");

	public AvisRestController(EditeurService editeurService, GenreService genreService,
			ClassificationService classificationService, PlateformeService plateformeService,
			ModeleEconomiqueService modeleEconomiqueService, JeuService jeuService, JoueurService joueurService,
			AvisService avisService, HttpSession httpSession) {
		super();
		this.editeurService = editeurService;
		this.genreService = genreService;
		this.classificationService = classificationService;
		this.plateformeService = plateformeService;
		this.modeleEconomiqueService = modeleEconomiqueService;
		this.jeuService = jeuService;
		this.joueurService = joueurService;
		this.avisService = avisService;
		this.httpSession = httpSession;
	}
	
	@PostMapping("/editeurs/{nom}")
	public Editeur ajouterEditeur(@PathVariable String nom){
		System.out.println(new Date() + "Demande d'ajout d'une Editeur");
		Editeur editeur = editeurService.ajouterEditeur(nom);
		return editeur;
	}
	
	@PostMapping(value="/jeux/{idJeu}/avis/{idJoueur}/{note}/{description}", produces = MediaType.APPLICATION_JSON_VALUE)
	public Avis ajouterAvis(@PathVariable Long idJoueur, @PathVariable Long idJeu, @PathVariable Float note, @PathVariable String description) {
		System.out.println(new Date() + "Demande d'ajout d'un avis");
		 Avis avis = avisService.ajouterAvis(new Avis(new Date(), note, description, jeuService.recupererJeuParId(idJeu), joueurService.recupererJoueurParId(idJoueur)));
		return avis;
	}
	
	@GetMapping(value="jeux/{idJeu}/avis", produces = MediaType.APPLICATION_JSON_VALUE)
	public List<Avis> recupererAvisSurJeu(@PathVariable Long idJeu) {
		List<Avis> listAvis = avisService.recupererDesAvisParJeuId(idJeu);
		return listAvis;
	}
	
	@PutMapping(value="jeux/{idJeu}/{nouveauNom}", produces = MediaType.APPLICATION_JSON_VALUE)
	public Jeu modifierNomJeu(@PathVariable Long idJeu, @PathVariable String nouveauNom) {
		Jeu jeu = jeuService.recupererJeuParId(idJeu);
		jeu.setNom(nouveauNom);
		jeuService.ajouterJeu(jeu);
		return jeu;
	}
	
	@PostMapping(value="/classification/{nom}", produces = MediaType.APPLICATION_JSON_VALUE)
	public Classification ajouterClassification(@PathVariable String nom){
		System.out.println(new Date() + "Demande d'ajout d'une Editeur");
		Classification classification = classificationService.ajouterClassification(nom);
		return classification;
	}
	
	@GetMapping(value="/joueurs/top", produces = MediaType.APPLICATION_JSON_VALUE)
	public Joueur recupererJoueurTopAvis() {
		List<Joueur> joueurs = joueurService.recupererJoueurs();
		Joueur topJoueur = null;
		for (Joueur joueur : joueurs) {
			if (topJoueur == null || joueur.getAvis().size() > topJoueur.getAvis().size()) {
				topJoueur = joueur;
			}
		}
		return topJoueur;
	}
	
	@DeleteMapping(value="/jeux/{idJeu}/avis/{idAvis}", produces = MediaType.APPLICATION_JSON_VALUE)
	public boolean supprimerAvis(@PathVariable Long idJeu, @PathVariable Long idAvis) {
		avisService.supprimerAvis(avisService.recupererAvisParId(idAvis));
		return true;
	}
	
	@GetMapping(value = "/jeux", produces = MediaType.APPLICATION_JSON_VALUE)
    public List<Jeu> recupererJeuxParDate(@RequestParam(name = "dateDebut" ) String dateDebut, @RequestParam(name = "dateFin" ) String dateFin)
    {
        Date dateDebut1 = null;
        Date dateFin1 = null;
        try {
			dateDebut1 = simpleDateFormatAmericain.parse(dateDebut);
            dateFin1 = simpleDateFormatAmericain.parse(dateFin);
        } catch (ParseException e) { 
            e.printStackTrace();
        }
        return jeuService.recupererJeux(dateDebut1, dateFin1);
    }
	
	@GetMapping("/joueur/{idJoueur}/dernierAvis")
    public Avis recupererDernierAvisDuJoueur(@PathVariable Long idJoueur) {
        Joueur joueur = joueurService.recupererJoueurParId(idJoueur);
        return avisService.recupererDernierAvisParJoueur(joueur);
    }
	
	@GetMapping("/jeux/top")
    public Jeu recupererJeuxAvecMeilleurMoyenne() {
        return jeuService.recupererJeuAvecMeilleurMoyenne();
    }
}
