package fr.humanbooster.max.avis.service;

import java.util.List;

import fr.humanbooster.max.avis.business.Plateforme;

public interface PlateformeService {

	Plateforme ajouterPlateforme(String nom);
	
	List<Plateforme> recupererPlateformes();
}
