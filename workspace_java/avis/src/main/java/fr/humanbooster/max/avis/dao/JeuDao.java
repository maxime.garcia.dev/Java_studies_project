package fr.humanbooster.max.avis.dao;

import java.util.Date;
import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import fr.humanbooster.max.avis.business.Editeur;
import fr.humanbooster.max.avis.business.Genre;
import fr.humanbooster.max.avis.business.Jeu;

public interface JeuDao extends JpaRepository<Jeu, Long> {
	
	 	List<Jeu> findByEditeur(Editeur editeur);
	 	
	 	List<Jeu> findByGenre(Genre genre);
 
	    List<Jeu> findByEditeurAndGenre(Editeur editeur, Genre genre);

	    List<Jeu> findByNomLike(String nom);
	    
	    List<Jeu> findByNomLikeAndDateSortieBetween(String nom, Date dateDebut, Date dateFin);

	    List<Jeu> findByEditeurAndNomLikeAndDateSortieBetween(Editeur editeur, String nom, Date dateDebut, Date dateFin);
	    
	    @Query("from Jeu where dateSortie between :dateDebut and :dateFin order by dateSortie")
	    List<Jeu> findByDateSortieBetween(@Param("dateDebut") Date dateDebut, @Param("dateFin") Date dateFin);

		Page<Jeu> findByGenre(Genre genre, Pageable pageable);

		@Query("select a.jeu from Avis a group by a.jeu order by avg(a.note) desc, a.jeu.dateSortie desc")
		List<Jeu> findFirstGroupByJeuOrderByMoyenneNoteAndDateSortie();
}
