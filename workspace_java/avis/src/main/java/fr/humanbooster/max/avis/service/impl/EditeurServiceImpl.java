package fr.humanbooster.max.avis.service.impl;

import java.util.List;

import org.springframework.stereotype.Service;

import fr.humanbooster.max.avis.business.Editeur;
import fr.humanbooster.max.avis.dao.EditeurDao;
import fr.humanbooster.max.avis.service.EditeurService;

@Service
public class EditeurServiceImpl implements EditeurService {

	private EditeurDao editeurDao;
	
	public EditeurServiceImpl(EditeurDao editeurDao) {
		super();
		this.editeurDao = editeurDao;
	}
	@Override
	public Editeur ajouterEditeur(String nom) {
		return editeurDao.save(new Editeur(nom));
	}
	@Override
	public List<Editeur> recupererEditeurs(String nom) {
		return editeurDao.findByNomLike("%"+nom+"%");
	}
	@Override
	public List<Editeur> recupererEditeurParGenre(String nom) {
		return editeurDao.findDistinctByJeuxGenreNomStartingWith(nom);
	}
	@Override
	public List<Editeur> recupererEditeurs() {
		return editeurDao.findAll();
	}

}
