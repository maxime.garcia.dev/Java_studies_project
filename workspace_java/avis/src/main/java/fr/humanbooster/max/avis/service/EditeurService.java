package fr.humanbooster.max.avis.service;

import java.util.List;

import fr.humanbooster.max.avis.business.Editeur;

public interface EditeurService {

	Editeur ajouterEditeur(String nom);
	
	List<Editeur> recupererEditeurs();
	
	List<Editeur> recupererEditeurs(String nom);
	
	List<Editeur> recupererEditeurParGenre(String nom);
}
