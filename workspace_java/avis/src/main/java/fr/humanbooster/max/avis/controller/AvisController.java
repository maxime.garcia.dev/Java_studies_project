package fr.humanbooster.max.avis.controller;

import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardCopyOption;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.PostConstruct;
import javax.servlet.http.HttpSession;
import javax.validation.Valid;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.web.PageableDefault;
import org.springframework.stereotype.Controller;
import org.springframework.util.StringUtils;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.ModelAndView;

import fr.humanbooster.max.avis.business.Avis;
import fr.humanbooster.max.avis.business.Jeu;
import fr.humanbooster.max.avis.business.Joueur;
import fr.humanbooster.max.avis.service.AvisService;
import fr.humanbooster.max.avis.service.ClassificationService;
import fr.humanbooster.max.avis.service.EditeurService;
import fr.humanbooster.max.avis.service.GenreService;
import fr.humanbooster.max.avis.service.JeuService;
import fr.humanbooster.max.avis.service.JoueurService;
import fr.humanbooster.max.avis.service.ModeleEconomiqueService;
import fr.humanbooster.max.avis.service.PlateformeService;

@Controller
public class AvisController {

	private static final String DOSSIER_IMAGES = "src/main/webapp/WEB-INF/jeu-images/";
	private EditeurService editeurService;
	private GenreService genreService;
	private ClassificationService classificationService;
	private PlateformeService plateformeService;
	private ModeleEconomiqueService modeleEconomiqueService;
	private JeuService jeuService;
	private JoueurService joueurService;
	private AvisService avisService;
	private HttpSession httpSession;

	public AvisController(EditeurService editeurService, GenreService genreService,
			ClassificationService classificationService, PlateformeService plateformeService,
			ModeleEconomiqueService modeleEconomiqueService, JeuService jeuService, JoueurService joueurService,
			AvisService avisService, HttpSession httpSession) {
		super();
		this.editeurService = editeurService;
		this.genreService = genreService;
		this.classificationService = classificationService;
		this.plateformeService = plateformeService;
		this.modeleEconomiqueService = modeleEconomiqueService;
		this.jeuService = jeuService;
		this.joueurService = joueurService;
		this.avisService = avisService;
		this.httpSession = httpSession;
	}

	@RequestMapping(value = { "/index", "/" })
	public ModelAndView accueil(@RequestParam(defaultValue = "0", value = "ID_GENRE") Long idGenre,
			@PageableDefault(size = 10, sort = "nom", direction = Sort.Direction.ASC) Pageable pageable) {
		ModelAndView mav = new ModelAndView("index");
		Page<Jeu> jeux;
		if (idGenre == 0) {
			jeux = jeuService.recupererJeux(pageable);
		} else {
			jeux = jeuService.recupererJeux(idGenre, pageable);
		}
		mav.addObject("genres", genreService.recuperergenres());
		mav.addObject("pageJeux", jeux);
		mav.addObject("jeux", jeux.getContent());
		mav.addObject("size", jeux.getSize());
		mav.addObject("sort", jeux.getSort().iterator().next().getProperty());
		mav.addObject("page", jeux.getNumber());
		mav.addObject("nbPages", jeux.getTotalPages());
		return mav;
	}

//	@RequestMapping(value = { "/index", "/" })
//    public ModelAndView accueil(@RequestParam(name = "sortBy", required=false) String sort) {
//		//On déclare un objet de type ModeleAndView et on l'instancie
//        ModelAndView mav = new ModelAndView();
//        //On renseigne la vue associé au ModelAndView
//        //La vue est index.jsp, grâce au fichier application.properties (ligne 13) 
//        //il n'est pas nécessaire de préciser l'extension .jsp
//        mav.setViewName("index");
//        //On ajoute dans la danette dans le compartiment une petite bille (Objet) stocké en base.
//        sort = sort == null ? "id" : sort;
//        mav.addObject("jeux", jeuService.recupererJeuxSort(Sort.by(sort)));
//        //On renvoie l'objet ModelAndView mav
//        return mav;
//    }

	@GetMapping(value = { "editeurs" })
	public ModelAndView editeurs() {
		ModelAndView mav = new ModelAndView("editeurs");
		mav.addObject("editeurs", editeurService.recupererEditeurs());
		return mav;
	}

	@GetMapping(value = { "ajoutAvis" })
	public ModelAndView ajoutAvisGet() {
		ModelAndView mav = new ModelAndView("ajoutAvis");
		Avis avis = new Avis();
		Joueur joueur = (Joueur) httpSession.getAttribute("joueur");
		avis.setJoueur(joueur);
		mav.addObject("jeux", jeuService.recupererJeux());
		mav.addObject("avis", avis);
		return mav;
	}

	@PostMapping("ajoutAvis")
	public ModelAndView ajoutAvisPost(@Valid @ModelAttribute Avis avis, BindingResult result) {
		if(result.hasErrors()) {
			System.out.println(result);
			ModelAndView mav = ajoutAvisGet();
			mav.addObject("avis", avis);
			return mav;
		} 
		else {
			avis.setDateEnvoi(new Date());
			avisService.ajouterAvis(avis);
			return new ModelAndView("redirect:index");
		}
	}

	@GetMapping(value = { "inscription" })
	public ModelAndView inscriptionGet() {
		ModelAndView mav = new ModelAndView("inscription");
		Joueur joueur = new Joueur();
		mav.addObject("joueur", joueur);
		return mav;
	}

	@PostMapping("inscription")
	public ModelAndView inscriptionPost(@Valid @ModelAttribute Joueur joueur, BindingResult result) {
		if (result.hasErrors()) {
			ModelAndView mav = inscriptionGet();
			mav.addObject("joueur", joueur);
			return mav;
		}
		else {
			joueur.setEstAdministrateur(false);
			joueur.setDateInscription(new Date());
			joueurService.ajouterJoueur(joueur);
			return new ModelAndView("redirect:index");
		}	
	}

	@GetMapping(value = {"jeu"})
	public ModelAndView jeuGet(@RequestParam(name="idJeu", required = false) Long idJeu) {
		ModelAndView mav = new ModelAndView("jeu");
		Jeu jeu;
		if (idJeu == null) {
			jeu = new Jeu();
		}
		else {
			jeu = jeuService.recupererJeuParId(idJeu);
		}
		mav.addObject("jeu", jeu);
		mav.addObject("editeurs", editeurService.recupererEditeurs());
		mav.addObject("classifications", classificationService.recupererClassifications());
		mav.addObject("plateformes", plateformeService.recupererPlateformes());
		mav.addObject("modeleEconomiques", modeleEconomiqueService.recupererModeleEconomiques());
		mav.addObject("genres", genreService.recuperergenres());
		return mav;
	}

	@PostMapping("jeu")
	public ModelAndView jeuPost(@Valid @ModelAttribute Jeu jeu, BindingResult result) {
		if (result.hasErrors()) {
			System.out.println(result);
			ModelAndView mav = jeuGet(jeu.getId());
			return mav;
		} else {
			jeu = jeuService.ajouterJeu(jeu);
			ModelAndView mav = new ModelAndView("redirect:index");
			return mav;
		}
	}
	
	@GetMapping(value = { "connexion" })
	public ModelAndView connexionGet() {
		return new ModelAndView("connexion");
	}

	@PostMapping("connexion")
//	public ModelAndView connexionPost(@RequestParam("PSEUDO") String pseudo,
//			@RequestParam("MOT_DE_PASSE") String motDePasse) {
	public ModelAndView connexionPost(@RequestParam Map<String, String> map) {
		System.out.println(map);
		String pseudo = map.get("PSEUDO");
		String motDePasse = map.get("MOT_DE_PASSE");
		ModelAndView mav = new ModelAndView("redirect:index");
		Joueur joueur = joueurService.recupererJoueurParPseudoEtMotDePasse(pseudo, motDePasse);
		if (joueur == null) {
			return new ModelAndView("connexion");
		} else {
			httpSession.setAttribute("joueur", joueur);
			return mav;
		}
	}

	@GetMapping("deconnexion")
	public ModelAndView deconnexion() {
		httpSession.invalidate();
		return new ModelAndView("redirect:index");
	}

	@PostMapping(value = "/supprimer")
    public ModelAndView supprimerGet(@RequestParam("ID_JEU") Long idJeu) {
        for (Avis avis : avisService.recupererDesAvisParJeuId(idJeu)) {
            avisService.supprimerAvis(avis);
        }
        jeuService.supprimerJeu(idJeu);
        ModelAndView mav = new ModelAndView("redirect:index");
        return mav;
    }
	@GetMapping("image")
    public ModelAndView imageGet(@RequestParam("id") Long id) {
        ModelAndView mav = new ModelAndView("image");
        
        mav.addObject("jeu", jeuService.recupererJeuParId(id));

        return mav;
    }
    
    @PostMapping("image")
    public ModelAndView imagePost(@RequestParam("id") Long id, @RequestParam("image") MultipartFile multipartFile) throws IOException {
        
        Jeu jeu = jeuService.recupererJeuParId(id);
        
        String fileName = StringUtils.cleanPath(multipartFile.getOriginalFilename());
        
        jeu.setImage(fileName);
        
        jeuService.ajouterJeu(jeu);
        
        String uploadDir = DOSSIER_IMAGES + jeu.getId();
        
        saveFile(uploadDir, fileName, multipartFile);
        
        return new ModelAndView("redirect:index");
    }
    
    private static void saveFile(String uploadDir, String fileName,
            MultipartFile multipartFile) throws IOException {
        Path uploadPath = Paths.get(uploadDir);
         
        if (!Files.exists(uploadPath)) {
            Files.createDirectories(uploadPath);
        }
         
        try (InputStream inputStream = multipartFile.getInputStream()) {
            Path filePath = uploadPath.resolve(fileName);
            Files.copy(inputStream, filePath, StandardCopyOption.REPLACE_EXISTING);
        } catch (IOException ioe) {        
            throw new IOException("Could not save image file: " + fileName, ioe);
        }      
    }

    @GetMapping("/mapJoueurs")
    public void mapGet(){
    	Map<Long, Joueur> map = new HashMap<>();
    	List<Joueur> joueurs = joueurService.recupererJoueurs();
    	for (Joueur joueur : joueurs) {
    		map.put(joueur.getId(), joueur);
		}
    	for (Long cle : map.keySet()) {
    		System.out.println(cle+":"+map.get(cle));
		} 
    }
//	@PostConstruct
//	public void ajouterDonneInitiale() {
//		Editeur editeur = editeurService.ajouterEditeur("Riot Games");
//	    Genre genre = genreService.ajouterGenre("MOBA");
//		Classification classification = classificationService.ajouterClassification("PEGI 10");
//		plateformeService.ajouterPlateforme("PC MASTER RACE");
//		ModeleEconomique modeleEconomique = modeleEconomiqueService.ajouterModeleEconomique("Free-To-Play");
//		List<Plateforme> plateformes = plateformeService.recupererPlateformes();
//		Date dateSortie = new Date();
//		try {
//			dateSortie = simpleDateFormat.parse("27/10/2009");
//		} catch (ParseException e) {
//			e.printStackTrace();
//		}
//		jeuService.ajouterJeu(new Jeu("League Of Legends", dateSortie, modeleEconomique, classification, genre, editeur, plateformes));
//	}
	@PostConstruct
	public void afficherDonnee() {
//		List<Jeu> jeux = jeuService.recupererJeux();
//		for (Jeu jeu : jeux) {
//			System.out.println(jeu.getNom());
//		}
//		System.out.println(editeurService.recupererEditeurs("Arts"));
//		for (Editeur editeur : editeurService.recupererEditeurs("Arts")) {
//			System.out.println(editeur.getNom());
//			editeur.setJeux(jeuService.recupererJeux(editeur));
//			for (Jeu jeu : editeur.getJeux()) {
//				System.out.println(jeu.getNom());
//			}
//		}
//		List<Jeu> jeux = jeuService.recupererJeux(genreService.recupererGenre("rpg"));
//		List<Editeur> editeurs = editeurService.recupererEditeurParGenre("rpg");
//		for (Jeu jeu : jeux) { 
//			if (!editeurs.contains(jeu.getEditeur())) {
//				editeurs.add(jeu.getEditeur());
//			}
//		}
//		for (Editeur editeur : editeurs) {
//			System.out.println(editeur.getNom());
//		}
//		Calendar c = Calendar.getInstance();
//		c.set(Calendar.YEAR, 2011);
//		Date dateDebut = c.getTime();
//		c.set(Calendar.YEAR, 2020);
//		Date dateFin = c.getTime();
//		for (Jeu jeu : jeuService.recupererJeux(dateDebut, dateFin)) {
//			System.out.println(jeu.getNom() + " " + jeu.getDateSortie());
//		}
	}
}
