package fr.humanbooster.max.avis.service.impl;

import java.util.List;

import org.springframework.stereotype.Service;

import fr.humanbooster.max.avis.business.ModeleEconomique;
import fr.humanbooster.max.avis.dao.ModeleEconomiqueDao;
import fr.humanbooster.max.avis.service.ModeleEconomiqueService;

@Service
public class ModeleEconomiqueServiceImpl implements ModeleEconomiqueService {

	private ModeleEconomiqueDao modeleEconomiqueDao;
	
	public ModeleEconomiqueServiceImpl(ModeleEconomiqueDao modeleEconomiqueDao) {
		super();
		this.modeleEconomiqueDao = modeleEconomiqueDao;
	}
	@Override
	public ModeleEconomique ajouterModeleEconomique(String nom) {
		return modeleEconomiqueDao.save(new ModeleEconomique(nom));
	}
	@Override
	public List<ModeleEconomique> recupererModeleEconomiques() {
		return modeleEconomiqueDao.findAll();
	}

}
