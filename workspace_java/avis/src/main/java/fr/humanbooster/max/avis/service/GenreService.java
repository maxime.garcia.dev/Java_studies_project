package fr.humanbooster.max.avis.service;

import java.util.List;

import fr.humanbooster.max.avis.business.Genre;

public interface GenreService {

	Genre ajouterGenre(String nom);
	
	Genre recupererGenre(String nom);
	
	List<Genre> recuperergenres();
	
}
