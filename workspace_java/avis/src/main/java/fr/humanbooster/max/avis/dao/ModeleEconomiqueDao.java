package fr.humanbooster.max.avis.dao;

import org.springframework.data.jpa.repository.JpaRepository;

import fr.humanbooster.max.avis.business.ModeleEconomique;

public interface ModeleEconomiqueDao extends JpaRepository<ModeleEconomique, Long> {

}
