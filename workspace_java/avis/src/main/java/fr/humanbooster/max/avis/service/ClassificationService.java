package fr.humanbooster.max.avis.service;

import java.util.List;

import fr.humanbooster.max.avis.business.Classification;

public interface ClassificationService {
	
	Classification ajouterClassification(String nom);
	
	List<Classification> recupererClassifications();
}
