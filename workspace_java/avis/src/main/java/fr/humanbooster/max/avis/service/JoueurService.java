package fr.humanbooster.max.avis.service;

import java.util.List;

import fr.humanbooster.max.avis.business.Joueur;

public interface JoueurService {
	
	Joueur ajouterJoueur(Joueur joueur);
	
	Joueur recupererJoueurParPseudoEtMotDePasse(String pseudo, String motDePasse);
	
	Joueur recupererJoueurParId(Long id);
	
	List<Joueur> recupererJoueurs();
}
