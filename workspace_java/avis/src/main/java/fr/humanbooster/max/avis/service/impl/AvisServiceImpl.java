package fr.humanbooster.max.avis.service.impl;

import java.util.List;

import org.springframework.stereotype.Service;

import fr.humanbooster.max.avis.business.Avis;
import fr.humanbooster.max.avis.business.Joueur;
import fr.humanbooster.max.avis.dao.AvisDao;
import fr.humanbooster.max.avis.service.AvisService;

@Service
public class AvisServiceImpl implements AvisService {

	private AvisDao avisDao;
	
	
	public AvisServiceImpl(AvisDao avisDao) {
		super();
		this.avisDao = avisDao;
	}

	@Override
	public Avis ajouterAvis(Avis avis) {		
		return avisDao.save(avis);
	}

	@Override
	public List<Avis> recupererDesAvisParJeuId(Long idJeu) {
		return avisDao.findAvisByJeuId(idJeu);
	}

	@Override
	public void supprimerAvis(Avis avis) {
		avisDao.delete(avis);;
	}

	@Override
	public Avis recupererAvisParId(Long id) {
		return avisDao.findById(id).orElse(null);
	}

	@Override
	public Avis recupererDernierAvisParJoueur(Joueur joueur) {
		return avisDao.findFirstByJoueurOrderByDateEnvoiDesc(joueur);
	}

}
