package fr.humanbooster.max.avis.dao;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import fr.humanbooster.max.avis.business.Avis;
import fr.humanbooster.max.avis.business.Joueur;

public interface AvisDao extends JpaRepository<Avis, Long> {

	List<Avis> findAvisByJeuId(Long idJeu);

	Avis findFirstByJoueurOrderByDateEnvoiDesc(Joueur joueur);
}
