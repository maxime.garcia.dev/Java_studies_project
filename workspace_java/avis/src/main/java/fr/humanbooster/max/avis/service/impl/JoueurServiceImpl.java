package fr.humanbooster.max.avis.service.impl;

import java.util.List;

import org.springframework.stereotype.Service;

import fr.humanbooster.max.avis.business.Joueur;
import fr.humanbooster.max.avis.dao.JoueurDao;
import fr.humanbooster.max.avis.service.JoueurService;

@Service
public class JoueurServiceImpl implements JoueurService {

	private JoueurDao joueurDao;
	
	public JoueurServiceImpl(JoueurDao joueurDao) {
		super();
		this.joueurDao = joueurDao;
	}

	@Override
	public Joueur ajouterJoueur(Joueur joueur) {
		return joueurDao.save(joueur);
	}

	@Override
	public Joueur recupererJoueurParPseudoEtMotDePasse(String pseudo, String motDePasse) {
		return joueurDao.findByPseudoAndMotDePasse(pseudo, motDePasse);
	}

	@Override
	public Joueur recupererJoueurParId(Long id) {
		return joueurDao.findById(id).orElse(null);
	}

	@Override
	public List<Joueur> recupererJoueurs() {
		return joueurDao.findAll();
	}
}