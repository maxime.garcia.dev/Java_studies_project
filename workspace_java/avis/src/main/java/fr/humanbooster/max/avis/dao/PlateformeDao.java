package fr.humanbooster.max.avis.dao;

import org.springframework.data.jpa.repository.JpaRepository;

import fr.humanbooster.max.avis.business.Plateforme;

public interface PlateformeDao extends JpaRepository<Plateforme, Long> {

}
