package fr.humanbooster.max.avis.service.impl;

import java.util.Date;
import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import fr.humanbooster.max.avis.business.Editeur;
import fr.humanbooster.max.avis.business.Genre;
import fr.humanbooster.max.avis.business.Jeu;
import fr.humanbooster.max.avis.dao.GenreDao;
import fr.humanbooster.max.avis.dao.JeuDao;
import fr.humanbooster.max.avis.service.JeuService;

@Service
public class JeuServiceImpl implements JeuService {

	private JeuDao jeuDao;	
	private GenreDao genreDao;
	
	public JeuServiceImpl(JeuDao jeuDao, GenreDao genreDao) {
		super();
		this.jeuDao = jeuDao;
		this.genreDao = genreDao;
	}
	
	@Override
	public Jeu ajouterJeu(Jeu jeu) {
		return jeuDao.save(jeu);
	}

	@Override
	public List<Jeu> recupererJeux() {
		return jeuDao.findAll();
		
	}

	@Override
	public List<Jeu> recupererJeux(Editeur editeur) {
		return jeuDao.findByEditeur(editeur);
	}

	@Override
	public List<Jeu> recupererJeux(Genre genre) {
		return jeuDao.findByGenre(genre);
	}

	@Override
	public List<Jeu> recupererJeux(Date dateDebut, Date dateFin) {
		return jeuDao.findByDateSortieBetween(dateDebut, dateFin);
	}

	@Override
	public List<Jeu> recupererJeux(Sort sort) {
		return jeuDao.findAll(sort);
	}

	@Override
	public Page<Jeu> recupererJeux(Pageable pageable) {
		return jeuDao.findAll(pageable);
	}

	@Override
	public Page<Jeu> recupererJeux(Long idGenre, Pageable pageable) {
		Genre genre = genreDao.getOne(idGenre);
		return jeuDao.findByGenre(genre, pageable);
	}

	@Override
	public Jeu recupererJeuParId(Long id) {
		return jeuDao.findById(id).orElse(null);
	}

	@Override
	public void supprimerJeu(Long idJeu) {
		jeuDao.deleteById(idJeu);		
	}

	@Override
	public Jeu recupererJeuAvecMeilleurMoyenne() {
		 List<Jeu> jeux = jeuDao.findFirstGroupByJeuOrderByMoyenneNoteAndDateSortie();
	        if (!jeux.isEmpty()) {
	            return jeux.get(0);
	        }
	        return null;
	}
}
