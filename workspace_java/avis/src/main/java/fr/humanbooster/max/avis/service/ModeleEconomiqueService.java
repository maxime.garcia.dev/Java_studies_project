package fr.humanbooster.max.avis.service;

import java.util.List;

import fr.humanbooster.max.avis.business.ModeleEconomique;

public interface ModeleEconomiqueService {

	ModeleEconomique ajouterModeleEconomique(String nom);
	
	List<ModeleEconomique> recupererModeleEconomiques();
}
