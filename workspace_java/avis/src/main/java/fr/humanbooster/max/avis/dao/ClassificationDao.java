package fr.humanbooster.max.avis.dao;

import org.springframework.data.jpa.repository.JpaRepository;

import fr.humanbooster.max.avis.business.Classification;

public interface ClassificationDao extends JpaRepository<Classification, Long> {

}
