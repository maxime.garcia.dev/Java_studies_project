package fr.humanbooster.max.avis.service;

import java.util.List;

import fr.humanbooster.max.avis.business.Avis;
import fr.humanbooster.max.avis.business.Joueur;

public interface AvisService {

	Avis ajouterAvis(Avis avis);
	
	Avis recupererAvisParId(Long id);

	List<Avis> recupererDesAvisParJeuId(Long idJeu);

	void supprimerAvis(Avis avis);

	Avis recupererDernierAvisParJoueur(Joueur joueur);
}
