package fr.humanbooster.max.avis.business;

import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import org.hibernate.validator.constraints.Range;

import com.fasterxml.jackson.annotation.JsonIgnore;

@Entity
public class Avis {
	
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private Long id;
	//@Temporal(TemporalType.DATE)
	private Date dateEnvoi;
	@Range(min=0, max=20, message="Veuillez entrer une note entre 0 et 20.")
	private Float note;
	@NotBlank(message="Veuillez entrer une description pour votre avis.")
	@Size(min=3, message="Veuillez entrer un avis d'au moins 3 caractères.")
	private String description;
	@ManyToOne(fetch= FetchType.EAGER)
	@NotNull
	private Jeu jeu;
	@ManyToOne
	@NotNull
	private Joueur joueur;
	
	public Avis() {
		// TODO Auto-generated constructor stub
	}

	public Avis(Date dateEnvoi,
			@Range(min = 0, max = 20, message = "Veuillez entrer une note entre 0 et 20.") Float note,
			@NotBlank(message = "Veuillez entrer une description pour votre avis.") @Size(min = 3, message = "Veuillez entrer un avis d'au moins 3 caractères.") String description,
			@NotNull Jeu jeu, @NotNull Joueur joueur) {
		super();
		this.dateEnvoi = dateEnvoi;
		this.note = note;
		this.description = description;
		this.jeu = jeu;
		this.joueur = joueur;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Date getDateEnvoi() {
		return dateEnvoi;
	}

	public void setDateEnvoi(Date dateEnvoi) {
		this.dateEnvoi = dateEnvoi;
	}

	public Float getNote() {
		return note;
	}

	public void setNote(Float note) {
		this.note = note;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public Jeu getJeu() {
		return jeu;
	}

	public void setJeu(Jeu jeu) {
		this.jeu = jeu;
	}

	public Joueur getJoueur() {
		return joueur;
	}

	public void setJoueur(Joueur joueur) {
		this.joueur = joueur;
	}

	@Override
	public String toString() {
		return "Avis [id=" + id + ", dateEnvoi=" + dateEnvoi + ", note=" + note + ", description=" + description
				+ ", jeu=" + jeu + ", joueur=" + joueur + "]";
	}
	
}
