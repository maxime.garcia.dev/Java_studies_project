package fr.humanbooster.max.avis.service;

import java.util.Date;
import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;

import fr.humanbooster.max.avis.business.Classification;
import fr.humanbooster.max.avis.business.Editeur;
import fr.humanbooster.max.avis.business.Genre;
import fr.humanbooster.max.avis.business.Jeu;
import fr.humanbooster.max.avis.business.ModeleEconomique;
import fr.humanbooster.max.avis.business.Plateforme;

public interface JeuService {

	Jeu ajouterJeu(Jeu jeu);
	
	Jeu recupererJeuParId(Long id);
	
	List<Jeu> recupererJeux();

	List<Jeu> recupererJeux(Editeur editeur);
	
	List<Jeu> recupererJeux(Genre genre);
	
	List<Jeu> recupererJeux(Date dateDebut, Date dateFin);

	List<Jeu> recupererJeux(Sort sort);
	
	Page<Jeu> recupererJeux(Pageable pageable);

	Page<Jeu> recupererJeux(Long idGenre, Pageable pageable);

	void supprimerJeu(Long idJeu);

	Jeu recupererJeuAvecMeilleurMoyenne();

}
