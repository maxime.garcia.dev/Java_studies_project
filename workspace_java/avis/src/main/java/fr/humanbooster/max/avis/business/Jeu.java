package fr.humanbooster.max.avis.business;

import java.util.Date;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Lob;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Past;

import org.springframework.data.annotation.Transient;
import org.springframework.format.annotation.DateTimeFormat;

import com.fasterxml.jackson.annotation.JsonIgnore;

@Entity
public class Jeu {
	
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private Long id;
	@NotBlank(message="Veuillez renseigner le nom du jeu")
	private String nom;
	@Lob
	private String description;
	private String image;
	@Temporal(TemporalType.DATE)
	@Past
	@DateTimeFormat(pattern="yyyy-MM-dd") 
	// Impose un format d'envoi à partir des formulaire d'ajout de jeu HTML
    // Evite l'écriture de la méhtode initBinder dans le controller
	private Date dateSortie;
	@JsonIgnore
	@OneToMany(mappedBy="jeu", cascade=CascadeType.REMOVE)
	private List<Avis> listeAvis;
	@ManyToOne
	@NotNull(message="Veuillez renseigner le modèle économique du jeu")
	private ModeleEconomique modeleEconomique;
	@ManyToOne
	@NotNull(message="Veuillez renseigner la classification du jeu")
	private Classification classification;
	@ManyToOne
	@NotNull(message="Veuillez renseigner le genre auquel le jeu appartient")
	private Genre genre;
	@ManyToOne
	@NotNull(message="Veuillez renseigner l'éditeur du jeu")
	private Editeur editeur;
	@JsonIgnore
	@ManyToMany
	@NotEmpty(message="Veuillez sélectionner au moins une plateforme")
	private List<Plateforme> plateformes;
	
	public Jeu() {
		// TODO Auto-generated constructor stub
	}

	
	public Jeu(String nom, Date dateSortie,ModeleEconomique modeleEconomique,Classification classification,Genre genre,Editeur editeur, List<Plateforme> plateformes) {
		this();
		this.nom = nom;
		this.dateSortie = dateSortie;
		this.modeleEconomique = modeleEconomique;
		this.classification = classification;
		this.genre = genre;
		this.editeur = editeur;
		this.plateformes = plateformes;
	}

	public Jeu(String nom, Date dateSortie,ModeleEconomique modeleEconomique,Classification classification,Genre genre,Editeur editeur, List<Plateforme> plateformes, String description) {
		this(nom, dateSortie, modeleEconomique, classification, genre, editeur,  plateformes);
		this.description = description;		
	}
	
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getNom() {
		return nom;
	}

	public void setNom(String nom) {
		this.nom = nom;
	}

	public String getDescription() {
		return description;
	}


	public void setDescription(String description) {
		this.description = description;
	}


	public String getImage() {
		return image;
	}


	public void setImage(String image) {
		this.image = image;
	}


	public Date getDateSortie() {
		return dateSortie;
	}


	public void setDateSortie(Date dateSortie) {
		this.dateSortie = dateSortie;
	}

	public List<Avis> getListeAvis() {
		return listeAvis;
	}

	public void setListeAvis(List<Avis> listeAvis) {
		this.listeAvis = listeAvis;
	}

	public ModeleEconomique getModeleEconomique() {
		return modeleEconomique;
	}

	public void setModeleEconomique(ModeleEconomique modeleEconomique) {
		this.modeleEconomique = modeleEconomique;
	}

	public Classification getClassification() {
		return classification;
	}

	public void setClassification(Classification classification) {
		this.classification = classification;
	}

	public Genre getGenre() {
		return genre;
	}

	public void setGenre(Genre genre) {
		this.genre = genre;
	}

	public Editeur getEditeur() {
		return editeur;
	}

	public void setEditeur(Editeur editeur) {
		this.editeur = editeur;
	}

	public List<Plateforme> getPlateformes() {
		return plateformes;
	}

	public void setPlateformes(List<Plateforme> plateformes) {
		this.plateformes = plateformes;
	}

	@Transient
    public String getCheminImage() {
        if (image == null || id==null) return null;
        return "jeu-images/" + id + "/" + image;
    }
	
	@Override
	public String toString() {
		return "Jeu [id=" + id + ", nom=" + nom + ", dateSortie=" + dateSortie + ", modeleEconomique="
				+ modeleEconomique + ", classification=" + classification + ", genre=" + genre + ", editeur=" + editeur
				+ "]";
	}
}
