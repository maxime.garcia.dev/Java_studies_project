<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Insert title here</title>
</head>
<body>
	<jsp:include page="entete.jsp"></jsp:include>
	<h1>Bienvenue</h1>
	<h2>Voici la liste des jeux</h2>
	<form action="?sort=${sort}&page=0" method="post">
		<label for="size">Nombre de lignes : </label><input
			style="width: 32px" type="number" id="size" name="size"
			value="${size}" /> <input type="submit" value="Changer" />
	</form>
	<br>
	<form action="?sort=${sort}&size=${size}&page=0" method="post">
		<select name="ID_GENRE">
			<option value="0">Tous</option>
			<c:forEach items="${genres}" var="genre">
				<option value="${genre.id}">${genre.nom}</option>
			</c:forEach>
		</select>
		<input type="submit" value="filtrer"/>
	</form>
	<a href="?sort=${sort}&size=${size}&page=0"> &#x23EE; </a>
	<c:if test="${page != 0}">
		<a href="?sort=${sort}&size=${size}&page=${page - 1}"> &#x23EA; </a>
	</c:if>
	<c:if test="${page == 0}">&#x23EA;</c:if>
	<c:if test="${nbPages <= 5 }">
		<c:forEach var="i" begin="1" end="${nbPages}">
			<c:if test="${page != i - 1}">
				<a href="?sort=${sort}&size=${size}&page=${i - 1}"> ${i} </a>
			</c:if>
			<c:if test="${page == i - 1}"> ${i} </c:if>
		</c:forEach>
	</c:if>
	<c:if test="${nbPages > 5 }">
		<c:forEach var="i" begin="${page - 1 <= 1 ? 1 : page - 1 }"
			end="${page + 3 >= nbPages ? nbPages : page + 3 }">
			<c:if test="${page != i - 1}">
				<a href="?sort=${sort}&size=${size}&page=${i - 1}"> ${i} </a>
			</c:if>
			<c:if test="${page == i - 1}"> ${i} </c:if>
		</c:forEach>
	</c:if>
	<c:if test="${page != nbPages - 1}">
		<a href="?sort=${sort}&size=${size}&page=${page + 1}">&#x23E9;</a>
	</c:if>
	<c:if test="${page == nbPages - 1}">&#x23E9;</c:if>
	<a href="?sort=${sort}&size=${size}&page=${nbPages - 1}"> &#x23ED;
	</a>
	<br>
	<table>
		<thead>
			<tr>
				<th><a href="?sort=nom&size=${size}&page=0">Nom</a></th>
				<th>Plateforme(s)</th>
				<th><a href="?sort=genre.nom&size=${size}&page=0">Genre</a></th>
				<th><a href="?sort=classification.nom&size=${size}&page=0">Classification</a></th>
				<th><a href="?sort=dateSortie&size=${size}&page=0">Date de
						sortie</a></th>
				<th><a href="?sort=modeleEconomique.nom&size=${size}&page=0">Modèle
						économique</a></th>
				<c:if test="${sessionScope.joueur.estAdministrateur eq true}"><th>Action</th></c:if>
				<c:if test="${sessionScope.joueur.estAdministrateur eq true}"><th>Action</th></c:if>
				<c:if test="${sessionScope.joueur.estAdministrateur eq true}"><th>Action</th></c:if>
			</tr>
		</thead>
		<tbody>
			<c:forEach items="${jeux}" var="jeu">
				<tr>
					<td>${jeu.nom}</td>
					<td><c:forEach items="${jeu.plateformes}" var="plateforme">${plateforme.nom}<br>
						</c:forEach></td>
					<td>${jeu.genre.nom}</td>
					<td>${jeu.classification.nom}</td>
					<td>${jeu.dateSortie}</td>
					<td>${jeu.modeleEconomique.nom}</td>
					<c:if test="${sessionScope.joueur.estAdministrateur eq true}"><td><a href="jeu?idJeu=${jeu.id}">Modifier</a></td></c:if>
					<c:if test="${sessionScope.joueur.estAdministrateur eq true}">
						<td>
							<form action="supprimer" method="post" onsubmit="return confirm('Etes-vous sur de vouloir supprimer ${jeu.nom} ?') ? true : false;">
                                <input type="hidden" name="ID_JEU" value="${jeu.id}" />
                                <input type="submit" value="Supprimer" />
                            </form>
                        </td>
                    </c:if>
                    <c:if test="${sessionScope.joueur.estAdministrateur eq true}"><td><a href="image?id=${jeu.id}">telecharger une image</a></td></c:if>
				</tr>
			</c:forEach>
		</tbody>
	</table>
	<br>
	<a href="editeurs">Liste des éditeurs</a>
	<c:if test="${sessionScope.joueur ne null}"><a href="ajoutAvis">Ajouter un avis</a></c:if>
	<c:if test="${sessionScope.joueur.estAdministrateur eq true}"><a href="jeu">Ajouter un jeu</a></c:if>
</body>
</html>