<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Insert title here</title>
<style>
.erreur {
	color: red;
}
</style>
</head>
<body>
	<h1>Ajouter un jeu</h1>
	<form:form modelAttribute="jeu" action="jeu" method="post">
	<form:hidden path="id"/>
		<table>
			<tr>
				<td>Nom</td>
				<td><form:input path="nom" /></td>
				<td><form:errors path="nom" cssClass="erreur"></form:errors></td>
			</tr>
			<tr>
				<td>Editeur</td>
				<td><form:select path="editeur">
						<form:option value="">Editeurs</form:option>
						<form:options items="${editeurs}" itemValue="id" itemLabel="nom" />
					</form:select></td>
					<td><form:errors path="editeur" cssClass="erreur"></form:errors></td>
			</tr>
			<tr>
				<td>Classification</td>
				<td><form:select path="classification">
						<form:option value="">Classifications</form:option>
						<form:options items="${classifications}" itemValue="id"
							itemLabel="nom" />
					</form:select></td>
					<td><form:errors path="classification" cssClass="erreur"></form:errors></td>
					
			</tr>
			<tr>
				<td>Plateforme(s)</td>
				<td><form:select path="plateformes" multiple="true">
						<form:option value="">Plateformes</form:option>
						<form:options items="${plateformes}" itemValue="id"
							itemLabel="nom" />
					</form:select></td>
					<td><form:errors path="plateformes" cssClass="erreur"></form:errors></td>
					
			</tr>
			<tr>
				<td>Modèle Economique</td>
				<td><form:select path="modeleEconomique">
						<form:option value="">Modèle Economique:</form:option>
						<form:options items="${modeleEconomiques}" itemValue="id"
							itemLabel="nom" />
					</form:select></td>
					<td><form:errors path="modeleEconomique" cssClass="erreur"></form:errors></td>
			</tr>
			<tr>
				<td>Genre</td>
				<td><form:select path="genre">
						<form:option value="">Genre</form:option>
						<form:options items="${genres}" itemValue="id" itemLabel="nom" />
					</form:select> </td>
					<td><form:errors path="genre" cssClass="erreur"></form:errors></td>
			</tr>
			<tr>
				<td>Date de Sortie</td>
				<td><form:input type="date" path="dateSortie"/></td>
				<td><form:errors path="dateSortie" cssClass="erreur"></form:errors></td>
			</tr>
			<tr>
				<td><form:button>Ajouter</form:button></td>
			</tr>
		</table>
	</form:form>
	<br />
	<a href="index">Retour à l'accueil</a>
</body>
</html>