<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Insert title here</title>
<style>
.erreur {
	color: red;
}
</style>
</head>
<body>
	<h1>Ajout d'un avis</h1>
	<form:form modelAttribute="avis" action="ajoutAvis" method="post">
		<form:hidden path="joueur"/>
		<table>
			<tr>
				<td>Jeu</td>
				<td>
					<form:select path="jeu">
						<form:option value="">Veuillez choisir</form:option>
						<form:options items="${jeux}" itemValue="id" itemLabel="nom"/>
					</form:select>
				</td>
				<td><form:errors path="jeu" cssClass="erreur"></form:errors></td>
			</tr>
			<tr>
				<td>Description</td>
				<td><form:textarea path="description"/></td>
				<td><form:errors path="description"></form:errors></td>
			</tr>
			<tr>
				<td>Note</td>
				<td>
					<form:select path="note">
						<form:option value="-1">Veuillez choisir</form:option>
						<c:forEach var="i" begin="0" end="20" step="1">
							<form:option value="${i}">${i}</form:option>
						</c:forEach>
					</form:select>
				</td>
				<td><form:errors path="note" cssClass="erreur"></form:errors></td>
			</tr>
			<tr>
				<td><form:button>Ajouter</form:button></td>
			</tr>
		</table>
	</form:form>
	<a href="index">Retour à l'accueil</a>
</body>
</html>