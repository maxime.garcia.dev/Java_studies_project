<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Insert title here</title>
</head>
	<body>
		<c:choose>
			<c:when test="${sessionScope.joueur eq null}">
				<p><a href="connexion">Connexion</a>  <a href="inscription">Inscription</a></p>
			</c:when>
			<c:when test="${sessionScope.joueur ne null }">
				<p>Bienvenue ${sessionScope.joueur.pseudo}<a href="deconnexion">Déconnexion</a></p>
				<br>
			</c:when>
		</c:choose>
	</body>
</html>