<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Insert title here</title>
<style>
.erreur {
	color: red;
}
</style>
</head>
	<body>
		<h1>Inscription</h1>
		<form:form modelAttribute="joueur" action="inscription" method="post">
			<table>
				<tr>
					<td>Veuillez choisir un Pseudonyme</td>
					<td><form:input path="pseudo"/></td>
					<td><form:errors path="pseudo" cssClass="erreur"></form:errors></td>
				</tr>
				<tr>
					<td>Veuillez choisir un Mot de Passe</td>
					<td><form:password path="motDePasse"/></td>
					<td><form:errors path="motDePasse" cssClass="erreur"></form:errors></td>
				</tr>
				<tr>
					<td><form:button>Inscription</form:button></td>
				</tr>
			</table>
		</form:form>
		<a href="index">Retour a l'accueil</a>
	</body>
</html>