package fr.humanbooster.palindrome;

import java.util.Scanner;

/**
 * cette classe demande � l'utilisateur de saisir un mot et lui indique si le mot saisie est un palindrome
 * exemple de plalindrome: bob, elle, kayak, radar, non, etc.
 * @author HB
 *
 */
public class App {

	public static void main(String[] args) {
		String mot = new String();
		Scanner sc = new Scanner(System.in);
		int length = 0;
		int i = 0;
		int j = 0;
		boolean isPalin = true;
		
		System.out.println("Veuillez entrer un mot:");
		mot = sc.nextLine();
		mot = mot.toLowerCase();
		length = mot.length();
		j = length - 1;
		while((i < length / 2) && (j > length / 2) && (isPalin == true)){
			if (mot.charAt(i) == mot.charAt(j)) {
				i++;
				j--;
			}
			else {
				isPalin = false;
			}
		}
		if (isPalin == true) {
			System.out.println(mot + " est un palindrome");
		}
		else {
			System.out.println(mot + " n'est pas un palindrome");
		}
		sc.close();
	}

}
