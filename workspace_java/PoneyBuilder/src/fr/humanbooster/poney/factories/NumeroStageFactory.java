package fr.humanbooster.poney.factories;

import fr.humanbooster.poney_club.business.NumeroStage;
import fr.humanbooster.poney_club.exception.ManquePoneysException;

/**
 * Cette classe est un singleton et une factory
 * Le patron singleton mis en oeuvre dans cette classe nous garantie
 * qu'il n'existera qu'un seul objet de type NumeroStageFactory
 * elle fabrique des objets Numero de stage
 */
public class NumeroStageFactory {

    private String baseNomStage = "HBG-13-";
    private int num = 0;

    // pour rendre une classe singleton
    // on rend le constructeur priv�
    private NumeroStageFactory() {
    }

    // il y a un objet de type StageFactory dans la classe StageFactory
    // en private static
    private static NumeroStageFactory numeroStageFactory;

    // on ajoute une m�thode static getInstance
    public static NumeroStageFactory getInstance() {
        // on test la pr�sence d'un objet NumeroStageFactory
        if (numeroStageFactory == null) {
            // si il n'y en a pas, on en cr�e un
            // Le bloc synchronizaed garantit qu'un seul processus � la fois pourra ex�cuter les instructions de ce bloc
            synchronized (NumeroStageFactory.class) {
                numeroStageFactory = new NumeroStageFactory();
                System.out.println("creation de l'objet unique");
            }
        }
        return numeroStageFactory;
    }

    public NumeroStage createNumeroStage() throws ManquePoneysException {

        try {
            NumeroStage numeroStage = new NumeroStage(baseNomStage + ++num);
            return numeroStage;
        } catch (IndexOutOfBoundsException e) {
            throw new ManquePoneysException("Tous les poneys sont malheureusement occup�s ce jour!");
        }
    }

    @Override
    public String toString() {
        return "NumeroStageFactory " + baseNomStage + num + "]";
    }
    
}