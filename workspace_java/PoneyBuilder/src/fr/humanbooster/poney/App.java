package fr.humanbooster.poney;

import fr.humanbooster.poney.builder.PoneyBuilder;
import fr.humanbooster.poney.builder.impl.PoneyBuilderImpl;
import fr.humanbooster.poney.business.Poney;

public class App {

	public static void main(String[] args) {
		PoneyBuilder poneyBuilderImpl = new PoneyBuilderImpl();
		Poney poney = poneyBuilderImpl.ajouterBride("rose").ajouterBride("bleu").ajouterCouvreRein("rose").ajouterRenes("noirs").ajouterSelle("cuir de buffle").buildPoney();
		System.out.println(poney);
	}
}
