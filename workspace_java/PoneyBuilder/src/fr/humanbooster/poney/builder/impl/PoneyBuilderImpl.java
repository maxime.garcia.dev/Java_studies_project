package fr.humanbooster.poney.builder.impl;

import java.util.ArrayList;
import java.util.List;

import fr.humanbooster.poney.builder.PoneyBuilder;
import fr.humanbooster.poney.business.Bride;
import fr.humanbooster.poney.business.CouvreRein;
import fr.humanbooster.poney.business.Equipement;
import fr.humanbooster.poney.business.Poney;
import fr.humanbooster.poney.business.Selle;

public class PoneyBuilderImpl implements PoneyBuilder {
	// Attributs, entre autre l'inventaire de l'�curie en d�but de journ�e.
	private static List<Poney> poneysDisponible = new ArrayList<>();
	private static List<Equipement> equipements = new ArrayList<>();
	private Poney poney = null;

	public PoneyBuilderImpl() {
		// ajout des poneys et des �quipements
		poneysDisponible.add(new Poney("Pinkie Pie"));
		poneysDisponible.add(new Poney("Twilight Sparkle"));
		poneysDisponible.add(new Poney("Fluttershy"));
		poneysDisponible.add(new Poney("Rarity"));
		poneysDisponible.add(new Poney("Rainbow Dash"));
		poneysDisponible.add(new Poney("Applejack"));

		/**
		 * m�thode alternative possible: gestions des stocks avec un int au sein des
		 * classes d'h�ritages avec un int stock que l'on incr�mente et d�cr�mente.
		 */

		for (int i = 0; i < 2; i++) {
			equipements.add(new CouvreRein("Couvre reins", "violet"));
		}
		for (int i = 0; i < 2; i++) {
			equipements.add(new CouvreRein("Couvre reins", "rose"));
		}
		for (int i = 0; i < 2; i++) {
			equipements.add(new CouvreRein("Couvre reins", "bleu"));
		}
		for (int i = 0; i < 4; i++) {
			equipements.add(new Selle("Selle", "cuir de buffle"));
		}
		for (int i = 0; i < 2; i++) {
			equipements.add(new Selle("Selle", "mati�re synth�tique"));
		}
		for (int i = 0; i < 3; i++) {
			equipements.add(new Bride("Bride", "rose"));
		}
		for (int i = 0; i < 3; i++) {
			equipements.add(new Bride("Bride", "bleu"));
		}
		for (int i = 0; i < 3; i++) {
			equipements.add(new Bride("R�nes", "roses"));
		}
		for (int i = 0; i < 3; i++) {
			equipements.add(new Bride("R�nes", "noirs"));
		}

		poney = choisirUnPoney();
	}

	public static List<Poney> getPoneysDisponible() {
		return poneysDisponible;
	}

	public static void setPoneysDisponible(List<Poney> poneysDisponible) {
		PoneyBuilderImpl.poneysDisponible = poneysDisponible;
	}

	public static List<Equipement> getEquipements() {
		return equipements;
	}

	public static void setEquipements(List<Equipement> equipements) {
		PoneyBuilderImpl.equipements = equipements;
	}

	public Poney getPoney() {
		return poney;
	}

	public void setPoney(Poney poney) {
		this.poney = poney;
	}

	private Poney choisirUnPoney() {
		this.poney = poneysDisponible.remove(0);
		return this.poney;
	}

	@Override
	public PoneyBuilder ajouterCouvreRein(String couleur) {
		//TODO on parcours la liste des �quipements du poney pour tester la pr�sence d'un objet de type CouvreRein
		for (Equipement equipement : this.poney.getEquipements()) {
			if (equipement instanceof CouvreRein) {
				return this;
			}
		}
			for (Equipement equipement : equipements) {
				if (equipement.getTypeEquipement().equals("Couvre reins") && equipement.getCouleur().equals(couleur)) {
					this.poney.getEquipements().add(equipement);
					equipements.remove(equipement);
					break;
				}
			}
		return this;
	}

	@Override
	public PoneyBuilder ajouterBride(String couleur) {
		if (this.poney.isBride() == true) {
			for (Equipement equipement : equipements) {
				if (equipement.getTypeEquipement().equals("Bride") && equipement.getCouleur().equals(couleur)) {
					this.poney.getEquipements().add(equipement);
					equipements.remove(equipement);
					this.poney.setBride(false);
					break;
				}
			}
		}
		return this;
	}

	@Override
	public PoneyBuilder ajouterSelle(String matiere) {
		Selle selle = null;
		if (this.poney.isSelle() == true) {
			for (Equipement equipement : equipements) {
				if (equipement.getTypeEquipement().equals("Selle")) {
					selle = (Selle) equipement;
					if (selle.getMatiere().equals(matiere)) {
						this.poney.getEquipements().add(equipement);
						equipements.remove(equipement);
						this.poney.setSelle(false);
						break;
					}
				}
			}
		}
		return this;
	}

	@Override
	public PoneyBuilder ajouterRenes(String couleur) {
		if (this.poney.isRene() == true) {
			for (Equipement equipement : equipements) {
				if (equipement.getTypeEquipement().equals("R�nes") && equipement.getCouleur().equals(couleur)) {
					this.poney.getEquipements().add(equipement);
					equipements.remove(equipement);
					this.poney.setRene(false);
					break;
				}
			}
		}
		return this;
	}

	@Override
	public Poney buildPoney() {
		return poney;
	}

}
