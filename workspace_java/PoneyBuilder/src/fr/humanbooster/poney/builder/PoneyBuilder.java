package fr.humanbooster.poney.builder;

import fr.humanbooster.poney.business.Poney;

public interface PoneyBuilder {

	PoneyBuilder ajouterCouvreRein(String couleur);

	PoneyBuilder ajouterBride(String couleur);

	PoneyBuilder ajouterRenes(String couleur);
	
	PoneyBuilder ajouterSelle(String matiere);

	Poney buildPoney();

}