package fr.humanbooster.poney.business;

public class Bride extends Equipement{

	public Bride(String typeEquipement) {
		super(typeEquipement);
	}

	public Bride(String typeEquipement, String couleur) {
		super(typeEquipement, couleur);
	}
}
