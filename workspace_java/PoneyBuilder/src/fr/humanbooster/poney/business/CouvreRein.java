package fr.humanbooster.poney.business;

public class CouvreRein extends Equipement {
	
	public CouvreRein(String typeEquipement) {
		super(typeEquipement);
	}

	public CouvreRein(String typeEquipement, String couleur) {
		super(typeEquipement, couleur);	
	}
	
}