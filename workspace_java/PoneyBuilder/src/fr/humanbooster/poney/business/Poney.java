package fr.humanbooster.poney.business;

import java.util.ArrayList;
import java.util.List;

public class Poney {

	private Long id;
	private static Long compteur = 0L;
	private String nom;
	private List<Equipement> equipements = new ArrayList<>();
	private boolean couvreReins = true;
	private boolean selle = true;
	private boolean bride = true;
	private boolean rene = true;

	public Poney() {
		super();
		id = ++compteur;
	}

	public Poney(String nom) {
		this();
		this.nom = nom;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getNom() {
		return nom;
	}

	public void setNom(String nom) {
		this.nom = nom;
	}

	public List<Equipement> getEquipements() {
		return equipements;
	}

	public void setEquipements(List<Equipement> equipements) {
		this.equipements = equipements;
	}

	public boolean isCouvreReins() {
		return couvreReins;
	}

	public void setCouvreReins(boolean couvreReins) {
		this.couvreReins = couvreReins;
	}

	public boolean isSelle() {
		return selle;
	}

	public void setSelle(boolean selle) {
		this.selle = selle;
	}

	public boolean isBride() {
		return bride;
	}

	public void setBride(boolean bride) {
		this.bride = bride;
	}

	public boolean isRene() {
		return rene;
	}

	public void setRene(boolean rene) {
		this.rene = rene;
	}

	@Override
	public String toString() {
		return "Poney [nom=" + nom + ", equipements=" + equipements + "]";
	}
	
}
