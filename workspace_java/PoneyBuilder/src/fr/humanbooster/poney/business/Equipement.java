package fr.humanbooster.poney.business;

public abstract class Equipement {
	
	private String typeEquipement;
	private String couleur;
	
	public Equipement(String typeEquipement) {
		super();
		this.typeEquipement = typeEquipement;
	}
	

	public Equipement(String typeEquipement, String couleur) {
		this(typeEquipement);
		this.couleur = couleur;
	}


	public String getTypeEquipement() {
		return typeEquipement;
	}

	public void setTypeEquipement(String typeEquipement) {
		this.typeEquipement = typeEquipement;
	}

	public String getCouleur() {
		return couleur;
	}

	public void setCouleur(String couleur) {
		this.couleur = couleur;
	}


	@Override
	public String toString() {
		return "Equipement [typeEquipement=" + typeEquipement + ", couleur=" + couleur + "]";
	}
	
}
