package fr.humanbooster.poney.business;

public class Selle extends Equipement {
	
	private String matiere;
	
	public Selle(String typeEquipement, String matiere) {
		super(typeEquipement);
		this.matiere = matiere;
	}

	public String getMatiere() {
		return matiere;
	}

	public void setMatiere(String matiere) {
		this.matiere = matiere;
	}

	@Override
	public String toString() {
		return "Selle [matiere=" + matiere + ", toString()=" + super.toString() + "]";
	}
}
