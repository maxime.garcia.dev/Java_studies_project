package fr.humanbooster.poney.business;

public class Rene extends Equipement{

	public Rene(String typeEquipement) {
		super(typeEquipement);
	}

	public Rene(String typeEquipement, String couleur) {
		super(typeEquipement, couleur);
	}
}
