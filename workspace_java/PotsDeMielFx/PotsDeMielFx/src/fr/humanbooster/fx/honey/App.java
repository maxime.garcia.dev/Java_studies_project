package fr.humanbooster.fx.honey;

import java.util.Scanner;

import fr.humanbooster.fx.honey.builders.PotBuilder;
import fr.humanbooster.fx.honey.business.Cadeau;
import fr.humanbooster.fx.honey.business.GeleeRoyale;
import fr.humanbooster.fx.honey.business.Pot;
import fr.humanbooster.fx.honey.business.Propolis;
import fr.humanbooster.fx.honey.business.Rayon;

public class App {

	private static Scanner scanner = new Scanner(System.in);

	public static void main(String[] args) {

		PotBuilder potBuilder = new PotBuilder();

		do {
			System.out.println("Numéro\tTarif\tNom");
			System.out.println("1\t0.8\tPropolis");
			System.out.println("2\t2.0\tRayon");
			System.out.println("3\t1.2\tGelée royale");
			System.out.print("Entrez le numéro de l'ingrédient: ");

			switch (scanner.nextLine()) {
			case "1":
				potBuilder.setPot(new Propolis(potBuilder.getPot()));
				System.out.println("Vous avez ajouté l'ingrédient Propolis");
				break;
			case "2":
				potBuilder.setPot(new Rayon(potBuilder.getPot()));
				System.out.println("Vous avez ajouté l'ingrédient Rayon");
				break;
			case "3":
				potBuilder.setPot(new GeleeRoyale(potBuilder.getPot()));
				System.out.println("Vous avez ajouté l'ingrédient Gelée royale");
				break;
			default:
				break;
			}
			System.out.print("Souhaitez-vous ajouter un nouvel ingrédient (O/N) ? ");
		} while (scanner.nextLine().toLowerCase().equals("o"));


		Pot pot = potBuilder.choisirMiel("Lavande").choisirPoids(500).ajouterEtiquette("Où est passé mon Ravintsara ?").build();
		System.out.println(pot);
		
		// Patron Iterator
		while (pot.getCollectionDeCadeaux().hasNext()) {
			Cadeau cadeau = (Cadeau) pot.getCollectionDeCadeaux().next();
			System.out.println(cadeau);
		}
		
		// Le ForEach est possible sur CollectionDeCadeau car elle implémente
		// l'interface Iterable
		for (Cadeau cadeau : pot.getCollectionDeCadeaux()) {
			System.out.println(cadeau);
		}

		scanner.close();
	}

}
