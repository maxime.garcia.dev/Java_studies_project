#------------------------------------------------------------
#        Script MySQL.
#------------------------------------------------------------


#------------------------------------------------------------
# Table: course
#------------------------------------------------------------

CREATE TABLE course(
        idCourse         bigint(20) Auto_increment  NOT NULL ,
        calories         float ,
        dateHeureDebut   Datetime ,
        dureeEnMinutes   Int ,
        distanceEnMetres Int ,
        idAdherent       bigint(20) ,
        idTapis          bigint(20) ,
        PRIMARY KEY (idCourse )
)ENGINE=InnoDB;


#------------------------------------------------------------
# Table: tapis
#------------------------------------------------------------

CREATE TABLE tapis(
        idTapis     bigint(20) Auto_increment  NOT NULL ,
        numeroSerie Varchar (25) ,
        designation Varchar (50) ,
        PRIMARY KEY (idTapis )
)ENGINE=InnoDB;


#------------------------------------------------------------
# Table: adherent
#------------------------------------------------------------

CREATE TABLE adherent(
        idAdherent bigint(20) Auto_increment  NOT NULL ,
        nom        Varchar (255) ,
        prenom     Varchar (255) ,
        email      Varchar (255) ,
        motDePasse Varchar (255) ,
        PRIMARY KEY (idAdherent )
)ENGINE=InnoDB;

ALTER TABLE course ADD CONSTRAINT FK_course_idAdherent FOREIGN KEY (idAdherent) REFERENCES adherent(idAdherent);
ALTER TABLE course ADD CONSTRAINT FK_course_idTapis FOREIGN KEY (idTapis) REFERENCES tapis(idTapis);