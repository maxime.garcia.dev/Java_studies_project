<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
    <%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
    <%@ taglib prefix = "fmt" uri = "http://java.sun.com/jsp/jstl/fmt" %>
<!DOCTYPE html>
<html>

<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Fitness Max</title>
</head>

	<body>
	
		<p><jsp:include page="entete.jsp" /></p>
		<br/>
		<br/>
		<h1>Vos courses</h1>
		<br/>
		<form action="courses" method="get">
			Courses effectuées entre le <input type="date" id="debut" name="DEBUT"> et le <input type="date" id="fin" name="FIN">
			<input type="submit" value="Filtrer"> 
		</form>
		<br>
		<table>
			<tr>
				<td>Date et heure<c:if test="${idTri ne 0}"><a href="courses?ID_TRI=0&DEBUT=${debut}&FIN=${fin}"><img src="images/desc.gif"></a></c:if></td>
				<td>Distance en (m)<c:if test="${idTri ne 1}"><a href="courses?ID_TRI=1&DEBUT=${debut}&FIN=${fin}"><img src="images/desc.gif"></a></c:if></td>
				<td>Durée<c:if test="${idTri ne 2}"><a href="courses?ID_TRI=2&DEBUT=${debut}&FIN=${fin}"><img src="images/desc.gif"></a></c:if></td>
				<td>Calories</td>
				<td>Action</td>
			</tr>
			<c:forEach var="course" items="${coursesCorrespondantes}">
				<tr>
					<td><fmt:formatDate pattern="dd/MM/yyyy hh:mm" value="${course.dateHeureDebut}" /></td>
					<td>${course.distanceEnMetres}</td>
					<td>${course.dureeEnMinutes}</td>
					<td>${course.calories}</td>
					<td><a href="">Modifier</a></td>
				</tr>
			</c:forEach>
		</table>
		<br>
		<p><a href="WEB-INF/course.jsp">Ajouter</a></p>
	</body>
</html>