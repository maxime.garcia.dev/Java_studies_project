<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
    <%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
    <%@ taglib prefix = "fmt" uri = "http://java.sun.com/jsp/jstl/fmt" %>
<c:choose>
	<c:when test="${sessionScope.joueur eq null}">
		<p><a href="connexion">Connexion</a></p>
	</c:when>
	<c:when test="${sessionScope.utilisateur ne null }">
		<p>Bienvenue ${sessionScope.adherent.prenom} ${sessionScope.adherent.nom} <a href="deconnexion">Déconnexion</a></p>
		<br>
	</c:when>
</c:choose>