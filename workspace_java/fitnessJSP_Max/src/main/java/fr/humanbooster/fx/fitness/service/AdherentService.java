package fr.humanbooster.fx.fitness.service;

import java.util.List;

import fr.humanbooster.fx.fitness.business.Adherent;

public interface AdherentService {
	
	Adherent ajouterAdherent(String nom, String prenom, String email, String motDePasse);
	
	Adherent ajouterAdherent(Adherent adherent);
	
	List<Adherent> recupererAdherents();
	
	Adherent recupererAdherent(String email, String motDePasse);
	
	Adherent recupererAdherentParId(Long id);
	
	boolean supprimerAdherent(Adherent adherent);
}
