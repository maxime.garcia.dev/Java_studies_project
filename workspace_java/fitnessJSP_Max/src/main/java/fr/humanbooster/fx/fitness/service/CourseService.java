package fr.humanbooster.fx.fitness.service;

import java.util.Date;
import java.util.List;

import fr.humanbooster.fx.fitness.business.Adherent;
import fr.humanbooster.fx.fitness.business.Course;

public interface CourseService {
	
	Course ajouterCourse(Course course);
	
	List<Course> recupererCourses();
	
	List<Course> recuperCoursesParAdherentEtEntreDeuxDates(Adherent adherent, Date debut, Date fin);
	
	Course recupererCourse(Long id);
	
	Course modifierCourse(Course course);
}
