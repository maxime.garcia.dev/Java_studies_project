package fr.humanbooster.fx.fitness.servlets;

import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import fr.humanbooster.fx.fitness.business.Course;
import fr.humanbooster.fx.fitness.service.AdherentService;
import fr.humanbooster.fx.fitness.service.CourseService;
import fr.humanbooster.fx.fitness.service.TapisService;
import fr.humanbooster.fx.fitness.service.impl.AdherentServiceImpl;
import fr.humanbooster.fx.fitness.service.impl.CourseServiceImpl;
import fr.humanbooster.fx.fitness.service.impl.TapisServiceImpl;
import fr.humanbooster.fx.fitness.util.ComparateurCoursesSurDate;
import fr.humanbooster.fx.fitness.util.ComparateurCoursesSurDistance;
import fr.humanbooster.fx.fitness.util.ComparateurCoursesSurDuree;

/**
 * Servlet implementation class CoursesServlet
 */
@WebServlet(urlPatterns = "/courses", loadOnStartup = 1)
public class CoursesServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	private AdherentService adherentService;
	private TapisService tapisService;
	private CourseService courseService;

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public CoursesServlet() {
		super();
		adherentService = new AdherentServiceImpl();
		tapisService = new TapisServiceImpl();
		courseService = new CourseServiceImpl();
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		String parseDebut = null;
		String parseFin = null;
		Date debut = null;
		Date fin = null;
		List<Course> coursesCorrespondantes = new ArrayList<>();
		List<Course> courses = courseService.recupererCourses();
		int idTri = 0;

		if (request.getParameter("DEBUT") != null) {
			parseDebut = request.getParameter("DEBUT");
			try {
				debut = new SimpleDateFormat("dd/MM/yyyy").parse(parseDebut);
			} catch (ParseException e) {
				e.printStackTrace();
			}
		}
		
		if (request.getParameter("FIN") != null) {
			parseFin = request.getParameter("FIN");
			try {
				fin = new SimpleDateFormat().parse(parseFin);
			} catch (ParseException e) {
				e.printStackTrace();
			}
		}
		
		if (request.getParameter("ID_TRI")!=null) {
			idTri = Integer.parseInt(request.getParameter("ID_TRI"));
		}
		
		for (Course course : courses) {
			if ((debut == null || course.getDateHeureDebut().after(debut))
					&& (fin == null || course.getDateHeureDebut().before(fin))){
				coursesCorrespondantes.add(course);
			}
		}
		
		if (idTri==0) {
			Collections.sort(coursesCorrespondantes, new ComparateurCoursesSurDate());
		}
		else if (idTri==1) {
			Collections.sort(coursesCorrespondantes, new ComparateurCoursesSurDistance());
		}
		else if (idTri==2){
			Collections.sort(coursesCorrespondantes, new ComparateurCoursesSurDuree());
		}
		
		request.setAttribute("courses", coursesCorrespondantes);
		request.setAttribute("debut", debut);
		request.setAttribute("fin", fin);
		request.setAttribute("idTri", idTri);
		request.getRequestDispatcher("WEB-INF/courses.jsp").include(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		doGet(request, response);
	}

}
