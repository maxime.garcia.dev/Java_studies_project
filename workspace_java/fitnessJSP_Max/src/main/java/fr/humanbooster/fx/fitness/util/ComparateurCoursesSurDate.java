package fr.humanbooster.fx.fitness.util;

import java.util.Comparator;

import fr.humanbooster.fx.fitness.business.Course;

public class ComparateurCoursesSurDate implements Comparator<Course>{

	@Override
	public int compare(Course course1, Course course2) {
		if (course1.getDateHeureDebut().equals(course2.getDateHeureDebut())){
			return 0;
		}
		else if (course1.getDateHeureDebut().after(course2.getDateHeureDebut())) {
			return -1;
		}
		else {
			return 1;
		}
	}
}