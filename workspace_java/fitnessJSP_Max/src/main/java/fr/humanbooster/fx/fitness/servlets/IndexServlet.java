package fr.humanbooster.fx.fitness.servlets;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import fr.humanbooster.fx.fitness.business.Adherent;
import fr.humanbooster.fx.fitness.service.AdherentService;
import fr.humanbooster.fx.fitness.service.impl.AdherentServiceImpl;

/**
 * Servlet implementation class IndexServlet
 */
@WebServlet(urlPatterns="/index", loadOnStartup=1)
public class IndexServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
    private AdherentService adherentService = new AdherentServiceImpl();
	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		request.getRequestDispatcher("WEB-INF/index.jsp").forward(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		Adherent adherent = adherentService.recupererAdherent(request.getParameter("EMAIL"), request.getParameter("MOT_DE_PASSE"));
		if(adherent != null) {
			request.getSession().setAttribute("adherent", adherent);
			request.getRequestDispatcher("WEB-INF/courses.jsp").forward(request, response);
		} else {
			request.setAttribute("erreur", "Utilisateur non trouvé. Le mail ou le mot de passe est incorrect");
			doGet(request, response);
		}
	}

}
