package fr.humanbooster.fx.fitness.service.impl;

import java.util.Date;
import java.util.List;

import fr.humanbooster.fx.fitness.business.Adherent;
import fr.humanbooster.fx.fitness.business.Course;
import fr.humanbooster.fx.fitness.dao.CourseDao;
import fr.humanbooster.fx.fitness.dao.impl.CourseDaoImpl;
import fr.humanbooster.fx.fitness.service.CourseService;

public class CourseServiceImpl implements CourseService {

	private CourseDao courseDao = new CourseDaoImpl();
	
	@Override
	public Course ajouterCourse(Course course) {
		try {
			return courseDao.create(course);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}

	@Override
	public List<Course> recupererCourses() {
		try {
			return courseDao.findAll();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}

	@Override
	public List<Course> recuperCoursesParAdherentEtEntreDeuxDates(Adherent adherent, Date debut, Date fin) {
		try {
			return courseDao.findByAdherentAndDateHeureDebutBetween(adherent, debut, fin);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}

	@Override
	public Course recupererCourse(Long id) {
		try {
			return courseDao.findOne(id);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}

	@Override
	public Course modifierCourse(Course course) {
		try {
			return courseDao.update(course);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}
}
