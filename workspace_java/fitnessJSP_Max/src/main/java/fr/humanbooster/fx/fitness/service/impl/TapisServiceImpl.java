package fr.humanbooster.fx.fitness.service.impl;

import java.util.List;

import fr.humanbooster.fx.fitness.business.Tapis;
import fr.humanbooster.fx.fitness.dao.TapisDao;
import fr.humanbooster.fx.fitness.dao.impl.TapisDaoImpl;
import fr.humanbooster.fx.fitness.service.TapisService;

public class TapisServiceImpl implements TapisService {

	private TapisDao tapisDao = new TapisDaoImpl();
	
	@Override
	public Tapis ajouterTapis(Tapis tapis) {
		try {
			return tapisDao.create(tapis);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}

	@Override
	public List<Tapis> recupererTapis() {
		try {
			return tapisDao.findAll();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}

	@Override
	public Tapis recupererUnTapis(Long id) {
		try {
			return tapisDao.findOne(id);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}

}
