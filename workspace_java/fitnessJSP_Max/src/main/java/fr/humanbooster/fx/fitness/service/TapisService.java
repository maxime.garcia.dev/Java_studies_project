package fr.humanbooster.fx.fitness.service;

import java.util.List;

import fr.humanbooster.fx.fitness.business.Tapis;

public interface TapisService {
	
	Tapis ajouterTapis(Tapis tapis);
	
	List<Tapis> recupererTapis();
	
	Tapis recupererUnTapis(Long id);
}
