package fr.humanbooster.fx.fitness.service.impl;

import java.util.List;

import fr.humanbooster.fx.fitness.business.Adherent;
import fr.humanbooster.fx.fitness.dao.AdherentDao;
import fr.humanbooster.fx.fitness.dao.impl.AdherentDaoImpl;
import fr.humanbooster.fx.fitness.service.AdherentService;

public class AdherentServiceImpl implements AdherentService {

	
	private AdherentDao adherentDao = new AdherentDaoImpl();
	
	public AdherentServiceImpl() {
		List<Adherent> adherents = null;
		try {
			adherents = adherentDao.findAll();
		} catch (Exception e) {
			e.printStackTrace();
		}
		if (adherents.isEmpty()) {
			ajouterAdherent("Max", "Garcia", "a@a.fr", "1234");
			ajouterAdherent("Tony", "Raza", "b@b.fr", "4321");
		}
	}
	
	@Override
	public Adherent ajouterAdherent(Adherent adherent) {
		try {
			return adherentDao.create(adherent);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}
	
	@Override
	public Adherent ajouterAdherent(String nom, String prenom, String email, String motDePasse) {
		Adherent adherent = new Adherent(nom, prenom, email, motDePasse);
		try {
			return adherentDao.create(adherent);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}

	@Override
	public List<Adherent> recupererAdherents() {
		try {
			return adherentDao.findAll();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}


	@Override
	public Adherent recupererAdherent(String email, String motDePasse) {
		try {
			return adherentDao.findByEmailAndMotDePasse(email, motDePasse);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}

	@Override
	public Adherent recupererAdherentParId(Long id) {
		try {
			return adherentDao.findOne(id);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}

	@Override
	public boolean supprimerAdherent(Adherent adherent) {
		try {
			return adherentDao.delete(adherent);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return false;
	}

	

}
