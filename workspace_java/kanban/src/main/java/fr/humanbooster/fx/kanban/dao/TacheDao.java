package fr.humanbooster.fx.kanban.dao;

import java.util.Date;
import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;

import fr.humanbooster.fx.kanban.business.Tache;

public interface TacheDao extends JpaRepository<Tache, Long> {

	List<Tache> findTacheByIntituleStartingWith(String intitule);
	
	// La méthode est transformée par Spring Data en requête SQL
	List<Tache> findTacheByDateCreationBetween(Date dateDebut, Date dateFin);
	
	List<Tache> findTop20ByNbHeuresEstimeesGreaterThan(Integer valeur);
	
	Page<Tache> findTop20ByNbHeuresEstimeesGreaterThan(Integer valeur, Pageable pageable);
	
}