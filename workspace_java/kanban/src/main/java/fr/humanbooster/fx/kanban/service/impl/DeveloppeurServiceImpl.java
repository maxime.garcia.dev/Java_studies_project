package fr.humanbooster.fx.kanban.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import fr.humanbooster.fx.kanban.business.Developpeur;
import fr.humanbooster.fx.kanban.dao.DeveloppeurDao;
import fr.humanbooster.fx.kanban.service.DeveloppeurService;

@Service
public class DeveloppeurServiceImpl implements DeveloppeurService {

	@Autowired
	private DeveloppeurDao developpeurDao;
	
	@Override
	public Developpeur ajouterDeveloppeur(String nom, String prenom) {
		Developpeur developpeur = new Developpeur();
		developpeur.setNom(nom);
		developpeur.setPrenom(prenom);
		developpeurDao.save(developpeur);
		return developpeur;
	}

	@Override
	public Developpeur ajouterDeveloppeur(String prenom) {
		Developpeur developpeur = new Developpeur();
		developpeur.setPrenom(prenom);
		developpeurDao.save(developpeur);
		return developpeur;
	}

	@Override
	public List<Developpeur> recupererDeveloppeurs() {
		return developpeurDao.findAll();
	}

}