package fr.humanbooster.fx.kanban.controller;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpServletResponse;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.HttpMediaTypeNotAcceptableException;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import fr.humanbooster.fx.kanban.business.Colonne;
import fr.humanbooster.fx.kanban.business.Tache;
import fr.humanbooster.fx.kanban.service.ColonneService;
import fr.humanbooster.fx.kanban.service.TacheService;

@RestController
public class KanbanRestController {

	private static final Logger logger = LogManager.getLogger(Logger.class.getName());

	@Autowired
	private TacheService tacheService;
	@Autowired
	private ColonneService colonneService;
	
	/**
	 * Cette méthode permet d'ajouter une tâche
	 * via le service Web
	 * post: pour ajouter (PostMapping)
	 * get: récuperer (GetMapping)
	 * put: mettre à jour(PutMapping)
	 * delete: supprimer(DeleteMapping)
	 * @return la nouvelle tâche
	 */
	@PostMapping("/taches/{intitule}")
	public Tache ajouterTache(@PathVariable String intitule)
	{
		System.out.println(new Date() + "Demande d'ajout d'une tâche");
		Tache tache = tacheService.ajouterTache(intitule);
		return tache;
	}

	/**
	 * Cette méthode met à jour la tâche dont l'id 
	 * est donné en paramètre
	 * 
	 * @param id de la tâche à modifier
	 * @param nouvelIntitule de la tâche
	 * @return la tâche mise à jour
	 */
	@PutMapping("/taches/{id}/{nouvelIntitule}")
	public Tache mettreAJourTache(@PathVariable Long id, @PathVariable String nouvelIntitule) {
		Tache tache = tacheService.recupererTache(id);
		tache.setIntitule(nouvelIntitule);
		tacheService.enregistrerTache(tache);
		return tache;
	}
		
	/**
	 * Méthode permettant d’obtenir toutes les tâches
	 * 
	 * @return
	 */
	@GetMapping(value = "/taches", produces = MediaType.APPLICATION_JSON_VALUE)
	public List<Tache> recupererTaches()
	{
		logger.info("Appel à la méthode recupererTaches()");
	    return tacheService.recupererTaches();
	}

	/**
	 * Méthode permettant d’obtenir la tâche dont l’id est précisé dans l’URL
	 * @param id
	 * @return
	 */
	@GetMapping(value = "/taches/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
	public Tache recupererTache(@PathVariable Long id)
	{
		logger.info("Appel à la méthode recupererTache()");
	    return tacheService.recupererTache(id);
	}

	/**
	 * Cette méthode permet d'ajouter une nouvelle tache
	 * 
	 * @param intitule de la tache
	 * @return un objet de type Tache
	 */
	@PostMapping(value="/taches/{intitule}", produces = MediaType.APPLICATION_JSON_VALUE)
	public Tache tachePost(@PathVariable String intitule) {
		Tache tache = new Tache();
		tache.setIntitule(intitule);
		return tache;
	}
		
	/**
	 *  Méthode permettant de supprimer une tâche en précisant son id
	 * @param id
	 * @param response
	 * @return
	 */
	@DeleteMapping(value="/taches/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
	public boolean supprimerTache(@PathVariable Long id, HttpServletResponse response)
	{
		Tache tacheASupprimer = tacheService.recupererTache(id);
		if (tacheASupprimer==null) {
			return false;
		}
		tacheService.supprimerTache(tacheASupprimer);
		Tache tache = tacheService.recupererTache(id);
		if (tache==null) {
			return true;
		}
		response.setStatus(500);
		return false;
	}
	
	/**
	 * Méthode permettant d’obtenir toutes les tâches dont l’intitulé contient le mot précisé dans l’URL
	 * @param filtre
	 * @return
	 */
	@GetMapping(value = "/tachesFiltrees", produces = MediaType.APPLICATION_JSON_VALUE)
	public List<Tache> recupererTachesFiltrees(@RequestParam String filtre)
	{
		List<Tache> tachesFiltrees = new ArrayList<>();
	    for (Tache tache : tacheService.recupererTaches()) {
	      if (tache.getIntitule().contains(filtre)) {
	        tachesFiltrees.add(tache);
	      }
	    }
	    return tachesFiltrees;
	}

	@ResponseBody
	@ExceptionHandler(HttpMediaTypeNotAcceptableException.class)
	public String handleHttpMediaTypeNotAcceptableException() {
	    return "acceptable MIME type:" + MediaType.APPLICATION_JSON_VALUE;
	}
	
	// on veut persister l etat du tableau suite maj page
	@PutMapping(value = "/taches/deplacer")
	public boolean deplacerTache(@RequestParam(name = "ID_TACHE") long idTache, @RequestParam(name = "ID_COLONNE") Long idColonne) {
		boolean retour = false;
		Colonne c1 = null;
		Tache tache = tacheService.recupererTache(idTache);
		List<Colonne> colonnesPossibles = colonneService.recupererColonnes();
		for (Colonne colonne : colonnesPossibles) {
			if(colonne.getId() == idColonne) {
				c1 = colonne;
			}
			
		}
		if(tache != null && c1 != null) {
			tache.setColonne(c1);
			tacheService.enregistrerTache(tache);
			retour = true;
		}
		return retour;
	}

}