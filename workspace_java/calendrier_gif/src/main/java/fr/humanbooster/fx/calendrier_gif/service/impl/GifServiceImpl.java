package fr.humanbooster.fx.calendrier_gif.service.impl;

import java.util.Date;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import fr.humanbooster.fx.calendrier_gif.business.Gif;
import fr.humanbooster.fx.calendrier_gif.business.GifDistant;
import fr.humanbooster.fx.calendrier_gif.business.Jour;
import fr.humanbooster.fx.calendrier_gif.business.Utilisateur;
import fr.humanbooster.fx.calendrier_gif.dao.GifDao;
import fr.humanbooster.fx.calendrier_gif.dao.GifDistantDao;
import fr.humanbooster.fx.calendrier_gif.dao.GifTeleverseDao;
import fr.humanbooster.fx.calendrier_gif.dao.UtilisateurDao;
import fr.humanbooster.fx.calendrier_gif.service.GifService;
import fr.humanbooster.fx.calendrier_gif.service.JourService;

@Service
public class GifServiceImpl implements GifService {

	@Autowired
	private JourService jourService;

	@Autowired
	private GifDao gifDao;

	@Autowired
	private GifDistantDao gifDistantDao;

	@Autowired
	private GifTeleverseDao gifTeleverseDao;

	@Autowired
	private UtilisateurDao utilisateurDao;
	
	@Override
	public GifDistant ajouterGifDistant(Date idJour, String url, Utilisateur utilisateur) {
		GifDistant gifDistant = new GifDistant();
		Jour jour = jourService.recupererJour(idJour);
		gifDistant.setJour(jour);
		gifDistant.setUrl(url);
		gifDistant.setUtilisateur(utilisateur);
		gifDistant = gifDistantDao.save(gifDistant);
		jour.setGif(gifDistant);
		jourService.enregistrerJour(jour);
		// Met à jour le solde de l'utilisateur
		utilisateur.setNbPoints(utilisateur.getNbPoints()-jour.getNbPoints());
		utilisateurDao.save(utilisateur);
		return gifDistant;
	}

	@Override
	public GifDistant ajouterGifDistant(GifDistant gifDistant, Utilisateur utilisateur) {
		gifDistant = gifDistantDao.save(gifDistant);
		gifDistant.setUtilisateur(utilisateur);
		Jour jour = jourService.recupererJour(gifDistant.getJour().getDate());
		jour.setGif(gifDistant);
		jourService.enregistrerJour(jour);
		// Met à jour le solde de l'utilisateur
		utilisateur.setNbPoints(utilisateur.getNbPoints()-jour.getNbPoints());
		utilisateurDao.save(utilisateur);
		return gifDistant;
	}

	@Override
	public Gif recupererGif(Long idGif) {
		if (gifDistantDao.findById(idGif)!=null) {
			return gifDistantDao.findById(idGif).orElse(null);
		}
		if (gifTeleverseDao.findById(idGif)!=null) {
			return gifTeleverseDao.findById(idGif).orElse(null);
		}
		return null;
	}

	@Override
	public Gif mettreAJourLegende(Gif gif, String nouvelleLegende) {
		gif.setLegende(nouvelleLegende);
		gifDao.save(gif);
		return gif;
	}

}