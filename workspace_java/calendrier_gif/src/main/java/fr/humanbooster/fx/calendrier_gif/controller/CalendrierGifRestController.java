package fr.humanbooster.fx.calendrier_gif.controller;

import java.text.SimpleDateFormat;
import java.util.Date;

import javax.servlet.ServletContext;
import javax.servlet.http.HttpSession;

import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import fr.humanbooster.fx.calendrier_gif.business.Reaction;
import fr.humanbooster.fx.calendrier_gif.business.Utilisateur;
import fr.humanbooster.fx.calendrier_gif.service.EmotionService;
import fr.humanbooster.fx.calendrier_gif.service.GifService;
import fr.humanbooster.fx.calendrier_gif.service.JourService;
import fr.humanbooster.fx.calendrier_gif.service.ReactionService;
import fr.humanbooster.fx.calendrier_gif.service.ThemeService;
import fr.humanbooster.fx.calendrier_gif.service.UtilisateurService;

@RestController
public class CalendrierGifRestController {
	
	private JourService jourService;
	private EmotionService emotionService;
	private UtilisateurService utilisateurService;
	private ThemeService themeService;
	private GifService gifService;
	private ReactionService reactionService;
	private HttpSession httpSession;
	private ServletContext servletContext;
	private SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd");
	
	public CalendrierGifRestController(JourService jourService, EmotionService emotionService,
			UtilisateurService utilisateurService, ThemeService themeService, GifService gifService,
			ReactionService reactionService, HttpSession httpSession, ServletContext servletContext) {
		super();
		this.jourService = jourService;
		this.emotionService = emotionService;
		this.utilisateurService = utilisateurService;
		this.themeService = themeService;
		this.gifService = gifService;
		this.reactionService = reactionService;
		this.httpSession = httpSession;
		this.servletContext = servletContext;
	}
	
	@PostMapping(value="/utilisateurs/{nom}/{prenom}/{email}/{motDePasse}/{nomTheme}")
	public Utilisateur ajouterUtilisateur(@PathVariable String nom, @PathVariable String prenom, @PathVariable String email, @PathVariable String motDePasse, @PathVariable String nomTheme) {
		Utilisateur utilisateur = new Utilisateur(nom, prenom, email, motDePasse, themeService.recupererTheme(nomTheme));
	    utilisateurService.ajouterUtilisateur(utilisateur);
		return utilisateur;
	}
	
	@DeleteMapping(value="/supprimer")
	public boolean supprimerReaction(@RequestParam("ID_REACTION") Long id, @RequestParam("EMAIL") String email, @RequestParam("DATE") String date) {
		Reaction reaction = reactionService.recupererReaction(id);
		if (reaction !=null && reaction.getUtilisateur().getEmail().equals(email) && simpleDateFormat.format(reaction.getDateHeure()).equals(date)) {
			reactionService.supprimerReaction(id);
			return true;
		}
		return false;
	}
}
