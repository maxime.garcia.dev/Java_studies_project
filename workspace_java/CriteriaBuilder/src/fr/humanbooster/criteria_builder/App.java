package fr.humanbooster.criteria_builder;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;

import fr.humanbooster.criteria_builder.business.Joueur;

public class App {

	public static void main(String[] args) {
		CriteriaBuilder criteriaBuilder = null;
		
		CriteriaQuery<Joueur> criteriaQuery = criteriaBuilder.createQuery(Joueur.class);
        Root<Joueur> lesJoueurs = criteriaQuery.from(Joueur.class);
        
        List<String> nomsRecherches = new ArrayList<>();
        nomsRecherches.add("MARTIN");
        nomsRecherches.add("ROSSI");    
        // Equivalent Java de la requ�te SQL : SELECT * FROM Joueur WHERE nom in ('MARTIN', 'ROSSI')
            criteriaQuery.select(lesJoueurs).where(lesJoueurs.get("nom").in(nomsRecherches));
	}

}
