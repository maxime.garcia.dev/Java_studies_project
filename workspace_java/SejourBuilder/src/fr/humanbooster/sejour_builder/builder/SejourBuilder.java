package fr.humanbooster.sejour_builder.builder;


import java.util.ArrayList;
import java.util.List;

import javax.print.attribute.standard.Destination;

import fr.humanbooster.sejour.business.Cadeau;
import fr.humanbooster.sejour.business.Sejour;
import fr.humanbooster.sejour.business.Voyageur;
import fr.humanbooster.sejour.factory.CodeFactory;
import fr.humanbooster.sejour.iterator.CollectionDeCadeaux;



public class SejourBuilder {

	private Sejour sejour;
	private static CodeFactory codeFactory = CodeFactory.getInstance();

	private static List<Voyageur> voyageurs = new ArrayList<>();
	private static List<Destination> destinations = new ArrayList<>();
	private static CollectionDeCadeaux collectionDeCadeaux = new CollectionDeCadeaux();
	
	public SejourBuilder() {
		
		sejour = new Sejour();
		Destination bejaia = new Destination("B�ja�a", 1200);
		Destination saintDenis = new Destination("Saint-Denis", 1800);
		Destination tassin = new Destination("Tassin la Demi-Lune", 800);

		destinations.add(bejaia);
		destinations.add(saintDenis);
		destinations.add(tassin);

		// 2 jeux de cartes Dixit
		for (int i = 0; i < 2; i++) {
			collectionDeCadeaux.ajouter(new Cadeau("Un jeu de carte Dixit"));			
		}
		
		// 1 figurine d'Alexandre
		collectionDeCadeaux.ajouter(new Cadeau("Une figurine d'Alexandre"));
		
		// 6 paquets de rousquilles
		for (int i = 0; i < 6; i++) {
			collectionDeCadeaux.ajouter(new Cadeau("Un paquet de rousquilles"));			
		}
		
		// 5 jours d'�coute sur Deezer
		for (int i = 0; i < 2; i++) {
			collectionDeCadeaux.ajouter(new Cadeau("Cinq jours d'�coute sur Deezer"));			
		}

	}
	
	public static List<Destination> getDestinations() {
		return destinations;
	}

	public static void setDestinations(List<Destination> destinations) {
		SejourBuilder.destinations = destinations;
	}

	public SejourBuilder ajouterVoyageurs(List<Voyageur> voyageurs) {
		for (Voyageur voyageur : voyageurs) {
			sejour.getCollectionDeVoyageurs().ajouter(voyageur);			
		}
		return this;
	}
	
	public SejourBuilder choisirDestination(String nom) {
		sejour.setDestination(recupererDestination(nom));
		sejour.setNom("Votre s�jour � " + sejour.getDestination().getNom());
		return this;
	}
	
	public Sejour build() {
		sejour.getCollectionDeCadeaux().ajouter(collectionDeCadeaux.getCadeaux().remove(0));
		sejour.setCode(codeFactory.fabriquerCode());
		return sejour;
	}
	
	public Sejour getSejour() {
		return sejour;
	}

	public void setSejour(Sejour sejour) {
		this.sejour = sejour;
	}

	public static CodeFactory getCodeFactory() {
		return codeFactory;
	}

	public static void setCodeFactory(CodeFactory codeFactory) {
		SejourBuilder.codeFactory = codeFactory;
	}

	public static List<Voyageur> getVoyageurs() {
		return voyageurs;
	}

	public static void setVoyageurs(List<Voyageur> voyageurs) {
		SejourBuilder.voyageurs = voyageurs;
	}

	public static CollectionDeCadeaux getCollectionDeCadeaux() {
		return collectionDeCadeaux;
	}

	public static void setCollectionDeCadeaux(CollectionDeCadeaux collectionDeCadeaux) {
		SejourBuilder.collectionDeCadeaux = collectionDeCadeaux;
	}

	private Destination recupererDestination(String nom) {
		for (Destination destination: SejourBuilder.destinations) {
			if (destination.getNom().equals(nom)) {
				return destination;
			}
		}
		return null;
	}

}