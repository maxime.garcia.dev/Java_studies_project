package fr.humanbooster.max.rencontre.business;

import java.util.Date;
import java.util.List;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
public class VieCommune {

	private static final int NB_CREDITS_PAR_DEFAUTS = 0;
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;
	private Date dateDebut;
	private Date dateFin;
	private int nbCredits;
	@OneToMany(mappedBy="vieCommune")
	private List<Message> messages;
	@OneToOne
	private Invitation invitation;
}
