package fr.humanbooster.max.rencontre.business;

import java.util.List;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;

@Entity
public class Genre {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;
	private String nom;
	@OneToMany(mappedBy="genre")
	private List<Personne> personnes;
	@OneToMany(mappedBy="genreRecherche")
	private List<Personne> personnesRecherchant;
}
