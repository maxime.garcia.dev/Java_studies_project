package fr.humanbooster.max.rencontre.business;

import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;

@Entity
public class Invitation {
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;
	private Date dateEnvoi;
	private Date dateLecture;
	private boolean estAccepte;
	@ManyToOne
	private Personne expediteur;
	@ManyToOne
	private Personne destinataire;
	@OneToOne
	private VieCommune vieCommune;
}
