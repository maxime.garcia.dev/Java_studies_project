package fr.humanbooster.max.rencontre.business;

import java.util.Date;
import java.util.List;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Lob;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.Email;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Past;
import javax.validation.constraints.Size;


@Entity
public class Personne {
	
	private static final int NB_CREDITS_INITIAL = 0;
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private Long id;
	private String pseudo;
	@NotBlank(message="Veuillez renseigner un mot de passe.")
	@Size(min=5, message="Votre mot dez passe doit contenir au moins 5 caractères.")
	private String motDePasse;
	@Email(message="Veuillez un email au bon format: example@example.com/fr")
	private String email;
	@Temporal(TemporalType.DATE)
	@Past(message="Merci de renseigner une date anterieur à la date actuelle.")
	private Date dateDeNaissance;
	@Lob
	@Size(min=20, message="Merci d'être plus bavard")
	private String bio;
	private int nbCredits;
	private boolean estFumeur;
	@OneToMany(mappedBy="expediteur")
	private List<Message> messagesEnvoye;
	@OneToMany(mappedBy="destinataire")
	private List<Message> messagesRecu;
	
	@NotNull(message="Veuillez préciser votre statut.")
	@ManyToOne
	private Statut statut;
	@NotNull(message="Veuillez préciser votre genre.")
	@ManyToOne
	private Genre genre;
	@ManyToOne
	private Genre genreRecherche;
	@ManyToOne
	private Ville ville;
	@ManyToMany
	private List<Interet> interets;
	
	public Personne() {
		super();
	}
}
