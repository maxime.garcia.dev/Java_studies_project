package fr.humanbooster.max.rencontre;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class RencontreApplication {

	public static void main(String[] args) {
		SpringApplication.run(RencontreApplication.class, args);
	}

}
