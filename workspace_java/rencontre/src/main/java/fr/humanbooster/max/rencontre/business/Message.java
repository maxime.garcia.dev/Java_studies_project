package fr.humanbooster.max.rencontre.business;

import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Lob;
import javax.persistence.ManyToOne;

@Entity
public class Message {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;
	@Lob
	private String contenu;
	private Date dateEnvoi;
	private Date dateLecture;
	@ManyToOne
	private Personne expediteur;
	@ManyToOne
	private Personne destinataire;
	@ManyToOne
	private VieCommune vieCommune;
}
