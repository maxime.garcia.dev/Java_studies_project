package fr.humanbooster.max.rencontre.business;

import java.util.List;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToMany;

@Entity
public class Interet {
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;
	private String nom;
	//Ici on précise le mappedBy car on est sur la classe satellite
	@ManyToMany(mappedBy="interets")
	private List<Personne> personnes;
}
