package fr.humanbooster.poker.business;

import java.util.ArrayList;

public class Joueur {

	// 1ère étape: on définit les attributs
	private static final float SOLDE_INITIAL = 100; 
	private String pseudo;
	private float solde;
	private ArrayList<Carte> main;
	
	// 2ème étape: on définit le ou les constructeurs
	public Joueur(String pseudo) {
		main = new ArrayList<>();
		solde = SOLDE_INITIAL;
		this.pseudo = pseudo;
	}

	// 3ème étape: on définit les accesseurs et mutateurs
	public String getPseudo() {
		return pseudo;
	}

	public void setPseudo(String pseudo) {
		this.pseudo = pseudo;
	}

	public float getSolde() {
		return solde;
	}

	public void setSolde(float solde) {
		this.solde = solde;
	}

	public ArrayList<Carte> getMain() {
		return main;
	}

	public void setMain(ArrayList<Carte> main) {
		this.main = main;
	}

	// 4ème étape: la méthode toString
	@Override
	public String toString() {
		return "Joueur [pseudo=" + pseudo + ", solde=" + solde
				+ ", main=" + main + "]";
	}
}