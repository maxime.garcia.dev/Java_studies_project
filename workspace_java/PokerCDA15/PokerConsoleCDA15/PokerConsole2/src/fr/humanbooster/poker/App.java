package fr.humanbooster.poker;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import fr.humanbooster.poker.business.Carte;
import fr.humanbooster.poker.business.Combinaison;
import fr.humanbooster.poker.business.Couleur;
import fr.humanbooster.poker.business.Joueur;

public class App {

	// On déclare une collection de couleurs
	private static List<Couleur> couleurs = new ArrayList<>();
	// On déclare une collection de cartes
	private static List<Carte> cartes = new ArrayList<>();

	private static List<Joueur> joueurs = new ArrayList<>();
	
	private static List<Combinaison> combinaisons = new ArrayList<>();
	
	public static void main(String[] args) {
		ajouterCouleurs();
		ajouterCartes();
		melangerCartes();
		//afficherCartes();
		ajouterJoueurs();
		distribuerCartes();
		afficherJoueurs();
		for (Joueur joueur : joueurs) {
			verifierMain(joueur);		
		}
	}
	
	// Big up Jennifer !
	public static void verifierMain(Joueur joueur) {
		List<Carte> main = joueur.getMain();
		int[] occurrences = new int[15];
		int nbPaires = 0;
		int nbBrelan = 0;
		//L'As pour vérifier la quinte flush royale (Si suite && As && couleur = forcément quinte
		//flush royale
		boolean asPresent = false;
		//Permet de vérifier avec la couleur précedente
		Couleur couleurTemp = joueur.getMain().get(0).getCouleur();
		//Par défaut la couleur est identique partout
		boolean estMemeCouleur = true;
		//On compte le nombre de fois où des occurences à 1 se suivent
		int compteurSuite = 0;
		for (Carte carte : main) {
			occurrences[carte.getValeur()]++;
			//Si la valeur est 14,l'as est présent
			if (carte.getValeur() == 14) {
				asPresent = true;
			}

			//Si la couleur est différente d'avant le main n'a pas la couleur
			if (carte.getCouleur() != couleurTemp) {
				estMemeCouleur = false;
			}

		}

		for (int i = 0; i < occurrences.length; i++) {
			switch (occurrences[i]) {
			case 1:
				//Si 1 occurence on regarde celle qui suit sauf à 14(fin de tableau)
				if (i != 14) {
					if (occurrences[i] == occurrences[i + 1]) {
						//On incrémente le compteur pour la suite si l'occurence suivante est aussi de 1
						compteurSuite++;
					}
				}

				break;
			case 2:
				nbPaires++;
				break;
			case 3:
				nbBrelan++;
				break;
			case 4:
				System.out.println(joueur.getPseudo() + " a un carré");
				break;
			default:
				break;
			}
		}

		if (nbPaires == 2) {
			System.out.println(joueur.getPseudo() + " a deux paires");
		} 
		//1 paire + 1 brelan = 1 full
		else if (nbPaires == 1 && nbBrelan == 1) {
			System.out.println(joueur.getPseudo() + " a un full");
		} else if (nbPaires == 0 && nbBrelan == 1) {
			System.out.println(joueur.getPseudo() + " a un brelan");
		} else if (nbPaires == 1 && nbBrelan == 0) {
			System.out.println(joueur.getPseudo() + " a une paire");
		} 
		//Si on a eu 4 fois l'élément suivant à 1,on a une suite
		//L'as est présent et la couleur aussi donc quinte flush royale
		else if (compteurSuite == 4 && estMemeCouleur && asPresent) {
			System.out.println(joueur.getPseudo() + " a une quinte flush royale");
		} 
		//Si suite et même couleur en même temps on a une quinte flush
		else if (compteurSuite == 4 && estMemeCouleur) {
			System.out.println(joueur.getPseudo() + " a une quinte flush");
		} 
		//On a uniquement la suite
		else if (compteurSuite == 4 && !estMemeCouleur) {
			System.out.println(joueur.getPseudo() + " a une suite");
		} 
		//On a la même couleur mais pas de suite
		else if (estMemeCouleur && compteurSuite != 4) {
			System.out.println(joueur.getPseudo() + " a une couleur");
		} 
		//Enfin on a trouvé aucune combinaison
		else {
			System.out.println(joueur.getPseudo() + " n'a aucune combinaison");
		}
	}
	
	private static void afficherJoueurs() {
		for (Joueur joueur : joueurs) {
			System.out.println(joueur);
		}
	}

	private static void distribuerCartes() {
		for (int i = 0; i < 5; i++) {
			for (Joueur joueur : joueurs) {
				
				// On retire une carte du jeu
				//Carte carte = cartes.remove(0);
				// On l'ajoute à la main du joueur
				//joueur.getMain().add(carte);
				
				// joueur.getMain(): on récupère la main du joueur
				// joueur.getMain().add(): on ajoute une carte à la main du joueur
				// joueur.getMain().add(cartes.remove(0)): on retire une carte de la collection carte
				//                                         et on la met dans la main du joueur
				joueur.getMain().add(cartes.remove(0));
			}
		}
	}

	private static void ajouterJoueurs() {
		for (int i = 1; i <= 5; i++) {
			joueurs.add(new Joueur("Joueur" + i));
		}
	}

	private static void ajouterCouleurs() {
		// On crée 4 objets de type couleur
		Couleur couleur1 = new Couleur("Coeur", "♥");
		couleurs.add(couleur1);
		couleurs.add(new Couleur("Carreau", "♦"));
		couleurs.add(new Couleur("Pique", "♠"));
		couleurs.add(new Couleur("Trèfle", "♣"));
	}

	private static void ajouterCartes() {
		// On constitue un jeu de 52 cartes
		// pour cela on parcourt la collection de couleurs
		// Pour chaque couleur de la collection de couleurs
		for (Couleur couleur : couleurs) {
			// Pour chaque couleur on crée 13 cartes
			for (int i = 2; i <= 14; i++) {
				cartes.add(new Carte(i, couleur));
			}
		}
	}
	
	public static void melangerCartes() {
		Collections.shuffle(cartes);
	}
	
	public static void afficherCartes() {
		for (Carte carte : cartes) {
			System.out.println(carte);
		}
	}	
	
}