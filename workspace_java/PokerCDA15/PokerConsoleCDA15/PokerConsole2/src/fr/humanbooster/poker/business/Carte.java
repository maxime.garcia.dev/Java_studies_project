package fr.humanbooster.poker.business;

import java.awt.Color;

public class Carte {

	// 1ère étape: on définit les attributs
	private String nom; // ex: Dame, 10, Roi
	private int valeur;
	private Couleur couleur;
	
	// 2ème étape: on ajoute le ou les constructeurs
	// Constructeur de carte avec deux paramètres
	public Carte(int valeur, Couleur couleur) {
		super();
		this.valeur = valeur;
		this.couleur = couleur;
		// Si la valeur est égale à 13, le nom doit être Roi
		// Si la valeur est égale à 14, le nom doit être As
		switch (valeur) {
		case 14:
			nom = "As";
			break;
		case 13:
			nom = "Roi";
			break;
		case 12:
			nom = "Dame";
			break;
		case 11:
			nom = "Valet";
			break;
		default:
			// On place la valeur de la carte dans une chaîne de caractères
			nom = String.valueOf(valeur);
			break;
		}
	}

	// 3ème étape: on ajoute les getters et les setters
	public String getNom() {
		return nom;
	}

	public void setNom(String nom) {
		this.nom = nom;
	}

	public int getValeur() {
		return valeur;
	}

	public void setValeur(int valeur) {
		this.valeur = valeur;
	}

	public Couleur getCouleur() {
		return couleur;
	}

	public void setCouleur(Couleur couleur) {
		this.couleur = couleur;
	}

	// 4ème étape: on ajoute la méthode toString
	@Override
	public String toString() {
		String resultat = nom + " ";
		if (couleur.getNom().equals("Coeur") || couleur.getNom().equals("Carreau")) {
			//resultat += "\033[31m";
			resultat += couleur.getSymbole();
			//resultat += "\033[30m";
		}
		else { resultat += couleur.getSymbole(); }
		return resultat;
	}
	
}