package fr.humanbooster.poker.business;

public class Couleur {

	// On définit les attributs
	private int id;
	private String nom;
	private String symbole;
	private static int compteur = 0;
	
	// On ajoute un constructeur
	// Une méthode permettant de constuire un objet de type Couleur	
	public Couleur(String nom, String symbole) {
		super();
		this.nom = nom;
		this.setSymbole(symbole);
		setId(compteur++);
	}

	// 3ème étape: on ajoute le getter et le setter
	public String getNom() {
		return nom;
	}

	public void setNom(String nom) {
		this.nom = nom;
	}

	public String getSymbole() {
		return symbole;
	}

	public void setSymbole(String symbole) {
		this.symbole = symbole;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	// 4ème étape: on ajoute la méthode toString
	@Override
	public String toString() {
		return "Couleur [nom=" + nom + "]";
	}

}