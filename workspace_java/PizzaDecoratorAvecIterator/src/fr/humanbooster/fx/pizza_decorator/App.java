package fr.humanbooster.fx.pizza_decorator;

import java.util.Scanner;

import fr.humanbooster.fx.pizza_decorator.business.Bacon;
import fr.humanbooster.fx.pizza_decorator.business.BoeufHache;
import fr.humanbooster.fx.pizza_decorator.business.Champignon;
import fr.humanbooster.fx.pizza_decorator.business.Cheddar;
import fr.humanbooster.fx.pizza_decorator.business.FruitsDeMer;
import fr.humanbooster.fx.pizza_decorator.business.Jambon;
import fr.humanbooster.fx.pizza_decorator.business.Mozzarella;
import fr.humanbooster.fx.pizza_decorator.business.Olive;
import fr.humanbooster.fx.pizza_decorator.business.Pizza;
import fr.humanbooster.fx.pizza_decorator.business.SauceBbq;
import fr.humanbooster.fx.pizza_decorator.business.SauceTomate;
import fr.humanbooster.fx.pizza_decorator.util.Pile;

public class App {
	
	static Scanner sc = new Scanner(System.in);
	
	public static void main(String[] args) {

		Pile pizzas = new Pile();
		Pizza pizza0 = new Mozzarella(new Cheddar(new Mozzarella(new Pizza())));
		pizzas.empiler(pizza0);
		System.out.println(pizza0);
		// On crée des pizzas en utilisant le patron Decorator
		// Pizza Burger
		Pizza pizza1 = new Bacon(new SauceBbq(new Cheddar(new Cheddar(new BoeufHache(new Pizza())))));
		pizzas.empiler(pizza1);
		System.out.println(pizza1);

		// Pizza Reine
		Pizza pizza2 = new Pizza();
		pizza2 = new Jambon(pizza2);
		pizza2 = new Olive(pizza2);
		pizza2 = new Champignon(pizza2);
		pizza2 = new Mozzarella(pizza2);
		pizzas.empiler(pizza2);
		System.out.println(pizza2);

		// Pizza aux fruits de mer
		Pizza pizza3 = new SauceTomate(new FruitsDeMer(new Pizza()));
		pizzas.empiler(pizza3);
		System.out.println(pizza3);

		Pizza pizza4 = new Pizza();
		pizza4 = ajouterIngredients(Mozzarella.class, pizza4);
		pizza4 = ajouterIngredients(Cheddar.class, pizza4);
		pizzas.empiler(pizza4);
		System.out.println(pizza4);
		pizzas.testerIterator();
		for (Pizza pizza : pizzas) {
			System.out.println(pizza);
		}
		sc.close();
	}

	private static Pizza ajouterIngredients(Class<? extends Pizza> ingredient, Pizza pizza) {
		System.out.println(ingredient.getSimpleName());
		int nbIngredient = 0;
		System.out.println("Combien voulez vous ajoutez d'ingr�dient?");
		nbIngredient = Integer.parseInt(sc.nextLine());
		for (int i = 0; i < nbIngredient; i++) {
			try {
				pizza = (Pizza) ingredient.getConstructor(Pizza.class).newInstance(pizza);
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
		return pizza;
	}
}
