package fr.humanbooster.fx.pizza_decorator.util;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import fr.humanbooster.fx.pizza_decorator.business.Pizza;

public class Pile implements Iterator<Pizza>, Iterable<Pizza> {
	
	private List<Pizza> stack = new ArrayList<>();
	private int offset = 0;
	
    public void empiler(Pizza e) {
        stack.add(e);
    }
   
    public Pizza depiler() {
        return (Pizza) stack.remove(stack.size()-1);
    }
    
    public void testerIterator() {
        Iterator<Pizza> iteratorTemp = stack.iterator();
        while (iteratorTemp.hasNext()) {
            Pizza e = (Pizza) iteratorTemp.next();
            System.out.println("Test iterator : "+e+" iterator.hasNext() = "+iteratorTemp.hasNext());
        }
    }
    @Override
    public boolean hasNext() {
        return (offset < stack.size());
    }
    @Override
    public Pizza next() {
        return stack.get(offset++);
    }
    @Override
    public Iterator<Pizza> iterator(){
        return stack.iterator();
    }
}