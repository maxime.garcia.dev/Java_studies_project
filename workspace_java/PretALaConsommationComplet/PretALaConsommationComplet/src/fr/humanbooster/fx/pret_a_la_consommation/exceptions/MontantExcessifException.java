package fr.humanbooster.fx.pret_a_la_consommation.exceptions;

public class MontantExcessifException extends Exception {

	/**
	 * La sérialisation est l'action d'exprimer un objet java sous un autre format (binaire, json ou xml)
	 * La desérialisation est le processus inverse
	 */
	private static final long serialVersionUID = 1L;

	public MontantExcessifException(String message) {
		super(message);
	}
	
}
