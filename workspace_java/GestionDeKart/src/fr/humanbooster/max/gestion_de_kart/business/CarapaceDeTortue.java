package fr.humanbooster.max.gestion_de_kart.business;

public class CarapaceDeTortue extends Option{

	private float PRIX = 3f;
	
	public CarapaceDeTortue(Kart kart) {
		super(kart);
		super.setPrix(kart.getPrix() + PRIX);
	}

}
