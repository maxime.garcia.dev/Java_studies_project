package fr.humanbooster.max.gestion_de_kart.business;

public class Kart {
	
	private float PRIX = 50f;
	
	private NumeroDImmatriculation numeroDImmatriculation;
	private float prix;
	
	// Constructeur par d�faut
	public Kart() {
		super();
		prix = PRIX;
	}

	public NumeroDImmatriculation getNumeroDImmatriculation() {
		return numeroDImmatriculation;
	}

	public void setNumeroDImmatriculation(NumeroDImmatriculation numeroDImmatriculation) {
		this.numeroDImmatriculation = numeroDImmatriculation;
	}

	public float getPrix() {
		return prix;
	}

	public void setPrix(float prix) {
		this.prix = prix;
	}
}
