package fr.humanbooster.max.gestion_de_kart.business;

public class LanceRoquette extends Option{
	private float PRIX = 2f;
	
	public LanceRoquette(Kart kart) {
		super(kart);
		super.setPrix(kart.getPrix() + PRIX);
	}
}
