package fr.humanbooster.max.gestion_de_kart.business;

public class Client {
	
	private Long id;
	private static Long compteur = 0L;
	private String nom;
	private String prenom;
	
	public Client() {
		super();
		id = ++compteur;
	}
	
	public Client(String nom, String prenom) {
		this();
		this.nom = nom;
		this.prenom = prenom;
	}


	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getNom() {
		return nom;
	}

	public void setNom(String nom) {
		this.nom = nom;
	}

	public String getPrenom() {
		return prenom;
	}

	public void setPrenom(String prenom) {
		this.prenom = prenom;
	}
}
