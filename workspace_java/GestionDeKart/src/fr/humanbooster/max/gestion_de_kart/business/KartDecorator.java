package fr.humanbooster.max.gestion_de_kart.business;

public class KartDecorator extends Kart{
	
	protected Kart kart;
	
	public KartDecorator(Kart kart) {
		super();
		this.kart = kart;
	}
}
