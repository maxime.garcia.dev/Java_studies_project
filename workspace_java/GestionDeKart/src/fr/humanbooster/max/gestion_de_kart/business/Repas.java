package fr.humanbooster.max.gestion_de_kart.business;

public abstract class Repas {
	
	// Attributs
	private Long id;
	private static Long compteur = 0L;
	
	// Constructeur par d�faut
	public Repas() {
		super();
		id = ++compteur;
	}
	
	// Constructeur(s)
	
	// Getters & Setters
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}
}
