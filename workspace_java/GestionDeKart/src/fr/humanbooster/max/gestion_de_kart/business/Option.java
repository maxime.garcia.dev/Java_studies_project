package fr.humanbooster.max.gestion_de_kart.business;

public abstract class Option extends KartDecorator{
	
	public Option(Kart kart) {
		super(kart);
	}
}
