package fr.humanbooster.max.gestion_de_kart.business;

public class Burger extends Repas{
	
	// Attributs
	private Long id;
	private static Long compteur = 0L;
	
	// Constructeur par d�faut
	public Burger() {
		super();
		id = ++compteur;
	}
	
	// Constructeur(s)
	
	// Getters & Setters
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}
}
