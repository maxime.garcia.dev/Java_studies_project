package fr.humanbooster.max.gestion_de_kart.business;

public class Banane extends Option {

	private float PRIX = 1f;

	public Banane(Kart kart) {
		super(kart);
		super.setPrix(kart.getPrix() + PRIX);
	}
}
