package fr.humanbooster.max.gestion_de_kart.service;

import java.util.List;

import fr.humanbooster.max.gestion_de_kart.business.Client;

public interface ClientService {

	Client ajouterClient(final String nom, final String prenom);

	List<Client> recupererClients();

	Client recupererClient(Long id);

	boolean supprimerClient(Long id);

}