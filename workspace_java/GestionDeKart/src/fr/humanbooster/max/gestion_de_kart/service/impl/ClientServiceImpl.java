package fr.humanbooster.max.gestion_de_kart.service.impl;

import java.util.ArrayList;
import java.util.List;

import fr.humanbooster.max.gestion_de_kart.business.Client;
import fr.humanbooster.max.gestion_de_kart.service.ClientService;

public class ClientServiceImpl implements ClientService {

	private List<Client> clients = new ArrayList<>();
	
	@Override
	public Client ajouterClient(final String nom, final String prenom) {
		Client client = new Client(nom, prenom);
		clients.add(client);
		return client;
	}

	@Override
	public List<Client> recupererClients() {
		return clients;
	}

	@Override
	public Client recupererClient(Long id) {
		for (Client client : clients) {
			if (client.getId().equals(id)) {
				return client;
			}
		}
		return null;
	}

	@Override
	public boolean supprimerClient(Long id) {
		Client clientASupprimer = recupererClient(id);
		if (clientASupprimer==null) {
			return false;
		} else {
			return clients.remove(clientASupprimer);
		}
	}

}
