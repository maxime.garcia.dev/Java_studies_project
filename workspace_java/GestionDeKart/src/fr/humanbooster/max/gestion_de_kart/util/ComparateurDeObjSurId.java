package fr.humanbooster.ar.x.util;

import java.util.Comparator;

import fr.humanbooster.ar.x.business.Obj;

public class ComparateurDeObjSurId implements Comparator<Obj> {
	
	@Override
	public int compare(Obj obj1, Obj obj2) {
		return Long.compare(obj1.getId(), obj2.getId());
	}

}
