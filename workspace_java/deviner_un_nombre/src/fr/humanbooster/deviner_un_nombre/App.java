package fr.humanbooster.deviner_un_nombre;

import java.util.Random;
import java.util.Scanner;

public class App {

	public static void main(String[] args) {
		//partie d�clarative//
		int nbADeviner = 0;
		int nbSaisie = 0;
		int nbEssai = 1;
		Random random = new Random();
		Scanner sc = new Scanner(System.in);
		
		//partie traitement//
		nbADeviner = random.nextInt(100) + 1;
		System.out.println("Bienvenue, veuillez saisir un nombre entre 1 et 100 inclus");
		
		
		while (nbSaisie != nbADeviner) {
			nbSaisie = sc.nextInt();
		System.out.println("vous avez saisie: " + nbSaisie);
			if (nbSaisie > nbADeviner) {
				System.out.println("c'est plus petit.");
			}
			else if (nbSaisie < nbADeviner){
				System.out.println("c'est plus grand.");
			}
			else {
				System.out.println("Bravo vous avez trouv� le nombre en " + nbEssai + " essais." );
			}
			nbEssai++;
		}
		sc.close();
	}

}
