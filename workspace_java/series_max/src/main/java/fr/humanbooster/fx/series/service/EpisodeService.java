package fr.humanbooster.fx.series.service;

import java.util.List;

import fr.humanbooster.fx.series.business.Episode;
import fr.humanbooster.fx.series.business.Saison;

public interface EpisodeService {
	
	Episode ajouterEpisode(String nom, Saison saison, int dureeEnMinutes);
	
	Episode ajouterEpisode(Episode episode);
	
	List<Episode> recupererEpisodeParSaisie(String saisie);
}
