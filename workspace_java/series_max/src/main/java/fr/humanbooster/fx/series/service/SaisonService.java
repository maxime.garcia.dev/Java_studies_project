package fr.humanbooster.fx.series.service;

import java.util.List;

import fr.humanbooster.fx.series.business.Saison;
import fr.humanbooster.fx.series.business.Serie;

public interface SaisonService {

	Saison ajouterSaison(String nom, Serie serie);

	List<Saison> recupererSaisons();
	
	List<Saison> recupererSaisonsParSerie(Long idSerie);
	
	boolean supprimerSaison(Long id);

	Saison recupererSaison(Long idSaison);
}
