&package fr.humanbooster.fx.pizza_decorator;

import java.util.Scanner;

import fr.humanbooster.fx.pizza_decorator.business.Bacon;
import fr.humanbooster.fx.pizza_decorator.business.BoeufHache;
import fr.humanbooster.fx.pizza_decorator.business.Champignon;
import fr.humanbooster.fx.pizza_decorator.business.Cheddar;
import fr.humanbooster.fx.pizza_decorator.business.FruitsDeMer;
import fr.humanbooster.fx.pizza_decorator.business.Jambon;
import fr.humanbooster.fx.pizza_decorator.business.Mozzarella;
import fr.humanbooster.fx.pizza_decorator.business.Olive;
import fr.humanbooster.fx.pizza_decorator.business.Pizza;
import fr.humanbooster.fx.pizza_decorator.business.SauceBbq;
import fr.humanbooster.fx.pizza_decorator.business.SauceTomate;

public class App {

	public static void main(String[] args) {

		Pizza pizza0 = new Mozzarella(new Cheddar(new Mozzarella(new Pizza())));
		System.out.println(pizza0);
		// On crée des pizzas en utilisant le patron Decorator
		// Pizza Burger
		Pizza pizza1 = new Bacon(new SauceBbq(new Cheddar(new Cheddar(new BoeufHache(new Pizza())))));
		System.out.println(pizza1);

		// Pizza Reine
		Pizza pizza2 = new Pizza();
		pizza2 = new Jambon(pizza2);
		pizza2 = new Olive(pizza2);
		pizza2 = new Champignon(pizza2);
		pizza2 = new Mozzarella(pizza2);
		System.out.println(pizza2);

		// Pizza aux fruits de mer
		Pizza pizza3 = new SauceTomate(new FruitsDeMer(new Pizza()));
		System.out.println(pizza3);

		int nbIngredient = 0;
		System.out.println("Combien voulez vous ajoutez d'ingr�dient?");
		Scanner sc = new Scanner(System.in);
		nbIngredient = Integer.parseInt(sc.nextLine());
		Pizza pizza4 = new Pizza();
		pizza4 = ajouterIngredients(Mozzarella.class, nbIngredient, pizza4);
		pizza4 = ajouterIngredients(Cheddar.class, nbIngredient, pizza4);
		System.out.println(pizza4);
		sc.close();
	}

	private static Pizza ajouterIngredients(Class<? extends Pizza> ingredient, int nbIngredient, Pizza pizza) {
		System.out.println(ingredient.getSimpleName());
			for (int i = 0; i < nbIngredient; i++) {
				try {
                    pizza = (Pizza) ingredient.getConstructor(Pizza.class).newInstance(pizza);
                } catch (Exception e) {
                    e.printStackTrace();
                }
			}
		return pizza;
	}
}
