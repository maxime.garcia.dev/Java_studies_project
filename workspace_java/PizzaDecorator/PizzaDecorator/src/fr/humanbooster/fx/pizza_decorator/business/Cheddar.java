package fr.humanbooster.fx.pizza_decorator.business;

public class Cheddar extends Fromage {

	private float PRIX_D_ACHAT = 0.56f;
	private float PRIX_DE_VENTE = 1.2f;

	public Cheddar(Pizza pizza) {
		super(pizza);
		super.setPrixDAchat(pizza.getPrixDAchat() + PRIX_D_ACHAT);
		super.setPrixDeVente(pizza.getPrixDeVente() + PRIX_DE_VENTE);
		// Le nouveau nom de la pizza est formé par le nom actuel
		// suivi du nom de la classe (Mozzarella)
		super.setNom(pizza.getNom() + " " + this.getClass().getSimpleName());

	}

}
