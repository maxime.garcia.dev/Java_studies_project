package fr.humanbooster.max.fenetrier.business;

import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
public class Expert extends Personne{

	@Temporal(TemporalType.DATE)
	private Date dateEmbauche;
}
