package fr.humanbooster.max.fenetrier.business;

import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Lob;
import javax.persistence.ManyToOne;

@Entity
public class Demande {
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;
	private Date dateDemande;
	private Date dateReponse;
	private float largeurEnCm;
	private float hauteurEnCm;
	private float prix;
	@Lob
	private String informationComplementaire;
	@ManyToOne
	private Materiau materiauInterieur;
	@ManyToOne
	private Materiau materiauExterieur;
	@ManyToOne
	private Couleur couleur;
	@ManyToOne
	private Vitrage vitrage;
	@ManyToOne
	private Client client;
	@ManyToOne
	private Expert expert;
}
