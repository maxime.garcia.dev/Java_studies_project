package fr.humanbooster.max.fenetrier;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class FenetrierApplication {

	public static void main(String[] args) {
		SpringApplication.run(FenetrierApplication.class, args);
	}

}
