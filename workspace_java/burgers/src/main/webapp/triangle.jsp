<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"
    import="java.util.Date"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Insert title here</title>
</head>
<body>
<%
//Nous sommes en mode Java
int nbLignes = 5;
int symbole = 1;
//On lis la valeur du paramètre nbLignes. Toutes les JSP on accès implicitement à l'objet request.
if (request.getParameter("nbLignes") != null){
	nbLignes = Integer.parseInt(request.getParameter("nbLignes"));
}
if (request.getParameter("symbole") != null){
	symbole = Integer.parseInt(request.getParameter("symbole"));
}
//TODO On lis le paramètres symbole si la valeur de symbole est égale à 1 on affiche "*", 
//si elle est égale à 2 on affiche un "o", si elle est égale a 3 on affiche le symbole "€". 
//Toutes les JSP ont acces implicitement à l'objet out.
out.print("Hello Java Server Page");
out.print("<br>");
for (int i = 1; i <= nbLignes; i++){
	for (int j = 0; j < i; j++){
		if (symbole == 1){
			out.print("*");
		}
		else if (symbole == 2){
			out.print("o");
		}
		else if (symbole == 3){
			out.print("€");
		}
	}
	out.print("<br>");
}
out.print(new Date());
%>
</body>
</html>