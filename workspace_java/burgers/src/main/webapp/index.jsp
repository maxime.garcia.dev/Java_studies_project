<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"
    import="java.util.Calendar"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Insert title here</title>
</head>
<body>
	<h1>Commande de restauration rapide</h1>
	<form action="commander">
	<%
		// Declaration du tableau de burgers
		String[] burgers = new String[4];
		burgers[0] = "Rice Burger";
		burgers[1] = "Burger Guacamole";
		burgers[2] = "Double Chicken Burger";
		burgers[3] = "BLT Burger";
		
		String[] boissons = new String[4];
	    boissons[0] = "Coca";
	    boissons[1] = "Fanta";
	    boissons[2] = "Ice-tea";
	    boissons[3] = "Tropico";

	    request.getServletContext().setAttribute("boissons", boissons);

	    String[] accompagnements = new String[4];
	    accompagnements[0] = "frite";
	    accompagnements[1] = "potatoes";
	    accompagnements[2] = "salade";
	    accompagnements[3] = "oignon rings";

	    request.getServletContext().setAttribute("accompagnements", accompagnements);
		
		//On rend disponible a toutes les JSP le tableau de burgers
		request.getServletContext().setAttribute("burgers", burgers);
	%>
	<input type="radio" id="sur_place" name="recuperation_commande" value="sur_place"><label for="sur_place">Sur place</label>
	<input type="radio" id="a_emporter" name="recuperation_commande" value="a_emporter"><label for="a_emporter">A emporter</label>
	<br>
	<label for="choix_burger">Votre Burger</label>
	<select name="choix_burger" id="choix_burger">
		<option value="-1">Aucun</option>
		<% for (int i = 0; i < burgers.length; i++){%>
				<option value="<%=i %>">
					<%=burgers[i]%>
				</option>
		<% } %>
	</select>
	<br>
	<label for="choix_accompagnement">Votre accompagnement</label>
	<select name="choix_accompagnement" id="choix_accompagnement">
       <option value="-1">Aucun</option>
       <%
       for (int i = 0; i < accompagnements.length; i++) {
       %>
       <option value="<%=i%>">
           <%=accompagnements[i]%>
       </option>
       <%
       }
       %>
    </select>
    <br>
    <label for="choix_boisson">Votre boisson</label>
    <select name="choix_boisson" id="choix_boisson">
    	<option value="-1">Aucun</option>
    	<%
       	for (int i = 0; i < boissons.length; i++) {
      	%>
        <option value="<%=i%>">
           <%=boissons[i]%>
        </option>
        <%
        }
        %>
    </select>
    <br>
    <label for="remarques">Remarques sur votre commande</label>
    <textarea name="commentaire"></textarea>
    <br>
    <label for="choix_sauces">Sauce(s)</label>
    <input type="checkbox" name="ketchup"> Ketchup
    <input type="checkbox" name="mayo"> Mayonnaise
    <br>
    <label for="numero_cb">Numéro de CB</label>
    <input type="number" name="cb" placeHolder="xxxx xxxx xxxx xxxx">
    <br>
    <label for="ID_MOIS">Mois</label>
	<select name="ID_MOIS">
		<%
		for (int i = 0; i < 12; i++) {
			out.print("<option value="+i+ ">" + (i + 1) + "</option>");
		}
		%>
	</select>
	<br>
	<label for="ID_ANNEE">Année</label>
	<select name="ID_ANNEE">
		<%
		// la méthode getInstance() prend en compte les paramètres locaux
		Calendar calendar = Calendar.getInstance();
		int year = calendar. get(Calendar. YEAR);
		// on place le calendrier en 2020
		calendar.set(Calendar.YEAR, year);
		for (int i = 0; i < 4; i++) {
			out.print("<option value=" + i + ">" + year++ + "</option>");
			
		}
		%>
	</select>
	<br>
	<label for="CRYPTOGRAMME">Cryptogramme</label>
	<input type="number" name="CRYPTOGRAMME" placeHolder="Cryptogramme">
	<br>
	<input type="submit" value="Commander">
	</form>
</body>
</html>