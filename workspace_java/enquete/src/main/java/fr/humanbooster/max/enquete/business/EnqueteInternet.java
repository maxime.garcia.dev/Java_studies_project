package fr.humanbooster.max.enquete.business;

import java.util.List;

import javax.persistence.Entity;
import javax.persistence.ManyToMany;

@Entity
public class EnqueteInternet extends Enquete{

	@ManyToMany
	private List<SitePartenaire> sitesPartenaires;
}
