package fr.humanbooster.max.enquete.business;

import javax.persistence.Entity;
import javax.validation.constraints.NotBlank;

@Entity
public class EnqueteTelephonique extends Enquete{

	@NotBlank
	private String scriptAccroche;
}
