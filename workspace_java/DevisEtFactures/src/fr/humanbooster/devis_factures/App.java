package fr.humanbooster.devis_factures;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Date;
import java.util.List;
import java.util.Random;

import fr.humanbooster.devis_factures.buisness.Client;
import fr.humanbooster.devis_factures.buisness.Devis;
import fr.humanbooster.devis_factures.buisness.Document;
import fr.humanbooster.devis_factures.buisness.Facture;
import fr.humanbooster.devis_factures.buisness.Ligne;
import fr.humanbooster.devis_factures.buisness.Prestation;
import fr.humanbooster.devis_factures.util.ComparateurDeClientsSurId;
import fr.humanbooster.devis_factures.util.ComparateurDeClientsSurNom;

public class App {

	private static final int NB_LIGNES_MAX = 3;
	private static final double QUANTITE_MAX = 10;
	private static final double REMISE_MAX = 90;
	private static final int NB_DOCUMENT_A_CREER = 10;
	private static List<Client> clients = new ArrayList<>();
	private static List<Prestation> prestations = new ArrayList<>();
	private static List<Document> documents = new ArrayList<>();
	static Random random = new Random();

	public static void main(String[] args) {
		
		ajouterClients();
		ajouterPrestations();
		ajouterDixDocumentsAleatoires(Facture.class);
		ajouterDixDocumentsAleatoires(Devis.class);
		genererUneDateAleatoire();
		afficherClientsTrierSurNom();
		afficherClientsTrierSurId();
	}
	
	private static Date genererUneDateAleatoire() {
		// la m�thode getInstance() prend en compte les param�tres locaux
        Calendar calendar = Calendar.getInstance();
        //Calendar calendar = new GregorianCalendar();
        // On enl�ve � la date courrante jusqu'� 20 ans
        calendar.set(Calendar.DAY_OF_YEAR, - random.nextInt(7300));
        Date date2 = calendar.getTime();
        // On formate la date au format fran�ais d�finit plus t�t
        return date2;
	}

	private static void afficherClientsTrierSurId() {
		Collections.sort(clients, new ComparateurDeClientsSurId());
		for (Client client : clients) {
			System.out.println(client.getId()+ " " + client.getNom());
		}
	}

	private static void afficherClientsTrierSurNom() {
		Collections.sort(clients, new ComparateurDeClientsSurNom());
		for (Client client : clients) {
			System.out.println(client.getNom());
		}
	}

	private static void ajouterPrestations() {
		Prestation presta1 = new Prestation("Audit", 800f);
		Prestation presta2 = new Prestation("Etude", 2000f);
		Prestation presta3 = new Prestation("D�veloppement", 4800f);

		prestations.add(presta1);
		prestations.add(presta2);
		prestations.add(presta3);
		System.out.println(prestations);
	}

	public static void ajouterClients() {
		Client client1 = new Client("Maxime");
		Client client2 = new Client("Adrien");
		Client client3 = new Client("Tony");

		clients.add(client1);
		clients.add(client2);
		clients.add(client3);
		System.out.println(clients);
	}

	private static void ajouterDixDocumentsAleatoires(Class<? extends Document> typeOfDoc) {
		for (int i = 0; i < NB_DOCUMENT_A_CREER; i++) {
			Document document = null;
			if (typeOfDoc == Facture.class) {
				document = new Facture(clients.get(random.nextInt(clients.size())), new Date());
			} else if (typeOfDoc == Devis.class) {
				document = new Devis(clients.get(random.nextInt(clients.size())), new Date());
			} else {
				return;
			}
			ajouterLignes(document);
			documents.add(document);
		}
	}
		
	private static void ajouterLignes(Document document) {
		for (int i = 0; i < random.nextInt(NB_LIGNES_MAX) + 1; i++) {
			new Ligne(random.nextDouble() * QUANTITE_MAX, random.nextDouble() * REMISE_MAX, document, prestations.get(random.nextInt(prestations.size())));
		}
	}
}