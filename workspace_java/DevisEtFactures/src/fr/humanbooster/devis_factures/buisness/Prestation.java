package fr.humanbooster.devis_factures.buisness;

public class Prestation {
	private Long id;
	private String nom;
	private float montant;
	private static Long compteur = 0L;
	
	public Prestation() {
		id = ++compteur;
	}
	
	public Prestation(String nom) {
		this();
		this.nom = nom;
	}

	public Prestation(String nom, float montant) {
		this();
		this.nom = nom;
		this.montant = montant;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getNom() {
		return nom;
	}

	public void setNom(String nom) {
		this.nom = nom;
	}

	public float getMontant() {
		return montant;
	}

	public void setMontant(float montant) {
		this.montant = montant;
	}

	@Override
	public String toString() {
		return "Prestation [id=" + id + ", nom=" + nom + ", montant=" + montant + "]";
	}
}
