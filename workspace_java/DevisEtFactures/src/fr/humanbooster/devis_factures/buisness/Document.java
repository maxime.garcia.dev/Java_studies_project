package fr.humanbooster.devis_factures.buisness;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public abstract class Document {
	private Long id;
	private static Long compteur = 0L;
	private Date dateCreation;
	private List<Ligne> lignes;
	private Client client;
	private float montantTotal;
	
	public Document() {
		id = ++compteur;
		dateCreation = new Date();
		lignes = new ArrayList<>();
	}
	
	public Document(Client client) {
		this();
		this.client = client;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Date getDateCreation() {
		return dateCreation;
	}

	public void setDateCreation(Date dateCreation) {
		this.dateCreation = dateCreation;
	}

	public List<Ligne> getLignes() {
		return lignes;
	}

	public void setLignes(List<Ligne> lignes) {
		this.lignes = lignes;
	}

	public Client getClient() {
		return client;
	}

	public void setClient(Client client) {
		this.client = client;
	}

	public float getMontantTotal() {
		Float montantTotal = 0F;
		for (Ligne ligne : lignes) {
			montantTotal += ((float)ligne.getPrestation().getMontant() * (float)ligne.getQuantite())*(float)(1 - ligne.getRemise()) ;
		}
		return montantTotal;
	}

	@Override
	public String toString() {
		return "Document [id=" + id + ", dateCreation=" + dateCreation + ", lignes=" + lignes + ", client=" + client
				+ ", montantTotal=" + montantTotal + "]";
	}
}
