package fr.humanbooster.devis_factures.buisness;

import java.util.ArrayList;
import java.util.List;

public class Client {
	private Long id;
	private String nom;
	private List<Document> documents;
	private static Long compteur = 0L;
	
	public Client() {
		documents = new ArrayList<>();
		id = ++compteur;
	}

	public Client(String nom) {
		this();
		this.nom = nom;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getNom() {
		return nom;
	}

	public void setNom(String nom) {
		this.nom = nom;
	}

	public List<Document> getDocuments() {
		return documents;
	}

	public void setDocuments(List<Document> documents) {
		this.documents = documents;
	}

	@Override
	public String toString() {
		return "Client [id=" + id + ", nom=" + nom + ", documents=" + documents + "]";
	}
}
