package fr.humanbooster.devis_factures.buisness;

import java.util.Date;

import fr.humanbooster.devis_factures.util.FormateurDate;

public class Devis extends Document {
	private Date dateValidite;

	public Devis() {
		super();
	}

	public Devis(Client client, Date dateValidite) {
		this();
		this.dateValidite = dateValidite;
		super.setClient(client);
	}

	public Date getDateValidite() {
		return dateValidite;
	}

	public void setDateValidite(Date dateValidite) {
		this.dateValidite = dateValidite;
	}

	public String toString() {
        return "Devis [dateValidite=" + FormateurDate.formaterDate(dateValidite) + ", nom=" + getClient().getNom() + "]";
    }
}
