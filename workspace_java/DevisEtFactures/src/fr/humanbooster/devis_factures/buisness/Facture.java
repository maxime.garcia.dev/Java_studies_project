package fr.humanbooster.devis_factures.buisness;

import java.util.Date;

import fr.humanbooster.devis_factures.util.FormateurDate;

public class Facture extends Document{
	private Date dateEcheance;
	
	public Facture() {
		super();
	}
	
	public Facture(Client client, Date dateEcheance) {
		this();
		this.dateEcheance = dateEcheance;
		super.setClient(client);
	}

	public Date getDateEcheance() {
		return dateEcheance;
	}

	public void setDateEcheance(Date dateEcheance) {
		this.dateEcheance = dateEcheance;
	}

	@Override
    public String toString() {
        return "Facture [dateEcheance=" + FormateurDate.formaterDate(dateEcheance) + ", nom=" + getClient().getNom() + "]";
    }
}
