package fr.humanbooster.devis_factures.buisness;

public class Ligne {
	private double quantite;
	private Double remise;
	private Document document;
	private Prestation prestation;
	
	public Ligne() {
	}

	public Ligne(double quantite, Double remise, Document document, Prestation prestation) {
		this();
		this.quantite = quantite;
		this.remise = remise;
		this.document = document;
		this.prestation = prestation;
	}

	public double getQuantite() {
		return quantite;
	}

	public void setQuantite(double quantite) {
		this.quantite = quantite;
	}

	public Double getRemise() {
		return remise;
	}

	public void setRemise(double remise) {
		this.remise = remise;
	}

	public Document getDocument() {
		return document;
	}

	public void setDocument(Document document) {
		this.document = document;
	}

	public Prestation getPrestation() {
		return prestation;
	}

	public void setPrestation(Prestation prestation) {
		this.prestation = prestation;
	}
	
	
}
