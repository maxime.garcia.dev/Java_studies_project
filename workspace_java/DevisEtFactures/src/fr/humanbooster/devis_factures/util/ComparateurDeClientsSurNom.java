package fr.humanbooster.devis_factures.util;

import java.util.Comparator;

import fr.humanbooster.devis_factures.buisness.Client;

public class ComparateurDeClientsSurNom implements Comparator<Client> {

	@Override
	public int compare(Client client1, Client client2) {
		return client1.getNom().compareTo(client2.getNom());
	}

}
