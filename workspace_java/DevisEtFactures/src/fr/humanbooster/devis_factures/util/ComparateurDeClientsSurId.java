package fr.humanbooster.devis_factures.util;

import java.util.Comparator;

import fr.humanbooster.devis_factures.buisness.Client;

public class ComparateurDeClientsSurId implements Comparator<Client> {

	@Override
	public int compare(Client client1, Client client2) {
		return client1.getId().compareTo(client2.getId());
	}

}
