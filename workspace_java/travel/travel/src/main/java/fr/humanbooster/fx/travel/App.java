package fr.humanbooster.fx.travel;

import java.util.Date;

import fr.humanbooster.fx.travel.service.AeroportService;
import fr.humanbooster.fx.travel.service.CompagnieService;
import fr.humanbooster.fx.travel.service.VolService;
import fr.humanbooster.fx.travel.service.impl.AeroportServiceImpl;
import fr.humanbooster.fx.travel.service.impl.CompagnieServiceImpl;
import fr.humanbooster.fx.travel.service.impl.VolServiceImpl;

public class App {

    private static AeroportService aeroportService = new AeroportServiceImpl();
    private static CompagnieService compagnieService = new CompagnieServiceImpl();
    private static VolService volService = new VolServiceImpl();

    public static void main(String[] args) {
    	
//        ajouter10Compagnies();
//        ajouter20Aeroports();
//        ajouterVol();
//        System.out.println(aeroportService.recupererAeroport(1l));
//        System.out.println(aeroportService.recupererAeroports());
//        System.out.println(compagnieService.recupererCompagnies());
        
//        System.out.println(volService.recupererVols());
       System.out.println(volService.recupererVol(1l));
    }

    public static void ajouter20Aeroports() {
        for (int i = 1; i < 21; i++) {
            aeroportService.ajouterAeroport("test " + i);
        }
    }

    public static void ajouter10Compagnies() {
        for (int i = 0; i < 10; i++) {
            compagnieService.ajouterCompagnie("Compagnie " + i);
        }
    }
    
    public static void ajouterVol() {
        Date dateDepart = new Date();
        Date dateArrivee = new Date();

        volService.ajouterVol(6, 5, 8, dateDepart, dateArrivee, 100);
    }
}