package fr.humanbooster.fx.travel.dao;

import java.sql.SQLException;
import java.util.List;

import fr.humanbooster.fx.travel.business.Aeroport;

public interface AeroportDao {
	
	public Aeroport create(Aeroport aeroport) throws SQLException;
	
	public Aeroport findOne(Long id) throws SQLException;
	
	public List<Aeroport> findAll() throws SQLException;
}
