package fr.humanbooster.fx.travel.dao.impl;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import fr.humanbooster.fx.travel.business.Compagnie;
import fr.humanbooster.fx.travel.dao.CompagnieDao;
import fr.humanbooster.fx.travel.dao.ConnexionBdd;
import fr.humanbooster.fx.travel.dao.Requetes;

public class CompagnieDaoImpl implements CompagnieDao {

	Connection connection;

	public CompagnieDaoImpl() {
		try {
			connection = ConnexionBdd.getConnection();
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}

	@Override
	public Compagnie create(Compagnie compagnie) throws SQLException {
		PreparedStatement ps = connection.prepareStatement(Requetes.AJOUT_COMPAGNIE, Statement.RETURN_GENERATED_KEYS);
		ps.setString(1, compagnie.getNom());
		ps.executeUpdate();
		ResultSet rs = ps.getGeneratedKeys();
		rs.next();
		compagnie.setId(rs.getLong(1));
		return compagnie;
	}

	@Override
	public Compagnie findOne(Long id) throws SQLException {
		Compagnie compagnie = null;
		PreparedStatement ps = connection.prepareStatement(Requetes.COMPAGNIE_PAR_ID);
		ps.setLong(1, id);
		ResultSet rs = ps.executeQuery();
		if (rs.next()) {
			compagnie = new Compagnie(rs.getLong("id"));
			compagnie.setNom(rs.getString("nom"));
		}
		return compagnie;
	}

	@Override
	public List<Compagnie> findAll() throws SQLException {
		List<Compagnie> compagnies = new ArrayList<>();
		PreparedStatement ps = connection.prepareStatement(Requetes.TOUTES_LES_COMPAGNIES);
		ResultSet rs = ps.executeQuery();
		while (rs.next()) {
			Compagnie compagnie = new Compagnie(rs.getLong("id"));
			compagnie.setNom(rs.getString("nom"));
			compagnies.add(compagnie);
		}
		return compagnies;
	}

	@Override
	public boolean deleteOne(Compagnie compagnie) throws SQLException {
		PreparedStatement ps = connection.prepareStatement(Requetes.SUPPRESSION_COMPAGNIE);
		ps.setLong(1, compagnie.getId());
		ps.executeUpdate();
		return true;
	}

}
