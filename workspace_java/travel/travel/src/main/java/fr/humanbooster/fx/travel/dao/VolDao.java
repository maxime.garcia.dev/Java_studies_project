package fr.humanbooster.fx.travel.dao;

import java.sql.SQLException;
import java.util.List;

import fr.humanbooster.fx.travel.business.Vol;

public interface VolDao {
	Vol create(Vol compagnie) throws SQLException;

	Vol findOne(Long id) throws SQLException;

	List<Vol> findAll() throws SQLException;

	boolean deleteOne(Vol vol) throws SQLException;
	
	boolean updateMontant(Vol vol, float PrixEnEuros) throws SQLException;
}
