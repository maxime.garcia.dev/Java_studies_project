package fr.humanbooster.fx.travel.business;

public class Aeroport {

	private Long id;
	private String nom;

	public Aeroport() {
		// TODO Auto-generated constructor stub
	}

	public Aeroport(String nom) {
		this();
		this.nom = nom;
	}

	public Aeroport(Long id, String nom) {
		this(nom);
		this.id = id;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getNom() {
		return nom;
	}

	public void setNom(String nom) {
		this.nom = nom;
	}

	@Override
	public String toString() {
		return "Aeroport [id=" + id + ", nom=" + nom + "]";
	}
	
}
