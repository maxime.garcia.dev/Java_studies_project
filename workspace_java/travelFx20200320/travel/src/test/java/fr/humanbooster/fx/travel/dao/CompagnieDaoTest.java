package fr.humanbooster.fx.travel.dao;

import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.sql.SQLException;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.MethodOrderer;
import org.junit.jupiter.api.Order;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestMethodOrder;

import fr.humanbooster.fx.travel.business.Compagnie;
import fr.humanbooster.fx.travel.dao.impl.CompagnieDaoImpl;

/**
 * @author Fx
 *
 *
 */
// On utilise une annotation qui fait partie du framework junit 5
// @TestMethodOrder(Alphanumeric.class)
// @TestMethodOrder(MonOrdreDeTests.class) (MonOrdreDeTests doit implémenter l'interface MethodOrderer)
@TestMethodOrder(MethodOrderer.OrderAnnotation.class)
public class CompagnieDaoTest {
	 //On test un objet de type CompagnieDao.
	private CompagnieDao compagnieDao = new CompagnieDaoImpl();
	//On déclare un objet métier.
	private static Compagnie compagnie = null;

	// @Test une annotation de JUnit: https://junit.org/junit5/docs/current/user-guide/
	@Test
	@Order(1)
	@DisplayName("teste la création d'une compagnie")
	public void testerCreate() {
		String nom = "Air Test 1"; 
		
		try {
			compagnie = compagnieDao.create(new Compagnie(nom));
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		//On passe à la phase d'évaluation.
		//On vérifie que la compagnie n'est pas null.
        assertNotNull(compagnie);
        //On vérifie que l'id de la compagnie n'est pas null.
        assertNotNull(compagnie.getId());
        //On vérifie que le nom de la compagnie correspond bien au nom donné en paramètre du constructeur.
        assertTrue(compagnie.getNom().equals(nom));
	}

	@Test
	@Order(2)
	@DisplayName("teste la suppression d'une compagnie")
	public void testerDelete() {
    	boolean aEteEfface  = false;
		try {
			aEteEfface = compagnieDao.deleteOne(compagnie);
		} catch (SQLException e) {
			e.printStackTrace();
		}
        assertNotNull(aEteEfface);
	}

}