package fr.humanbooster.fx.travel.dao;

import static org.junit.jupiter.api.Assertions.assertNotNull;

import java.sql.SQLException;
import java.util.List;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.MethodOrderer;
import org.junit.jupiter.api.Order;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestMethodOrder;

import fr.humanbooster.fx.travel.business.Aeroport;
import fr.humanbooster.fx.travel.dao.impl.AeroportDaoImpl;

@TestMethodOrder(MethodOrderer.OrderAnnotation.class)
class AeroportDaoTest {

    private static List<Aeroport> aeroports ;
    private AeroportDao aeroportDao = new AeroportDaoImpl();
    private static Aeroport aeroport = null;

    @Test
    @Order(1)
    @DisplayName("Test la méthode de création d'aeroport")
    void testCreate() {
        try {
            aeroport = aeroportDao.create(new Aeroport("Lyon"));
        } catch (SQLException e) {
            e.printStackTrace();
        }
        assertNotNull(aeroport.getNom());
    }

    @Test
    @Order(2)
    @DisplayName("Test la méthode de recherche d'un aeroport")
    void testFindOne() {
        try {
            aeroport = aeroportDao.findOne(aeroport.getId()); 
            } catch (SQLException e) {
                e.printStackTrace();
            }
         assertNotNull(aeroport);
         assertNotNull(aeroport.getId());
         assertNotNull(aeroport.getNom());
    }

    @Test
    @Order(3)
    @DisplayName("Test la méthode de recherche de la liste des aeroports")
    void testFindAll() {
        try {
            aeroports = aeroportDao.findAll();
        } catch (Exception e) {
            e.printStackTrace();
        }
        assert(aeroports.size() > 0);
        for (Aeroport aeroport : aeroports) {
            assertNotNull(aeroport);
            assertNotNull(aeroport.getId());
            assertNotNull(aeroport.getNom());
        }
    }
}
