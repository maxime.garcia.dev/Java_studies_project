package fr.humanbooster.fx.travel.dao;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.MethodOrderer;
import org.junit.jupiter.api.Order;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestMethodOrder;

import fr.humanbooster.fx.travel.business.Aeroport;
import fr.humanbooster.fx.travel.business.Compagnie;
import fr.humanbooster.fx.travel.business.Vol;
import fr.humanbooster.fx.travel.dao.impl.AeroportDaoImpl;
import fr.humanbooster.fx.travel.dao.impl.CompagnieDaoImpl;
import fr.humanbooster.fx.travel.dao.impl.VolDaoImpl;

@TestMethodOrder(MethodOrderer.OrderAnnotation.class)
class VolDaoTest {
	private CompagnieDao compagnieDao = new CompagnieDaoImpl();
	private AeroportDao aeroportDao = new AeroportDaoImpl();
	private VolDao volDao = new VolDaoImpl();
	private static Vol vol = null;
	private static Compagnie compagnie = null;
	private static Aeroport aeroport1 = null;
	private static Aeroport aeroport2 = null;
	private List<Vol> vols = new ArrayList<>();
	private Date date1 = new Date();
	private Date date2 = new Date();
	private float montant = 50;

	@Test
	@Order(1)
	@DisplayName("Test la méthode de création d'un vol")
	void testCreate() {
		try {
			compagnie = compagnieDao.create(new Compagnie("Jrespecte AIR"));
			aeroport1 = aeroportDao.create(new Aeroport("Lyon"));
			aeroport2 = aeroportDao.create(new Aeroport("Paris"));
			vol = new Vol(aeroport1, aeroport2, date1, date2, montant, compagnie);
			vol = volDao.create(vol);
		} catch (Exception e) {
			e.printStackTrace();
		}
		// On vérifie que l'objet vol n'est pas null
		assertNotNull(vol);
		// On vérifie que l'id de l'objet vol n'est pas null
		assertNotNull(vol.getId());
		// On vérifie que l'aeroport de départ vol n'est pas null
		assertNotNull(vol.getAeroportDepart());
		// On vérifie que l'objet vol n'est pas null
		assertTrue(vol.getAeroportDepart().equals(aeroport1));
		assertNotNull(vol.getAeroportArrivee());
		assertTrue(vol.getAeroportArrivee().equals(aeroport2));
		assertNotNull(vol.getDateHeureDepart());
		assertTrue(vol.getDateHeureDepart().equals(date1));
		assertNotNull(vol.getDateHeureArrivee());
		assertTrue(vol.getDateHeureArrivee().equals(date2));
		assertNotNull(vol.getCompagnie());
		assertTrue(vol.getCompagnie().equals(compagnie));
		assertNotNull(vol.getPrixEnEuros());
		assertEquals(vol.getPrixEnEuros(), montant);
	}

	@Test
    @Order(2)
	@DisplayName("Test la méthode de recherche d'un vol")
    void testFindOne() {
		Vol test = null;
        try {
            test = volDao.findOne(vol.getId());
        } catch (SQLException e) {
            e.printStackTrace();
        }
        assertNotNull(test);
        assertNotNull(test.getId());
        assertEquals(test.getId(), vol.getId());
        assertEquals(test.getPrixEnEuros(), montant);
    }
	
	@Test
	@Order(3)
	@DisplayName("Test la méthode de recherche de la liste des vols")
	void testFindAll() {
		try {
			vols = volDao.findAll();
		} catch (Exception e) {
			e.printStackTrace();
		}
		assertNotNull(vols);
		assertTrue(vols.size() > 0);
		for (Vol vol : vols) {
            assertNotNull(vol);
            assertNotNull(vol.getId());
            assertEquals(vol.getPrixEnEuros(), montant);
        }
	}
	
	@Test
    @Order(4)
    void testDeleteOne() {
        try {
            assertTrue(volDao.deleteOne(vol));
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }
}
