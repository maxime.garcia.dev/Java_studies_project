package fr.humanbooster.fx.travel.service;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.util.ArrayList;
import java.util.List;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.MethodOrderer;
import org.junit.jupiter.api.Order;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestMethodOrder;

import fr.humanbooster.fx.travel.business.Compagnie;
import fr.humanbooster.fx.travel.service.impl.CompagnieServiceImpl;

//l'annotation ci-dessous s'utilise uniquement au dessus d'une classe
@TestMethodOrder(MethodOrderer.OrderAnnotation.class)
class CompagnieServiceTest {

	private CompagnieService compagnieService = new CompagnieServiceImpl();
	private static Compagnie compagnie = null;
	private static List<Compagnie> compagnies = new ArrayList<>();
	
	@Test
	@Order(1)
	@DisplayName("test de la méthode d'ajout de compagnie")
	void testAjouterCompagnie() {
		compagnie = compagnieService.ajouterCompagnie("AIR V");
		assertNotNull(compagnie);
		assertEquals(compagnie.getNom(), "AIR V");
	}

	@Test
	@Order(2)
	@DisplayName("test de la méthode de recupération d'une compagnie")
	void testRecupererCompagnie() {
		Compagnie test = compagnieService.recupererCompagnie(compagnie.getId());
		assertNotNull(test);
		assertEquals(test.getNom(), compagnie.getNom());
		assertEquals(test.getId(), compagnie.getId());
	}

	@Test
	@Order(3)
	@DisplayName("test de la méthode de recuperation de la liste de compagnies")
	void testRecupererCompagnies() {
		compagnies = compagnieService.recupererCompagnies();
		assertNotNull(compagnies);
		assertTrue(!compagnies.isEmpty());
		for (Compagnie test : compagnies) {
			assertNotNull(test);
			assertNotNull(test.getId());
			assertNotNull(test.getNom());
		}
	}
	
	@Test
    @Order(4)
    @DisplayName("teste la modification d'un compagnie")
    public void testerModifierCompagnie() {
        compagnie.setNom("Compagnie test update");
        int nombreDeModification = 0;
        nombreDeModification = compagnieService.modifierCompagnie(compagnie);
        
        assertTrue(nombreDeModification > 0);
        Compagnie testCompagnie = compagnieService.recupererCompagnie(compagnie.getId());
        assertEquals(testCompagnie.getNom(), "Compagnie test update");
    }
	
	@Test
	@Order(5)
	@DisplayName("test de la méthode de suppression de compagnie")
	void testSupprimerCompagnie() {
		boolean resultTest = compagnieService.supprimerCompagnie(compagnie.getId());
		assertTrue(resultTest);
	}
}
