<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
    <%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Insert title here</title>
</head>
<body>
<form action="ajouterCarte" method="post">
	<label for="numero">Numéro</label>
	<input type="text" name="NUMERO" maxlength="16" required><br>
	<label for="mois">Expiration</label>
	<select name="MOIS" required>
	<c:forEach items="${mois }" var="moisCourant">
		<option value="${moisCourant }">${moisCourant}</option>
	</c:forEach>
	</select>
	<select name="ANNEE" required>
	<c:forEach items="${annees }" var="annee">
		<option value="${annee }">${annee }</option>
	</c:forEach>
	</select><br>
	<label for="cryptogramme">Cryptogramme</label>
	<input type="text" name="CRYPTOGRAMME" maxlength="3" required><br>
	<input type="submit" value="Ajouter">
</form>
</body>
</html>