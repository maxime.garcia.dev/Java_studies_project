<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
    <%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
    <%@ taglib prefix = "fmt" uri = "http://java.sun.com/jsp/jstl/fmt" %>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Insert title here</title>
</head>
<body>
<p>Client connecté : ${client.nom } ${client.prenom } <a href="deconnexion">Deconnexion</a></p>
<h2>Facture numéro ${facture.id }</h2>
<p>Montant : ${facture.montantEnEuros }</p>
<p>Date de création : <fmt:formatDate pattern="dd/MM/yyyy" value="${facture.dateCreation }" /> </p>
<p>Date d'échéance : <fmt:formatDate pattern="dd/MM/yyyy" value="${facture.dateEcheance }" /> </p>

<h3>Paiements reçus</h3>
	<table>
		<tr>
			<td>Montant</td>
			<td>Date effective</td>
			<td>Carte</td>
		</tr>
		<c:forEach items="${paiements }" var="paiement">
		<tr>
			<td>${paiement.montantEnEuros }</td>
			<td><fmt:formatDate pattern="dd/MM/yyyy" value="${paiement.dateEffective }" /></td>
			<td>${paiement.carteBancaire.numero }</td>
		</tr>
		</c:forEach>
	</table>
	<a href="tableauDeBord">Retour au tableau de bord</a>
</body>
</html>