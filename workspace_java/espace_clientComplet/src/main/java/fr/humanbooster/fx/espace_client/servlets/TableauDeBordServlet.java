package fr.humanbooster.fx.espace_client.servlets;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import fr.humanbooster.fx.espace_client.business.Client;
import fr.humanbooster.fx.espace_client.service.CarteBancaireService;
import fr.humanbooster.fx.espace_client.service.FactureService;
import fr.humanbooster.fx.espace_client.service.impl.CarteBancaireServiceImpl;
import fr.humanbooster.fx.espace_client.service.impl.FactureServiceImpl;

/**
 * Servlet implementation class TableauDeBordServlet
 */
@WebServlet("/tableauDeBord")
public class TableauDeBordServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
	private FactureService factureService = new FactureServiceImpl();
	private CarteBancaireService carteBancaireService = new CarteBancaireServiceImpl();
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public TableauDeBordServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		Client client = (Client) request.getSession().getAttribute("client");
		request.setAttribute("facturesReglees", factureService.recupererFacturesReglees(client.getId()));
		request.setAttribute("facturesARegler", factureService.recupererFacturesARegler(client.getId()));
		request.setAttribute("cartesBancaires", carteBancaireService.recupererCartesBancaires(client.getId()));
		request.getRequestDispatcher("WEB-INF/tableauDeBord.jsp").forward(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
