package fr.humanbooster.fx.espace_client.servlets;

import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import fr.humanbooster.fx.espace_client.business.Client;
import fr.humanbooster.fx.espace_client.business.Facture;
import fr.humanbooster.fx.espace_client.service.CarteBancaireService;
import fr.humanbooster.fx.espace_client.service.FactureService;
import fr.humanbooster.fx.espace_client.service.PaiementService;
import fr.humanbooster.fx.espace_client.service.impl.CarteBancaireServiceImpl;
import fr.humanbooster.fx.espace_client.service.impl.FactureServiceImpl;
import fr.humanbooster.fx.espace_client.service.impl.PaiementServiceImpl;

/**
 * Servlet implementation class ReglerFactureServlet
 */
@WebServlet("/reglerFacture")
public class ReglerFactureServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
	private FactureService factureService = new FactureServiceImpl();
	private CarteBancaireService carteBancaireService = new CarteBancaireServiceImpl();
	private PaiementService paiementService = new PaiementServiceImpl();
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public ReglerFactureServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		Facture facture = factureService.recupererFacture(Long.parseLong(request.getParameter("ID")));
		Client client = (Client) request.getSession().getAttribute("client");
		request.setAttribute("cartes", carteBancaireService.recupererCartesBancaires(client.getId()));
		System.out.println(request.getAttribute("cartes"));
		request.setAttribute("facture", facture);
		request.getRequestDispatcher("WEB-INF/reglerFacture.jsp").forward(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
		Long idFacture = Long.parseLong(request.getParameter("ID_FACTURE"));
		Long idCarte = Long.parseLong(request.getParameter("ID_CARTE"));
		Float montantEnEuros = Float.parseFloat(request.getParameter("MONTANT"));
		Date date = null;
		try {
			date = sdf.parse(request.getParameter("DATE_ECHEANCE"));
		} catch (ParseException e) {
			e.printStackTrace();
		}
		paiementService.ajouterPaiement(idFacture, idCarte, montantEnEuros, date);
		response.sendRedirect("tableauDeBord");
	}

}
