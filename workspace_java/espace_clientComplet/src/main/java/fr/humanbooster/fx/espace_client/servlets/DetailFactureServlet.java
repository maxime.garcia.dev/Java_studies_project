package fr.humanbooster.fx.espace_client.servlets;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import fr.humanbooster.fx.espace_client.business.Facture;
import fr.humanbooster.fx.espace_client.service.FactureService;
import fr.humanbooster.fx.espace_client.service.PaiementService;
import fr.humanbooster.fx.espace_client.service.impl.FactureServiceImpl;
import fr.humanbooster.fx.espace_client.service.impl.PaiementServiceImpl;

/**
 * Servlet implementation class DetailFactureServlet
 */
@WebServlet("/voirFactureReglee")
public class DetailFactureServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
	private FactureService factureService = new FactureServiceImpl();
	private PaiementService paiementService = new PaiementServiceImpl();
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public DetailFactureServlet() {
        super();
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		Facture facture = factureService.recupererFacture(Long.parseLong(request.getParameter("ID")));
		request.setAttribute("facture", facture);
		request.setAttribute("paiements", paiementService.recupererPaiements(facture.getId()));
		request.getRequestDispatcher("WEB-INF/voirFactureReglee.jsp").forward(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
