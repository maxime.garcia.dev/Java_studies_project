package fr.humanbooster.fx.espace_client.servlets;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import fr.humanbooster.fx.espace_client.business.Client;
import fr.humanbooster.fx.espace_client.service.ClientService;
import fr.humanbooster.fx.espace_client.service.impl.CarteBancaireServiceImpl;
import fr.humanbooster.fx.espace_client.service.impl.ClientServiceImpl;
import fr.humanbooster.fx.espace_client.service.impl.FactureServiceImpl;

/**
 * Servlet implementation class IndexServlet
 */
@WebServlet(urlPatterns="/index", loadOnStartup=1)
public class IndexServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
	private ClientService clientService = new ClientServiceImpl();
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public IndexServlet() {
    	new ClientServiceImpl();
        new FactureServiceImpl();
        new CarteBancaireServiceImpl();
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		request.getRequestDispatcher("WEB-INF/index.jsp").forward(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		Client client = clientService.recupererClient(request.getParameter("EMAIL"), request.getParameter("MOT_DE_PASSE"));
		if(client != null) {
			request.getSession().setAttribute("client", client);
			response.sendRedirect("tableauDeBord");
		} else {
			request.setAttribute("erreur", "Utilisateur non trouvé. Le mail ou le mot de passe est incorrect");
			doGet(request, response);
		}
	}

}
