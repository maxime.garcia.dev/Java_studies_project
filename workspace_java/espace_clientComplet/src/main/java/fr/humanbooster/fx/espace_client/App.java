package fr.humanbooster.fx.espace_client;

import java.util.Date;

import fr.humanbooster.fx.espace_client.business.CarteBancaire;
import fr.humanbooster.fx.espace_client.business.Client;
import fr.humanbooster.fx.espace_client.business.Facture;
import fr.humanbooster.fx.espace_client.business.Paiement;
import fr.humanbooster.fx.espace_client.service.CarteBancaireService;
import fr.humanbooster.fx.espace_client.service.ClientService;
import fr.humanbooster.fx.espace_client.service.FactureService;
import fr.humanbooster.fx.espace_client.service.PaiementService;
import fr.humanbooster.fx.espace_client.service.impl.CarteBancaireServiceImpl;
import fr.humanbooster.fx.espace_client.service.impl.ClientServiceImpl;
import fr.humanbooster.fx.espace_client.service.impl.FactureServiceImpl;
import fr.humanbooster.fx.espace_client.service.impl.PaiementServiceImpl;

public class App {

	private static CarteBancaireService carteBancaireService = new CarteBancaireServiceImpl();
	private static ClientService clientService = new ClientServiceImpl();
	private static FactureService factureService = new FactureServiceImpl();
	private static PaiementService paiementService = new PaiementServiceImpl();
	
	public static void main(String[] args) {
		Client client = clientService.ajouterClient("DURAND", "Samuel", "sd@sd.fr", "1234");
		Facture facture1 = factureService.ajouterFacture(client.getId(), new Date(), 50);
		Facture facture2 = factureService.ajouterFacture(client.getId(), new Date(), 50);
		CarteBancaire carte1 = carteBancaireService.ajouterCarteBancaire("4321432143214321", 2021, 4, "222", client.getId());
		
		paiementService.ajouterPaiement(facture1.getId(), carte1.getId(), 50, new Date());
		
		//System.out.println(facture1);
		
		System.out.println("Factures à régler:");
		System.out.println(factureService.recupererFacturesARegler(client.getId()));
		
		System.out.println("Factures réglées:");
		System.out.println(factureService.recupererFacturesReglees(client.getId()));

		System.out.println(paiementService.recupererPaiements(facture1.getId()));
	}

}
