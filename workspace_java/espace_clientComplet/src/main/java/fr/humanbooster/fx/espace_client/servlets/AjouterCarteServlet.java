package fr.humanbooster.fx.espace_client.servlets;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import fr.humanbooster.fx.espace_client.business.Client;
import fr.humanbooster.fx.espace_client.service.CarteBancaireService;
import fr.humanbooster.fx.espace_client.service.impl.CarteBancaireServiceImpl;

/**
 * Servlet implementation class AjouterCarteServlet
 */
@WebServlet("/ajouterCarte")
public class AjouterCarteServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
	private CarteBancaireService carteBancaireService = new CarteBancaireServiceImpl();
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public AjouterCarteServlet() {
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		Calendar calendar = Calendar.getInstance();
		List<Integer> mois = new ArrayList<>();
		List<Integer> annees = new ArrayList<>();
		for (int i = 1; i <= 12; i++) {
			mois.add(i);
		}
		for (int i = 0; i <= 5; i++) {
			annees.add(calendar.get(Calendar.YEAR) + i);
		}
		request.setAttribute("mois", mois);
		request.setAttribute("annees", annees);
		request.getRequestDispatcher("WEB-INF/ajouterCarteBancaire.jsp").forward(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		Client client = (Client) request.getSession().getAttribute("client");
		carteBancaireService.ajouterCarteBancaire(request.getParameter("NUMERO"), 
				Integer.parseInt(request.getParameter("ANNEE")), 
				Integer.parseInt(request.getParameter("MOIS")), request.getParameter("CRYPTOGRAMME"), client.getId());
		response.sendRedirect("tableauDeBord");
	}

}
