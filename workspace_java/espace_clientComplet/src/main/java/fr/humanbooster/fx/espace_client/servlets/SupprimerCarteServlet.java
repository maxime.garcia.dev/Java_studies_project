package fr.humanbooster.fx.espace_client.servlets;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import fr.humanbooster.fx.espace_client.business.CarteBancaire;
import fr.humanbooster.fx.espace_client.service.CarteBancaireService;
import fr.humanbooster.fx.espace_client.service.impl.CarteBancaireServiceImpl;

/**
 * Servlet implementation class SupprimerCarteServlet
 */
@WebServlet("/supprimerCarte")
public class SupprimerCarteServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
	private CarteBancaireService carteBancaireService = new CarteBancaireServiceImpl();
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public SupprimerCarteServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		CarteBancaire carte = carteBancaireService.recupererCarteBancaire(Long.parseLong(request.getParameter("ID")));
		request.setAttribute("carte", carte);
		request.getRequestDispatcher("WEB-INF/supprimerCarteBancaire.jsp").forward(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
