package fr.humanbooster.fx.espace_client.servlets;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import fr.humanbooster.fx.espace_client.business.Client;
import fr.humanbooster.fx.espace_client.service.CarteBancaireService;
import fr.humanbooster.fx.espace_client.service.impl.CarteBancaireServiceImpl;

/**
 * Servlet implementation class ConfirmerSuppressionCarteServlet
 */
@WebServlet("/confirmerSuppressionCarte")
public class ConfirmerSuppressionCarteServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
	private CarteBancaireService carteBancaireService = new CarteBancaireServiceImpl();
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public ConfirmerSuppressionCarteServlet() {
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		Long idCarte = Long.parseLong(request.getParameter("ID"));
		Client client = (Client) request.getSession().getAttribute("client");
		carteBancaireService.supprimerCarteBancaire(idCarte, client.getId());
		response.sendRedirect("tableauDeBord");
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
