<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Tableau de bord</title>
</head>
<body>
	<p>
		Client connecté : ${sessionScope.client.prenom}
		${sessionScope.client.nom} <a href="deconnexion">Deconnexion</a>
	</p>

	<h2>Factures à régler</h2>
	<table>
		<tr>
			<td>Numéro</td>
			<td>Date d'échéance</td>
			<td>Montant</td>
			<td>Action</td>
		</tr>
		<c:forEach items="${facturesARegler}" var="facture">
			<tr>
				<td>${facture.id}</td>
				<td><fmt:formatDate pattern="dd/MM/yyyy"
						value="${facture.dateEcheance}" /></td>
				<td>${facture.montantEnEuros}</td>
				<td><a href="reglerFacture?ID=${facture.id}">Régler</a></td>
			</tr>
		</c:forEach>
	</table>

	<h2>Factures réglées</h2>
	<table>
		<tr>
			<td>Numéro</td>
			<td>Date d'échéance</td>
			<td>Montant</td>
			<td>Action</td>
		</tr>
		<c:forEach items="${facturesReglees}" var="facture">
			<tr>
				<td>${facture.id}</td>
				<td><fmt:formatDate pattern="dd/MM/yyyy"
						value="${facture.dateEcheance}" /></td>
				<td>${facture.montantEnEuros}</td>
				<td><a href="voirFactureReglee?ID=${facture.id}">Voir</a></td>
			</tr>
		</c:forEach>
	</table>

	<h2>Cartes bancaires</h2>
	<table>
		<tr>
			<td>Numéro</td>
			<td>Expiration</td>
			<td>Cryptogramme</td>
			<td>Action</td>
		</tr>
		<c:forEach items="${cartesBancaires}" var="carte">
			<tr>
				<td>${carte.numero}</td>
				<td>${carte.moisExpiration }/${carte.anneeExpiration}</td>
				<td>${carte.cryptogramme}</td>
				<td><a href="supprimerCarte?ID=${carte.id}">Supprimer</a></td>
			</tr>
		</c:forEach>
	</table>
	<a href="ajouterCarte">Ajouter une carte bancaire</a>
</body>
</html>