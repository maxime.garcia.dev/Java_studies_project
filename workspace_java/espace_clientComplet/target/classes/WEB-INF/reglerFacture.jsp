<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Régler une facture</title>
</head>
<body>
<p>Client connecté : ${client.nom } ${client.prenom } <a href="deconnexion">Deconnexion</a></p> 
<h2>Réglement de la facture ${facture.id }</h2>
<p>Montant de la facture : ${facture.montantEnEuros }€</p>
<form action="reglerFacture" method="post">
	<label for="montant">Montant du réglement</label>
	<input type="number" name="MONTANT" max="${facture.montantEnEuros - facture.totalPaiementsRecus }" required><br>
	<label for="echeance">Date d'échéance</label>
	<input type="date" name="DATE_ECHEANCE" required><br>
	<label for="carte">Carte bancaire</label>
	<select name="ID_CARTE" required>
	<c:forEach items="${cartes}" var="carte">
		<option value="${carte.id}">${carte.numero}</option>
	</c:forEach>
	</select><br>
	<input type="hidden" name="ID_FACTURE" value="${facture.id }">
	<input type="submit" value="Régler">
</form>
</body>
</html>