package fr.humanbooster.poker.buisness;

public class Carte {
	Couleur couleur;
	private String nom;
	private int value;
	
	public Carte() {
		super();
	}
	
	public Carte(Couleur couleur, int value) {
		this();
		// si la valeur est �gale � 11 alors nom = "valet"
		// si la valeur est �gale � 12 alors nom = "dame"
		// si la valeur est �gale � 13 alors nom = "roi"
		// si la valeur est �gale � 14 alors nom = "as" 
		switch (value) {
		case 11:
			nom = "valet";
			break;
		case 12:
			nom = "dame";
			break;
		case 13:
			nom = "roi";
			break;
		case 14:
			nom = "as";
			break;
			
		default:
			nom = String.valueOf(value);
			break;
		}
		this.couleur = couleur;
		this.value = value;
	}

	public Couleur getCouleur() {
		return couleur;
	}

	public void setCouleur(Couleur couleur) {
		this.couleur = couleur;
	}

	public String getNom() {
		return nom;
	}

	public void setNom(String nom) {
		this.nom = nom;
	}

	public int getValue() {
		return value;
	}

	public void setValue(int value) {
		this.value = value;
	}

	@Override
	public String toString() {
		return nom + " de " + couleur.getNom();
	}
	
	
}
