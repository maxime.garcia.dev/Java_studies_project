package fr.humanbooster.poker.buisness;

import java.util.ArrayList;
import java.util.List;

public class Joueur {
	private List<Carte> main;
	private Long id;
	private String nom;
	private String prenom;
	private float solde;
	private static Long compteur = 0L;

	public Joueur() {
		main = new ArrayList<>();
		id = ++compteur;
	}
	/**
	 * Cette m�thode est un constructeur permettant de cr�er un joueur en pr�cisant son nom puis son pr�nom
	 * @param nom Nom du joueur
	 * @param prenom Pr�nom du joueur
	 */
	public Joueur(String nom, String prenom) {
		this();
		this.nom = nom;
		this.prenom = prenom;
	}

	public Joueur(String prenom) {
		this(null, prenom);
	}

	public Joueur(List<Carte> main) {
		super();
		this.main = main;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}
	
	public List<Carte> getMain() {
		return main;
	}

	public void setMain(List<Carte> main) {
		this.main = main;
	}

	public String getNom() {
		return nom;
	}

	public void setNom(String nom) {
		this.nom = nom;
	}

	public String getPrenom() {
		return prenom;
	}

	public void setPrenom(String prenom) {
		this.prenom = prenom;
	}

	@Override
	public String toString() {
		return "Joueur [main=" + main + ", id=" + id + ", nom=" + nom + ", prenom=" + prenom + "]";
	}

}
