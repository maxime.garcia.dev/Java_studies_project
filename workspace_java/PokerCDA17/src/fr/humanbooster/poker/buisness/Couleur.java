package fr.humanbooster.poker.buisness;

public class Couleur {
	//attribut de la classe
	private String nom;

	//constructeur : m�thode qui va permettre de cr�er unb objet de type couleur
	
	//ce constructeur est le constructeur par defaut (default constructor)
	public Couleur() {
		super();
	}
	
	
	public Couleur(String nom) {
		this();
		this.nom = nom; /*this.nom fait r�f�rence � l'attribut nom. "this sert � lever l'ambiguit� 
						entre le param�tre et l'attribut */
	}


	public String getNom() {
		return nom;
	}


	public void setNom(String nom) {
		this.nom = nom;
	}


	@Override
	public String toString() {
		return "Couleur [nom=" + nom + "]";
	}
	
}
