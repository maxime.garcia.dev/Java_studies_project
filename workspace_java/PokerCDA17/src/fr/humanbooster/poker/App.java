package fr.humanbooster.poker;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import fr.humanbooster.poker.buisness.Carte;
import fr.humanbooster.poker.buisness.Couleur;
import fr.humanbooster.poker.buisness.Joueur;

public class App {
	private static final int QUINTE_FLUSH_ROYALE = 10;
	private static final int QUINTE_FLUSH = 9;
	private static final int QUINTE = 5;
	
	// Partie d�clarative
	// Tous ce que la m�thode main touche doit �tre static
	private static List<Carte> cartes = new ArrayList<>();
	private static List<Couleur> couleurs = new ArrayList<>();
	private static List<Joueur> joueurs = new ArrayList<>();

	public static void main(String[] args) {

		constituerJeu();
		afficherJeu();
		creerJoueurs();
		afficherJoueurs();
		melangerCartes();
		afficherJeu();
		distribuerCartes();
		afficherMains();
		analyserMain(joueurs);
	}

	private static void constituerJeu() {

		Couleur coeur = new Couleur("coeur");
		Couleur pique = new Couleur("pique");
		Couleur carreau = new Couleur("carreau");
		Couleur trefle = new Couleur();
		trefle.setNom("trefle");

		couleurs.add(coeur);
		couleurs.add(pique);
		couleurs.add(carreau);
		couleurs.add(trefle);

		for (Couleur couleur : couleurs) {
			for (int i = 2; i <= 14; i++) {
				cartes.add(new Carte(couleur, i));
			}
		}

	}

	private static void afficherJeu() {
		for (Carte carte : cartes) {
			System.out.println(carte.getNom() + " de " + carte.getCouleur().getNom());
		}
	}

	private static void afficherJoueurs() {
		for (Joueur joueur : joueurs) {
			System.out.println(joueur.getPrenom());
		}
	}

	private static void afficherMains() {
		for (Joueur joueur : joueurs) {
			System.out.println(joueur.getMain());
		}
	}

	// TODO ajouter une m�thode afficherMains
	private static void creerJoueurs() {

		Joueur joueur1 = new Joueur("Garcia", "Maxime");
		Joueur joueur2 = new Joueur("Adrien");
		joueurs.add(joueur1);
		joueurs.add(joueur2);
	}

	private static void melangerCartes() {
		Collections.shuffle(cartes);
	}

	private static void distribuerCartes() {

		for (int i = 0; i < 5; i++) {
			// le foreach garanti un parcours complet de la collection
			for (Joueur joueur : joueurs) {
				joueur.getMain().add(cartes.remove(0));
			}
		}

	}

	/**
	 * cette m�thode analyse la main du joueur donn� en param�tre
	 * 
	 * @param joueurs : joueur dont on souhaite analyser la main
	 */
	private static void analyserMain(List<Joueur> joueurs) {
		int forceMain = 1;
		int newForce = 0;
		// utilisation d'un comparateur qui prend en compte une variable que l'on d�fini.
		Comparator<Carte> compareValeur = (Carte carte1, Carte carte2) -> Integer.compare(carte1.getValue(),
				carte2.getValue());
		// des qu'on fait un foreach on parcours la liste des joueurs.
		for (Joueur joueur : joueurs) {
			// On trie la main du joueur par valeur ascendante.
			Collections.sort(joueur.getMain(), compareValeur);
			if (forceMain < (newForce = checkerQuinte(joueur.getMain()))) {
				forceMain = newForce;
			}
		}
	}
	
	private static int checkerQuinte(List<Carte> main) {
		int forceRetour = 0;
		for (int i = 0; i < main.size() - 1; i++) {
			if (main.get(i).getValue() != (main.get(i + 1).getValue()) + 1) {
				return (forceRetour);
			}
		}
		forceRetour = QUINTE;
		for (int i = 0; i < main.size() - 1; i++) {
			if (main.get(i).getCouleur().getNom() != main.get(i + 1).getCouleur().getNom()) {
				return (forceRetour);
			}
		}
		forceRetour = QUINTE_FLUSH;
		if (main.get(0).getValue() == 10) {
			forceRetour = QUINTE_FLUSH_ROYALE;
		}
		return (forceRetour);
	}
}