package fr.humanbooster.fx.gestion_de_series.util;

import java.util.Comparator;

import fr.humanbooster.fx.gestion_de_series.business.Serie;

public class ComparateurDeSerieParPrix implements Comparator<Serie> {

	@Override
	public int compare(Serie serie1, Serie serie2) {
		return Float.valueOf(serie1.getPrixEnEuros()).compareTo(serie2.getPrixEnEuros());
	}


}
