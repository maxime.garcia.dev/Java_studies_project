package fr.humanbooster.fx.gestion_de_series.service.impl;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import fr.humanbooster.fx.gestion_de_series.business.Episode;
import fr.humanbooster.fx.gestion_de_series.business.Saison;
import fr.humanbooster.fx.gestion_de_series.service.EpisodeService;

public class EpisodeServiceImpl implements EpisodeService {
	
	private List<Episode> episodes = new ArrayList<>();
	
	@Override
	public Episode ajouterEpisode(Saison saison, String nom, int dureeEnMinutes, Date dateAjout) {
		
				Episode episode = new Episode(nom, dureeEnMinutes, dateAjout, saison);

				Calendar calendar = Calendar.getInstance();
				calendar.setTime(episode.getDateAjout());
				episodes.add(episode);
								
				return episode;
	}

	@Override
	public List<Episode> recupererEpisodes() {
		return episodes;
	}

	@Override
	public Episode recupererEpisode(Long id) {
		for (Episode episode : episodes) {
			if (episode.getId().equals(id)) {
				return episode;
			}
		}
		return null;
	}

	@Override
	public List<Episode> recupererEpisodes(Saison saison) {
		return saison.getEpisodes();
	}

	@Override
	public List<Episode> recupererEpisodes(Date dateDebut, Date dateFin) {
		List<Episode> episodesCorrespondants = new ArrayList<>();
		for (Episode episode : episodes) {
			if ((episode.getDateAjout().equals(dateDebut) || episode.getDateAjout().after(dateDebut))
					&& (episode.getDateAjout().equals(dateFin) || episode.getDateAjout().before(dateFin))) {
				episodesCorrespondants.add(episode);
			}
		}
		return episodesCorrespondants;
	}

	@Override
	public boolean supprimerEpisode(Long id) {
		Episode episodeASupprimer = recupererEpisode(id);
		if (episodeASupprimer==null) {
			return false;
		} else {
			Saison saison = episodeASupprimer.getSaison();
			saison.getEpisodes().remove(episodeASupprimer);
			return episodes.remove(episodeASupprimer);
		}
	}

}