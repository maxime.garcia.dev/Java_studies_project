package fr.humanbooster.fx.gestion_de_series.service;

import java.util.Date;
import java.util.List;

import fr.humanbooster.fx.gestion_de_series.business.Episode;
import fr.humanbooster.fx.gestion_de_series.business.Saison;

public interface EpisodeService {
	Episode ajouterEpisode(Saison saison, String string, int i, Date date);

	List<Episode> recupererEpisodes();

	Episode recupererEpisode(Long id);

	List<Episode> recupererEpisodes(Saison saison);

	List<Episode> recupererEpisodes(Date dateDebut, Date dateFin);

	boolean supprimerEpisode(Long id);

}
