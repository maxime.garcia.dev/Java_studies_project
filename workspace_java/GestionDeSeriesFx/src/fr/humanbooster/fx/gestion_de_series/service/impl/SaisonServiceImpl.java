package fr.humanbooster.fx.gestion_de_series.service.impl;

import java.util.ArrayList;
import java.util.List;

import fr.humanbooster.fx.gestion_de_series.business.Saison;
import fr.humanbooster.fx.gestion_de_series.business.Serie;
import fr.humanbooster.fx.gestion_de_series.service.SaisonService;

public class SaisonServiceImpl implements SaisonService {
	private List<Saison> saisons = new ArrayList<>();

	@Override
	public Saison ajouterSaison(String nom, Serie serie) {
		Saison saison = new Saison(nom, serie);
		saisons.add(saison);	
		return saison;
	}

	@Override
	public List<Saison> recupererSaisons() {
		return saisons;
	}

	@Override
	public Saison recupererSaison(Long id) {
		for (Saison saison : saisons) {
			if (saison.getId().equals(id)) {
				return saison;
			}
		}
		return null;
	}

	@Override
	public List<Saison> recupererSaisons(Serie serie) {
		return serie.getSaisons();
	}

}
