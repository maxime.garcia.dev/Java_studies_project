package fr.humanbooster.fx.gestion_de_series.business;

import java.util.ArrayList;
import java.util.List;

public class Serie implements Comparable<Serie>{

	//attributs
	private static Long compteur = 0L;
	private Long id;
	private String nom;
	private float prixEnEuros;
	private List<Saison> saisons;
	
	
	//Constructeur
	public Serie(String nom, float prixEnEuros) {
		super();
		this.id = ++compteur;
		this.nom = nom;
		this.prixEnEuros = prixEnEuros;
		this.saisons = new ArrayList<>();
	}

	//Get & Set
	public Long getId() {
		return id;
	}


	public void setId(Long id) {
		this.id = id;
	}


	public String getNom() {
		return nom;
	}


	public void setNom(String nom) {
		this.nom = nom;
	}


	public float getPrixEnEuros() {
		return prixEnEuros;
	}


	public void setPrixEnEuros(float prixEnEuros) {
		this.prixEnEuros = prixEnEuros;
	}


	public List<Saison> getSaisons() {
		return saisons;
	}


	public void setSaisons(List<Saison> saisons) {
		this.saisons = saisons;
	}
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		result = prime * result + ((nom == null) ? 0 : nom.hashCode());
		result = prime * result + Float.floatToIntBits(prixEnEuros);
		result = prime * result + ((saisons == null) ? 0 : saisons.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Serie other = (Serie) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		if (nom == null) {
			if (other.nom != null)
				return false;
		} else if (!nom.equals(other.nom))
			return false;
		if (Float.floatToIntBits(prixEnEuros) != Float.floatToIntBits(other.prixEnEuros))
			return false;
		if (saisons == null) {
			if (other.saisons != null)
				return false;
		} else if (!saisons.equals(other.saisons))
			return false;
		return true;
	}

	@Override
	public int compareTo(Serie autreSerie) {
		return this.getNom().compareTo(autreSerie.getNom());
	}

	@Override
	public String toString() {
		return "Serie [id=" + id + ", nom=" + nom + ", prixEnEuros=" + prixEnEuros + ", saisons=" + saisons + "]";
	}
	
}