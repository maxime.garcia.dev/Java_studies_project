package fr.humanbooster.fx.gestion_de_series;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Scanner;

import fr.humanbooster.fx.gestion_de_series.business.Episode;
import fr.humanbooster.fx.gestion_de_series.business.Saison;
import fr.humanbooster.fx.gestion_de_series.business.Serie;
import fr.humanbooster.fx.gestion_de_series.service.EpisodeService;
import fr.humanbooster.fx.gestion_de_series.service.SaisonService;
import fr.humanbooster.fx.gestion_de_series.service.SerieService;
import fr.humanbooster.fx.gestion_de_series.service.impl.EpisodeServiceImpl;
import fr.humanbooster.fx.gestion_de_series.service.impl.SaisonServiceImpl;
import fr.humanbooster.fx.gestion_de_series.service.impl.SerieServiceImpl;

public class App {

	private static final int AFFICHER_SERIE = 1;
	private static final int AJOUTER_EPISODE = 2;
	private static final int SUPPRIMER_EPISODE = 3;
	private static final int VOIR_SERIES_TRIES_PAR_NOM = 4;
	private static final int VOIR_SERIES_TRIEES_PAR_PRIX = 5;
	private static final int VOIR_EPISODES_ENTRE_DEUX_DATES = 6;
	private static final int QUITTER = 7;

	private static SerieService serieService = new SerieServiceImpl();
	private static SaisonService saisonService = new SaisonServiceImpl();
	private static EpisodeService episodeService = new EpisodeServiceImpl();

	private static SimpleDateFormat simpleDateFormatJour = new SimpleDateFormat("dd/MM/yyyy");
	private static Calendar calendar = Calendar.getInstance();
	private static Scanner scanner = new Scanner(System.in);

	public static void main(String[] args) {
		ajouterDonnees();

		while (true) {
			afficherMenuPrincipal();
			int choix = demanderChoix("Veuillez saisir votre choix : ", 1, QUITTER);

			switch (choix) {
			case AFFICHER_SERIE:
				afficherSeriesChoix();
				Long choixSerie = ((long) demanderChoix("Veuillez choisir une série dans la liste : ", 1,
						serieService.recupererSeries().size()));
				System.out.println(serieService.recupererSerie(choixSerie));
				break;
			case AJOUTER_EPISODE:
				ajouterEpisode();
				break;
			case SUPPRIMER_EPISODE:
				supprimerEpisode();
				break;
			case VOIR_SERIES_TRIES_PAR_NOM:
				serieService.trierSeriesParNom();
				afficherSeries();
				break;
			case VOIR_SERIES_TRIEES_PAR_PRIX:
				serieService.trierSeriesParPrix();
				afficherSeries();
				break;
			case VOIR_EPISODES_ENTRE_DEUX_DATES:
				afficherEpisodesAjoutesEntreDeuxDates();
				break;
			case QUITTER:
				System.out.println("Au revoir");
				scanner.close();
				System.exit(0);
				break;
			default:
				break;
			}
		}

	}

	private static void afficherMenuPrincipal() {
		System.out.println("Menu principal");
		System.out.println(AFFICHER_SERIE + ". afficher toutes les informations sur une serie");
		System.out.println(AJOUTER_EPISODE + ". ajouter un épisode");
		System.out.println(SUPPRIMER_EPISODE + ". supprimer un épisode");
		System.out.println(VOIR_SERIES_TRIES_PAR_NOM + ". voir les séries triées par nom");
		System.out.println(VOIR_SERIES_TRIEES_PAR_PRIX + ". voir les séries triées par prix");
		System.out.println(VOIR_EPISODES_ENTRE_DEUX_DATES + ". voir les épisodes ajoutés entre deux dates");
		System.out.println(QUITTER + ". Quitter");
	}

	private static void afficherSeries() {
		for (Serie serie : serieService.recupererSeries()) {
			System.out.println(serie);
		}
	}

	private static void afficherSeriesChoix() {
		for (Serie serie : serieService.recupererSeries()) {
			System.out.println(serie.getId() + ". " + serie.getNom());
		}
	}

	private static void afficherSaisonsSerie(Serie serie) {
		int compteur = 0;
		for (Saison saison : saisonService.recupererSaisons(serie)) {
			System.out.println(++compteur + ". " + saison.getNom());
		}
	}

	private static void afficherEpisodeSaison(Saison saison) {
		int compteur = 0;
		for (Episode episode : episodeService.recupererEpisodes(saison)) {
			System.out.println(++compteur + ". " + episode.getNom());
		}
	}

	private static void afficherEpisodesAjoutesEntreDeuxDates() {
		Date dateDebut = null;
		Date dateFin = null;
		dateDebut = demanderDate("Entrez la date de début au format dd/MM/yyyy : ", simpleDateFormatJour);
		do {
			dateFin = demanderDate("Entrez la date de fin au format dd/MM/yyyy : ", simpleDateFormatJour);

			if (dateFin.before(dateDebut))
				System.out.println("Veuillez entrer une date de fin après la date de début");
		} while (dateFin.before(dateDebut));
		List<Episode> episodes = episodeService.recupererEpisodes(dateDebut, dateFin);
		if (episodes == null || episodes.isEmpty()) {
			System.out.println("Aucun épisode n'a été trouvé");
		} else {
			for (Episode episode : episodes) {
				System.out.println(episode);
			}
		}
	}

	private static void ajouterEpisode() {
		Serie serie = null;
		int dureeEnMinutes = 0;
		Date dateAjout = null;
		Saison saison = null;
		String nom = null;

		afficherSeriesChoix();

		Long choixSerie = ((long) demanderChoix("Veuillez choisir une série parmi la liste : ", 1,
				serieService.recupererSeries().size()));

		serie = serieService.recupererSerie(choixSerie);

		afficherSaisonsSerie(serie);

		int choixSaison = demanderChoix("Veuillez choisir la saison parmi la liste : ", 1,
				saisonService.recupererSaisons(serie).size());

		saison = saisonService.recupererSaisons(serie).get(choixSaison - 1);

		System.out.println("Veuillez saisir le nom de l'épisode");

		nom = scanner.nextLine();

		do {
			System.out.println("Veuillez saisir la durée de l'épisode");
			try {
				dureeEnMinutes = Integer.parseInt(scanner.nextLine());
			} catch (Exception e) {
				System.out.println("Veuillez saisir une valeur en chiffre");
				dureeEnMinutes = 0;
			}
		} while (dureeEnMinutes <= 0);

		dateAjout = demanderDate("Veuillez saisir la date d'ajout de l'épisode au format dd/MM/yyyy : ",
				simpleDateFormatJour);

		episodeService.ajouterEpisode(nom, dureeEnMinutes, dateAjout, saison);
	}

	private static void supprimerEpisode() {
		Serie serie = null;
		Saison saison = null;
		Long choixEpisode = 0L;
		int indexEpisode = 0;

		afficherSeriesChoix();

		Long choixSerie = ((long) demanderChoix("Veuillez choisir une série parmi la liste : ", 1,
				serieService.recupererSeries().size()));

		serie = serieService.recupererSerie(choixSerie);

		afficherSaisonsSerie(serie);

		int choixSaison = demanderChoix("Veuillez choisir la saison parmi la liste : ", 1,
				saisonService.recupererSaisons(serie).size());

		saison = saisonService.recupererSaisons(serie).get(choixSaison - 1);

		afficherEpisodeSaison(saison);

		indexEpisode = demanderChoix("Veuillez choisir l'épisode parmi la liste : ", 1,
				episodeService.recupererEpisodes(saison).size());

		choixEpisode = episodeService.recupererEpisodes(saison).get(indexEpisode - 1).getId();
		episodeService.supprimerEpisode(choixEpisode);
	}

	private static void ajouterDonnees() {
		serieService.ajouterSerie("Breaking Bad", 5.0f);
		serieService.ajouterSerie("The Boys", 8.0f);
		saisonService.ajouterSaison("Saison 1", serieService.recupererSerie(1L));
		saisonService.ajouterSaison("Saison 2", serieService.recupererSerie(1L));
		saisonService.ajouterSaison("Saison 1", serieService.recupererSerie(2L));
		saisonService.ajouterSaison("Saison 2", serieService.recupererSerie(2L));
		episodeService.ajouterEpisode(saisonService.recupererSaison(1L),"lala", 20, new Date());
		episodeService.ajouterEpisode(saisonService.recupererSaison(1L),"lolo", 20, new Date());
		episodeService.ajouterEpisode(saisonService.recupererSaison(1L),"lulu", 20, new Date());
		episodeService.ajouterEpisode(saisonService.recupererSaison(1L),"lele", 20, new Date());
		episodeService.ajouterEpisode(saisonService.recupererSaison(2L),"tata", 20, new Date());
		episodeService.ajouterEpisode(saisonService.recupererSaison(2L),"toto", 20, new Date());
		episodeService.ajouterEpisode(saisonService.recupererSaison(2L),"tutu", 20, new Date());
		episodeService.ajouterEpisode(saisonService.recupererSaison(2L),"tete", 20, new Date());
		episodeService.ajouterEpisode(saisonService.recupererSaison(3L),"papa", 20, new Date());
		episodeService.ajouterEpisode(saisonService.recupererSaison(3L),"popo", 20, new Date());
		episodeService.ajouterEpisode(saisonService.recupererSaison(3L),"pupu", 20, new Date());
		episodeService.ajouterEpisode(saisonService.recupererSaison(3L),"pepe", 20, new Date());
		episodeService.ajouterEpisode(saisonService.recupererSaison(4L),"rara", 20, new Date());
		episodeService.ajouterEpisode(saisonService.recupererSaison(4L),"roro", 20, new Date());
		episodeService.ajouterEpisode(saisonService.recupererSaison(4L),"ruru", 20, new Date());
		episodeService.ajouterEpisode(saisonService.recupererSaison(4L),"rere", 20, new Date());
	}

	private static int demanderChoix(String message, int borneMin, int borneMax) {
		int valeur = borneMin - 1;
		do {
			System.out.print(message);
			try {
				String saisie = scanner.nextLine();
				valeur = Integer.parseInt(saisie);
				if (valeur < borneMin || valeur > borneMax) {
					System.out.println("Merci de saisir un nombre compris entre " + borneMin + " et " + borneMax);
				}
			} catch (Exception e) {
				System.out.println("Merci de saisir un nombre");
			}

		} while (!(valeur >= borneMin && valeur <= borneMax));
		return valeur;
	}

	private static Date demanderDate(String message, SimpleDateFormat simpleDateFormat) {
		// On déclare un objet de type Date.
		Date date = null;
		// On déclare un objet de type Calendar et on place le calendrirer à la date et
		// heure actuelle.
		Calendar calendar = Calendar.getInstance();
		// On place le calendrier sur l'année 2000
		calendar.set(Calendar.YEAR, 2000);

		do {
			// On affiche le message donné en paramètre.
			System.out.println(message);
			try {// On essaye de transformer la chaine saisie par l'utilisateur en objet de type
					// Date.
				date = simpleDateFormat.parse(scanner.nextLine());
			} catch (ParseException e) {
				System.out.println("Date au mauvais format");
			}
			if (date != null) {
				// On place le calendrier sur la date correspondant à l'objet date.
				calendar.setTime(date);
			}
		} while (!(date != null));

		calendar.set(Calendar.HOUR_OF_DAY, 0);
		calendar.set(Calendar.MINUTE, 0);
		calendar.set(Calendar.SECOND, 0);
		return calendar.getTime();
	}
}
