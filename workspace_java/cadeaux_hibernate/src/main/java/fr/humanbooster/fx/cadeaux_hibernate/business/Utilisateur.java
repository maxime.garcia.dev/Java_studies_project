package fr.humanbooster.fx.cadeaux_hibernate.business;

import java.util.Date;
import java.util.List;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
public class Utilisateur {
    
    @Id
    //permet d'auto incrementer la colonne id
    @GeneratedValue(strategy=GenerationType.IDENTITY)
    private Long id;
    private String nom;
    private String prenom;
    private String motDepasse;
    private Date dateInscription;
    @Temporal(TemporalType.DATE)
    private Date dateDeNaissance;
    @ManyToOne
    private Ville villeDeNaissance;
    @ManyToOne
    private Ville villeDeResidence;
    
    @OneToMany(mappedBy="utilisateur")
    private List<Commande> commandes;
    
    public Utilisateur() {
		// TODO Auto-generated constructor stub
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getNom() {
		return nom;
	}

	public void setNom(String nom) {
		this.nom = nom;
	}

	public String getPrenom() {
		return prenom;
	}

	public void setPrenom(String prenom) {
		this.prenom = prenom;
	}

	public String getMotDepasse() {
		return motDepasse;
	}

	public void setMotDepasse(String motDepasse) {
		this.motDepasse = motDepasse;
	}

	public Date getDateInscription() {
		return dateInscription;
	}

	public void setDateInscription(Date dateInscription) {
		this.dateInscription = dateInscription;
	}

	public Date getDateDeNaissance() {
		return dateDeNaissance;
	}

	public void setDateDeNaissance(Date dateDeNaissance) {
		this.dateDeNaissance = dateDeNaissance;
	}

	public Ville getVilleDeNaissance() {
		return villeDeNaissance;
	}

	public void setVilleDeNaissance(Ville villeDeNaissance) {
		this.villeDeNaissance = villeDeNaissance;
	}

	public Ville getVilleDeResidence() {
		return villeDeResidence;
	}

	public void setVilleDeResidence(Ville villeDeResidence) {
		this.villeDeResidence = villeDeResidence;
	}

	public List<Commande> getCommandes() {
		return commandes;
	}

	public void setCommandes(List<Commande> commandes) {
		this.commandes = commandes;
	}

	@Override
	public String toString() {
		return "Utilisateur [id=" + id + ", nom=" + nom + ", prenom=" + prenom + ", motDepasse=" + motDepasse
				+ ", dateInscription=" + dateInscription + ", dateDeNaissance=" + dateDeNaissance
				+ ", villeDeNaissance=" + villeDeNaissance + ", villeDeResidence=" + villeDeResidence + "]";
	}
    
    
}