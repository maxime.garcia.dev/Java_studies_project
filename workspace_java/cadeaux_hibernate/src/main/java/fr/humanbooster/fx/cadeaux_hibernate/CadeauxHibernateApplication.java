package fr.humanbooster.fx.cadeaux_hibernate;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class CadeauxHibernateApplication {

	public static void main(String[] args) {
		SpringApplication.run(CadeauxHibernateApplication.class, args);
	}

}
