package fr.humanbooster.fx.cadeaux_hibernate.business;

import java.util.List;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;

@Entity
public class Ville {

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private Long id;
	private String nom;
	//@Column(name ="nombre_habitants")
	private int nbHabitants;
	@ManyToOne
	private Departement departement;
	@OneToMany(mappedBy="villeDeNaissance")
	private List<Utilisateur> utilisateurNee;
	@OneToMany(mappedBy="villeDeResidence")
	private List<Utilisateur> utilisateurResident;
	
	public Ville() {
		// TODO Auto-generated constructor stub
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getNom() {
		return nom;
	}

	public void setNom(String nom) {
		this.nom = nom;
	}

	public int getNbHabitants() {
		return nbHabitants;
	}

	public void setNbHabitants(int nbHabitants) {
		this.nbHabitants = nbHabitants;
	}

	public Departement getDepartement() {
		return departement;
	}

	public void setDepartement(Departement departement) {
		this.departement = departement;
	}

	public List<Utilisateur> getUtilisateurNee() {
		return utilisateurNee;
	}

	public void setUtilisateurNee(List<Utilisateur> utilisateurNee) {
		this.utilisateurNee = utilisateurNee;
	}

	public List<Utilisateur> getUtilisateurResident() {
		return utilisateurResident;
	}

	public void setUtilisateurResident(List<Utilisateur> utilisateurResident) {
		this.utilisateurResident = utilisateurResident;
	}

	@Override
	public String toString() {
		return "Ville [id=" + id + ", nom=" + nom + ", nbHabitants=" + nbHabitants + ", departement=" + departement
				+ "]";
	}
}