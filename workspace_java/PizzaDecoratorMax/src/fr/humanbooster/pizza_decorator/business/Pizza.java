package fr.humanbooster.pizza_decorator.business;

public class Pizza {
	public static final Float PRIX_D_ACHAT = 0.1f;
	public static final Float PRIX_DE_VENTE = 1f;
	
	private Float prixDeVente;
	private Float prixDAchat;
	private String nom;
	
	public Pizza() {
		nom = "Pizza";
		prixDeVente = PRIX_DE_VENTE;
		prixDAchat = PRIX_D_ACHAT;
	}
	public Pizza(Float prixDeVente, Float prixDAchat, String nom) {
		this();
		this.prixDeVente = prixDeVente;
		this.prixDAchat = prixDAchat;
		this.nom = nom;
	}
	public Float getPrixDeVente() {
		return prixDeVente;
	}
	public void setPrixDeVente(Float prixDeVente) {
		this.prixDeVente = prixDeVente;
	}
	public Float getPrixDAchat() {
		return prixDAchat;
	}
	public void setPrixDAchat(Float prixDAchat) {
		this.prixDAchat = prixDAchat;
	}
	public String getNom() {
		return nom;
	}
	public void setNom(String nom) {
		this.nom = nom;
	}
	@Override
	public String toString() {
		return "Pizza [prixDeVente=" + prixDeVente + ", prixDAchat=" + prixDAchat + ", nom=" + nom + "]";
	}

}
