package fr.humanbooster.pizza_decorator.business;

public abstract class Viande extends PizzaDecorator {
	
	public Viande(Pizza pizza) {
        super(pizza);
    }
}
