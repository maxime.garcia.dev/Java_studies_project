package fr.humanbooster.pizza_decorator.business;

public abstract class PizzaDecorator extends Pizza {
	
	protected Pizza pizza;
	
	public PizzaDecorator(Pizza pizza) {
		super();
		this.pizza = pizza;
	}

	public Pizza getPizza() {
		return pizza;
	}

	public void setPizza(Pizza pizza) {
		this.pizza = pizza;
	}
}
