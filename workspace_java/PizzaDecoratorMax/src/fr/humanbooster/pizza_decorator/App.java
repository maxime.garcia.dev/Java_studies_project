package fr.humanbooster.pizza_decorator;

import fr.humanbooster.pizza_decorator.business.Artichaut;
import fr.humanbooster.pizza_decorator.business.Mozzarella;
import fr.humanbooster.pizza_decorator.business.Pizza;
import fr.humanbooster.pizza_decorator.business.Poivron;

public class App {

	public static void main(String[] args) {
		
		Pizza pizza1 = new Pizza();
		System.out.println(pizza1);
		
		Pizza pizza2 = new Mozzarella(new Pizza());
		System.out.println(pizza2);
		
		Pizza pizza3 = new Mozzarella (new Mozzarella(new Pizza()));
		System.out.println(pizza3);
		
		Pizza pizza4 = new Artichaut(new Mozzarella (new Mozzarella(new Pizza())));
		System.out.println(pizza4);
		
		Pizza pizza5 = new Pizza();
		pizza5 = new Artichaut(pizza5);
		pizza5 = new Poivron(pizza5);
		pizza5 = new Mozzarella(pizza5);
		System.out.println(pizza5);
	}

}
