package fr.humanbooster.cafe_factory.factory;

import fr.humanbooster.cafe_factory.business.Cappucino;
import fr.humanbooster.cafe_factory.business.Espresso;
import fr.humanbooster.cafe_factory.business.Macchiato;

public class CafeFactory {
	public Cafe getCafe(String cafeType, String torrefaction) {
		
		
		if (cafeType == null || cafeType.equals("")) {
			return null;
		}
		else if (cafeType.equalsIgnoreCase("espresso")) {
			return new Espresso(torrefaction);
		}
		else if (cafeType.equalsIgnoreCase("cappucino")) {
			return new Cappucino(torrefaction);
		}
		else if (cafeType.equalsIgnoreCase("macchiato")) {
			return new Macchiato(torrefaction);
		}
		else {
			return null;
		}
	}
}
