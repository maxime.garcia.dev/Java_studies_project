package fr.humanbooster.cafe_factory.factory;

public interface Cafe {
	public String getTorrefaction();
	public void makeCoffee();
}
