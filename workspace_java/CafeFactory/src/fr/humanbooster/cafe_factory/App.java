package fr.humanbooster.cafe_factory;

import fr.humanbooster.cafe_factory.factory.Cafe;
import fr.humanbooster.cafe_factory.factory.CafeFactory;

public class App {

	public static void main(String[] args) {
		CafeFactory cafeFactory = new CafeFactory();
		
		Cafe cafe1 = cafeFactory.getCafe("cappucino", "brune");
		cafe1.makeCoffee();
		System.out.println("torrefaction " + cafe1.getTorrefaction());
		
		Cafe cafe2 = cafeFactory.getCafe("espresso", "blonde");
		cafe2.makeCoffee();
		System.out.println("torrefaction " + cafe2.getTorrefaction());
		
		Cafe cafe3 = cafeFactory.getCafe("MaCcHiato", "ambre");
		cafe3.makeCoffee();
		System.out.println("torrefaction " + cafe3.getTorrefaction());
	}

}
