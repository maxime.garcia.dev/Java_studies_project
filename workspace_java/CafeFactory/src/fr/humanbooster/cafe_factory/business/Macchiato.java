package fr.humanbooster.cafe_factory.business;

import fr.humanbooster.cafe_factory.factory.Cafe;

public class Macchiato implements Cafe{

	protected String torrefaction;
	
	public Macchiato(String torrefaction) {
		this.torrefaction = torrefaction;
	}
	
	@Override
	public String getTorrefaction() {
		return this.torrefaction;
	}

	@Override
	public void makeCoffee() {
		System.out.println("Make macchiato");
	}
}
