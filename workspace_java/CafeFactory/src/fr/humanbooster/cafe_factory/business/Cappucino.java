package fr.humanbooster.cafe_factory.business;

import fr.humanbooster.cafe_factory.factory.Cafe;

public class Cappucino implements Cafe {
	
	protected String torrefaction;

	public Cappucino(String torrefaction) {
		this.torrefaction = torrefaction;
	}

	@Override
	public String getTorrefaction() {
		return this.torrefaction;
	}

	@Override
	public void makeCoffee() {
		System.out.println("Make cappucino");
	}
}
