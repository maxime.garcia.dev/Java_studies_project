package fr.humanbooster.pile_generique.util;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

public class PileGeneriquePlus<E> implements Iterator<E>, Iterable<E>{
    private List<E> stack = new ArrayList<>();
    /**
     * Cette m�thode ajoute � la collection l'�l�ment e donn� en param�tre. Cet �l�ment sera ajout� au sommet de la pile
     * @param e l'�l�ment � ajouter � la pile
     */
    public void empiler(E e) {
        stack.add(e);
    }
    /**
     * Cette m�thode retire l'�l�ment qui se trouve � la fin de la collection, donc au sommet de la pile
     * @return un �l�ment de type E .
     */
    public E depiler() {
        return (E) stack.remove(stack.size()-1);
    }
    
    public void testerIterator() {
        Iterator<E> iteratorTemp = stack.iterator();
        while (iteratorTemp.hasNext()) {
            E e = (E) iteratorTemp.next();
            System.out.println("Test iterator : "+e+" iterator.hasNext() = "+iteratorTemp.hasNext());
        }
    }
    @Override
    public boolean hasNext() {
        return stack.iterator().hasNext();
    }
    @Override
    public E next() {
        return stack.iterator().next();
    }
    @Override
    public Iterator<E> iterator(){
        return stack.iterator();
    }
}