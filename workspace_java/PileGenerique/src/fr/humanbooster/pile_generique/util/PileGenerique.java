package fr.humanbooster.pile_generique.util;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

/**
 * Cette classe est g�n�rique
 * Elle peut accueillir tous types d'objet.
 * @author HB
 *
 * @param <E>
 */
public class PileGenerique<E> implements Iterator<E>{
	
	
	private List<E> stack = new ArrayList<>();
	
	public void empiler(E e) {
		stack.add(e);
	}
	
	public E depiler() {
		E e = stack.get(stack.size() - 1);
		stack.remove(stack.size() - 1);
		return e;
	}
	

    
    public void testerIterator() {
    	Iterator<E> iteratorTemp = stack.iterator();
		while (iteratorTemp.hasNext()) {
			E e = iteratorTemp.next();
			System.out.println("Test iterator : " +e+" iterator.hasNext() = "+iteratorTemp.hasNext());
		}
    }

	@Override
	public boolean hasNext() {
		return stack.iterator().hasNext();
	}

	@Override
	public E next() {
		return stack.iterator().next();
	}
}
