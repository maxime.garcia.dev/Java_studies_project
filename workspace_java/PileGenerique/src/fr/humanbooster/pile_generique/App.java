package fr.humanbooster.pile_generique;

import fr.humanbooster.pile_generique.util.PileGenerique;

public class App {

	public static void main(String[] args) {
		PileGenerique<Long> pileGenerique1 = new PileGenerique<>();
		PileGenerique<String> pileGenerique2 = new PileGenerique<>();
		
		pileGenerique1.empiler(1L);
		pileGenerique1.empiler(5L);
		pileGenerique2.empiler("bonjour");
		pileGenerique2.empiler("hello");
		pileGenerique1.testerIterator();
		pileGenerique2.testerIterator();
		
		pileGenerique1.depiler();
		pileGenerique2.depiler();
	}

}
