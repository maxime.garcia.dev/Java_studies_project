package fr.humanbooster.fx.villes;

import java.io.BufferedInputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

import org.apache.commons.csv.CSVFormat;
import org.apache.commons.csv.CSVParser;
import org.apache.commons.csv.CSVRecord;

import fr.humanbooster.fx.villes.business.Ville;

/**
 * Villes
 *
 */
public class App 
{
	private static List<Ville> villes = new ArrayList<>();
	private static Scanner scanner = new Scanner(System.in);
	
    public static void main( String[] args )
    {
    	chargerVilles();
    	System.out.println("Il y a " + villes.size() + " villes en mémoire");
    	afficherVilles(recupererVilles(demanderCodePostal()));
    }

	private static void afficherVilles(List<Ville> villesCorrespondantes) {
		for (Ville ville : villesCorrespondantes) {
			System.out.println(ville);
		}
	}

	private static List<Ville> recupererVilles(String codePostal) {
		List<Ville> villesCorrespondantes = new ArrayList<>();
		for (Ville ville : villes) {
			if (ville.getCodePostal().equals(codePostal)) {
				villesCorrespondantes.add(ville);
			}
		}
		return villesCorrespondantes;
	}

	private static String demanderCodePostal() {
		System.out.print("Veuillez saisir un code postal : ");
		String codePostalSaisi = scanner.nextLine();
		scanner.close();
		return codePostalSaisi;
	}

	private static void chargerVilles() {
		
		try {
			// On crée un objet de type URL
            URL url = new URL("https://www.clelia.fr/villes.csv");

			// On crée un lecteur à partir du flux d'entrée générée par l'URL
			// == méthode GET du protocole HTTP
			Reader reader = new InputStreamReader(new BufferedInputStream(url.openStream()), "UTF-8");

			// On déclare un format de CSV
			CSVFormat csvFormat = CSVFormat.DEFAULT.withDelimiter(';').withFirstRecordAsHeader();
			
			// On crée un parser en donnant en paramètre le reader et le format CSV
			CSVParser csvParser = new CSVParser(reader, csvFormat);

			// On parcourt le csvParser, on ajoute un objet de type Ville à liste villes
			for (CSVRecord record : csvParser) {
				Ville ville = new Ville(record.get(0), record.get(1));
				villes.add(ville);
			}
			csvParser.close();
			reader.close();

		}
		catch (Exception e) {
			e.printStackTrace();
		}
	}	
}