package fr.humanbooster.max.mire.dao;

public class Requetes {
	public static final String AJOUT_UTILISATEUR ="INSERT INTO utilisateur (nom, prenom, email, motDePasse) VALUES (?,?,?,?)";
	public static final String UTILISATEUR_PAR_ID="SELECT id, nom, prenom, email, motDePasse FROM utilisateur WHERE id=?";
	public static final String TOUS_LES_UTILISATEURS="SELECT id, nom, prenom, email, motDePasse FROM utilisateur";
}
