package fr.humanbooster.max.mire.dao;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class ConnectionBDD {
	public static Connection getConnection() throws SQLException, ClassNotFoundException {

		Class.forName("com.mysql.cj.jdbc.Driver");
		Connection connection = DriverManager.getConnection(
				"jdbc:mysql://localhost:3307/mire?verifyServerCertificate=false&useSSL=true", "root", "");
		return connection;
	}

	public static void close() throws SQLException, ClassNotFoundException {
		getConnection().close();
	}
}
