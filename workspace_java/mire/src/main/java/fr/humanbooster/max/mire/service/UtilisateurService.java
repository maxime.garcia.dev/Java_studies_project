package fr.humanbooster.max.mire.service;

import java.util.List;

import fr.humanbooster.max.mire.business.Utilisateur;

public interface UtilisateurService {

	Utilisateur ajouterUtilisateur(Utilisateur utilisateur);
	
	Utilisateur recupererUtilisateur(Long id);
	
	List<Utilisateur> recupererListeUtilisateurs();
}
