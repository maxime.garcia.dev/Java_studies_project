package fr.humanbooster.max.mire.dao;

import java.sql.SQLException;
import java.util.List;

import fr.humanbooster.max.mire.business.Utilisateur;

public interface UtilisateurDao {
	
	Utilisateur create(Utilisateur utilisateur) throws SQLException;
	
	Utilisateur findOne(Long id) throws SQLException;
	
	List<Utilisateur> findAll() throws SQLException;
}
