package fr.humanbooster.max.mire.servlet;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import fr.humanbooster.max.mire.business.Utilisateur;
import fr.humanbooster.max.mire.service.UtilisateurService;
import fr.humanbooster.max.mire.service.impl.UtilisateurServiceImpl;

/**
 * Servlet implementation class InscriptionServlet
 * Une servlet est une classe Java capable de traiter une requete HTTP
 * et de formuler une réponse
 * La servlet correspond au controleur dans le modèle MVC
 * M (Modèle) correspond à l'ensemble des classes business
 * V (Vues) correspond à l'ensmble des fichers dans src/main/webapp
 * C (Controleur) correspond à l'ensemble des classe dans le package servlet
 */

@WebServlet("/inscription")
public class InscriptionServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       //La servlet utilise des objets de type service
	private UtilisateurService utilisateurService = new UtilisateurServiceImpl();
    /**
     * @see HttpServlet#HttpServlet()
     */
    public InscriptionServlet() {
        super();
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		response.getWriter().append("Served at: ").append(request.getContextPath());
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	//Cette méthode traite les requêtes HTTP ayant la méthode post
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		//On récupère la donnée saisie sur le champs "nom"
		String nom = request.getParameter("nom");
		String prenom = request.getParameter("prenom");
		String email = request.getParameter("email");
		String motDePasse = request.getParameter("pwd");
		
		Utilisateur utilisateur = new Utilisateur(nom, prenom, email, motDePasse);
		utilisateurService.ajouterUtilisateur(utilisateur);
		response.getWriter().append(nom + " " + prenom + " souhaite s'inscrire" + "<br>" + email);
	}

}
