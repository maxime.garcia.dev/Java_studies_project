package fr.humanbooster.max.mire.dao.impl;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import fr.humanbooster.max.mire.business.Utilisateur;
import fr.humanbooster.max.mire.dao.ConnectionBDD;
import fr.humanbooster.max.mire.dao.Requetes;
import fr.humanbooster.max.mire.dao.UtilisateurDao;

public class UtilisateurDaoImpl implements UtilisateurDao {

	private Connection connection;
	
	public UtilisateurDaoImpl() {
		try {
			connection = ConnectionBDD.getConnection();
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}
	
	@Override
	public Utilisateur create(Utilisateur utilisateur) throws SQLException {
		PreparedStatement ps = connection.prepareStatement(Requetes.AJOUT_UTILISATEUR, Statement.RETURN_GENERATED_KEYS);
		ps.setString(1, utilisateur.getNom());
		ps.setString(2, utilisateur.getPrenom());
		ps.setString(3, utilisateur.getEmail());
		ps.setString(4, utilisateur.getMotDePasse());
		ps.executeUpdate();
		ResultSet rs = ps.getGeneratedKeys();
		rs.next();
		utilisateur.setId(rs.getLong(1));
		return utilisateur;
	}

	@Override
	public Utilisateur findOne(Long id) throws SQLException {
		Utilisateur utilisateur = null;
		PreparedStatement ps = connection.prepareStatement(Requetes.UTILISATEUR_PAR_ID);
		ps.setLong(1, id);
		ResultSet rs = ps.executeQuery();
		if (rs.next()) {
			utilisateur = new Utilisateur(rs.getLong("id"));
			utilisateur.setNom(rs.getString("nom"));
			utilisateur.setPrenom(rs.getString("prenom"));
			utilisateur.setEmail(rs.getString("email"));
			utilisateur.setMotDePasse(rs.getString("motDePasse"));
		}
		return utilisateur;
	}

	@Override
	public List<Utilisateur> findAll() throws SQLException {
		List<Utilisateur> utilisateurs = new ArrayList<>();
		PreparedStatement ps = connection.prepareStatement(Requetes.TOUS_LES_UTILISATEURS);
		ResultSet rs = ps.executeQuery();
		while (rs.next()) {
			Utilisateur utilisateur = new Utilisateur(rs.getLong("id"));
			utilisateur.setNom(rs.getString("nom"));
			utilisateur.setPrenom(rs.getString("prenom"));
			utilisateur.setEmail(rs.getString("email"));
			utilisateur.setMotDePasse(rs.getString("motDePasse"));
			utilisateurs.add(utilisateur);
		}
		return utilisateurs;
	}

}
