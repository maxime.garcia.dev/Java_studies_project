package fr.humanbooster.max.mire.service.impl;

import java.util.List;

import fr.humanbooster.max.mire.business.Utilisateur;
import fr.humanbooster.max.mire.dao.UtilisateurDao;
import fr.humanbooster.max.mire.dao.impl.UtilisateurDaoImpl;
import fr.humanbooster.max.mire.service.UtilisateurService;

public class UtilisateurServiceImpl implements UtilisateurService {

	private UtilisateurDao utilisateurDao = new UtilisateurDaoImpl();
	
	@Override
	public Utilisateur ajouterUtilisateur(Utilisateur utilisateur) {
		try {
			return utilisateurDao.create(utilisateur);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}

	@Override
	public Utilisateur recupererUtilisateur(Long id) {
		try {
			return utilisateurDao.findOne(id);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}

	@Override
	public List<Utilisateur> recupererListeUtilisateurs() {
		try {
			return utilisateurDao.findAll();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}

}
