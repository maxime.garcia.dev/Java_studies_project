package fr.humanbooster.max.mire.dao;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;

import java.util.ArrayList;
import java.util.List;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.MethodOrderer;
import org.junit.jupiter.api.Order;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestMethodOrder;

import fr.humanbooster.max.mire.business.Utilisateur;
import fr.humanbooster.max.mire.dao.impl.UtilisateurDaoImpl;

@TestMethodOrder(MethodOrderer.OrderAnnotation.class)
public class UtilisateurDaoTest {

	private UtilisateurDao utilisateurDao = new UtilisateurDaoImpl();
	private static Utilisateur utilisateur = null;
	
	@Test
	@Order(1)
	@DisplayName("Test méthode de creation d'utilisateur")
	public void testerCreate() {
		utilisateur = new Utilisateur();
		String nom = "VOLETOI";
		String prenom = "Ferdinand";
		String email = "FerdinandVoletoiLoin@yahoo.fr";
		String motDePasse = "Heudebert";
		try {
			utilisateur.setNom(nom);
			utilisateur.setPrenom(prenom);
			utilisateur.setEmail(email);
			utilisateur.setMotDePasse(motDePasse);
			utilisateur = utilisateurDao.create(utilisateur);
		} catch (Exception e) {
			e.printStackTrace();
		}
		assertNotNull(utilisateur);
		assertNotNull(utilisateur.getId());
		assertNotNull(utilisateur.getNom());
		assertNotNull(utilisateur.getPrenom());
		assertNotNull(utilisateur.getEmail());
		assertNotNull(utilisateur.getMotDePasse());
		assertEquals(utilisateur.getNom(), nom);
		assertEquals(utilisateur.getPrenom(), prenom);
		assertEquals(utilisateur.getEmail(), email);
		assertEquals(utilisateur.getMotDePasse(), motDePasse);
	}
	
	@Test
	@Order(2)
	@DisplayName("Test méthode de récupération d'un utilisateur")
	public void testerFindOne() {
		Utilisateur test = null;
		try {
			test = utilisateurDao.findOne(utilisateur.getId());
		} catch (Exception e) {
			e.printStackTrace();
		}
		assertNotNull(test);
		assertNotNull(test.getId());
		assertNotNull(test.getNom());
		assertNotNull(test.getPrenom());
		assertNotNull(test.getEmail());
		assertEquals(utilisateur.getNom(), test.getNom());
		assertEquals(utilisateur.getPrenom(), test.getPrenom());
		assertEquals(utilisateur.getEmail(), test.getEmail());
		assertEquals(utilisateur.getMotDePasse(), test.getMotDePasse());	
	}
	
	@Test
	@Order(3)
	@DisplayName("Test méthode de recupération de la liste des utilisateurs")
	public void testerFindAll() {
		List<Utilisateur> utilisateurs = new ArrayList<>();
	}
}
