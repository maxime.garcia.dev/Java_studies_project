#------------------------------------------------------------
#        Script MySQL.
#------------------------------------------------------------


#------------------------------------------------------------
# Table: groupe
#------------------------------------------------------------

CREATE TABLE groupe(
        idGroupe       bigint(20) Auto_increment  NOT NULL ,
        nom            Varchar (255) ,
        dateDebut      Date ,
        dateDebutStage Date ,
        dateFin        Date ,
        PRIMARY KEY (idGroupe )
)ENGINE=InnoDB;


#------------------------------------------------------------
# Table: ville
#------------------------------------------------------------

CREATE TABLE ville(
        idVille bigint(20) Auto_increment  NOT NULL ,
        nom     Varchar (255) ,
        PRIMARY KEY (idVille )
)ENGINE=InnoDB;


#------------------------------------------------------------
# Table: stagiaire
#------------------------------------------------------------

CREATE TABLE stagiaire(
        idStagiaire bigint(20) Auto_increment  NOT NULL ,
        nom         Varchar (255) ,
        prenom      Varchar (255) ,
        idVille     bigint(20) ,
        idGroupe    bigint(20) ,
        PRIMARY KEY (idStagiaire )
)ENGINE=InnoDB;

ALTER TABLE stagiaire ADD CONSTRAINT FK_stagiaire_idVille FOREIGN KEY (idVille) 
REFERENCES ville(idVille);
ALTER TABLE stagiaire ADD CONSTRAINT FK_stagiaire_idGroupe FOREIGN KEY (idGroupe
) REFERENCES groupe(idGroupe);
