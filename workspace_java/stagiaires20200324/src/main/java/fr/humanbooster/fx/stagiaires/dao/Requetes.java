package fr.humanbooster.fx.stagiaires.dao;

public class Requetes {
	
	public static final String AJOUT_GROUPE = "INSERT INTO groupe (nom, dateDebut, dateDebutStage, dateFin) VALUES (?, ?, ?, ?)";
	public static final String GROUPE_PAR_ID = "SELECT * FROM groupe WHERE idGroupe = ?";
	public static final String TOUS_LES_GROUPES = "SELECT * FROM groupe";

	public static final String AJOUT_STAGIAIRE = "INSERT INTO stagiaire (nom, prenom, idVille, idGroupe) VALUES (?, ?, ?, ?)";
	public static final String STAGIAIRE_PAR_ID = "SELECT * FROM stagiaire WHERE idStagiaire = ?";
	public static final String TOUS_LES_STAGIAIRES = "SELECT * FROM stagiaire";
	public static final String STAGIAIRES_PAR_GROUPE = "SELECT * FROM stagiaire WHERE idGroupe = ?";
	public static final String STAGIAIRES_PAR_VILLE = "SELECT * FROM stagiaire WHERE idVille = ?";
	public static final String RETIRE_STAGIAIRE = "UPDATE stagiaire SET idGroupe=null WHERE idStagiaire=?";

	public static final String AJOUT_VILLE = "INSERT INTO ville (nom) VALUES (?)";
	public static final String VILLE_PAR_ID = "SELECT * FROM ville WHERE idVille = ?";
	public static final String TOUTES_LES_VILLES = "SELECT * FROM ville";
	public static final String SUPPRESSION_VILLE = "DELETE FROM ville WHERE idVille = ?";

}