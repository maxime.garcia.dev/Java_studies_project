package fr.humanbooster.fx.stagiaires;

import java.util.Date;
import java.util.List;
import java.util.Scanner;

import fr.humanbooster.fx.stagiaires.business.Groupe;
import fr.humanbooster.fx.stagiaires.business.Stagiaire;
import fr.humanbooster.fx.stagiaires.business.Ville;
import fr.humanbooster.fx.stagiaires.service.GroupeService;
import fr.humanbooster.fx.stagiaires.service.StagiaireService;
import fr.humanbooster.fx.stagiaires.service.VilleService;
import fr.humanbooster.fx.stagiaires.service.impl.GroupeServiceImpl;
import fr.humanbooster.fx.stagiaires.service.impl.StagiaireServiceImpl;
import fr.humanbooster.fx.stagiaires.service.impl.VilleServiceImpl;

public class App {
	
	private static final int AJOUTER_STAGIAIRE = 1;
	private static final int RETIRER_STAGIAIRE = 2;
	
	private static GroupeService groupeService = new GroupeServiceImpl();
	private static VilleService villeService = new VilleServiceImpl();
	private static StagiaireService stagiaireService = new StagiaireServiceImpl();
	private static Scanner sc = new Scanner(System.in);
	
	public static void main(String[] args) {

		boolean continuer = true;

		ajouterGroupes();
		ajouterVilles();
		
		System.out.println("Bienvenue sur l'application stagiaires !");

		while (continuer) {
			afficherGroupes();

			System.out.print("Veuillez choisir un groupe par son id (0 pour quitter) : ");
			Long idGroupe = Long.parseLong(sc.nextLine());
			if (idGroupe != 0) {
				boolean action = true;
				while (action) {
					Groupe groupe = groupeService.recupererGroupe(idGroupe);
					System.out.println("Voici la liste des stagiaires du groupe " + groupe.getNom() + " :");
					afficherStagiaires(groupe);

					afficherMenuPrincipal();
					int choix = Integer.parseInt(sc.nextLine());
					
					if (choix == AJOUTER_STAGIAIRE) { 
						System.out.print("Entrez le nom du stagiaire : ");
						String nom = sc.nextLine();
						System.out.print("Entrez le prénom du stagiaire : ");
						String prenom = sc.nextLine();
						afficherVilles();
						System.out.print("Veuillez choisir sa ville en donnant son id : ");
						Long idVille = Long.parseLong(sc.nextLine());
						Ville ville = villeService.recupererVille(idVille);
						stagiaireService.ajouter(nom, prenom, ville, groupe);
					} 
					else if (choix == RETIRER_STAGIAIRE) {
						System.out.println("Veuillez saisir le stagiaire à retirer par son id :");
						Long idStagiaire = Long.parseLong(sc.nextLine());
						Stagiaire stagiaire = stagiaireService.recupererStagiaire(idStagiaire);
						if (!stagiaireService.retirerStagiaire(stagiaire, groupe)) {
							System.err.println("ATTENTION!! Aucun stagiaire n'a été retiré du groupe!");
						}
					} else {
						action = false;
					}
				}
			} else {
				continuer = false;
			}
		}
		System.out.println("Au revoir");
		sc.close();
	}

	private static void afficherStagiaires(Groupe groupe) {
		for (Stagiaire stagiaire : groupe.getStagiaires()) {
			System.out.println(stagiaire);
		}
	}

	private static void afficherMenuPrincipal() {
		System.out.println("Menu principal");
		System.out.println(AJOUTER_STAGIAIRE + ". ajouter un stagiaire");
		System.out.println(RETIRER_STAGIAIRE + ". retirer un stagiaire du groupe");
		System.out.println("3 : retour à la liste des groupes");
	}

	private static void afficherGroupes() {
		System.out.println("Voici la liste des groupes :");
		for (Groupe groupe : groupeService.recupererGroupes()) {
			System.out.println(groupe);
		}
	}

	private static void afficherVilles() {
		System.out.println("Voici la liste des villes :");
		for (Ville ville : villeService.recupererVilles()) {
			System.out.println(ville);
		}
	}

	private static void ajouterVilles() {
		if (villeService.recupererVilles().isEmpty()) {
			villeService.ajouter("Lyon");
			villeService.ajouter("Clermont-Ferrand");
			villeService.ajouter("Belfort");
			villeService.ajouter("Saint-Galmier");			
		}
	}

	private static void ajouterGroupes() {
		List<Groupe> groupes = groupeService.recupererGroupes();
		if (groupes.isEmpty()) {
			for (int i = 0; i < 17; i++) {
				groupeService.ajouter("groupe " + (i + 1), new Date(), new Date(), new Date());
			}
		}
	}
}
