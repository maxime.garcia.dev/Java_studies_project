package fr.humanbooster.fx.stagiaires.service;

import java.util.List;

import fr.humanbooster.fx.stagiaires.business.Groupe;
import fr.humanbooster.fx.stagiaires.business.Stagiaire;
import fr.humanbooster.fx.stagiaires.business.Ville;

public interface StagiaireService {

	Stagiaire ajouter(String nom, String prenom, Ville ville, Groupe groupe);
	
	List<Stagiaire> recupererStagiaires();
	Stagiaire recupererStagiaire(Long id);
	List<Stagiaire> recupererStagiaires(Groupe groupe);
	
	List<Stagiaire> recupererStagiaires(Ville ville);

	boolean retirerStagiaire(Stagiaire stagiaire, Groupe groupe);
}