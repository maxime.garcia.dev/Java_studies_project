package fr.humanbooster.fx.stagiaires.service.impl;

import java.sql.SQLException;
import java.util.Date;
import java.util.List;

import fr.humanbooster.fx.stagiaires.business.Groupe;
import fr.humanbooster.fx.stagiaires.dao.GroupeDao;
import fr.humanbooster.fx.stagiaires.dao.StagiaireDao;
import fr.humanbooster.fx.stagiaires.dao.impl.GroupeDaoImpl;
import fr.humanbooster.fx.stagiaires.dao.impl.StagiaireDaoImpl;
import fr.humanbooster.fx.stagiaires.service.GroupeService;

public class GroupeServiceImpl implements GroupeService {

	private GroupeDao groupeDao = new GroupeDaoImpl();
	private StagiaireDao stagiaireDao = new StagiaireDaoImpl();

	@Override
	public Groupe ajouter(String nom, Date dateDebut, Date dateDebutStage, Date dateFin) {
		try {
			return groupeDao.create(new Groupe(nom, dateDebut, dateDebutStage, dateFin));
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return null;
	}

	@Override
	public List<Groupe> recupererGroupes() {
		try {
			return groupeDao.findAll();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return null;
	}

	@Override
	public Groupe recupererGroupe(Long id) {
		Groupe groupe = null;
		try {
			groupe = groupeDao.findOne(id);
			groupe.setStagiaires(stagiaireDao.findByGroupe(groupe));
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return groupe;
	}

}