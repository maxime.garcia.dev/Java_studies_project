package fr.humanbooster.fx.stagiaires.dao;

import java.sql.SQLException;
import java.util.List;

import fr.humanbooster.fx.stagiaires.business.Groupe;
import fr.humanbooster.fx.stagiaires.business.Stagiaire;
import fr.humanbooster.fx.stagiaires.business.Ville;

public interface StagiaireDao {

	Stagiaire create(Stagiaire stagiaire) throws SQLException;

	List<Stagiaire> findAll() throws SQLException;
	Stagiaire findOne(Long idStagiaire) throws SQLException;
	List<Stagiaire> findByGroupe(Groupe groupe) throws SQLException;
	List<Stagiaire> findByVille(Ville ville) throws SQLException;

	public boolean remove(Stagiaire stagiaire, Groupe groupe) throws SQLException; 
}
