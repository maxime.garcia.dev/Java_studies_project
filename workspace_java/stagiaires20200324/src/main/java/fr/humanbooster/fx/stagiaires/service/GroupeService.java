package fr.humanbooster.fx.stagiaires.service;

import java.util.Date;
import java.util.List;

import fr.humanbooster.fx.stagiaires.business.Groupe;

public interface GroupeService {

	Groupe ajouter(String nom, Date dateDebut, Date dateDebutStage, Date dateFin);
	
	List<Groupe> recupererGroupes();
	Groupe recupererGroupe(Long id);
}
