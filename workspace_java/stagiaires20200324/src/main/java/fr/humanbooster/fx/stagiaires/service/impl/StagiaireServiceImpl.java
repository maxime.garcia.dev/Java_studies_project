package fr.humanbooster.fx.stagiaires.service.impl;

import java.sql.SQLException;
import java.util.List;

import fr.humanbooster.fx.stagiaires.business.Groupe;
import fr.humanbooster.fx.stagiaires.business.Stagiaire;
import fr.humanbooster.fx.stagiaires.business.Ville;
import fr.humanbooster.fx.stagiaires.dao.StagiaireDao;
import fr.humanbooster.fx.stagiaires.dao.impl.StagiaireDaoImpl;
import fr.humanbooster.fx.stagiaires.service.StagiaireService;

public class StagiaireServiceImpl implements StagiaireService {

	private StagiaireDao stagiaireDao = new StagiaireDaoImpl();

	@Override
	public Stagiaire ajouter(String nom, String prenom, Ville ville, Groupe groupe) {
		try {
			return stagiaireDao.create(new Stagiaire(nom, prenom, ville, groupe));
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return null;
	}

	@Override
	public List<Stagiaire> recupererStagiaires() {
		try {
			return stagiaireDao.findAll();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return null;
	}

	@Override
	public Stagiaire recupererStagiaire(Long id) {
		try {
			return stagiaireDao.findOne(id);
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return null;
	}

	@Override
	public List<Stagiaire> recupererStagiaires(Groupe groupe) {
		try {
			return stagiaireDao.findByGroupe(groupe);
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return null;
	}

	@Override
	public List<Stagiaire> recupererStagiaires(Ville ville) {
		try {
			return stagiaireDao.findByVille(ville);
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return null;
	}

	@Override
	public boolean retirerStagiaire(Stagiaire stagiaire, Groupe groupe) {
		try {
			return stagiaireDao.remove(stagiaire, groupe);
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return false;
	}

}
