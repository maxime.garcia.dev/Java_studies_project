package fr.humanbooster.fx.stagiaires.service.impl;

import java.sql.SQLException;
import java.util.List;

import fr.humanbooster.fx.stagiaires.business.Ville;
import fr.humanbooster.fx.stagiaires.dao.StagiaireDao;
import fr.humanbooster.fx.stagiaires.dao.VilleDao;
import fr.humanbooster.fx.stagiaires.dao.impl.StagiaireDaoImpl;
import fr.humanbooster.fx.stagiaires.dao.impl.VilleDaoImpl;
import fr.humanbooster.fx.stagiaires.service.VilleService;

public class VilleServiceImpl implements VilleService {

	private VilleDao villeDao = new VilleDaoImpl();
	private StagiaireDao stagiaireDao = new StagiaireDaoImpl();

	@Override
	public Ville ajouter(String nom) {
		try {
			return villeDao.create(new Ville(nom));
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return null;
	}

	@Override
	public List<Ville> recupererVilles() {
		try {
			return villeDao.findAll();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return null;
	}

	@Override
	public Ville recupererVille(Long id) {
		Ville ville = null;
		try {
			ville = villeDao.findOne(id);
			ville.setStagiaires(stagiaireDao.findByVille(ville));
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return ville;
	}

	@Override
	public boolean effacerVille(Long id) {
		try {
			return villeDao.delete(id);
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return false;
	}

}