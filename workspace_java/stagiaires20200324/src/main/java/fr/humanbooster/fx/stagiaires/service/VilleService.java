package fr.humanbooster.fx.stagiaires.service;

import java.util.List;

import fr.humanbooster.fx.stagiaires.business.Ville;

public interface VilleService {

	Ville ajouter(String nom);
	
	List<Ville> recupererVilles();
	Ville recupererVille(Long id);
	
	boolean effacerVille(Long id);
}
