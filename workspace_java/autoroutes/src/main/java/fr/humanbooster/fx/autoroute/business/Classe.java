package fr.humanbooster.fx.autoroute.business;

public class Classe {
    
    private Long id;
    private String nom;
    
    public Classe() {
    }

	public Classe(String nom) {
		this();
		this.nom = nom;
	}

	public Classe(Long id) {
		this();
		this.id = id;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getNom() {
		return nom;
	}

	public void setNom(String nom) {
		this.nom = nom;
	}

	@Override
	public String toString() {
		return "Classe [id=" + id + ", nom=" + nom + "]";
	}
    
}
