package fr.humanbooster.fx.autoroute.dao;

import java.sql.SQLException;
import java.util.List;

import fr.humanbooster.fx.autoroute.business.Classe;

public interface ClasseDAO {
	Classe create(Classe classe) throws SQLException;

	Classe findOne(Long id) throws SQLException;

	List<Classe> findAll() throws SQLException;
}
