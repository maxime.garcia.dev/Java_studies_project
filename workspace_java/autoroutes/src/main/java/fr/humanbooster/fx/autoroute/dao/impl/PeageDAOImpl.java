package fr.humanbooster.fx.autoroute.dao.impl;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import fr.humanbooster.fx.autoroute.business.Peage;
import fr.humanbooster.fx.autoroute.dao.ConnexionBdd;
import fr.humanbooster.fx.autoroute.dao.PeageDAO;
import fr.humanbooster.fx.autoroute.dao.Requetes;

public class PeageDAOImpl implements PeageDAO {

	private Connection connection;
	
	public PeageDAOImpl() {
		try {
			connection = ConnexionBdd.getConnection();
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}
	@Override
	public Peage create(Peage peage) throws SQLException {
		PreparedStatement ps = connection.prepareStatement(Requetes.AJOUT_PEAGE);
		ps.setString(1, peage.getNom());
		ps.executeUpdate();
		ResultSet rs = ps.getGeneratedKeys();
		rs.next();
		peage.setId(rs.getLong(1));
		return peage;
	}

	@Override
	public Peage findOne(Long id) throws SQLException {
		Peage peage = null;
		PreparedStatement ps = connection.prepareStatement(Requetes.PEAGE_PAR_ID);
		ps.setLong(1, id);
		ResultSet rs = ps.executeQuery();
		if (rs.next())
		{
			peage = new Peage(rs.getLong("id"));
			peage.setNom(rs.getString("nom"));
		}
		return peage;
	}

	@Override
	public List<Peage> findAll() throws SQLException {
		List<Peage> peages = new ArrayList<>();
		PreparedStatement ps = connection.prepareStatement(Requetes.TOUS_LES_PEAGES);
		ResultSet rs = ps.executeQuery();
		while (rs.next()) {
			Peage peage = new Peage(rs.getString("nom"));
			peage.setId(rs.getLong("id"));
			peages.add(peage);
		}
		return peages;
	}

}
