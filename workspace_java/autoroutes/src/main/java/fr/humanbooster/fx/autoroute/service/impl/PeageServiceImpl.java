package fr.humanbooster.fx.autoroute.service.impl;

import java.util.List;

import fr.humanbooster.fx.autoroute.business.Peage;
import fr.humanbooster.fx.autoroute.dao.PeageDAO;
import fr.humanbooster.fx.autoroute.dao.impl.PeageDAOImpl;
import fr.humanbooster.fx.autoroute.service.PeageService;

public class PeageServiceImpl implements PeageService {

	private PeageDAO peageDAO = new PeageDAOImpl();

	@Override
	public Peage ajouterPeage(String nom) {
		Peage peage = new Peage(nom);
		try {
			peageDAO.create(peage);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return peage;
	}

	@Override
	public Peage recupererPeage(Long id) {
		try {
			return peageDAO.findOne(id);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}

	@Override
	public List<Peage> recupererPeages() {
		try {
			return peageDAO.findAll();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}
}
