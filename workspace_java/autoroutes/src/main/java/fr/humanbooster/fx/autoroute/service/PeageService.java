package fr.humanbooster.fx.autoroute.service;

import java.util.List;

import fr.humanbooster.fx.autoroute.business.Peage;

public interface PeageService {
	Peage ajouterPeage(String nom);

	Peage recupererPeage(Long id);

	List<Peage> recupererPeages();
}
