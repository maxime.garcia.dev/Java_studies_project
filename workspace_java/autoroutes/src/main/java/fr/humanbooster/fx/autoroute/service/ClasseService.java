package fr.humanbooster.fx.autoroute.service;

import java.util.List;

import fr.humanbooster.fx.autoroute.business.Classe;

public interface ClasseService {

	Classe ajouterClasse(String nom);

	Classe recupererClasse(Long id);

	List<Classe> recupererClasses();

}