package fr.humanbooster.fx.autoroute.dao;

import java.sql.SQLException;
import java.util.List;

import fr.humanbooster.fx.autoroute.business.Peage;
import fr.humanbooster.fx.autoroute.business.Tarif;

public interface TarifDAO {
	
	Tarif create(Tarif tarif) throws SQLException;
	
	Tarif findOne(Long id) throws SQLException;
	
	List<Tarif> findAll() throws SQLException;
	
	boolean deleteOne(Tarif tarif) throws SQLException;
	
	boolean modifMontant(Long id, float montant) throws SQLException;  
	
	List<Tarif> findAllPeageTarifs(Peage peage) throws SQLException;
}
