package fr.humanbooster.fx.autoroute.service.impl;

import java.sql.SQLException;
import java.util.List;

import fr.humanbooster.fx.autoroute.business.Classe;
import fr.humanbooster.fx.autoroute.dao.ClasseDAO;
import fr.humanbooster.fx.autoroute.dao.impl.ClasseDAOImpl;
import fr.humanbooster.fx.autoroute.service.ClasseService;

public class ClasseServiceImpl implements ClasseService {
	private ClasseDAO classeDao = new ClasseDAOImpl();

	@Override
	public Classe ajouterClasse(String nom) {
		Classe classe = new Classe(nom);
		try {
			//On confie l'objet à la DAO.
			classeDao.create(classe);
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return classe;
	}

	@Override
	public Classe recupererClasse(Long id) {
		try {
			return classeDao.findOne(id);
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return null;
	}

	@Override
	public List<Classe> recupererClasses() {
		try {
			return classeDao.findAll();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return null;
	}
}
