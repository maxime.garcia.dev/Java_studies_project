package fr.humanbooster.fx.autoroute.service.impl;

import java.sql.SQLException;
import java.util.List;

import fr.humanbooster.fx.autoroute.business.Tarif;
import fr.humanbooster.fx.autoroute.dao.TarifDAO;
import fr.humanbooster.fx.autoroute.dao.impl.TarifDAOImpl;
import fr.humanbooster.fx.autoroute.service.TarifService;

public class TarifServiceImpl implements TarifService {

	private TarifDAO tarifDao = new TarifDAOImpl();

	@Override
	public Tarif ajouterTarif(Tarif tarif) {
		try {
			tarifDao.create(tarif);
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return tarif;
	}

	@Override
	public Tarif recupererTarif(Long id) {
		
		return null;
	}

	@Override
	public List<Tarif> recupererTarifs() {
		
		return null;
	}

	@Override
	public Tarif supprimerTarif(Long id) {
		
		return null;
	}

	@Override
	public Tarif updateTarif(Long id) {
		
		return null;
	}
}
