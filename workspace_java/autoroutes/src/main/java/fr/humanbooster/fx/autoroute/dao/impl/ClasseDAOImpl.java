package fr.humanbooster.fx.autoroute.dao.impl;

//On utilise systématiquement les classes et interfaces du packages java.sql
//Si on change le type de base de données, l'implémentation des DAO n'aura pas à changé.
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import fr.humanbooster.fx.autoroute.business.Classe;
import fr.humanbooster.fx.autoroute.dao.ClasseDAO;
import fr.humanbooster.fx.autoroute.dao.ConnexionBdd;
import fr.humanbooster.fx.autoroute.dao.Requetes;

public class ClasseDAOImpl implements ClasseDAO {

	private Connection connection;

	public ClasseDAOImpl() {
		try {
			connection = ConnexionBdd.getConnection();
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}

	@Override
	public Classe create(Classe classe) throws SQLException {
		// On déclare un objet de type PreparedStatement.
		PreparedStatement ps = connection.prepareStatement(Requetes.AJOUT_CLASSE, Statement.RETURN_GENERATED_KEYS);
		// On remplace le 1er point d'interrogation par le nom de la classe donnée en
		// paramètres.
		ps.setString(1, classe.getNom());
		// On envoie l'ordre SQL a MariaDB.
		ps.executeUpdate();
		// On déclare un objet de type ResultSet.
		ResultSet rs = ps.getGeneratedKeys();
		// Le ResultSet dans ce cas de figure contient une seule cellule. Dans cette
		// cellule se trouve l'Id choisit par MariaDB au moment d'ajouter la classe
		// Ici on se place sur l'unique cellule.
		rs.next();
		// On affecte à la classe l'id choisit par MariaDB.
		// Pour obtenir cette id, on lis dans la cellule unique.
		classe.setId(rs.getLong(1));
		// On renvoi l'objet classe que l'on rendu persistant en le rentrant dans la
		// BDD.
		return classe;
	}

	@Override
	public Classe findOne(Long id) throws SQLException {
		Classe classe = null;
		PreparedStatement ps = connection.prepareStatement(Requetes.CLASSE_PAR_ID);
		ps.setLong(1, id);
		ResultSet rs = ps.executeQuery();
		if (rs.next()) {
			classe = new Classe(rs.getLong("id"));
			classe.setNom(rs.getString("nom"));
		}
		return classe;
	}

	@Override
	public List<Classe> findAll() throws SQLException {
		List<Classe> classes = new ArrayList<>();
		PreparedStatement ps = connection.prepareStatement(Requetes.TOUTES_LES_CLASSES);
		ResultSet rs = ps.executeQuery();
		while (rs.next()) {
			Classe classe = new Classe(rs.getString("nom"));
			classe.setId(rs.getLong("id"));
			classes.add(classe);
		}
		return classes;
	}

}
