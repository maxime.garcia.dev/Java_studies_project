package fr.humanbooster.max.calendrier.dao;

import org.springframework.data.jpa.repository.JpaRepository;

import fr.humanbooster.max.calendrier.business.GifTeleverse;

public interface GifTeleverseDao extends JpaRepository<GifTeleverse, Long> {

}
