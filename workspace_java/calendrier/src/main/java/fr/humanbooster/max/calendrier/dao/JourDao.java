package fr.humanbooster.max.calendrier.dao;

import java.util.Date;

import org.springframework.data.jpa.repository.JpaRepository;

import fr.humanbooster.max.calendrier.business.Jour;

public interface JourDao extends JpaRepository<Jour, Date> {

}
