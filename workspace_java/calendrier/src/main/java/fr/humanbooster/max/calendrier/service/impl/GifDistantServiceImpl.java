package fr.humanbooster.max.calendrier.service.impl;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import fr.humanbooster.max.calendrier.business.GifDistant;
import fr.humanbooster.max.calendrier.dao.GifDistantDao;
import fr.humanbooster.max.calendrier.service.GifDistantService;

@Service
public class GifDistantServiceImpl implements GifDistantService {

	private GifDistantDao gifDistantDao;
	
	public GifDistantServiceImpl(GifDistantDao gifDistantDao) {
		super();
		this.gifDistantDao = gifDistantDao;
	}

	@Override
	public GifDistant ajouterGifDistant(GifDistant gifdistant) {
		return gifDistantDao.save(gifdistant);
	}

	@Override
	public Page<GifDistant> recuperGifDistants(Pageable pageable) {
		return gifDistantDao.findAll(pageable);
	}

}
