package fr.humanbooster.max.calendrier.service;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import fr.humanbooster.max.calendrier.business.GifDistant;

public interface GifDistantService {

	GifDistant ajouterGifDistant(GifDistant gifdistant);

	Page<GifDistant> recuperGifDistants(Pageable pageable);
}
