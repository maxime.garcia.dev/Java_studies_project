package fr.humanbooster.max.calendrier.service;

import java.util.Date;

import fr.humanbooster.max.calendrier.business.Gif;
import fr.humanbooster.max.calendrier.business.GifDistant;
import fr.humanbooster.max.calendrier.business.Utilisateur;

public interface GifService {

	GifDistant ajouterGifDistant(Date idJour, String url, Utilisateur utilisateur);

	GifDistant ajouterGifDistant(GifDistant gifDistant, Utilisateur utilisateur);

	Gif recupererGif(Long idGif);

	Gif mettreAJourLegende(Gif gif, String nouvelleLegende);
	
}
