package fr.humanbooster.max.calendrier.dao;

import org.springframework.data.jpa.repository.JpaRepository;

import fr.humanbooster.max.calendrier.business.Emotion;

public interface EmotionDao extends JpaRepository<Emotion, Long> {

	Emotion findByNom(String nom);

}
