package fr.humanbooster.max.calendrier.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import fr.humanbooster.max.calendrier.business.Theme;
import fr.humanbooster.max.calendrier.dao.ThemeDao;
import fr.humanbooster.max.calendrier.service.ThemeService;

@Service
public class ThemeServiceImpl implements ThemeService {

	@Autowired
	private ThemeDao themeDao;
	
	@Override
	public Theme ajouterTheme(String nom) {
		return themeDao.save(new Theme(nom));
	}

	@Override
	public List<Theme> recupererThemes() {
		return themeDao.findAll();
	}

	@Override
	public Theme recupererTheme(Long id) {
		return themeDao.findById(id).orElse(null);
	}

	@Override
	public Theme recupererTheme(String nom) {
		return themeDao.findByNom(nom);
	}

}
