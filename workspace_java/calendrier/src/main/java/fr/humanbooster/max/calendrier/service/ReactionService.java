package fr.humanbooster.max.calendrier.service;

import java.util.List;

import fr.humanbooster.max.calendrier.business.Gif;
import fr.humanbooster.max.calendrier.business.Reaction;
import fr.humanbooster.max.calendrier.business.Utilisateur;

public interface ReactionService {

	Reaction ajouterReaction(Long idGif, Long idEmotion, Utilisateur utilisateur);
	
	List<Reaction> recupererReactions(Gif gif);
	
	boolean supprimerReaction(Long idGif, Long idEmotion, Long idUtilisateur, String email);

}