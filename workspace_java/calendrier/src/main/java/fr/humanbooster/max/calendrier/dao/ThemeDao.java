package fr.humanbooster.max.calendrier.dao;

import org.springframework.data.jpa.repository.JpaRepository;

import fr.humanbooster.max.calendrier.business.Theme;

public interface ThemeDao extends JpaRepository<Theme, Long> {

	Theme findByNom(String nom);

}
