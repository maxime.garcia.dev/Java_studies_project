package fr.humanbooster.max.calendrier.service.impl;

import org.springframework.stereotype.Service;

import fr.humanbooster.max.calendrier.business.GifTeleverse;
import fr.humanbooster.max.calendrier.dao.GifTeleverseDao;
import fr.humanbooster.max.calendrier.service.GifTeleverseService;

@Service
public class GifTeleverseServiceImpl implements GifTeleverseService {

	private GifTeleverseDao gifTeleverseDao;
	
	public GifTeleverseServiceImpl(GifTeleverseDao gifTeleverseDao) {
		super();
		this.gifTeleverseDao = gifTeleverseDao;
	}

	@Override
	public GifTeleverse ajouterGifTeleverse(GifTeleverse gifTeleverse) {
		return gifTeleverseDao.save(gifTeleverse);
	}

}
