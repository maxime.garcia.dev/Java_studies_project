package fr.humanbooster.max.calendrier.business;

import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;

@Entity
public class Reaction {

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private Long id;

	@ManyToOne
	private Gif gif;
	
	@ManyToOne
	private Utilisateur utilisateur;

	@ManyToOne
	private Emotion emotion;
	
	private Date dateHeure;
	
	public Reaction() {
		dateHeure = new Date();
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Gif getGif() {
		return gif;
	}

	public void setGif(Gif gif) {
		this.gif = gif;
	}

	public Utilisateur getUtilisateur() {
		return utilisateur;
	}

	public void setUtilisateur(Utilisateur utilisateur) {
		this.utilisateur = utilisateur;
	}

	public Emotion getEmotion() {
		return emotion;
	}

	public void setEmotion(Emotion emotion) {
		this.emotion = emotion;
	}

	public Date getDateHeure() {
		return dateHeure;
	}

	public void setDateHeure(Date dateHeure) {
		this.dateHeure = dateHeure;
	}

	@Override
	public String toString() {
		return "Reaction [id=" + id + ", gif=" + gif + ", utilisateur=" + utilisateur + ", emotion=" + emotion
				+ ", dateHeure=" + dateHeure + "]";
	}
	
}