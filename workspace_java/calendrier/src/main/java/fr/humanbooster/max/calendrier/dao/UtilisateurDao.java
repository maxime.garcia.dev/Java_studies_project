package fr.humanbooster.max.calendrier.dao;

import org.springframework.data.jpa.repository.JpaRepository;

import fr.humanbooster.max.calendrier.business.Utilisateur;

public interface UtilisateurDao extends JpaRepository<Utilisateur, Long> {

	Utilisateur findLastByEmailAndMotDePasse(String email, String motDePasse);

	Utilisateur findByIdAndEmail(Long idUtilisateur, String email);

}
