package fr.humanbooster.max.calendrier.dao;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import fr.humanbooster.max.calendrier.business.Emotion;
import fr.humanbooster.max.calendrier.business.Gif;
import fr.humanbooster.max.calendrier.business.Reaction;
import fr.humanbooster.max.calendrier.business.Utilisateur;

public interface ReactionDao extends JpaRepository<Reaction, Long> {

	List<Reaction> findByGif(Gif gif);

	Reaction findLastByGifAndUtilisateurAndEmotion(Gif recupererGif, Utilisateur utilisateur, Emotion findById);

}