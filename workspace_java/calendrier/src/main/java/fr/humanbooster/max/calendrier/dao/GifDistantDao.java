package fr.humanbooster.max.calendrier.dao;

import org.springframework.data.jpa.repository.JpaRepository;

import fr.humanbooster.max.calendrier.business.GifDistant;

public interface GifDistantDao extends JpaRepository<GifDistant, Long> {

}
