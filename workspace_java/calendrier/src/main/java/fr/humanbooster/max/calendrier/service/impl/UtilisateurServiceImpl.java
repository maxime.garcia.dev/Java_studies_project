package fr.humanbooster.max.calendrier.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import fr.humanbooster.max.calendrier.business.Theme;
import fr.humanbooster.max.calendrier.business.Utilisateur;
import fr.humanbooster.max.calendrier.dao.UtilisateurDao;
import fr.humanbooster.max.calendrier.service.UtilisateurService;

@Service
public class UtilisateurServiceImpl implements UtilisateurService {

	@Autowired
	private UtilisateurDao utilisateurDao;
	
	@Override
	public Utilisateur ajouterUtilisateur(Utilisateur utilisateur) {
		return utilisateurDao.save(utilisateur);
	}

	@Override
	public Utilisateur ajouterUtilisateur(String nom, String prenom, String email, String motDePasse,
			Theme theme) {
		Utilisateur utilisateur = new Utilisateur();
		utilisateur.setNom(nom);
		utilisateur.setPrenom(prenom);
		utilisateur.setEmail(email);
		utilisateur.setMotDePasse(motDePasse);
		utilisateur.setTheme(theme);
		return utilisateurDao.save(utilisateur);
	}
	
	@Override
	public Utilisateur recupererUtilisateur(String email, String motDePasse) {
		return utilisateurDao.findLastByEmailAndMotDePasse(email, motDePasse);
	}
	
}