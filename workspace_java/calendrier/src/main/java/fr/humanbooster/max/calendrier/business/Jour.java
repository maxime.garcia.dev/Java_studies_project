package fr.humanbooster.max.calendrier.business;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.OneToOne;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.Max;
import javax.validation.constraints.Min;

@Entity
public class Jour {

	public static final String FORMAT_DATE_AMERICAIN = "yyyy-MM-dd";
	public static final String FORMAT_DATE_FRANCAIS = "dd/MM/yyyy";
	
	private static SimpleDateFormat simpleDateFormatAmericain = new SimpleDateFormat(FORMAT_DATE_AMERICAIN);
	private static SimpleDateFormat simpleDateFormatFrancais = new SimpleDateFormat(FORMAT_DATE_FRANCAIS);

	@Id
	@Temporal(TemporalType.DATE)
	private Date date;
	
	@Min(20)
	@Max(50)
	private int nbPoints;
	
	@OneToOne
	private Gif gif;
	
	private static Calendar calendar = Calendar.getInstance();
	
	public Jour() {
	}
	
	public Jour(Date date, int nbPoints) {
		this.date = date;
		this.nbPoints = nbPoints;
	}

	public Date getDate() {
		return date;
	}

	public String getDateFormatAmericain() {
		return simpleDateFormatAmericain.format(date);
	}

	public String getDateFormatFrancais() {
		return simpleDateFormatFrancais.format(date);
	}

	public void setDate(Date date) {
		this.date = date;
	}

	public int getNbPoints() {
		return nbPoints;
	}

	public void setNbPoints(int nbPoints) {
		this.nbPoints = nbPoints;
	}

	public Gif getGif() {
		return gif;
	}

	public void setGif(Gif gif) {
		this.gif = gif;
	}

	public String toString() {
		calendar.setTime(date);
		return calendar.get(Calendar.DAY_OF_MONTH) + "/" + (1+calendar.get(Calendar.MONTH));
	}
	
}