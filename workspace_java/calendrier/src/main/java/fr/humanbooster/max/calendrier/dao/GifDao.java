package fr.humanbooster.max.calendrier.dao;

import org.springframework.data.jpa.repository.JpaRepository;

import fr.humanbooster.max.calendrier.business.Gif;

public interface GifDao extends JpaRepository<Gif, Long> {

}
