package fr.humanbooster.max.calendrier.service;

import fr.humanbooster.max.calendrier.business.Theme;
import fr.humanbooster.max.calendrier.business.Utilisateur;

public interface UtilisateurService {

	Utilisateur ajouterUtilisateur(Utilisateur utilisateur);
	
	Utilisateur ajouterUtilisateur(String nom, String prenom, String email, String motDePasse, Theme recupererTheme);

	Utilisateur recupererUtilisateur(String email, String motDePasse);

}
