package fr.humanbooster.max.calendrier.service;

import java.util.List;

import fr.humanbooster.max.calendrier.business.Emotion;

public interface EmotionService {

	Emotion ajouterEmotion(String nom, String nomFichier);

	List<Emotion> recupererEmotions();
	
	Emotion recupererEmotion(Long id);
	
	Emotion recupererEmotion(String nom);
}
