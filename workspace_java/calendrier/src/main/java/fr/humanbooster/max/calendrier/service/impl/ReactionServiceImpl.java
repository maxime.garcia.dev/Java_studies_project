package fr.humanbooster.max.calendrier.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import fr.humanbooster.max.calendrier.business.Emotion;
import fr.humanbooster.max.calendrier.business.Gif;
import fr.humanbooster.max.calendrier.business.Reaction;
import fr.humanbooster.max.calendrier.business.Utilisateur;
import fr.humanbooster.max.calendrier.dao.EmotionDao;
import fr.humanbooster.max.calendrier.dao.ReactionDao;
import fr.humanbooster.max.calendrier.dao.UtilisateurDao;
import fr.humanbooster.max.calendrier.service.GifService;
import fr.humanbooster.max.calendrier.service.ReactionService;

@Service
public class ReactionServiceImpl implements ReactionService {

	@Autowired
	private ReactionDao reactionDao;

	@Autowired
	private EmotionDao emotionDao;

	@Autowired
	private UtilisateurDao utilisateurDao;

	@Autowired
	private GifService gifService;

	@Override
	public Reaction ajouterReaction(Long idGif, Long idEmotion, Utilisateur utilisateur) {
		Emotion emotion = emotionDao.findById(idEmotion).orElse(null);
		Gif gif = gifService.recupererGif(idGif);
		Reaction reaction = new Reaction();
		reaction.setGif(gif);
		reaction.setUtilisateur(utilisateur);
		reaction.setEmotion(emotion);
		reactionDao.save(reaction);
		return reaction;
	}

	@Override
	public List<Reaction> recupererReactions(Gif gif) {
		return reactionDao.findByGif(gif);
	}

	@Override
	public boolean supprimerReaction(Long idGif, Long idEmotion, Long idUtilisateur, String email) {
		Reaction reaction = reactionDao.findLastByGifAndUtilisateurAndEmotion(gifService.recupererGif(idGif), utilisateurDao.findByIdAndEmail(idUtilisateur, email), emotionDao.findById(idEmotion).orElse(null));
		if (reaction==null) {
			return false;
		}
		reactionDao.delete(reaction);
		return true;
	}

}