CREATE TABLE groupe(
        idGroupe       bigint(20) Auto_increment  NOT NULL PRIMARY KEY,
        nom            Varchar (255) ,
        dateDebut      Date ,
        dateDebutStage Date ,
        dateFin        Date
);
CREATE TABLE ville(
        idVille bigint(20) Auto_increment  NOT NULL PRIMARY KEY,
        nom     Varchar (255),
        codePostale Varchar(10)
);
CREATE TABLE stagiaire(
        idStagiaire bigint(20) Auto_increment  NOT NULL PRIMARY KEY,
        nom         Varchar (255) ,
        prenom      Varchar (255) ,
        idVille     bigint(20) ,
        idGroupe    bigint(20)
);
ALTER TABLE stagiaire ADD CONSTRAINT FK_stagiaire_idVille FOREIGN KEY (idVille) 
REFERENCES ville(idVille);
ALTER TABLE stagiaire ADD CONSTRAINT FK_stagiaire_idGroupe FOREIGN KEY (idGroupe
) REFERENCES groupe(idGroupe);
