package fr.humanbooster.fx.stagiaires.dao.impl;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import fr.humanbooster.fx.stagiaires.business.Groupe;
import fr.humanbooster.fx.stagiaires.dao.ConnexionBdd;
import fr.humanbooster.fx.stagiaires.dao.GroupeDao;
import fr.humanbooster.fx.stagiaires.dao.Requetes;

public class GroupeDaoImpl implements GroupeDao {

	private Connection connection;

	public GroupeDaoImpl() {
		try {
			connection = ConnexionBdd.getConnection();
		} catch (ClassNotFoundException | SQLException e) {
			e.printStackTrace();
		}
	}

	@Override
	public Groupe create(Groupe groupe) throws SQLException {
		PreparedStatement ps = connection.prepareStatement(Requetes.AJOUT_GROUPE, Statement.RETURN_GENERATED_KEYS);
		ps.setString(1, groupe.getNom());
		if (groupe.getDateDebut()!=null) {
			ps.setDate(2, new java.sql.Date(groupe.getDateDebut().getTime()));			
		}
		else {
			ps.setDate(2, null);
		}
		if (groupe.getDateDebutStage()!=null) {
			ps.setDate(3, new java.sql.Date(groupe.getDateDebutStage().getTime()));			
		}
		else {
			ps.setDate(3, null);
		}
		if (groupe.getDateFin()!=null) {
			ps.setDate(4, new java.sql.Date(groupe.getDateFin().getTime()));			
		}
		else {
			ps.setDate(4, null);
		}
		ps.executeUpdate();
		ResultSet rs = ps.getGeneratedKeys();
		if (rs.next()) {
			groupe.setId(rs.getLong(1));
		}
		return groupe;
	}

	@Override
	public List<Groupe> findAll() throws SQLException {
		List<Groupe> groupes = new ArrayList<>();
		PreparedStatement ps = connection.prepareStatement(Requetes.TOUS_LES_GROUPES);
		ResultSet rs = ps.executeQuery();
		while (rs.next()) {
			Groupe groupe = new Groupe(rs.getString("nom"), rs.getDate("dateDebut"), rs.getDate("dateDebutStage"),
					rs.getDate("dateFin"));
			groupe.setId(rs.getLong("idGroupe"));
			groupes.add(groupe);
		}
		return groupes;
	}

	@Override
	public Groupe findOne(Long id) throws SQLException {
		Groupe groupe = null;
		PreparedStatement ps = connection.prepareStatement(Requetes.GROUPE_PAR_ID);
		ps.setLong(1, id);
		ResultSet rs = ps.executeQuery();
		if (rs.next()) {
			groupe = new Groupe(rs.getString("nom"), rs.getDate("dateDebut"), rs.getDate("dateDebutStage"),
					rs.getDate("dateFin"));
			groupe.setId(rs.getLong("idGroupe"));
		}
		return groupe;
	}

	@Override
	protected void finalize() throws Throwable {
		connection.close();
	}

}
