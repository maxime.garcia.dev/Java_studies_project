package fr.humanbooster.fx.stagiaires.dao;

import java.sql.SQLException;
import java.util.List;

import fr.humanbooster.fx.stagiaires.business.Groupe;

public interface GroupeDao {

	Groupe create(Groupe groupe) throws SQLException;

	List<Groupe> findAll() throws SQLException;
	Groupe findOne(Long id) throws SQLException;
}
