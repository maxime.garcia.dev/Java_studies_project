package fr.humanbooster.fx.stagiaires.service.impl;


import java.io.BufferedInputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import java.net.URL;
import java.sql.SQLException;
import java.util.List;

import org.apache.commons.csv.CSVFormat;
import org.apache.commons.csv.CSVParser;
import org.apache.commons.csv.CSVRecord;

import fr.humanbooster.fx.stagiaires.business.Ville;
import fr.humanbooster.fx.stagiaires.dao.StagiaireDao;
import fr.humanbooster.fx.stagiaires.dao.VilleDao;
import fr.humanbooster.fx.stagiaires.dao.impl.StagiaireDaoImpl;
import fr.humanbooster.fx.stagiaires.dao.impl.VilleDaoImpl;
import fr.humanbooster.fx.stagiaires.service.VilleService;

public class VilleServiceImpl implements VilleService {

	private VilleDao villeDao = new VilleDaoImpl();
	private StagiaireDao stagiaireDao = new StagiaireDaoImpl();

	@Override
	public Ville ajouter(String nom, String codePostale) {
		try {
			return villeDao.create(new Ville(nom, codePostale));
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return null;
	}

	@Override
	public List<Ville> recupererVilles() {
		try {
			return villeDao.findAll();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return null;
	}

	@Override
	public Ville recupererVille(Long id) {
		Ville ville = null;
		try {
			ville = villeDao.findOne(id);
			ville.setStagiaires(stagiaireDao.findByVille(ville));
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return ville;
	}

	@Override
	public boolean effacerVille(Long id) {
		try {
			return villeDao.delete(id);
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return false;
	}
	
	@Override
    public Ville importerVille() {
        try {
            // On crée un objet de type URL
            URL url = new URL("https://www.clelia.fr/villes2020.csv");

            // On crée un lecteur à partir du flux d'entrée générée par l'URL
            // == méthode GET du protocole HTTP
            Reader reader = new InputStreamReader(new BufferedInputStream(url.openStream()), "UTF-8");

            // On déclare un format de CSV
            CSVFormat csvFormat = CSVFormat.DEFAULT.withDelimiter(';').withFirstRecordAsHeader();
            
            // On crée un parser en donnant en paramètre le reader et le format CSV
            CSVParser csvParser = new CSVParser(reader, csvFormat);

            // On parcourt le csvParser, on ajoute un objet de type Ville à liste villes
            for (CSVRecord record : csvParser) {
                Ville ville = new Ville(record.get(1), record.get(2));
                villeDao.create(ville);
            }
            csvParser.close();
            reader.close();
        }
        catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

}