package fr.humanbooster.fx.stagiaires.dao.impl;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import fr.humanbooster.fx.stagiaires.business.Groupe;
import fr.humanbooster.fx.stagiaires.business.Stagiaire;
import fr.humanbooster.fx.stagiaires.business.Ville;
import fr.humanbooster.fx.stagiaires.dao.ConnexionBdd;
import fr.humanbooster.fx.stagiaires.dao.GroupeDao;
import fr.humanbooster.fx.stagiaires.dao.Requetes;
import fr.humanbooster.fx.stagiaires.dao.StagiaireDao;
import fr.humanbooster.fx.stagiaires.dao.VilleDao;

public class StagiaireDaoImpl implements StagiaireDao {

	private Connection connection;
	private VilleDao villeDao = new VilleDaoImpl();
	private GroupeDao groupeDao = new GroupeDaoImpl();

	public StagiaireDaoImpl() {
		try {
			connection = ConnexionBdd.getConnection();
		} catch (ClassNotFoundException | SQLException e) {
			e.printStackTrace();
		}
	}

	@Override
	public Stagiaire create(Stagiaire stagiaire) throws SQLException {
		PreparedStatement ps = connection.prepareStatement(Requetes.AJOUT_STAGIAIRE, Statement.RETURN_GENERATED_KEYS);
		ps.setString(1, stagiaire.getNom());
		ps.setString(2, stagiaire.getPrenom());
		ps.setLong(3, stagiaire.getVille().getId());
		ps.setLong(4, stagiaire.getGroupe().getId());
		ps.executeUpdate();
		ResultSet rs = ps.getGeneratedKeys();
		if (rs.next()) {
			stagiaire.setId(rs.getLong(1));
			stagiaire.getGroupe().getStagiaires().add(stagiaire);
			stagiaire.getVille().getStagiaires().add(stagiaire);
		}
		return stagiaire;
	}

	@Override
	public List<Stagiaire> findAll() throws SQLException {
		List<Stagiaire> stagiaires = new ArrayList<>();
		PreparedStatement ps = connection.prepareStatement(Requetes.TOUS_LES_STAGIAIRES);
		ResultSet rs = ps.executeQuery();
		while (rs.next()) {
			Ville ville = villeDao.findOne(rs.getLong("idVille"));
			Groupe groupe = groupeDao.findOne(rs.getLong("idGroupe"));
			Stagiaire stagiaire = new Stagiaire(rs.getString("nom"), rs.getString("prenom"), ville, groupe);
			stagiaire.setId(rs.getLong("idStagiaire"));
			stagiaires.add(stagiaire);
		}
		return stagiaires;
	}

	@Override
	public Stagiaire findOne(Long id) throws SQLException {
		Stagiaire stagiaire = null;
		PreparedStatement ps = connection.prepareStatement(Requetes.STAGIAIRE_PAR_ID);
		ps.setLong(1, id);
		ResultSet rs = ps.executeQuery();
		if (rs.next()) {
			Ville ville = villeDao.findOne(rs.getLong("idVille"));
			Groupe groupe = groupeDao.findOne(rs.getLong("idGroupe"));
			stagiaire = new Stagiaire(rs.getString("nom"), rs.getString("prenom"), ville, groupe);
			stagiaire.setId(rs.getLong("idStagiaire"));
		}
		return stagiaire;
	}

	@Override
	public List<Stagiaire> findByGroupe(Groupe groupe) throws SQLException {
		List<Stagiaire> stagiaires = new ArrayList<>();
		PreparedStatement ps = connection.prepareStatement(Requetes.STAGIAIRES_PAR_GROUPE);
		ps.setLong(1, groupe.getId());
		ResultSet rs = ps.executeQuery();
		while (rs.next()) {
			Ville ville = villeDao.findOne(rs.getLong("idVille"));
			Stagiaire stagiaire = new Stagiaire(rs.getString("nom"), rs.getString("prenom"), ville, groupe);
			stagiaire.setId(rs.getLong("idStagiaire"));
			stagiaires.add(stagiaire);
		}
		return stagiaires;
	}

	@Override
	public List<Stagiaire> findByVille(Ville ville) throws SQLException {
		List<Stagiaire> stagiaires = new ArrayList<>();
		PreparedStatement ps = connection.prepareStatement(Requetes.STAGIAIRES_PAR_VILLE);
		ps.setLong(1, ville.getId());
		ResultSet rs = ps.executeQuery();
		while (rs.next()) {
			Groupe groupe = groupeDao.findOne(rs.getLong("idGroupe"));
			Stagiaire stagiaire = new Stagiaire(rs.getString("nom"), rs.getString("prenom"), ville, groupe);
			stagiaire.setId(rs.getLong("idStagiaire"));
			stagiaires.add(stagiaire);
		}
		return stagiaires;
	}

	@Override
	public boolean remove(Stagiaire stagiaire, Groupe groupe) throws SQLException {
		boolean estPresent = stagiaire != null && stagiaire.getGroupe().getId() == groupe.getId();
		if (!estPresent) {
			return false;
		}
		
		PreparedStatement ps = connection.prepareStatement(Requetes.RETIRE_STAGIAIRE);
		ps.setLong(1, stagiaire.getId());
		ps.executeUpdate();
		return true;
	}
	
	@Override
	protected void finalize() throws Throwable {
		connection.close();
	}
}
