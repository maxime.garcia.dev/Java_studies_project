package fr.humanbooster.fx.stagiaires.dao.impl;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import fr.humanbooster.fx.stagiaires.business.Ville;
import fr.humanbooster.fx.stagiaires.dao.ConnexionBdd;
import fr.humanbooster.fx.stagiaires.dao.Requetes;
import fr.humanbooster.fx.stagiaires.dao.VilleDao;

public class VilleDaoImpl implements VilleDao {

	private Connection connection;

	public VilleDaoImpl() {
		try {
			connection = ConnexionBdd.getConnection();
		} catch (ClassNotFoundException | SQLException e) {
			e.printStackTrace();
		}
	}

	@Override
	public Ville create(Ville ville) throws SQLException {
		PreparedStatement ps = connection.prepareStatement(Requetes.AJOUT_VILLE, Statement.RETURN_GENERATED_KEYS);
		ps.setString(1, ville.getNom());
		ps.setString(2, ville.getCodePostale());
		ps.executeUpdate();
		ResultSet rs = ps.getGeneratedKeys();
		if (rs.next()) {
			ville.setId(rs.getLong(1));
		}
		return ville;
	}

	@Override
	public List<Ville> findAll() throws SQLException {
		List<Ville> villes = new ArrayList<>();
		PreparedStatement ps = connection.prepareStatement(Requetes.TOUTES_LES_VILLES);
		ResultSet rs = ps.executeQuery();
		while (rs.next()) {
			Ville ville = new Ville(rs.getString("nom"));
			ville.setId(rs.getLong("idVille"));
			ville.setCodePostale(rs.getString("codePostale"));
			villes.add(ville);
		}
		return villes;
	}

	@Override
	public Ville findOne(Long id) throws SQLException {
		Ville ville = null;
		PreparedStatement ps = connection.prepareStatement(Requetes.VILLE_PAR_ID);
		ps.setLong(1, id);
		ResultSet rs = ps.executeQuery();
		if (rs.next()) {
			ville = new Ville(rs.getString("nom"));
			ville.setId(rs.getLong("idVille"));
			ville.setCodePostale(rs.getString("codePostale"));
		}
		return ville;
	}

	@Override
	public boolean delete(Long id) throws SQLException {
		Ville ville = findOne(id);
		if (ville==null) {
			return false;
		}
		PreparedStatement ps = connection.prepareStatement(Requetes.SUPPRESSION_VILLE);
		ps.setLong(1, id);
		ps.executeUpdate();
		return true;
	}

	@Override
	protected void finalize() throws Throwable {
		connection.close();
	}
}