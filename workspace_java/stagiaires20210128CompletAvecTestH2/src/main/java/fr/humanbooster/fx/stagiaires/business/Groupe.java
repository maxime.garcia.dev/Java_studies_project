package fr.humanbooster.fx.stagiaires.business;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class Groupe {

	private Long id;
	private String nom;
	private Date dateDebut;
	private Date dateDebutStage;
	private Date dateFin;
	private List<Stagiaire> stagiaires;

	public Groupe() {
		stagiaires = new ArrayList<>();
	}

	public Groupe(String nom, Date dateDebut, Date dateDebutStage, Date dateFin) {
		this();
		this.nom = nom;
		this.dateDebut = dateDebut;
		this.dateDebutStage = dateDebutStage;
		this.dateFin = dateFin;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getNom() {
		return nom;
	}

	public void setNom(String nom) {
		this.nom = nom;
	}

	public Date getDateDebut() {
		return dateDebut;
	}

	public void setDateDebut(Date dateDebut) {
		this.dateDebut = dateDebut;
	}

	public Date getDateDebutStage() {
		return dateDebutStage;
	}

	public void setDateDebutStage(Date dateDebutStage) {
		this.dateDebutStage = dateDebutStage;
	}

	public Date getDateFin() {
		return dateFin;
	}

	public void setDateFin(Date dateFin) {
		this.dateFin = dateFin;
	}

	public List<Stagiaire> getStagiaires() {
		return stagiaires;
	}

	public void setStagiaires(List<Stagiaire> stagiaires) {
		this.stagiaires = stagiaires;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((dateDebut == null) ? 0 : dateDebut.hashCode());
		result = prime * result + ((dateDebutStage == null) ? 0 : dateDebutStage.hashCode());
		result = prime * result + ((dateFin == null) ? 0 : dateFin.hashCode());
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		result = prime * result + ((nom == null) ? 0 : nom.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Groupe other = (Groupe) obj;
		if (dateDebut == null) {
			if (other.dateDebut != null)
				return false;
		} else if (!dateDebut.equals(other.dateDebut))
			return false;
		if (dateDebutStage == null) {
			if (other.dateDebutStage != null)
				return false;
		} else if (!dateDebutStage.equals(other.dateDebutStage))
			return false;
		if (dateFin == null) {
			if (other.dateFin != null)
				return false;
		} else if (!dateFin.equals(other.dateFin))
			return false;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		if (nom == null) {
			if (other.nom != null)
				return false;
		} else if (!nom.equals(other.nom))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "Groupe [id=" + id + ", nom=" + nom + ", dateDebut=" + dateDebut + ", dateDebutStage="
				+ dateDebutStage + ", dateFin=" + dateFin + "]";
	}
}
