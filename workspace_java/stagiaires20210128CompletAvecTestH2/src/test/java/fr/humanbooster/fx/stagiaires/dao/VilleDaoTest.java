package fr.humanbooster.fx.stagiaires.dao;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertNotNull;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.List;

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.MethodOrderer;
import org.junit.jupiter.api.Order;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestMethodOrder;

import fr.humanbooster.fx.stagiaires.business.Ville;
import fr.humanbooster.fx.stagiaires.dao.impl.VilleDaoImpl;

@TestMethodOrder(MethodOrderer.OrderAnnotation.class)
public class VilleDaoTest {

	private VilleDao villeDao = new VilleDaoImpl();
	private static Ville ville = null;

	@Test
	@Order(1)
	@DisplayName("teste la création d'un ville")
	public void testerCreate() {
		ville = new Ville();
		String nom = "Lyon";
		String codePostale = "69007";
		try {
			ville.setNom(nom);
			ville.setCodePostale(codePostale);
			ville = villeDao.create(ville);
		} catch (SQLException e) {
			e.printStackTrace();
		}
		assertNotNull(ville);
		assertNotNull(ville.getId());
		assertNotNull(ville.getNom());
		assertEquals(ville.getNom(), nom);

	}

	@Test
	@Order(2)
	@DisplayName("teste de la recherche d'un ville par id")
	public void testerFindOne() {
		Ville ville2 = null;
		try {
			ville2 = villeDao.findOne(ville.getId());
		} catch (SQLException e) {
			e.printStackTrace();
		}
		assertNotNull(ville2);
		assertNotNull(ville2.getId());
		assertEquals(ville2.getId(), ville.getId());
		assertNotNull(ville2.getNom());
		assertEquals(ville2.getNom(), ville.getNom());

	}

	@Test
	@Order(3)
	public void testerFindAll() {
		List<Ville> villes = null;
		try {
			villes = villeDao.findAll();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		assertNotNull(villes);
		assertFalse(villes.isEmpty());
	}

	@Test
	@Order(4)
	public void testerDelete() {
		boolean aEteEfface = false;
		try {
			aEteEfface = villeDao.delete(ville.getId());
		} catch (SQLException e) {
			e.printStackTrace();
		}

		assertNotNull(aEteEfface);
	}

}