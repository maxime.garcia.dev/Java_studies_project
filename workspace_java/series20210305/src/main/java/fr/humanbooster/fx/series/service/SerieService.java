package fr.humanbooster.fx.series.service;

import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import fr.humanbooster.fx.series.business.Serie;

public interface SerieService {

	Serie ajouterSerie(String nom, float prixEnEuros);
	
	Serie recupererSerie(Long id);
	
	Serie modifierSerie(Serie serie);
	
	List<Serie> recupererSeries();

	Page<Serie> recupererSeries(Pageable pageable);

	boolean supprimerSerie(Long id);

}
