package fr.humanbooster.fx.series.service;

import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import fr.humanbooster.fx.series.business.Utilisateur;

public interface UtilisateurService {

	Utilisateur ajouterUtilisateur(Utilisateur utilisateur);

	List<Utilisateur> recupererUtilisateurs();

	Utilisateur recupererUtilisateur(String email, String motDePasse);

	Page<Utilisateur> recupererUtilisateurs(Pageable pageable);

}
