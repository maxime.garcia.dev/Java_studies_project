package fr.humanbooster.fx.series.service.impl;

import java.util.List;

import org.springframework.stereotype.Service;

import fr.humanbooster.fx.series.business.Episode;
import fr.humanbooster.fx.series.business.Saison;
import fr.humanbooster.fx.series.dao.EpisodeDao;
import fr.humanbooster.fx.series.service.EpisodeService;

@Service
public class EpisodeServiceImpl implements EpisodeService {

	private EpisodeDao episodeDao;
	
	public EpisodeServiceImpl(EpisodeDao episodeDao) {
		super();
		this.episodeDao = episodeDao;
	}

	@Override
	public Episode ajouterEpisode(String nom, Saison saison, int dureeEnMinutes) {
		Episode episode = new Episode(nom, saison, dureeEnMinutes);
		return episodeDao.save(episode);
	}

	@Override
	public Episode ajouterEpisode(Episode episode) {
		return episodeDao.save(episode);
	}

	@Override
	public List<Episode> recupererEpisodeParSaisie(String saisie) {
		return episodeDao.findEpisodesByNomContaining(saisie);
	}
}
