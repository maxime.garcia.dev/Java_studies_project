package fr.humanbooster.fx.series.controller;

import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort.Direction;
import org.springframework.data.web.PageableDefault;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import fr.humanbooster.fx.series.business.Episode;
import fr.humanbooster.fx.series.business.Saison;
import fr.humanbooster.fx.series.business.Serie;
import fr.humanbooster.fx.series.service.EpisodeService;
import fr.humanbooster.fx.series.service.SaisonService;
import fr.humanbooster.fx.series.service.SerieService;
import fr.humanbooster.fx.series.service.UtilisateurService;

@RestController
public class SeriesRestController {
	private static final String FORMAT_DATE_AMERICAIN = "yyyy-MM-dd";

	private SerieService serieService;
	private SaisonService saisonService;
	private UtilisateurService utilisateurService;
	private EpisodeService episodeService;
	
	public SeriesRestController(SerieService serieService, SaisonService saisonService,
			UtilisateurService utilisateurService, EpisodeService episodeService) {
		super();
		this.serieService = serieService;
		this.saisonService = saisonService;
		this.utilisateurService = utilisateurService;
		this.episodeService = episodeService;
	}

	//QUESTION A
	@PostMapping("/series/{nom}/{prixEnEuros}")
	public Serie ajouterSerie(@PathVariable String nom, @PathVariable float prixEnEuros) {
		Serie serie = serieService.ajouterSerie(nom, prixEnEuros);
		return serie;
	}
	
	//QUESTION B
	@PutMapping("/series/{idSerie}/{nom}/{prixEnEuros}")
	public Serie modifierSerie(@PathVariable Long idSerie, @PathVariable String nom, @PathVariable float prixEnEuros) {
		Serie serie = serieService.recupererSerie(idSerie);
		if (serie != null) {
			serie.setNom(nom);
			serie.setPrixEnEuros(prixEnEuros);
			serieService.modifierSerie(serie);
		}
		return serie;
	}
	
	//QUESTION C
	@DeleteMapping("/series/{idSerie}")
	public boolean supprimerSerie(@PathVariable Long idSerie) {
		return serieService.supprimerSerie(idSerie);
	}
	
	//QUESTION D
	@GetMapping(value="/series")
	public Page<Serie> recupererPageDeSeries(@PageableDefault(size=10, page=1, sort="id", direction = Direction.ASC) Pageable pageable){
		Page<Serie> series = serieService.recupererSeries(pageable);
		return series;
	}
	
	//QUESTION E
	@GetMapping(value="series/{idSerie}/saisons", produces = MediaType.APPLICATION_JSON_VALUE)
	public List<Saison> recupererSaisonSurSerie(@PathVariable Long idSerie){
		List<Saison> saisons = saisonService.recupererSaisonsParSerie(idSerie);
		if (saisons.isEmpty()) {
			return null;
		}
		return saisons;
	}
	
	//QUESTION F
	@PostMapping("/series/{idSerie}/saisons/{nom}")
	public Saison ajouterSaisonSerie(@PathVariable Long idSerie, @PathVariable String nom) {
		Serie serie = serieService.recupererSerie(idSerie);
		if (serie != null) {
			Saison saison = saisonService.ajouterSaison(nom, serie);
			return saison;
		}
		return null;
	}
	
	//QUESTION G
	@PostMapping("/saisons/{idSaison}/episodes/{nom}/{dureeEnMinutes}")
	public Episode ajouterSaisonSerie(@PathVariable Long idSaison, @PathVariable String nom, @PathVariable int dureeEnMinutes) {
		Saison saison = saisonService.recupererSaison(idSaison);
		if (saison != null) {
			Episode episode = episodeService.ajouterEpisode(nom, saison, dureeEnMinutes);
			return episode;
		}
		return null;
	}
	
	//QUESTION H
	@GetMapping(value="/serieMax", produces = MediaType.APPLICATION_JSON_VALUE)
	public Serie recupererSerieLaPlusChere() {
		List<Serie> series = serieService.recupererSeries();
		if (series.isEmpty()) {
			return null;
		}
		Serie serieMax = null;
		for (Serie serie : series) {
			if (serieMax == null || serie.getPrixEnEuros() > serieMax.getPrixEnEuros()) {
				serieMax = serie;
			}
		}
		return serieMax;
	}
	
	//Question I
	@GetMapping(value ="/episodes", produces = MediaType.APPLICATION_JSON_VALUE)
	public List<Episode> recupererEpisodeContenantSaisie(@RequestParam("nom") String saisie){
		List<Episode> episodes = episodeService.recupererEpisodeParSaisie(saisie);
		if (episodes.isEmpty()) {
			return null;
		}
		return episodes;
	}
}
