package fr.humanbooster.fx.series.dao;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import fr.humanbooster.fx.series.business.Saison;

public interface SaisonDao extends JpaRepository<Saison, Long> {

	List<Saison> findSaisonsBySerieId(Long idSerie);
}
