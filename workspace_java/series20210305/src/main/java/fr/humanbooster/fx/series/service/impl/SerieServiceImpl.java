package fr.humanbooster.fx.series.service.impl;

import java.util.List;
import java.util.Optional;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import fr.humanbooster.fx.series.business.Serie;
import fr.humanbooster.fx.series.dao.SerieDao;
import fr.humanbooster.fx.series.service.SerieService;

@Service
public class SerieServiceImpl implements SerieService {

	// Important : on délègue la création de l'objet au framework Spring
	// Principe Hollywood : ne nous appelez pas, nous vous appelerons
	// Injection de dépendances
	private SerieDao serieDao;

	public SerieServiceImpl(SerieDao serieDao) {
		super();
		this.serieDao = serieDao;
	}

	@Override
	public Serie ajouterSerie(String nom, float prixEnEuros) {
		Serie serie = new Serie();
		serie.setNom(nom);
		serie.setPrixEnEuros(prixEnEuros);
		serieDao.save(serie);
		return serie;
	}

	@Override
	public Serie recupererSerie(Long id) {
		return serieDao.findById(id).orElse(null);
	}

	@Override
	@Transactional(readOnly=true)
	public List<Serie> recupererSeries() {
		return serieDao.findAll();
	}

	@Override
	@Transactional(readOnly=true)
	public Page<Serie> recupererSeries(Pageable pageable) {
		return serieDao.findAll(pageable);
	}
	
	@Override
	public boolean supprimerSerie(Long id) {
		Optional<Serie> serie = serieDao.findById(id);
		
		if (!serie.isPresent()) {
			return false;
		}
		serieDao.delete(serie.get());
		return true;
	}

	@Override
	public Serie modifierSerie(Serie serie) {
		return serieDao.save(serie);
	}

}
