package fr.humanbooster.horloge_de_berlin;

import java.util.Scanner;

public class App {

	public static void main(String[] args) {
		int heureSaisie = 0;
		int minuteSaisie = 0;
		Scanner sc = new Scanner(System.in);
		
		System.out.println("veuillez entrer les heures");
		heureSaisie = sc.nextInt();
		System.out.println("veuillez entrer les minutes");
		minuteSaisie = sc.nextInt();
		
		while(heureSaisie >= 5) {
			System.out.print("X");
			heureSaisie -= 5;
		}
		System.out.println();
		while(heureSaisie > 0) {
			System.out.print("x");
			heureSaisie -= 1;
		}
		System.out.println();
		while(minuteSaisie >= 5) {
			System.out.print("O");
			minuteSaisie -= 5;
		}
		System.out.println();
		while(minuteSaisie > 0) {
			System.out.print("o");
			minuteSaisie -= 1; 
		}
	}

}
