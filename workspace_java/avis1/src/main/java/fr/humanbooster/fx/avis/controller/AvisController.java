package fr.humanbooster.fx.avis.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

@Controller
@RequestMapping("/")
public class AvisController {

	private static final String FORMAT_DATE_AMERICAIN = "yyyy-MM-dd";

	@RequestMapping(value = { "/index", "/" })
	public ModelAndView accueil() {
		ModelAndView mav = new ModelAndView("index");
		return mav;
	}

}