package fr.humanbooster.fx.buisness_case;

import java.util.ArrayList;
import java.util.List;

import fr.humanbooster.fx.buisness_case.buisness.Candidat;
import fr.humanbooster.fx.buisness_case.service.CandidatService;
import fr.humanbooster.fx.buisness_case.service.impl.CandidatServiceImpl;

public class App {
	private static CandidatService candidatService = new CandidatServiceImpl();
	
	public static void main(String[] args) {
		List<Candidat> candidats = new ArrayList<>();
		candidats = candidatService.importerCandidats();
		for (Candidat candidat : candidats) {
			System.out.println(candidat);
		}
	}
}
