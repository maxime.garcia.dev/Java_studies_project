package fr.humanbooster.fx.buisness_case.service;

import java.util.Date;
import java.util.List;

import fr.humanbooster.fx.buisness_case.buisness.Candidat;

public interface CandidatService {

	Candidat ajouterCandidat(String nom, String prenom, Date dateDeNaissance, String email);

	List<Candidat> importerCandidats();
	
	List<Candidat> recupererCandidats();

	Candidat recupererCandidat(Long id);
	
	boolean supprimerCandidat(Long id);
}
