package fr.humanbooster.fx.buisness_case.service.impl;

import java.util.ArrayList;
import java.util.List;

import fr.humanbooster.fx.buisness_case.buisness.Question;
import fr.humanbooster.fx.buisness_case.service.QuestionService;

public class QuestionServiceImpl implements QuestionService {
	private static List<Question> questions = new ArrayList<>();
	
	@Override
	public Question ajouterQuestion(String libelle) {
		Question question = new Question(libelle);
		questions.add(question);
		return question;
	}

	@Override
	public List<Question> recupererQuestion() {
		return questions;
	}

	@Override
	public Question recupererQuestion(Long id) {
		for (Question question : questions) {
			if (question.getId().equals(id)) {
				return (question);
			}
		}
		return null;
	}

	@Override
	public boolean supprimerQuestion(Long id) {
		Question question = recupererQuestion(id);
		if (question != null) {
			return (questions.remove(question));
		}
		return false;
	}

}