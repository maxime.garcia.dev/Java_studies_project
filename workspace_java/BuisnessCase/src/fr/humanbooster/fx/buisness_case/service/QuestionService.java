package fr.humanbooster.fx.buisness_case.service;

import java.util.List;

import fr.humanbooster.fx.buisness_case.buisness.Question;

public interface QuestionService {
	Question ajouterQuestion(String libelle);
	
	List<Question> recupererQuestion();
	
	Question recupererQuestion(Long id);
	
	boolean supprimerQuestion(Long id);
}
