package fr.humanbooster.fx.buisness_case.service.impl;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.MalformedURLException;
import java.net.URL;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import fr.humanbooster.fx.buisness_case.buisness.Candidat;
import fr.humanbooster.fx.buisness_case.service.CandidatService;
import fr.humanbooster.fx.buisness_case.util.DateConvertor;

public class CandidatServiceImpl implements CandidatService {

	private static List<Candidat> candidats = new ArrayList<>();
	
	@Override
	public Candidat ajouterCandidat(String nom, String prenom, Date dateDeNaissance, String email) {
		Candidat candidat = new Candidat(nom, prenom, dateDeNaissance, email);
		candidats.add(candidat);
		return candidat;
	}

	@Override
	public List<Candidat> importerCandidats(){
		DateConvertor convertor = new DateConvertor();
		URL url = null;
		BufferedReader bufferedReader = null;
		String str = null;
		String[] candidat = null;
		try {
			url = new URL("https://www.clelia.fr/candidats.csv");
		} catch (MalformedURLException e) {
			System.out.println();
			e.printStackTrace();
		}
		try {
			bufferedReader = new BufferedReader(new InputStreamReader(url.openStream()));
		} catch (IOException e) {
			e.printStackTrace();
		}
		try {
			bufferedReader.readLine();
		} catch (IOException e) {
			e.printStackTrace();
		}
		
		do {
			try {
				str = bufferedReader.readLine();
				if (str != null) {
					candidat = str.split(";");
					if (candidat.length >= 4) {
						ajouterCandidat(candidat[0], candidat[1], convertor.convertStringToDate(candidat[2]),candidat[3]);
					}
				}
			} catch (IOException e) {
				System.out.println("probl�me de lecture");
			} catch (ParseException e) {
				System.out.println("date incorrect");
			}
		} while (str != null);
		return candidats;
	}
	
	@Override
	public List<Candidat> recupererCandidats() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Candidat recupererCandidat(Long id) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public boolean supprimerCandidat(Long id) {
		// TODO Auto-generated method stub
		return false;
	}
}
