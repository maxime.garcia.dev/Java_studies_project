﻿package fr.humanbooster.fx.buisness_case.buisness;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class Reponse {
	
	private Long id;
	private List<ReponsePossible> reponsesPossibles;
	private Date dateEnvoi;
	private Date dateReponse;
	// Pour savoir à quel candidat appartient la réponse
	private Candidat candidat;
	private static Long compteur = 0L;
	
	// Constructor par défaut (sans paramètre)
	public Reponse() {
		setId(++compteur);
		reponsesPossibles = new ArrayList<>();
	}

	// Getters & Setters
	public List<ReponsePossible> getReponsesPossibles() {
		return reponsesPossibles;
	}

	public void setReponsesPossibles(List<ReponsePossible> reponsesPossibles) {
		this.reponsesPossibles = reponsesPossibles;
	}

	public Date getDateReponse() {
		return dateReponse;
	}

	public void setDateReponse(Date dateEnvoi) {
		this.dateReponse = dateEnvoi;
	}

	public Candidat getCandidat() {
		return candidat;
	}

	public void setCandidat(Candidat candidat) {
		this.candidat = candidat;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Date getDateEnvoi() {
		return dateEnvoi;
	}

	public void setDateEnvoi(Date dateEnvoi) {
		this.dateEnvoi = dateEnvoi;
	}

	@Override
	public String toString() {
		return "Reponse [reponsePossible=" + reponsesPossibles + ", dateReponse=" + dateReponse + ", candidat=" + candidat
				+ "]";
	}

}
