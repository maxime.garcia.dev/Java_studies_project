﻿package fr.humanbooster.fx.buisness_case.buisness;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import fr.humanbooster.fx.buisness_case.util.DateConvertor;

public class Candidat {

	private Long id;
	private String email;
	private String nom;
	private String prenom;
	private Date dateDeNaissance;
	private String motDePasse;
	private static Long compteur = 0L;
	private static SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
	
	// Liste des réponses que le candidat a fourni
	private List<Reponse> reponses;
	
	public Candidat(String prenom) {
		super();
		setId(++compteur);
		this.prenom = prenom;
	}
	
	public Candidat(String nom, String prenom) {
		this(prenom);
		this.nom = nom;
	}

	public Candidat(String nom, String prenom, Date dateDeNaissance, String email) {
		this(nom, prenom);
		this.email = email;
		this.dateDeNaissance = dateDeNaissance;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getNom() {
		return nom;
	}

	public void setNom(String nom) {
		this.nom = nom;
	}

	public String getPrenom() {
		return prenom;
	}

	public void setPrenom(String prenom) {
		this.prenom = prenom;
	}

	public Date getDateDeNaissance() {
		return dateDeNaissance;
	}

	public void setDateDeNaissance(Date dateDeNaissance) {
		this.dateDeNaissance = dateDeNaissance;
	}

	public List<Reponse> getReponses() {
		return reponses;
	}

	public void setReponses(List<Reponse> reponses) {
		this.reponses = reponses;
	}

	

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getMotDePasse() {
		return motDePasse;
	}

	public void setMotDePasse(String motDePasse) {
		this.motDePasse = motDePasse;
	}
	
	@Override
	public String toString() {
		return "Candidat [email=" + email + ", nom=" + nom + ", prenom=" + prenom + ", dateDeNaissance=" + sdf.format(dateDeNaissance)
				+ ", reponses=" + reponses + "]";
	}
}
