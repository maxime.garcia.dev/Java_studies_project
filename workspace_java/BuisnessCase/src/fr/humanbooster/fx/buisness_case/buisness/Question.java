package fr.humanbooster.fx.buisness_case.buisness;

import java.util.ArrayList;
import java.util.List;

public class Question {

	private Long id;
	private String libelle;
	private List<ReponsePossible> reponsesPossibles = new ArrayList<>();
	private static Long compteur = 0L;
	// On définit la bonne réponse à cette question
	private List<ReponsePossible> bonnesReponses;
	
	private Question() {
		id = ++compteur;
		bonnesReponses = new ArrayList<>();
	}

	public Question(String libelle) {
		this();
		this.libelle = libelle;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getLibelle() {
		return libelle;
	}

	public void setLibelle(String libelle) {
		this.libelle = libelle;
	}

	public List<ReponsePossible> getReponsesPossibles() {
		return reponsesPossibles;
	}

	public void setReponsesPossibles(List<ReponsePossible> reponsesPossibles) {
		this.reponsesPossibles = reponsesPossibles;
	}

	public ReponsePossible ajouterReponsePossible(ReponsePossible reponsePossible) {
		// TODO
		return null;
	}
	

	public List<ReponsePossible> getBonnesReponses() {
		return bonnesReponses;
	}

	public void setBonnesReponses(List<ReponsePossible> bonnesReponses) {
		this.bonnesReponses = bonnesReponses;
	}
	
	@Override
	public String toString() {
		return "Question [id=" + id + ", libelle=" + libelle + ", reponsesPossibles=" + reponsesPossibles + "]";
	}
}
