package fr.humanbooster.fx.buisness_case.buisness;

import java.util.ArrayList;
import java.util.List;

public class ReponsePossible implements Comparable<ReponsePossible> {

	private Long id;
	private String contenu;
	private List<Reponse> reponses = new ArrayList<>();
	private Question question;
	private static Long compteur = 0L;
	
	// Constructeur
	public ReponsePossible(String contenu, Question question) {
		super();
		setId(++compteur);
		this.contenu = contenu;
		this.question = question;
		// On ajoute la nouvelle réponse possible à la liste des réponses
		// possibles de la question donnée en paramètre
		//
		// this correspond au nouvel objet de type ReponsePossible
		// créé par ce constructeur
		
		// La méthode getReponsesPossibles() renvoit une liste d'objets
		// ReponsePossible
		question.getReponsesPossibles().add(this);
	}

	// Getters & Setters
	
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getContenu() {
		return contenu;
	}

	public void setContenu(String contenu) {
		this.contenu = contenu;
	}

	public List<Reponse> getReponses() {
		return reponses;
	}

	public void setReponses(List<Reponse> reponses) {
		this.reponses = reponses;
	}

	public Question getQuestion() {
		return question;
	}

	public void setQuestion(Question question) {
		this.question = question;
	}

	@Override
	public int compareTo(ReponsePossible autreReponsePossible) {
		return getId().compareTo(autreReponsePossible.getId());
	}
	
	// Methode toString()
	@Override
	public String toString() {
		return "ReponsePossible [reponsePossible=" + contenu + "]";
	}

}