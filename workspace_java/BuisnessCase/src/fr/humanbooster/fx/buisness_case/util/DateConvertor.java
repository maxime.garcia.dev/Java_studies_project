package fr.humanbooster.fx.buisness_case.util;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

public class DateConvertor {
	
	private static SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
	
	public Date convertStringToDate(String dateSaisie) throws ParseException {
		return sdf.parse(dateSaisie);
	}
}
