package fr.humanbooster.fx.villes.business;

public class Ville {

	   private int id;
	   private String codePostal;
	   private String nom;
	   private static int compteur = 0;

	   public Ville(String codePostal, String nom)
	   {
	      this.codePostal = codePostal;
	      this.nom = nom;
	      this.id=compteur;

	      compteur++;
	   }

	   public int getId()
	   {
	      return id;
	   }

	   public String getCodePostal()
	   {
	      return codePostal;
	   }

	   public String getNom()
	   {
	      return nom;
	   }

	@Override
	public String toString() {
		return "Ville [codePostal=" + codePostal + ", nom=" + nom + "]";
	}
	   
}