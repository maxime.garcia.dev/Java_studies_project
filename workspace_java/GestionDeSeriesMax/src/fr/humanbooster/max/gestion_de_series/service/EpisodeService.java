package fr.humanbooster.max.gestion_de_series.service;

import java.util.Date;
import java.util.List;

import fr.humanbooster.max.gestion_de_series.business.Episode;
import fr.humanbooster.max.gestion_de_series.business.Saison;

public interface EpisodeService {
	
	Episode ajouterEpisode(Saison saison, String nom, int dureeEnMinutes, Date dateAjout);
	
	List<Episode> recupererEpisodes();
	
	Episode recupererEpsiode(Long id);
	
	boolean supprimerEpisode(Long id);
}
