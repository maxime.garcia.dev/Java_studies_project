package fr.humanbooster.max.gestion_de_series.service.impl;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import fr.humanbooster.max.gestion_de_series.business.Paiement;
import fr.humanbooster.max.gestion_de_series.business.PaiementParCB;
import fr.humanbooster.max.gestion_de_series.business.PaiementParCheque;
import fr.humanbooster.max.gestion_de_series.business.Serie;
import fr.humanbooster.max.gestion_de_series.business.Utilisateur;
import fr.humanbooster.max.gestion_de_series.service.PaiementService;

public class PaiementServiceImpl implements PaiementService {

	private List<Paiement> paiements = new ArrayList<>();

	@Override
	public Paiement ajouterPaiement(Class<? extends Paiement> typeOfPaiement, Serie serie,
			Utilisateur utilisateur, Date datePaiement, float montant, String refOuNum) {
		Paiement paiement = null;
		if (typeOfPaiement == PaiementParCB.class) {
			paiement = new PaiementParCB(serie, utilisateur, datePaiement, montant, refOuNum);

		} else if (typeOfPaiement == PaiementParCheque.class) {
			paiement = new PaiementParCheque(serie, utilisateur, datePaiement, montant, refOuNum);
		} else {
			return null;
		}
		paiements.add(paiement);
		return paiement;
	}

	@Override
	public List<Paiement> recupererPaiements() {
		return paiements;
	}

	@Override
	public Paiement recupererPaiement(Long id) {
		for (Paiement paiement : paiements) {
			if (paiement.getId().equals(id)) {
				return paiement;
			}
		}
		return null;
	}

	@Override
	public boolean supprimerPaiement(Long id) {
		Paiement paiement = recupererPaiement(id);
		if (paiement != null) {
			return paiements.remove(paiement);
		}
		return false;
	}
}
