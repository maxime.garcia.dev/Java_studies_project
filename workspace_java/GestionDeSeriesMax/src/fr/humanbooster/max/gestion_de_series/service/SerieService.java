package fr.humanbooster.max.gestion_de_series.service;

import java.util.List;

import fr.humanbooster.max.gestion_de_series.business.Serie;

public interface SerieService {
	
	Serie ajouterSerie(String nom, float prixEnEuros);
	
	List<Serie> recupererSeries();
	
	Serie recupererSerie(Long id);
	
	boolean supprimerSerie(Long id);
}
