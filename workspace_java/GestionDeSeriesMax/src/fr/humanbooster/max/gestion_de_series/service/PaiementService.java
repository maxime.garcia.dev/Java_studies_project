package fr.humanbooster.max.gestion_de_series.service;

import java.util.Date;
import java.util.List;

import fr.humanbooster.max.gestion_de_series.business.Paiement;
import fr.humanbooster.max.gestion_de_series.business.Serie;
import fr.humanbooster.max.gestion_de_series.business.Utilisateur;

public interface PaiementService {
	
	Paiement ajouterPaiement(Class<? extends Paiement> typeOfPaiement,Serie serie,
			Utilisateur utilisateur, Date datePaiement, float montant, String refOuNum);
	
	List<Paiement> recupererPaiements();
	
	Paiement recupererPaiement(Long id);
	
	boolean supprimerPaiement(Long id);
}
