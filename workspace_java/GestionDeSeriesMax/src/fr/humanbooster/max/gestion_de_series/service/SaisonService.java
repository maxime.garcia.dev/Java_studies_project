package fr.humanbooster.max.gestion_de_series.service;

import java.util.List;

import fr.humanbooster.max.gestion_de_series.business.Saison;
import fr.humanbooster.max.gestion_de_series.business.Serie;

public interface SaisonService {
	
	Saison ajouterSaison(Serie serie, String nom);
	
	List<Saison> recupererSaisons();
	
	Saison recupererSaison(Long id);
	
	boolean supprimerSaison(Long id);
}
