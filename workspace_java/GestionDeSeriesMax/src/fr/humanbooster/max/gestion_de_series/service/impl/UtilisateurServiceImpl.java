package fr.humanbooster.max.gestion_de_series.service.impl;

import java.util.ArrayList;
import java.util.List;

import fr.humanbooster.max.gestion_de_series.business.Utilisateur;
import fr.humanbooster.max.gestion_de_series.service.UtilisateurService;

public class UtilisateurServiceImpl implements UtilisateurService {

	private List<Utilisateur> utilisateurs = new ArrayList<>();
	
	@Override
	public Utilisateur ajouterUtilisateur(String nom, String prenom, String motDePasse, String email) {
		Utilisateur utilisateur = new Utilisateur(nom, prenom, motDePasse, email);
		utilisateurs.add(utilisateur);
		return utilisateur;
	}

	@Override
	public List<Utilisateur> recupererUtilisateurs() {
		return utilisateurs;
	}

	@Override
	public Utilisateur recupererUtilisateur(Long id) {
		for (Utilisateur utilisateur : utilisateurs) {
			if (utilisateur.getId().equals(id)) {
				return utilisateur;
			}
		}
		return null;
	}

	@Override
	public boolean supprimerUtilisateur(Long id) {
		Utilisateur utilisateur = recupererUtilisateur(id);
		if (utilisateur != null) {
			return utilisateurs.remove(utilisateur);
		}
		return false;
	}

}
