package fr.humanbooster.max.gestion_de_series.service.impl;

import java.util.ArrayList;
import java.util.List;

import fr.humanbooster.max.gestion_de_series.business.Serie;
import fr.humanbooster.max.gestion_de_series.service.SerieService;

public class SerieServiceImpl implements SerieService {

	private List<Serie> series = new ArrayList<>();
	
	@Override
	public Serie ajouterSerie(String nom, float prixEnEuros) {
		Serie serie = new Serie(nom, prixEnEuros);
		series.add(serie);
		return serie;
	}

	@Override
	public List<Serie> recupererSeries() {		
		return series;
	}

	@Override
	public Serie recupererSerie(Long id) {
		for (Serie serie : series) {
			if (serie.getId().equals(id)) {
				return serie;
			}
		}
		return null;
	}

	@Override
	public boolean supprimerSerie(Long id) {
		Serie serie = recupererSerie(id);
		if (serie != null) {
			return series.remove(serie);
		}
		return false;
	}

}
