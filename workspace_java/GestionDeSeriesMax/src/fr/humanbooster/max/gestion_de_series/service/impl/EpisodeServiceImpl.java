package fr.humanbooster.max.gestion_de_series.service.impl;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import fr.humanbooster.max.gestion_de_series.business.Episode;
import fr.humanbooster.max.gestion_de_series.business.Saison;
import fr.humanbooster.max.gestion_de_series.service.EpisodeService;

public class EpisodeServiceImpl implements EpisodeService {
	
	private List<Episode> episodes = new ArrayList<>();
	
	@Override
	public Episode ajouterEpisode(Saison saison, String nom, int dureeEnMinutes, Date dateAjout) {
		Episode episode = new Episode(saison, nom, dureeEnMinutes, dateAjout);
		episodes.add(episode);
		return episode;
	}

	@Override
	public List<Episode> recupererEpisodes() {
		return episodes;
	}

	@Override
	public Episode recupererEpsiode(Long id) {
		for (Episode episode : episodes) {
			if (episode.getId().equals(id)) {
				return episode;
			}
		}
		return null;
	}

	@Override
	public boolean supprimerEpisode(Long id) {
		Episode episode = recupererEpsiode(id);
		if (episode != null) {
			return episodes.remove(episode);
		}
		return false;
	}
}
