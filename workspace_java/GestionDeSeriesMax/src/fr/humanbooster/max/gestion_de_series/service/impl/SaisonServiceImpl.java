package fr.humanbooster.max.gestion_de_series.service.impl;

import java.util.ArrayList;
import java.util.List;

import fr.humanbooster.max.gestion_de_series.business.Saison;
import fr.humanbooster.max.gestion_de_series.business.Serie;
import fr.humanbooster.max.gestion_de_series.service.SaisonService;

public class SaisonServiceImpl implements SaisonService {

	private List<Saison> saisons = new ArrayList<>();
	
	@Override
	public Saison ajouterSaison(Serie serie, String nom) {
		Saison saison = new Saison(serie, nom);
		saisons.add(saison);
		return saison;
	}

	@Override
	public List<Saison> recupererSaisons() {
		return saisons;
	}

	@Override
	public Saison recupererSaison(Long id) {
		for (Saison saison : saisons) {
			if (saison.getId().equals(id)) {
				return saison;
			}
		}
		return null;
	}

	@Override
	public boolean supprimerSaison(Long id) {
		Saison saison = recupererSaison(id);
		if (saison != null) {
			return saisons.remove(saison);
		}
		return false;
	}
}
