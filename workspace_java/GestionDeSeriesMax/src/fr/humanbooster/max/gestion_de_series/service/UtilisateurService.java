package fr.humanbooster.max.gestion_de_series.service;

import java.util.List;

import fr.humanbooster.max.gestion_de_series.business.Utilisateur;

public interface UtilisateurService {

	Utilisateur ajouterUtilisateur(String nom, String prenom, String motDePasse, String email);
	
	List<Utilisateur> recupererUtilisateurs();
	
	Utilisateur recupererUtilisateur(Long id);
	
	boolean supprimerUtilisateur(Long id);
}
