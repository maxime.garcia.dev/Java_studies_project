package fr.humanbooster.max.gestion_de_series.business;

import java.util.List;

public class Serie {
	private List<Saison> saisons;
	private static Long compteur = 0L;
	private Long id;
	private String nom;
	private float prixEnEuros;
	
	public Serie() {
		id = ++compteur;
	}

	public Serie(String nom, float prixEnEuros) {
		this();
		this.nom = nom;
		this.prixEnEuros = prixEnEuros;
	}

	public Serie(List<Saison> saisons, String nom, float prixEnEuros) {
		this(nom, prixEnEuros);
		this.saisons = saisons;
	}

	public List<Saison> getSaisons() {
		return saisons;
	}

	public void setSaisons(List<Saison> saisons) {
		this.saisons = saisons;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getNom() {
		return nom;
	}

	public void setNom(String nom) {
		this.nom = nom;
	}

	public float getPrixEnEuros() {
		return prixEnEuros;
	}

	public void setPrixEnEuros(float prixEnEuros) {
		this.prixEnEuros = prixEnEuros;
	}

	@Override
	public String toString() {
		return "Serie [id=" + id + ", nom=" + nom + ", prixEnEuros=" + prixEnEuros + "]";
	}
}
