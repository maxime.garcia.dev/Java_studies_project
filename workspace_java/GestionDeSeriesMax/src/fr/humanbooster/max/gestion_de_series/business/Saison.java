package fr.humanbooster.max.gestion_de_series.business;

import java.util.List;

public class Saison {
	private List<Episode> episodes;
	private Serie serie;
	private static Long compteur = 0L;
	private Long id;
	private String nom;
	
	public Saison() {
		id = ++compteur;
	}

	public Saison(Serie serie, String nom) {
		this();
		this.serie = serie;
		this.nom = nom;
	}

	public Saison(List<Episode> episodes, Serie serie, String nom) {
		this(serie, nom);
		this.episodes = episodes;
	}

	public List<Episode> getEpisodes() {
		return episodes;
	}

	public void setEpisodes(List<Episode> episodes) {
		this.episodes = episodes;
	}

	public Serie getSerie() {
		return serie;
	}

	public void setSerie(Serie serie) {
		this.serie = serie;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getNom() {
		return nom;
	}

	public void setNom(String nom) {
		this.nom = nom;
	}

	@Override
	public String toString() {
		return "Saison [serie=" + serie + ", id=" + id + ", nom=" + nom + "]";
	}
}
