package fr.humanbooster.max.gestion_de_series.business;

import java.util.Date;

public class PaiementParCheque extends Paiement{
	private String numero;
	
	public PaiementParCheque(Serie serie, Utilisateur utilisateur, Date datePaiement, float montant, String numero) {
		super(serie, utilisateur, datePaiement, montant);
		this.numero = numero;
	}

	public String getNumero() {
		return numero;
	}

	public void setNumero(String numero) {
		this.numero = numero;
	}

	@Override
	public String toString() {
		return "PaiementParChèque [reference=" + numero + ", getUtilisateur()=" + super.getUtilisateur() + "]";
	}
}
