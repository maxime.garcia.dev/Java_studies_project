package fr.humanbooster.max.gestion_de_series.business;

import java.util.Date;

public class Episode {
	private Saison saison;
	private static Long compteur = 0L;
	private Long id;
	private String nom;
	private int dureeEnMinutes;
	private Date dateAjout;
	
	public Episode() {
		id = ++compteur;
	}

	public Episode(String nom, int dureeEnMinutes, Date dateAjout) {
		this();
		this.nom = nom;
		this.dureeEnMinutes = dureeEnMinutes;
		this.dateAjout = dateAjout;
	}

	public Episode(Saison saison, String nom, int dureeEnMinutes, Date dateAjout) {
		this(nom, dureeEnMinutes, dateAjout);
		this.saison = saison;
	}

	public Saison getSaison() {
		return saison;
	}

	public void setSaison(Saison saison) {
		this.saison = saison;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getNom() {
		return nom;
	}

	public void setNom(String nom) {
		this.nom = nom;
	}

	public int getDureeEnMinutes() {
		return dureeEnMinutes;
	}

	public void setDureeEnMinutes(int dureeEnMinutes) {
		this.dureeEnMinutes = dureeEnMinutes;
	}

	public Date getDateAjout() {
		return dateAjout;
	}

	public void setDateAjout(Date dateAjout) {
		this.dateAjout = dateAjout;
	}

	@Override
	public String toString() {
		return "Episode [saison=" + saison + ", id=" + id + ", nom=" + nom + ", dureeEnMinutes=" + dureeEnMinutes
				+ ", dateAjout=" + dateAjout + "]";
	}
}
