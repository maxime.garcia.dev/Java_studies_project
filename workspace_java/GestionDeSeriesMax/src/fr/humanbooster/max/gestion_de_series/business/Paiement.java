package fr.humanbooster.max.gestion_de_series.business;

import java.util.Date;

public abstract class Paiement {
	private Serie serie;
	private Utilisateur utilisateur;
	private static Long compteur = 0L;
	private Long id;
	private Date datePaiement;
	private float montant;
	
	public Paiement() {
		id = ++compteur;
	}

	public Paiement(Serie serie, Utilisateur utilisateur, Date datePaiement, float montant) {
		this();
		this.utilisateur = utilisateur;
		this.datePaiement = datePaiement;
		this.montant = montant;
	}

	public Serie getSerie() {
		return serie;
	}

	public void setSerie(Serie serie) {
		this.serie = serie;
	}

	public Utilisateur getUtilisateur() {
		return utilisateur;
	}

	public void setUtilisateur(Utilisateur utilisateur) {
		this.utilisateur = utilisateur;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Date getDatePaiement() {
		return datePaiement;
	}

	public void setDatePaiement(Date datePaiement) {
		this.datePaiement = datePaiement;
	}

	public float getMontant() {
		return montant;
	}

	public void setMontant(float montant) {
		this.montant = montant;
	}

	@Override
	public String toString() {
		return "Paiement [serie=" + serie + ", utilisateur=" + utilisateur + ", id=" + id + ", datePaiement="
				+ datePaiement + ", montant=" + montant + "]";
	}
}
