package fr.humanbooster.max.gestion_de_series.business;

import java.util.List;

public class Utilisateur {
	
	private List<Paiement> paiements;
	private static Long compteur = 0L;
	private Long id;
	private String nom;
	private String prenom;
	private String email;
	private String motDePasse;
	
	public Utilisateur() {
		id = ++compteur;
	}

	public Utilisateur(String nom, String prenom, String motDePasse) {
		this();
		this.nom = nom;
		this.prenom = prenom;
		this.motDePasse = motDePasse;
	}

	public Utilisateur(String nom, String prenom, String motDePasse, String email) {
		this(nom, prenom, motDePasse);
		this.email = email;
	}

	public List<Paiement> getPaiements() {
		return paiements;
	}

	public void setPaiements(List<Paiement> paiements) {
		this.paiements = paiements;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getNom() {
		return nom;
	}

	public void setNom(String nom) {
		this.nom = nom;
	}

	public String getPrenom() {
		return prenom;
	}

	public void setPrenom(String prenom) {
		this.prenom = prenom;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getMotDePasse() {
		return motDePasse;
	}

	public void setMotDePasse(String motDePasse) {
		this.motDePasse = motDePasse;
	}

	@Override
	public String toString() {
		return "Utilisateur [id=" + id + ", nom=" + nom + ", prenom=" + prenom + ", email=" + email + "]";
	}
}
