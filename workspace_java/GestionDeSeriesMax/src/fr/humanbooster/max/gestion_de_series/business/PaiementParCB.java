package fr.humanbooster.max.gestion_de_series.business;

import java.util.Date;

public class PaiementParCB extends Paiement{
	private String reference;
	
	public PaiementParCB(Serie serie, Utilisateur utilisateur, Date datePaiement, float montant, String reference) {
		super(serie, utilisateur, datePaiement, montant);
	}

	public String getReference() {
		return reference;
	}

	public void setReference(String reference) {
		this.reference = reference;
	}

	@Override
	public String toString() {
		return "PaiementParCB [reference=" + reference + ", getUtilisateur()=" + super.getUtilisateur() + "]";
	}
}