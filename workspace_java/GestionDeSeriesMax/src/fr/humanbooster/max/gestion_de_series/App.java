package fr.humanbooster.max.gestion_de_series;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Scanner;

import fr.humanbooster.max.gestion_de_series.business.Saison;
import fr.humanbooster.max.gestion_de_series.business.Serie;
import fr.humanbooster.max.gestion_de_series.service.EpisodeService;
import fr.humanbooster.max.gestion_de_series.service.PaiementService;
import fr.humanbooster.max.gestion_de_series.service.SaisonService;
import fr.humanbooster.max.gestion_de_series.service.SerieService;
import fr.humanbooster.max.gestion_de_series.service.UtilisateurService;
import fr.humanbooster.max.gestion_de_series.service.impl.EpisodeServiceImpl;
import fr.humanbooster.max.gestion_de_series.service.impl.PaiementServiceImpl;
import fr.humanbooster.max.gestion_de_series.service.impl.SaisonServiceImpl;
import fr.humanbooster.max.gestion_de_series.service.impl.SerieServiceImpl;
import fr.humanbooster.max.gestion_de_series.service.impl.UtilisateurServiceImpl;

public class App {
	
	private static final int AFFICHER_INFOS_SERIES = 1;
	private static final int AJOUTER_EPISODE = 2;
	private static final int SUPPRIMER_EPISODE = 3;
	private static final int SERIE_TRIEES_SUR_NOM = 4;
	private static final int SERIE_TRIEE_SUR_PRIX = 5;
	private static final int VOIR_EPISODE_AJOUTER_ENTRE_DEUX_DATES = 6;
	private static final int QUITTER = 7;
	
	private static EpisodeService episodeService = new EpisodeServiceImpl();
	private static SaisonService saisonService = new SaisonServiceImpl();
	private static SerieService serieService = new SerieServiceImpl();
	private static UtilisateurService utilisateurService = new UtilisateurServiceImpl();
	private static PaiementService paiementService = new PaiementServiceImpl();
	private static Scanner sc = new Scanner(System.in);
	
	public static void main(String[] args) {
		ajouterDonnees();
		System.out.println(serieService.recupererSeries());
	}
	
	private static void ajouterDonnees() {
		serieService.ajouterSerie("Breaking Bad", 5.0f);
		serieService.ajouterSerie("The Boys", 8.0f);
		saisonService.ajouterSaison(serieService.recupererSerie(1L), "Saison 1");
		saisonService.ajouterSaison(serieService.recupererSerie(1L), "Saison 2");
		saisonService.ajouterSaison(serieService.recupererSerie(2L), "Saison 1");
		saisonService.ajouterSaison(serieService.recupererSerie(2L), "Saison 2");
		episodeService.ajouterEpisode(saisonService.recupererSaison(1L),"lala", 20, new Date());
		episodeService.ajouterEpisode(saisonService.recupererSaison(1L),"lolo", 20, new Date());
		episodeService.ajouterEpisode(saisonService.recupererSaison(1L),"lulu", 20, new Date());
		episodeService.ajouterEpisode(saisonService.recupererSaison(1L),"lele", 20, new Date());
		episodeService.ajouterEpisode(saisonService.recupererSaison(2L),"tata", 20, new Date());
		episodeService.ajouterEpisode(saisonService.recupererSaison(2L),"toto", 20, new Date());
		episodeService.ajouterEpisode(saisonService.recupererSaison(2L),"tutu", 20, new Date());
		episodeService.ajouterEpisode(saisonService.recupererSaison(2L),"tete", 20, new Date());
		episodeService.ajouterEpisode(saisonService.recupererSaison(3L),"papa", 20, new Date());
		episodeService.ajouterEpisode(saisonService.recupererSaison(3L),"popo", 20, new Date());
		episodeService.ajouterEpisode(saisonService.recupererSaison(3L),"pupu", 20, new Date());
		episodeService.ajouterEpisode(saisonService.recupererSaison(3L),"pepe", 20, new Date());
		episodeService.ajouterEpisode(saisonService.recupererSaison(4L),"rara", 20, new Date());
		episodeService.ajouterEpisode(saisonService.recupererSaison(4L),"roro", 20, new Date());
		episodeService.ajouterEpisode(saisonService.recupererSaison(4L),"ruru", 20, new Date());
		episodeService.ajouterEpisode(saisonService.recupererSaison(4L),"rere", 20, new Date());
	}
	
	private static void afficherMenuPrincipal() {
		System.out.println(AFFICHER_INFOS_SERIES + ". afficher toutes les informations sur une s�rie (l�utilisateur saisit l�id de la s�rie)");
		System.out.println(AJOUTER_EPISODE + ". ajouter un �pisode (l�utilisateur choisit la s�rie (en pr�cisant son id), la saison\r\n"
				+ "(en pr�cisant son id), puis saisit le nom et la date � laquelle l'�pisode a �t� ajout�)");
		System.out.println(SUPPRIMER_EPISODE + ". supprimer un �pisode (l�utilisateur choisit la s�rie (en pr�cisant son id), la saison\r\n"
				+ "(en pr�cisant son id) et l�id de l��pisode � supprimer)");
		System.out.println(SERIE_TRIEES_SUR_NOM + ". voir les s�ries tri�es sur le nom");
		System.out.println(SERIE_TRIEE_SUR_PRIX + ". voir les s�ries tri�es sur le prix");
		System.out.println(VOIR_EPISODE_AJOUTER_ENTRE_DEUX_DATES + ". voir les �pisodes ajout�s entre deux dates");
		System.out.println(QUITTER + ". Quitter");
	}
}
